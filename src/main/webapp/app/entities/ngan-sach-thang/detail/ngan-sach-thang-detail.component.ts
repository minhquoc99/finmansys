import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { INganSachThang } from '../ngan-sach-thang.model';

@Component({
  selector: 'jhi-ngan-sach-thang-detail',
  templateUrl: './ngan-sach-thang-detail.component.html',
})
export class NganSachThangDetailComponent implements OnInit {
  nganSachThang: INganSachThang | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ nganSachThang }) => {
      this.nganSachThang = nganSachThang;
    });
  }

  previousState(): void {
    window.history.back();
  }
}
