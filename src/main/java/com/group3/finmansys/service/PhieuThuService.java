package com.group3.finmansys.service;

import com.group3.finmansys.service.dto.PhieuThuDTO;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Service Interface for managing {@link com.group3.finmansys.domain.PhieuThu}.
 */
public interface PhieuThuService {
    /**
     * Save a phieuThu.
     *
     * @param phieuThuDTO the entity to save.
     * @return the persisted entity.
     */
    PhieuThuDTO save(PhieuThuDTO phieuThuDTO);

    /**
     * Partially updates a phieuThu.
     *
     * @param phieuThuDTO the entity to update partially.
     * @return the persisted entity.
     */
    Optional<PhieuThuDTO> partialUpdate(PhieuThuDTO phieuThuDTO);

    /**
     * Get all the phieuThus.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<PhieuThuDTO> findAll(Pageable pageable);

    /**
     * Get the "id" phieuThu.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<PhieuThuDTO> findOne(Long id);

    /**
     * Delete the "id" phieuThu.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
