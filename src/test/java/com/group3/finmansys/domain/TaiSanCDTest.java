package com.group3.finmansys.domain;

import static org.assertj.core.api.Assertions.assertThat;

import com.group3.finmansys.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class TaiSanCDTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(TaiSanCD.class);
        TaiSanCD taiSanCD1 = new TaiSanCD();
        taiSanCD1.setId(1L);
        TaiSanCD taiSanCD2 = new TaiSanCD();
        taiSanCD2.setId(taiSanCD1.getId());
        assertThat(taiSanCD1).isEqualTo(taiSanCD2);
        taiSanCD2.setId(2L);
        assertThat(taiSanCD1).isNotEqualTo(taiSanCD2);
        taiSanCD1.setId(null);
        assertThat(taiSanCD1).isNotEqualTo(taiSanCD2);
    }
}
