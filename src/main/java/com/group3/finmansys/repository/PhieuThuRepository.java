package com.group3.finmansys.repository;

import com.group3.finmansys.domain.PhieuThu;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data SQL repository for the PhieuThu entity.
 */
@SuppressWarnings("unused")
@Repository
public interface PhieuThuRepository extends JpaRepository<PhieuThu, Long>, JpaSpecificationExecutor<PhieuThu> {}
