import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { IPhong, Phong } from '../phong.model';

import { PhongService } from './phong.service';

describe('Service Tests', () => {
  describe('Phong Service', () => {
    let service: PhongService;
    let httpMock: HttpTestingController;
    let elemDefault: IPhong;
    let expectedResult: IPhong | IPhong[] | boolean | null;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule],
      });
      expectedResult = null;
      service = TestBed.inject(PhongService);
      httpMock = TestBed.inject(HttpTestingController);

      elemDefault = {
        id: 0,
        ten: 'AAAAAAA',
        loai: 'AAAAAAA',
        gia: 0,
      };
    });

    describe('Service methods', () => {
      it('should find an element', () => {
        const returnedFromService = Object.assign({}, elemDefault);

        service.find(123).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(elemDefault);
      });

      it('should create a Phong', () => {
        const returnedFromService = Object.assign(
          {
            id: 0,
          },
          elemDefault
        );

        const expected = Object.assign({}, returnedFromService);

        service.create(new Phong()).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'POST' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should update a Phong', () => {
        const returnedFromService = Object.assign(
          {
            id: 1,
            ten: 'BBBBBB',
            loai: 'BBBBBB',
            gia: 1,
          },
          elemDefault
        );

        const expected = Object.assign({}, returnedFromService);

        service.update(expected).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'PUT' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should partial update a Phong', () => {
        const patchObject = Object.assign(
          {
            loai: 'BBBBBB',
          },
          new Phong()
        );

        const returnedFromService = Object.assign(patchObject, elemDefault);

        const expected = Object.assign({}, returnedFromService);

        service.partialUpdate(patchObject).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'PATCH' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should return a list of Phong', () => {
        const returnedFromService = Object.assign(
          {
            id: 1,
            ten: 'BBBBBB',
            loai: 'BBBBBB',
            gia: 1,
          },
          elemDefault
        );

        const expected = Object.assign({}, returnedFromService);

        service.query().subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush([returnedFromService]);
        httpMock.verify();
        expect(expectedResult).toContainEqual(expected);
      });

      it('should delete a Phong', () => {
        service.delete(123).subscribe(resp => (expectedResult = resp.ok));

        const req = httpMock.expectOne({ method: 'DELETE' });
        req.flush({ status: 200 });
        expect(expectedResult);
      });

      describe('addPhongToCollectionIfMissing', () => {
        it('should add a Phong to an empty array', () => {
          const phong: IPhong = { id: 123 };
          expectedResult = service.addPhongToCollectionIfMissing([], phong);
          expect(expectedResult).toHaveLength(1);
          expect(expectedResult).toContain(phong);
        });

        it('should not add a Phong to an array that contains it', () => {
          const phong: IPhong = { id: 123 };
          const phongCollection: IPhong[] = [
            {
              ...phong,
            },
            { id: 456 },
          ];
          expectedResult = service.addPhongToCollectionIfMissing(phongCollection, phong);
          expect(expectedResult).toHaveLength(2);
        });

        it("should add a Phong to an array that doesn't contain it", () => {
          const phong: IPhong = { id: 123 };
          const phongCollection: IPhong[] = [{ id: 456 }];
          expectedResult = service.addPhongToCollectionIfMissing(phongCollection, phong);
          expect(expectedResult).toHaveLength(2);
          expect(expectedResult).toContain(phong);
        });

        it('should add only unique Phong to an array', () => {
          const phongArray: IPhong[] = [{ id: 123 }, { id: 456 }, { id: 32606 }];
          const phongCollection: IPhong[] = [{ id: 123 }];
          expectedResult = service.addPhongToCollectionIfMissing(phongCollection, ...phongArray);
          expect(expectedResult).toHaveLength(3);
        });

        it('should accept varargs', () => {
          const phong: IPhong = { id: 123 };
          const phong2: IPhong = { id: 456 };
          expectedResult = service.addPhongToCollectionIfMissing([], phong, phong2);
          expect(expectedResult).toHaveLength(2);
          expect(expectedResult).toContain(phong);
          expect(expectedResult).toContain(phong2);
        });

        it('should accept null and undefined values', () => {
          const phong: IPhong = { id: 123 };
          expectedResult = service.addPhongToCollectionIfMissing([], null, phong, undefined);
          expect(expectedResult).toHaveLength(1);
          expect(expectedResult).toContain(phong);
        });
      });
    });

    afterEach(() => {
      httpMock.verify();
    });
  });
});
