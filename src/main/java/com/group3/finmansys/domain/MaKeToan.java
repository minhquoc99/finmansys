package com.group3.finmansys.domain;

import java.io.Serializable;
import javax.persistence.*;
import javax.validation.constraints.*;

/**
 * A MaKeToan.
 */
@Entity
@Table(name = "ma_ke_toan")
public class MaKeToan implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @DecimalMin(value = "1")
    @Column(name = "so_hieu", nullable = false, unique = true)
    private Double soHieu;

    @NotNull
    @Column(name = "ten", nullable = false)
    private String ten;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public MaKeToan id(Long id) {
        this.id = id;
        return this;
    }

    public Double getSoHieu() {
        return this.soHieu;
    }

    public MaKeToan soHieu(Double soHieu) {
        this.soHieu = soHieu;
        return this;
    }

    public void setSoHieu(Double soHieu) {
        this.soHieu = soHieu;
    }

    public String getTen() {
        return this.ten;
    }

    public MaKeToan ten(String ten) {
        this.ten = ten;
        return this;
    }

    public void setTen(String ten) {
        this.ten = ten;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof MaKeToan)) {
            return false;
        }
        return id != null && id.equals(((MaKeToan) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "MaKeToan{" +
            "id=" + getId() +
            ", soHieu=" + getSoHieu() +
            ", ten='" + getTen() + "'" +
            "}";
    }
}
