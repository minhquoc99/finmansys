package com.group3.finmansys.service.mapper;

import com.group3.finmansys.domain.*;
import com.group3.finmansys.service.dto.MaKeToanDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link MaKeToan} and its DTO {@link MaKeToanDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface MaKeToanMapper extends EntityMapper<MaKeToanDTO, MaKeToan> {
    @Named("soHieu")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    @Mapping(target = "soHieu", source = "soHieu")
    @Mapping(target = "ten", source = "ten")
    MaKeToanDTO toDtoSoHieu(MaKeToan maKeToan);

    @Named("ten")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    @Mapping(target = "ten", source = "ten")
    @Mapping(target = "soHieu", source = "soHieu")
    MaKeToanDTO toDtoTen(MaKeToan maKeToan);
}
