import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { INganSachThang, NganSachThang } from '../ngan-sach-thang.model';

import { NganSachThangService } from './ngan-sach-thang.service';

describe('Service Tests', () => {
  describe('NganSachThang Service', () => {
    let service: NganSachThangService;
    let httpMock: HttpTestingController;
    let elemDefault: INganSachThang;
    let expectedResult: INganSachThang | INganSachThang[] | boolean | null;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule],
      });
      expectedResult = null;
      service = TestBed.inject(NganSachThangService);
      httpMock = TestBed.inject(HttpTestingController);

      elemDefault = {
        id: 0,
        thang: 0,
        soTien: 0,
      };
    });

    describe('Service methods', () => {
      it('should find an element', () => {
        const returnedFromService = Object.assign({}, elemDefault);

        service.find(123).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(elemDefault);
      });

      it('should create a NganSachThang', () => {
        const returnedFromService = Object.assign(
          {
            id: 0,
          },
          elemDefault
        );

        const expected = Object.assign({}, returnedFromService);

        service.create(new NganSachThang()).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'POST' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should update a NganSachThang', () => {
        const returnedFromService = Object.assign(
          {
            id: 1,
            thang: 1,
            soTien: 1,
          },
          elemDefault
        );

        const expected = Object.assign({}, returnedFromService);

        service.update(expected).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'PUT' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should partial update a NganSachThang', () => {
        const patchObject = Object.assign(
          {
            soTien: 1,
          },
          new NganSachThang()
        );

        const returnedFromService = Object.assign(patchObject, elemDefault);

        const expected = Object.assign({}, returnedFromService);

        service.partialUpdate(patchObject).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'PATCH' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should return a list of NganSachThang', () => {
        const returnedFromService = Object.assign(
          {
            id: 1,
            thang: 1,
            soTien: 1,
          },
          elemDefault
        );

        const expected = Object.assign({}, returnedFromService);

        service.query().subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush([returnedFromService]);
        httpMock.verify();
        expect(expectedResult).toContainEqual(expected);
      });

      it('should delete a NganSachThang', () => {
        service.delete(123).subscribe(resp => (expectedResult = resp.ok));

        const req = httpMock.expectOne({ method: 'DELETE' });
        req.flush({ status: 200 });
        expect(expectedResult);
      });

      describe('addNganSachThangToCollectionIfMissing', () => {
        it('should add a NganSachThang to an empty array', () => {
          const nganSachThang: INganSachThang = { id: 123 };
          expectedResult = service.addNganSachThangToCollectionIfMissing([], nganSachThang);
          expect(expectedResult).toHaveLength(1);
          expect(expectedResult).toContain(nganSachThang);
        });

        it('should not add a NganSachThang to an array that contains it', () => {
          const nganSachThang: INganSachThang = { id: 123 };
          const nganSachThangCollection: INganSachThang[] = [
            {
              ...nganSachThang,
            },
            { id: 456 },
          ];
          expectedResult = service.addNganSachThangToCollectionIfMissing(nganSachThangCollection, nganSachThang);
          expect(expectedResult).toHaveLength(2);
        });

        it("should add a NganSachThang to an array that doesn't contain it", () => {
          const nganSachThang: INganSachThang = { id: 123 };
          const nganSachThangCollection: INganSachThang[] = [{ id: 456 }];
          expectedResult = service.addNganSachThangToCollectionIfMissing(nganSachThangCollection, nganSachThang);
          expect(expectedResult).toHaveLength(2);
          expect(expectedResult).toContain(nganSachThang);
        });

        it('should add only unique NganSachThang to an array', () => {
          const nganSachThangArray: INganSachThang[] = [{ id: 123 }, { id: 456 }, { id: 46249 }];
          const nganSachThangCollection: INganSachThang[] = [{ id: 123 }];
          expectedResult = service.addNganSachThangToCollectionIfMissing(nganSachThangCollection, ...nganSachThangArray);
          expect(expectedResult).toHaveLength(3);
        });

        it('should accept varargs', () => {
          const nganSachThang: INganSachThang = { id: 123 };
          const nganSachThang2: INganSachThang = { id: 456 };
          expectedResult = service.addNganSachThangToCollectionIfMissing([], nganSachThang, nganSachThang2);
          expect(expectedResult).toHaveLength(2);
          expect(expectedResult).toContain(nganSachThang);
          expect(expectedResult).toContain(nganSachThang2);
        });

        it('should accept null and undefined values', () => {
          const nganSachThang: INganSachThang = { id: 123 };
          expectedResult = service.addNganSachThangToCollectionIfMissing([], null, nganSachThang, undefined);
          expect(expectedResult).toHaveLength(1);
          expect(expectedResult).toContain(nganSachThang);
        });
      });
    });

    afterEach(() => {
      httpMock.verify();
    });
  });
});
