import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IPhieuThu } from '../phieu-thu.model';

@Component({
  selector: 'jhi-phieu-thu-detail',
  templateUrl: './phieu-thu-detail.component.html',
})
export class PhieuThuDetailComponent implements OnInit {
  phieuThu: IPhieuThu | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ phieuThu }) => {
      this.phieuThu = phieuThu;
    });
  }

  previousState(): void {
    window.history.back();
  }
}
