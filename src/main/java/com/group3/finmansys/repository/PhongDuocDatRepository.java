package com.group3.finmansys.repository;

import com.group3.finmansys.domain.PhongDuocDat;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data SQL repository for the PhongDuocDat entity.
 */
@SuppressWarnings("unused")
@Repository
public interface PhongDuocDatRepository extends JpaRepository<PhongDuocDat, Long>, JpaSpecificationExecutor<PhongDuocDat> {}
