package com.group3.finmansys.domain;

import java.io.Serializable;
import javax.persistence.*;
import javax.validation.constraints.*;

/**
 * A Phong.
 */
@Entity
@Table(name = "phong")
public class Phong implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(name = "ten", nullable = false)
    private String ten;

    @NotNull
    @Column(name = "loai", nullable = false)
    private String loai;

    @NotNull
    @DecimalMin(value = "1")
    @Column(name = "gia", nullable = false)
    private Double gia;

    @ManyToOne(optional = false)
    @NotNull
    private KhachSan khachSan;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Phong id(Long id) {
        this.id = id;
        return this;
    }

    public String getTen() {
        return this.ten;
    }

    public Phong ten(String ten) {
        this.ten = ten;
        return this;
    }

    public void setTen(String ten) {
        this.ten = ten;
    }

    public String getLoai() {
        return this.loai;
    }

    public Phong loai(String loai) {
        this.loai = loai;
        return this;
    }

    public void setLoai(String loai) {
        this.loai = loai;
    }

    public Double getGia() {
        return this.gia;
    }

    public Phong gia(Double gia) {
        this.gia = gia;
        return this;
    }

    public void setGia(Double gia) {
        this.gia = gia;
    }

    public KhachSan getKhachSan() {
        return this.khachSan;
    }

    public Phong khachSan(KhachSan khachSan) {
        this.setKhachSan(khachSan);
        return this;
    }

    public void setKhachSan(KhachSan khachSan) {
        this.khachSan = khachSan;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Phong)) {
            return false;
        }
        return id != null && id.equals(((Phong) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Phong{" +
            "id=" + getId() +
            ", ten='" + getTen() + "'" +
            ", loai='" + getLoai() + "'" +
            ", gia=" + getGia() +
            "}";
    }
}
