package com.group3.finmansys.repository;

import com.group3.finmansys.domain.ChiTietTangTSCD;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data SQL repository for the ChiTietTangTSCD entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ChiTietTangTSCDRepository extends JpaRepository<ChiTietTangTSCD, Long>, JpaSpecificationExecutor<ChiTietTangTSCD> {}
