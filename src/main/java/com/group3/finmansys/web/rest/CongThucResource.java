package com.group3.finmansys.web.rest;

import com.group3.finmansys.repository.CongThucRepository;
import com.group3.finmansys.service.CongThucQueryService;
import com.group3.finmansys.service.CongThucService;
import com.group3.finmansys.service.criteria.CongThucCriteria;
import com.group3.finmansys.service.dto.CongThucDTO;
import com.group3.finmansys.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.PaginationUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link com.group3.finmansys.domain.CongThuc}.
 */
@RestController
@RequestMapping("/api")
public class CongThucResource {

    private final Logger log = LoggerFactory.getLogger(CongThucResource.class);

    private static final String ENTITY_NAME = "congThuc";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final CongThucService congThucService;

    private final CongThucRepository congThucRepository;

    private final CongThucQueryService congThucQueryService;

    public CongThucResource(
        CongThucService congThucService,
        CongThucRepository congThucRepository,
        CongThucQueryService congThucQueryService
    ) {
        this.congThucService = congThucService;
        this.congThucRepository = congThucRepository;
        this.congThucQueryService = congThucQueryService;
    }

    /**
     * {@code POST  /cong-thucs} : Create a new congThuc.
     *
     * @param congThucDTO the congThucDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new congThucDTO, or with status {@code 400 (Bad Request)} if the congThuc has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/cong-thucs")
    public ResponseEntity<CongThucDTO> createCongThuc(@Valid @RequestBody CongThucDTO congThucDTO) throws URISyntaxException {
        log.debug("REST request to save CongThuc : {}", congThucDTO);
        if (congThucDTO.getId() != null) {
            throw new BadRequestAlertException("A new congThuc cannot already have an ID", ENTITY_NAME, "idexists");
        }
        CongThucDTO result = congThucService.save(congThucDTO);
        return ResponseEntity
            .created(new URI("/api/cong-thucs/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /cong-thucs/:id} : Updates an existing congThuc.
     *
     * @param id the id of the congThucDTO to save.
     * @param congThucDTO the congThucDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated congThucDTO,
     * or with status {@code 400 (Bad Request)} if the congThucDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the congThucDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/cong-thucs/{id}")
    public ResponseEntity<CongThucDTO> updateCongThuc(
        @PathVariable(value = "id", required = false) final Long id,
        @Valid @RequestBody CongThucDTO congThucDTO
    ) throws URISyntaxException {
        log.debug("REST request to update CongThuc : {}, {}", id, congThucDTO);
        if (congThucDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, congThucDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!congThucRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        CongThucDTO result = congThucService.save(congThucDTO);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, congThucDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /cong-thucs/:id} : Partial updates given fields of an existing congThuc, field will ignore if it is null
     *
     * @param id the id of the congThucDTO to save.
     * @param congThucDTO the congThucDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated congThucDTO,
     * or with status {@code 400 (Bad Request)} if the congThucDTO is not valid,
     * or with status {@code 404 (Not Found)} if the congThucDTO is not found,
     * or with status {@code 500 (Internal Server Error)} if the congThucDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/cong-thucs/{id}", consumes = "application/merge-patch+json")
    public ResponseEntity<CongThucDTO> partialUpdateCongThuc(
        @PathVariable(value = "id", required = false) final Long id,
        @NotNull @RequestBody CongThucDTO congThucDTO
    ) throws URISyntaxException {
        log.debug("REST request to partial update CongThuc partially : {}, {}", id, congThucDTO);
        if (congThucDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, congThucDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!congThucRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<CongThucDTO> result = congThucService.partialUpdate(congThucDTO);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, congThucDTO.getId().toString())
        );
    }

    /**
     * {@code GET  /cong-thucs} : get all the congThucs.
     *
     * @param pageable the pagination information.
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of congThucs in body.
     */
    @GetMapping("/cong-thucs")
    public ResponseEntity<List<CongThucDTO>> getAllCongThucs(CongThucCriteria criteria, Pageable pageable) {
        log.debug("REST request to get CongThucs by criteria: {}", criteria);
        Page<CongThucDTO> page = congThucQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    @GetMapping("/cong-thucs/bang-can-doi-ke-toan")
    public ResponseEntity<List<CongThucDTO>> getCanDoiKeToan(
        @RequestParam(name = "startDate", required = false) String startDate,
        @RequestParam(name = "endDate", required = false) String endDate
    ) {
        List<CongThucDTO> page = congThucQueryService.getBangCanDoiKeToan(startDate, endDate);
        return ResponseEntity.ok().body(page);
    }

    /**
     * {@code GET  /cong-thucs/count} : count all the congThucs.
     *
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
     */
    @GetMapping("/cong-thucs/count")
    public ResponseEntity<Long> countCongThucs(CongThucCriteria criteria) {
        log.debug("REST request to count CongThucs by criteria: {}", criteria);
        return ResponseEntity.ok().body(congThucQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /cong-thucs/:id} : get the "id" congThuc.
     *
     * @param id the id of the congThucDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the congThucDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/cong-thucs/{id}")
    public ResponseEntity<CongThucDTO> getCongThuc(@PathVariable Long id) {
        log.debug("REST request to get CongThuc : {}", id);
        Optional<CongThucDTO> congThucDTO = congThucService.findOne(id);
        return ResponseUtil.wrapOrNotFound(congThucDTO);
    }

    /**
     * {@code DELETE  /cong-thucs/:id} : delete the "id" congThuc.
     *
     * @param id the id of the congThucDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/cong-thucs/{id}")
    public ResponseEntity<Void> deleteCongThuc(@PathVariable Long id) {
        log.debug("REST request to delete CongThuc : {}", id);
        congThucService.delete(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
            .build();
    }
}
