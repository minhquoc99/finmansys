import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { mergeMap } from 'rxjs/operators';

import { IDichVu, DichVu } from '../dich-vu.model';
import { DichVuService } from '../service/dich-vu.service';

@Injectable({ providedIn: 'root' })
export class DichVuRoutingResolveService implements Resolve<IDichVu> {
  constructor(protected service: DichVuService, protected router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IDichVu> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        mergeMap((dichVu: HttpResponse<DichVu>) => {
          if (dichVu.body) {
            return of(dichVu.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new DichVu());
  }
}
