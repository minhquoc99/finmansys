import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import * as dayjs from 'dayjs';

import { DATE_TIME_FORMAT } from 'app/config/input.constants';
import { IChiTietTangTSCD, ChiTietTangTSCD } from '../chi-tiet-tang-tscd.model';

import { ChiTietTangTSCDService } from './chi-tiet-tang-tscd.service';

describe('Service Tests', () => {
  describe('ChiTietTangTSCD Service', () => {
    let service: ChiTietTangTSCDService;
    let httpMock: HttpTestingController;
    let elemDefault: IChiTietTangTSCD;
    let expectedResult: IChiTietTangTSCD | IChiTietTangTSCD[] | boolean | null;
    let currentDate: dayjs.Dayjs;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule],
      });
      expectedResult = null;
      service = TestBed.inject(ChiTietTangTSCDService);
      httpMock = TestBed.inject(HttpTestingController);
      currentDate = dayjs();

      elemDefault = {
        id: 0,
        ngayThuMua: currentDate,
        soLuong: 0,
        chiPhiMua: 0,
      };
    });

    describe('Service methods', () => {
      it('should find an element', () => {
        const returnedFromService = Object.assign(
          {
            ngayThuMua: currentDate.format(DATE_TIME_FORMAT),
          },
          elemDefault
        );

        service.find(123).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(elemDefault);
      });

      it('should create a ChiTietTangTSCD', () => {
        const returnedFromService = Object.assign(
          {
            id: 0,
            ngayThuMua: currentDate.format(DATE_TIME_FORMAT),
          },
          elemDefault
        );

        const expected = Object.assign(
          {
            ngayThuMua: currentDate,
          },
          returnedFromService
        );

        service.create(new ChiTietTangTSCD()).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'POST' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should update a ChiTietTangTSCD', () => {
        const returnedFromService = Object.assign(
          {
            id: 1,
            ngayThuMua: currentDate.format(DATE_TIME_FORMAT),
            soLuong: 1,
            chiPhiMua: 1,
          },
          elemDefault
        );

        const expected = Object.assign(
          {
            ngayThuMua: currentDate,
          },
          returnedFromService
        );

        service.update(expected).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'PUT' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should partial update a ChiTietTangTSCD', () => {
        const patchObject = Object.assign(
          {
            ngayThuMua: currentDate.format(DATE_TIME_FORMAT),
            soLuong: 1,
          },
          new ChiTietTangTSCD()
        );

        const returnedFromService = Object.assign(patchObject, elemDefault);

        const expected = Object.assign(
          {
            ngayThuMua: currentDate,
          },
          returnedFromService
        );

        service.partialUpdate(patchObject).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'PATCH' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should return a list of ChiTietTangTSCD', () => {
        const returnedFromService = Object.assign(
          {
            id: 1,
            ngayThuMua: currentDate.format(DATE_TIME_FORMAT),
            soLuong: 1,
            chiPhiMua: 1,
          },
          elemDefault
        );

        const expected = Object.assign(
          {
            ngayThuMua: currentDate,
          },
          returnedFromService
        );

        service.query().subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush([returnedFromService]);
        httpMock.verify();
        expect(expectedResult).toContainEqual(expected);
      });

      it('should delete a ChiTietTangTSCD', () => {
        service.delete(123).subscribe(resp => (expectedResult = resp.ok));

        const req = httpMock.expectOne({ method: 'DELETE' });
        req.flush({ status: 200 });
        expect(expectedResult);
      });

      describe('addChiTietTangTSCDToCollectionIfMissing', () => {
        it('should add a ChiTietTangTSCD to an empty array', () => {
          const chiTietTangTSCD: IChiTietTangTSCD = { id: 123 };
          expectedResult = service.addChiTietTangTSCDToCollectionIfMissing([], chiTietTangTSCD);
          expect(expectedResult).toHaveLength(1);
          expect(expectedResult).toContain(chiTietTangTSCD);
        });

        it('should not add a ChiTietTangTSCD to an array that contains it', () => {
          const chiTietTangTSCD: IChiTietTangTSCD = { id: 123 };
          const chiTietTangTSCDCollection: IChiTietTangTSCD[] = [
            {
              ...chiTietTangTSCD,
            },
            { id: 456 },
          ];
          expectedResult = service.addChiTietTangTSCDToCollectionIfMissing(chiTietTangTSCDCollection, chiTietTangTSCD);
          expect(expectedResult).toHaveLength(2);
        });

        it("should add a ChiTietTangTSCD to an array that doesn't contain it", () => {
          const chiTietTangTSCD: IChiTietTangTSCD = { id: 123 };
          const chiTietTangTSCDCollection: IChiTietTangTSCD[] = [{ id: 456 }];
          expectedResult = service.addChiTietTangTSCDToCollectionIfMissing(chiTietTangTSCDCollection, chiTietTangTSCD);
          expect(expectedResult).toHaveLength(2);
          expect(expectedResult).toContain(chiTietTangTSCD);
        });

        it('should add only unique ChiTietTangTSCD to an array', () => {
          const chiTietTangTSCDArray: IChiTietTangTSCD[] = [{ id: 123 }, { id: 456 }, { id: 37568 }];
          const chiTietTangTSCDCollection: IChiTietTangTSCD[] = [{ id: 123 }];
          expectedResult = service.addChiTietTangTSCDToCollectionIfMissing(chiTietTangTSCDCollection, ...chiTietTangTSCDArray);
          expect(expectedResult).toHaveLength(3);
        });

        it('should accept varargs', () => {
          const chiTietTangTSCD: IChiTietTangTSCD = { id: 123 };
          const chiTietTangTSCD2: IChiTietTangTSCD = { id: 456 };
          expectedResult = service.addChiTietTangTSCDToCollectionIfMissing([], chiTietTangTSCD, chiTietTangTSCD2);
          expect(expectedResult).toHaveLength(2);
          expect(expectedResult).toContain(chiTietTangTSCD);
          expect(expectedResult).toContain(chiTietTangTSCD2);
        });

        it('should accept null and undefined values', () => {
          const chiTietTangTSCD: IChiTietTangTSCD = { id: 123 };
          expectedResult = service.addChiTietTangTSCDToCollectionIfMissing([], null, chiTietTangTSCD, undefined);
          expect(expectedResult).toHaveLength(1);
          expect(expectedResult).toContain(chiTietTangTSCD);
        });
      });
    });

    afterEach(() => {
      httpMock.verify();
    });
  });
});
