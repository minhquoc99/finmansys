jest.mock('@angular/router');

import { TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ActivatedRouteSnapshot, Router } from '@angular/router';
import { of } from 'rxjs';

import { IChiTietTangTSCD, ChiTietTangTSCD } from '../chi-tiet-tang-tscd.model';
import { ChiTietTangTSCDService } from '../service/chi-tiet-tang-tscd.service';

import { ChiTietTangTSCDRoutingResolveService } from './chi-tiet-tang-tscd-routing-resolve.service';

describe('Service Tests', () => {
  describe('ChiTietTangTSCD routing resolve service', () => {
    let mockRouter: Router;
    let mockActivatedRouteSnapshot: ActivatedRouteSnapshot;
    let routingResolveService: ChiTietTangTSCDRoutingResolveService;
    let service: ChiTietTangTSCDService;
    let resultChiTietTangTSCD: IChiTietTangTSCD | undefined;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule],
        providers: [Router, ActivatedRouteSnapshot],
      });
      mockRouter = TestBed.inject(Router);
      mockActivatedRouteSnapshot = TestBed.inject(ActivatedRouteSnapshot);
      routingResolveService = TestBed.inject(ChiTietTangTSCDRoutingResolveService);
      service = TestBed.inject(ChiTietTangTSCDService);
      resultChiTietTangTSCD = undefined;
    });

    describe('resolve', () => {
      it('should return IChiTietTangTSCD returned by find', () => {
        // GIVEN
        service.find = jest.fn(id => of(new HttpResponse({ body: { id } })));
        mockActivatedRouteSnapshot.params = { id: 123 };

        // WHEN
        routingResolveService.resolve(mockActivatedRouteSnapshot).subscribe(result => {
          resultChiTietTangTSCD = result;
        });

        // THEN
        expect(service.find).toBeCalledWith(123);
        expect(resultChiTietTangTSCD).toEqual({ id: 123 });
      });

      it('should return new IChiTietTangTSCD if id is not provided', () => {
        // GIVEN
        service.find = jest.fn();
        mockActivatedRouteSnapshot.params = {};

        // WHEN
        routingResolveService.resolve(mockActivatedRouteSnapshot).subscribe(result => {
          resultChiTietTangTSCD = result;
        });

        // THEN
        expect(service.find).not.toBeCalled();
        expect(resultChiTietTangTSCD).toEqual(new ChiTietTangTSCD());
      });

      it('should route to 404 page if data not found in server', () => {
        // GIVEN
        spyOn(service, 'find').and.returnValue(of(new HttpResponse({ body: null })));
        mockActivatedRouteSnapshot.params = { id: 123 };

        // WHEN
        routingResolveService.resolve(mockActivatedRouteSnapshot).subscribe(result => {
          resultChiTietTangTSCD = result;
        });

        // THEN
        expect(service.find).toBeCalledWith(123);
        expect(resultChiTietTangTSCD).toEqual(undefined);
        expect(mockRouter.navigate).toHaveBeenCalledWith(['404']);
      });
    });
  });
});
