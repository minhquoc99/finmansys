import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import * as dayjs from 'dayjs';

import { DATE_TIME_FORMAT } from 'app/config/input.constants';
import { IDichVuSuDung, DichVuSuDung } from '../dich-vu-su-dung.model';

import { DichVuSuDungService } from './dich-vu-su-dung.service';

describe('Service Tests', () => {
  describe('DichVuSuDung Service', () => {
    let service: DichVuSuDungService;
    let httpMock: HttpTestingController;
    let elemDefault: IDichVuSuDung;
    let expectedResult: IDichVuSuDung | IDichVuSuDung[] | boolean | null;
    let currentDate: dayjs.Dayjs;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule],
      });
      expectedResult = null;
      service = TestBed.inject(DichVuSuDungService);
      httpMock = TestBed.inject(HttpTestingController);
      currentDate = dayjs();

      elemDefault = {
        id: 0,
        soLuong: 0,
        gia: 0,
        ngaySuDung: currentDate,
      };
    });

    describe('Service methods', () => {
      it('should find an element', () => {
        const returnedFromService = Object.assign(
          {
            ngaySuDung: currentDate.format(DATE_TIME_FORMAT),
          },
          elemDefault
        );

        service.find(123).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(elemDefault);
      });

      it('should create a DichVuSuDung', () => {
        const returnedFromService = Object.assign(
          {
            id: 0,
            ngaySuDung: currentDate.format(DATE_TIME_FORMAT),
          },
          elemDefault
        );

        const expected = Object.assign(
          {
            ngaySuDung: currentDate,
          },
          returnedFromService
        );

        service.create(new DichVuSuDung()).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'POST' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should update a DichVuSuDung', () => {
        const returnedFromService = Object.assign(
          {
            id: 1,
            soLuong: 1,
            gia: 1,
            ngaySuDung: currentDate.format(DATE_TIME_FORMAT),
          },
          elemDefault
        );

        const expected = Object.assign(
          {
            ngaySuDung: currentDate,
          },
          returnedFromService
        );

        service.update(expected).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'PUT' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should partial update a DichVuSuDung', () => {
        const patchObject = Object.assign(
          {
            gia: 1,
            ngaySuDung: currentDate.format(DATE_TIME_FORMAT),
          },
          new DichVuSuDung()
        );

        const returnedFromService = Object.assign(patchObject, elemDefault);

        const expected = Object.assign(
          {
            ngaySuDung: currentDate,
          },
          returnedFromService
        );

        service.partialUpdate(patchObject).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'PATCH' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should return a list of DichVuSuDung', () => {
        const returnedFromService = Object.assign(
          {
            id: 1,
            soLuong: 1,
            gia: 1,
            ngaySuDung: currentDate.format(DATE_TIME_FORMAT),
          },
          elemDefault
        );

        const expected = Object.assign(
          {
            ngaySuDung: currentDate,
          },
          returnedFromService
        );

        service.query().subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush([returnedFromService]);
        httpMock.verify();
        expect(expectedResult).toContainEqual(expected);
      });

      it('should delete a DichVuSuDung', () => {
        service.delete(123).subscribe(resp => (expectedResult = resp.ok));

        const req = httpMock.expectOne({ method: 'DELETE' });
        req.flush({ status: 200 });
        expect(expectedResult);
      });

      describe('addDichVuSuDungToCollectionIfMissing', () => {
        it('should add a DichVuSuDung to an empty array', () => {
          const dichVuSuDung: IDichVuSuDung = { id: 123 };
          expectedResult = service.addDichVuSuDungToCollectionIfMissing([], dichVuSuDung);
          expect(expectedResult).toHaveLength(1);
          expect(expectedResult).toContain(dichVuSuDung);
        });

        it('should not add a DichVuSuDung to an array that contains it', () => {
          const dichVuSuDung: IDichVuSuDung = { id: 123 };
          const dichVuSuDungCollection: IDichVuSuDung[] = [
            {
              ...dichVuSuDung,
            },
            { id: 456 },
          ];
          expectedResult = service.addDichVuSuDungToCollectionIfMissing(dichVuSuDungCollection, dichVuSuDung);
          expect(expectedResult).toHaveLength(2);
        });

        it("should add a DichVuSuDung to an array that doesn't contain it", () => {
          const dichVuSuDung: IDichVuSuDung = { id: 123 };
          const dichVuSuDungCollection: IDichVuSuDung[] = [{ id: 456 }];
          expectedResult = service.addDichVuSuDungToCollectionIfMissing(dichVuSuDungCollection, dichVuSuDung);
          expect(expectedResult).toHaveLength(2);
          expect(expectedResult).toContain(dichVuSuDung);
        });

        it('should add only unique DichVuSuDung to an array', () => {
          const dichVuSuDungArray: IDichVuSuDung[] = [{ id: 123 }, { id: 456 }, { id: 71285 }];
          const dichVuSuDungCollection: IDichVuSuDung[] = [{ id: 123 }];
          expectedResult = service.addDichVuSuDungToCollectionIfMissing(dichVuSuDungCollection, ...dichVuSuDungArray);
          expect(expectedResult).toHaveLength(3);
        });

        it('should accept varargs', () => {
          const dichVuSuDung: IDichVuSuDung = { id: 123 };
          const dichVuSuDung2: IDichVuSuDung = { id: 456 };
          expectedResult = service.addDichVuSuDungToCollectionIfMissing([], dichVuSuDung, dichVuSuDung2);
          expect(expectedResult).toHaveLength(2);
          expect(expectedResult).toContain(dichVuSuDung);
          expect(expectedResult).toContain(dichVuSuDung2);
        });

        it('should accept null and undefined values', () => {
          const dichVuSuDung: IDichVuSuDung = { id: 123 };
          expectedResult = service.addDichVuSuDungToCollectionIfMissing([], null, dichVuSuDung, undefined);
          expect(expectedResult).toHaveLength(1);
          expect(expectedResult).toContain(dichVuSuDung);
        });
      });
    });

    afterEach(() => {
      httpMock.verify();
    });
  });
});
