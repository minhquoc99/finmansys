import { IMaKeToan } from 'app/entities/ma-ke-toan/ma-ke-toan.model';

export interface ILoaiTaiSan {
  id?: number;
  ten?: string | null;
  moTa?: string | null;
  maKeToan?: IMaKeToan;
}

export class LoaiTaiSan implements ILoaiTaiSan {
  constructor(public id?: number, public ten?: string | null, public moTa?: string | null, public maKeToan?: IMaKeToan) {}
}

export function getLoaiTaiSanIdentifier(loaiTaiSan: ILoaiTaiSan): number | undefined {
  return loaiTaiSan.id;
}
