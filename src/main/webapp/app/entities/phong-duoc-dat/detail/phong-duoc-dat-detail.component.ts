import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IPhongDuocDat } from '../phong-duoc-dat.model';

@Component({
  selector: 'jhi-phong-duoc-dat-detail',
  templateUrl: './phong-duoc-dat-detail.component.html',
})
export class PhongDuocDatDetailComponent implements OnInit {
  phongDuocDat: IPhongDuocDat | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ phongDuocDat }) => {
      this.phongDuocDat = phongDuocDat;
    });
  }

  previousState(): void {
    window.history.back();
  }
}
