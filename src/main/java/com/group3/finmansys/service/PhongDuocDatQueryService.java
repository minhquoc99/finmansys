package com.group3.finmansys.service;

import com.group3.finmansys.domain.*; // for static metamodels
import com.group3.finmansys.domain.PhongDuocDat;
import com.group3.finmansys.repository.PhongDuocDatRepository;
import com.group3.finmansys.service.criteria.PhongDuocDatCriteria;
import com.group3.finmansys.service.dto.PhongDuocDatDTO;
import com.group3.finmansys.service.mapper.PhongDuocDatMapper;
import java.util.List;
import javax.persistence.criteria.JoinType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tech.jhipster.service.QueryService;

/**
 * Service for executing complex queries for {@link PhongDuocDat} entities in the database.
 * The main input is a {@link PhongDuocDatCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link PhongDuocDatDTO} or a {@link Page} of {@link PhongDuocDatDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class PhongDuocDatQueryService extends QueryService<PhongDuocDat> {

    private final Logger log = LoggerFactory.getLogger(PhongDuocDatQueryService.class);

    private final PhongDuocDatRepository phongDuocDatRepository;

    private final PhongDuocDatMapper phongDuocDatMapper;

    public PhongDuocDatQueryService(PhongDuocDatRepository phongDuocDatRepository, PhongDuocDatMapper phongDuocDatMapper) {
        this.phongDuocDatRepository = phongDuocDatRepository;
        this.phongDuocDatMapper = phongDuocDatMapper;
    }

    /**
     * Return a {@link List} of {@link PhongDuocDatDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<PhongDuocDatDTO> findByCriteria(PhongDuocDatCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<PhongDuocDat> specification = createSpecification(criteria);
        return phongDuocDatMapper.toDto(phongDuocDatRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link PhongDuocDatDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<PhongDuocDatDTO> findByCriteria(PhongDuocDatCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<PhongDuocDat> specification = createSpecification(criteria);
        return phongDuocDatRepository.findAll(specification, page).map(phongDuocDatMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(PhongDuocDatCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<PhongDuocDat> specification = createSpecification(criteria);
        return phongDuocDatRepository.count(specification);
    }

    /**
     * Function to convert {@link PhongDuocDatCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<PhongDuocDat> createSpecification(PhongDuocDatCriteria criteria) {
        Specification<PhongDuocDat> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getId(), PhongDuocDat_.id));
            }
            if (criteria.getNgayDat() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getNgayDat(), PhongDuocDat_.ngayDat));
            }
            if (criteria.getKhuyenMai() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getKhuyenMai(), PhongDuocDat_.khuyenMai));
            }
            if (criteria.getCheckIn() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getCheckIn(), PhongDuocDat_.checkIn));
            }
            if (criteria.getCheckOut() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getCheckOut(), PhongDuocDat_.checkOut));
            }
            if (criteria.getGia() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getGia(), PhongDuocDat_.gia));
            }
            if (criteria.getKhachHangId() != null) {
                specification =
                    specification.and(
                        buildSpecification(
                            criteria.getKhachHangId(),
                            root -> root.join(PhongDuocDat_.khachHang, JoinType.LEFT).get(KhachHang_.id)
                        )
                    );
            }
            if (criteria.getPhongDatId() != null) {
                specification =
                    specification.and(
                        buildSpecification(
                            criteria.getPhongDatId(),
                            root -> root.join(PhongDuocDat_.phongDats, JoinType.LEFT).get(PhongDat_.id)
                        )
                    );
            }
            if (criteria.getDichVuSuDungId() != null) {
                specification =
                    specification.and(
                        buildSpecification(
                            criteria.getDichVuSuDungId(),
                            root -> root.join(PhongDuocDat_.dichVuSuDungs, JoinType.LEFT).get(DichVuSuDung_.id)
                        )
                    );
            }
        }
        return specification;
    }
}
