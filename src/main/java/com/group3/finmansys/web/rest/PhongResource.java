package com.group3.finmansys.web.rest;

import com.group3.finmansys.repository.PhongRepository;
import com.group3.finmansys.service.PhongQueryService;
import com.group3.finmansys.service.PhongService;
import com.group3.finmansys.service.criteria.PhongCriteria;
import com.group3.finmansys.service.dto.PhongDTO;
import com.group3.finmansys.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.PaginationUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link com.group3.finmansys.domain.Phong}.
 */
@RestController
@RequestMapping("/api")
public class PhongResource {

    private final Logger log = LoggerFactory.getLogger(PhongResource.class);

    private static final String ENTITY_NAME = "phong";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final PhongService phongService;

    private final PhongRepository phongRepository;

    private final PhongQueryService phongQueryService;

    public PhongResource(PhongService phongService, PhongRepository phongRepository, PhongQueryService phongQueryService) {
        this.phongService = phongService;
        this.phongRepository = phongRepository;
        this.phongQueryService = phongQueryService;
    }

    /**
     * {@code POST  /phongs} : Create a new phong.
     *
     * @param phongDTO the phongDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new phongDTO, or with status {@code 400 (Bad Request)} if the phong has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/phongs")
    public ResponseEntity<PhongDTO> createPhong(@Valid @RequestBody PhongDTO phongDTO) throws URISyntaxException {
        log.debug("REST request to save Phong : {}", phongDTO);
        if (phongDTO.getId() != null) {
            throw new BadRequestAlertException("A new phong cannot already have an ID", ENTITY_NAME, "idexists");
        }
        PhongDTO result = phongService.save(phongDTO);
        return ResponseEntity
            .created(new URI("/api/phongs/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /phongs/:id} : Updates an existing phong.
     *
     * @param id the id of the phongDTO to save.
     * @param phongDTO the phongDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated phongDTO,
     * or with status {@code 400 (Bad Request)} if the phongDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the phongDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/phongs/{id}")
    public ResponseEntity<PhongDTO> updatePhong(
        @PathVariable(value = "id", required = false) final Long id,
        @Valid @RequestBody PhongDTO phongDTO
    ) throws URISyntaxException {
        log.debug("REST request to update Phong : {}, {}", id, phongDTO);
        if (phongDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, phongDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!phongRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        PhongDTO result = phongService.save(phongDTO);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, phongDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /phongs/:id} : Partial updates given fields of an existing phong, field will ignore if it is null
     *
     * @param id the id of the phongDTO to save.
     * @param phongDTO the phongDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated phongDTO,
     * or with status {@code 400 (Bad Request)} if the phongDTO is not valid,
     * or with status {@code 404 (Not Found)} if the phongDTO is not found,
     * or with status {@code 500 (Internal Server Error)} if the phongDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/phongs/{id}", consumes = "application/merge-patch+json")
    public ResponseEntity<PhongDTO> partialUpdatePhong(
        @PathVariable(value = "id", required = false) final Long id,
        @NotNull @RequestBody PhongDTO phongDTO
    ) throws URISyntaxException {
        log.debug("REST request to partial update Phong partially : {}, {}", id, phongDTO);
        if (phongDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, phongDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!phongRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<PhongDTO> result = phongService.partialUpdate(phongDTO);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, phongDTO.getId().toString())
        );
    }

    /**
     * {@code GET  /phongs} : get all the phongs.
     *
     * @param pageable the pagination information.
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of phongs in body.
     */
    @GetMapping("/phongs")
    public ResponseEntity<List<PhongDTO>> getAllPhongs(PhongCriteria criteria, Pageable pageable) {
        log.debug("REST request to get Phongs by criteria: {}", criteria);
        Page<PhongDTO> page = phongQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /phongs/count} : count all the phongs.
     *
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
     */
    @GetMapping("/phongs/count")
    public ResponseEntity<Long> countPhongs(PhongCriteria criteria) {
        log.debug("REST request to count Phongs by criteria: {}", criteria);
        return ResponseEntity.ok().body(phongQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /phongs/:id} : get the "id" phong.
     *
     * @param id the id of the phongDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the phongDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/phongs/{id}")
    public ResponseEntity<PhongDTO> getPhong(@PathVariable Long id) {
        log.debug("REST request to get Phong : {}", id);
        Optional<PhongDTO> phongDTO = phongService.findOne(id);
        return ResponseUtil.wrapOrNotFound(phongDTO);
    }

    /**
     * {@code DELETE  /phongs/:id} : delete the "id" phong.
     *
     * @param id the id of the phongDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/phongs/{id}")
    public ResponseEntity<Void> deletePhong(@PathVariable Long id) {
        log.debug("REST request to delete Phong : {}", id);
        phongService.delete(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
            .build();
    }
}
