package com.group3.finmansys.service;

import com.group3.finmansys.service.dto.LoaiTaiSanDTO;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Service Interface for managing {@link com.group3.finmansys.domain.LoaiTaiSan}.
 */
public interface LoaiTaiSanService {
    /**
     * Save a loaiTaiSan.
     *
     * @param loaiTaiSanDTO the entity to save.
     * @return the persisted entity.
     */
    LoaiTaiSanDTO save(LoaiTaiSanDTO loaiTaiSanDTO);

    /**
     * Partially updates a loaiTaiSan.
     *
     * @param loaiTaiSanDTO the entity to update partially.
     * @return the persisted entity.
     */
    Optional<LoaiTaiSanDTO> partialUpdate(LoaiTaiSanDTO loaiTaiSanDTO);

    /**
     * Get all the loaiTaiSans.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<LoaiTaiSanDTO> findAll(Pageable pageable);

    /**
     * Get the "id" loaiTaiSan.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<LoaiTaiSanDTO> findOne(Long id);

    /**
     * Delete the "id" loaiTaiSan.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
