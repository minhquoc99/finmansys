package com.group3.finmansys.service.dto;

import static org.assertj.core.api.Assertions.assertThat;

import com.group3.finmansys.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class KhoanMucChiPhiDTOTest {

    @Test
    void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(KhoanMucChiPhiDTO.class);
        KhoanMucChiPhiDTO khoanMucChiPhiDTO1 = new KhoanMucChiPhiDTO();
        khoanMucChiPhiDTO1.setId(1L);
        KhoanMucChiPhiDTO khoanMucChiPhiDTO2 = new KhoanMucChiPhiDTO();
        assertThat(khoanMucChiPhiDTO1).isNotEqualTo(khoanMucChiPhiDTO2);
        khoanMucChiPhiDTO2.setId(khoanMucChiPhiDTO1.getId());
        assertThat(khoanMucChiPhiDTO1).isEqualTo(khoanMucChiPhiDTO2);
        khoanMucChiPhiDTO2.setId(2L);
        assertThat(khoanMucChiPhiDTO1).isNotEqualTo(khoanMucChiPhiDTO2);
        khoanMucChiPhiDTO1.setId(null);
        assertThat(khoanMucChiPhiDTO1).isNotEqualTo(khoanMucChiPhiDTO2);
    }
}
