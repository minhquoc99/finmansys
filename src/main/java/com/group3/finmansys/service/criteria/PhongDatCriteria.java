package com.group3.finmansys.service.criteria;

import java.io.Serializable;
import java.util.Objects;
import tech.jhipster.service.Criteria;
import tech.jhipster.service.filter.BooleanFilter;
import tech.jhipster.service.filter.DoubleFilter;
import tech.jhipster.service.filter.Filter;
import tech.jhipster.service.filter.FloatFilter;
import tech.jhipster.service.filter.InstantFilter;
import tech.jhipster.service.filter.IntegerFilter;
import tech.jhipster.service.filter.LongFilter;
import tech.jhipster.service.filter.StringFilter;

/**
 * Criteria class for the {@link com.group3.finmansys.domain.PhongDat} entity. This class is used
 * in {@link com.group3.finmansys.web.rest.PhongDatResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /phong-dats?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class PhongDatCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private InstantFilter ngayDat;

    private LongFilter phongId;

    private LongFilter phongDuocDatId;

    public PhongDatCriteria() {}

    public PhongDatCriteria(PhongDatCriteria other) {
        this.id = other.id == null ? null : other.id.copy();
        this.ngayDat = other.ngayDat == null ? null : other.ngayDat.copy();
        this.phongId = other.phongId == null ? null : other.phongId.copy();
        this.phongDuocDatId = other.phongDuocDatId == null ? null : other.phongDuocDatId.copy();
    }

    @Override
    public PhongDatCriteria copy() {
        return new PhongDatCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public LongFilter id() {
        if (id == null) {
            id = new LongFilter();
        }
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public InstantFilter getNgayDat() {
        return ngayDat;
    }

    public InstantFilter ngayDat() {
        if (ngayDat == null) {
            ngayDat = new InstantFilter();
        }
        return ngayDat;
    }

    public void setNgayDat(InstantFilter ngayDat) {
        this.ngayDat = ngayDat;
    }

    public LongFilter getPhongId() {
        return phongId;
    }

    public LongFilter phongId() {
        if (phongId == null) {
            phongId = new LongFilter();
        }
        return phongId;
    }

    public void setPhongId(LongFilter phongId) {
        this.phongId = phongId;
    }

    public LongFilter getPhongDuocDatId() {
        return phongDuocDatId;
    }

    public LongFilter phongDuocDatId() {
        if (phongDuocDatId == null) {
            phongDuocDatId = new LongFilter();
        }
        return phongDuocDatId;
    }

    public void setPhongDuocDatId(LongFilter phongDuocDatId) {
        this.phongDuocDatId = phongDuocDatId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final PhongDatCriteria that = (PhongDatCriteria) o;
        return (
            Objects.equals(id, that.id) &&
            Objects.equals(ngayDat, that.ngayDat) &&
            Objects.equals(phongId, that.phongId) &&
            Objects.equals(phongDuocDatId, that.phongDuocDatId)
        );
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, ngayDat, phongId, phongDuocDatId);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "PhongDatCriteria{" +
            (id != null ? "id=" + id + ", " : "") +
            (ngayDat != null ? "ngayDat=" + ngayDat + ", " : "") +
            (phongId != null ? "phongId=" + phongId + ", " : "") +
            (phongDuocDatId != null ? "phongDuocDatId=" + phongDuocDatId + ", " : "") +
            "}";
    }
}
