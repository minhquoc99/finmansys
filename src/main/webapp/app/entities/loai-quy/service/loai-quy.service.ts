import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { isPresent } from 'app/core/util/operators';
import { ApplicationConfigService } from 'app/core/config/application-config.service';
import { createRequestOption } from 'app/core/request/request-util';
import { ILoaiQuy, getLoaiQuyIdentifier } from '../loai-quy.model';

export type EntityResponseType = HttpResponse<ILoaiQuy>;
export type EntityArrayResponseType = HttpResponse<ILoaiQuy[]>;

@Injectable({ providedIn: 'root' })
export class LoaiQuyService {
  public resourceUrl = this.applicationConfigService.getEndpointFor('api/loai-quies');

  constructor(protected http: HttpClient, private applicationConfigService: ApplicationConfigService) {}

  create(loaiQuy: ILoaiQuy): Observable<EntityResponseType> {
    return this.http.post<ILoaiQuy>(this.resourceUrl, loaiQuy, { observe: 'response' });
  }

  update(loaiQuy: ILoaiQuy): Observable<EntityResponseType> {
    return this.http.put<ILoaiQuy>(`${this.resourceUrl}/${getLoaiQuyIdentifier(loaiQuy) as number}`, loaiQuy, { observe: 'response' });
  }

  partialUpdate(loaiQuy: ILoaiQuy): Observable<EntityResponseType> {
    return this.http.patch<ILoaiQuy>(`${this.resourceUrl}/${getLoaiQuyIdentifier(loaiQuy) as number}`, loaiQuy, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<ILoaiQuy>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<ILoaiQuy[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  addLoaiQuyToCollectionIfMissing(loaiQuyCollection: ILoaiQuy[], ...loaiQuiesToCheck: (ILoaiQuy | null | undefined)[]): ILoaiQuy[] {
    const loaiQuies: ILoaiQuy[] = loaiQuiesToCheck.filter(isPresent);
    if (loaiQuies.length > 0) {
      const loaiQuyCollectionIdentifiers = loaiQuyCollection.map(loaiQuyItem => getLoaiQuyIdentifier(loaiQuyItem)!);
      const loaiQuiesToAdd = loaiQuies.filter(loaiQuyItem => {
        const loaiQuyIdentifier = getLoaiQuyIdentifier(loaiQuyItem);
        if (loaiQuyIdentifier == null || loaiQuyCollectionIdentifiers.includes(loaiQuyIdentifier)) {
          return false;
        }
        loaiQuyCollectionIdentifiers.push(loaiQuyIdentifier);
        return true;
      });
      return [...loaiQuiesToAdd, ...loaiQuyCollection];
    }
    return loaiQuyCollection;
  }
}
