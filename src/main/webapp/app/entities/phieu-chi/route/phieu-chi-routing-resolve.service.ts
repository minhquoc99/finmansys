import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { mergeMap } from 'rxjs/operators';

import { IPhieuChi, PhieuChi } from '../phieu-chi.model';
import { PhieuChiService } from '../service/phieu-chi.service';

@Injectable({ providedIn: 'root' })
export class PhieuChiRoutingResolveService implements Resolve<IPhieuChi> {
  constructor(protected service: PhieuChiService, protected router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IPhieuChi> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        mergeMap((phieuChi: HttpResponse<PhieuChi>) => {
          if (phieuChi.body) {
            return of(phieuChi.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new PhieuChi());
  }
}
