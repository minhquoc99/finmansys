jest.mock('@angular/router');

import { TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ActivatedRouteSnapshot, Router } from '@angular/router';
import { of } from 'rxjs';

import { IKhoanPhaiThu, KhoanPhaiThu } from '../khoan-phai-thu.model';
import { KhoanPhaiThuService } from '../service/khoan-phai-thu.service';

import { KhoanPhaiThuRoutingResolveService } from './khoan-phai-thu-routing-resolve.service';

describe('Service Tests', () => {
  describe('KhoanPhaiThu routing resolve service', () => {
    let mockRouter: Router;
    let mockActivatedRouteSnapshot: ActivatedRouteSnapshot;
    let routingResolveService: KhoanPhaiThuRoutingResolveService;
    let service: KhoanPhaiThuService;
    let resultKhoanPhaiThu: IKhoanPhaiThu | undefined;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule],
        providers: [Router, ActivatedRouteSnapshot],
      });
      mockRouter = TestBed.inject(Router);
      mockActivatedRouteSnapshot = TestBed.inject(ActivatedRouteSnapshot);
      routingResolveService = TestBed.inject(KhoanPhaiThuRoutingResolveService);
      service = TestBed.inject(KhoanPhaiThuService);
      resultKhoanPhaiThu = undefined;
    });

    describe('resolve', () => {
      it('should return IKhoanPhaiThu returned by find', () => {
        // GIVEN
        service.find = jest.fn(id => of(new HttpResponse({ body: { id } })));
        mockActivatedRouteSnapshot.params = { id: 123 };

        // WHEN
        routingResolveService.resolve(mockActivatedRouteSnapshot).subscribe(result => {
          resultKhoanPhaiThu = result;
        });

        // THEN
        expect(service.find).toBeCalledWith(123);
        expect(resultKhoanPhaiThu).toEqual({ id: 123 });
      });

      it('should return new IKhoanPhaiThu if id is not provided', () => {
        // GIVEN
        service.find = jest.fn();
        mockActivatedRouteSnapshot.params = {};

        // WHEN
        routingResolveService.resolve(mockActivatedRouteSnapshot).subscribe(result => {
          resultKhoanPhaiThu = result;
        });

        // THEN
        expect(service.find).not.toBeCalled();
        expect(resultKhoanPhaiThu).toEqual(new KhoanPhaiThu());
      });

      it('should route to 404 page if data not found in server', () => {
        // GIVEN
        spyOn(service, 'find').and.returnValue(of(new HttpResponse({ body: null })));
        mockActivatedRouteSnapshot.params = { id: 123 };

        // WHEN
        routingResolveService.resolve(mockActivatedRouteSnapshot).subscribe(result => {
          resultKhoanPhaiThu = result;
        });

        // THEN
        expect(service.find).toBeCalledWith(123);
        expect(resultKhoanPhaiThu).toEqual(undefined);
        expect(mockRouter.navigate).toHaveBeenCalledWith(['404']);
      });
    });
  });
});
