jest.mock('@angular/router');

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { of, Subject } from 'rxjs';

import { ChiTietGiamTSCDService } from '../service/chi-tiet-giam-tscd.service';
import { IChiTietGiamTSCD, ChiTietGiamTSCD } from '../chi-tiet-giam-tscd.model';
import { ITaiSanCD } from 'app/entities/tai-san-cd/tai-san-cd.model';
import { TaiSanCDService } from 'app/entities/tai-san-cd/service/tai-san-cd.service';

import { ChiTietGiamTSCDUpdateComponent } from './chi-tiet-giam-tscd-update.component';

describe('Component Tests', () => {
  describe('ChiTietGiamTSCD Management Update Component', () => {
    let comp: ChiTietGiamTSCDUpdateComponent;
    let fixture: ComponentFixture<ChiTietGiamTSCDUpdateComponent>;
    let activatedRoute: ActivatedRoute;
    let chiTietGiamTSCDService: ChiTietGiamTSCDService;
    let taiSanCDService: TaiSanCDService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule],
        declarations: [ChiTietGiamTSCDUpdateComponent],
        providers: [FormBuilder, ActivatedRoute],
      })
        .overrideTemplate(ChiTietGiamTSCDUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(ChiTietGiamTSCDUpdateComponent);
      activatedRoute = TestBed.inject(ActivatedRoute);
      chiTietGiamTSCDService = TestBed.inject(ChiTietGiamTSCDService);
      taiSanCDService = TestBed.inject(TaiSanCDService);

      comp = fixture.componentInstance;
    });

    describe('ngOnInit', () => {
      it('Should call TaiSanCD query and add missing value', () => {
        const chiTietGiamTSCD: IChiTietGiamTSCD = { id: 456 };
        const taiSanCD: ITaiSanCD = { id: 74452 };
        chiTietGiamTSCD.taiSanCD = taiSanCD;

        const taiSanCDCollection: ITaiSanCD[] = [{ id: 97673 }];
        spyOn(taiSanCDService, 'query').and.returnValue(of(new HttpResponse({ body: taiSanCDCollection })));
        const additionalTaiSanCDS = [taiSanCD];
        const expectedCollection: ITaiSanCD[] = [...additionalTaiSanCDS, ...taiSanCDCollection];
        spyOn(taiSanCDService, 'addTaiSanCDToCollectionIfMissing').and.returnValue(expectedCollection);

        activatedRoute.data = of({ chiTietGiamTSCD });
        comp.ngOnInit();

        expect(taiSanCDService.query).toHaveBeenCalled();
        expect(taiSanCDService.addTaiSanCDToCollectionIfMissing).toHaveBeenCalledWith(taiSanCDCollection, ...additionalTaiSanCDS);
        expect(comp.taiSanCDSSharedCollection).toEqual(expectedCollection);
      });

      it('Should update editForm', () => {
        const chiTietGiamTSCD: IChiTietGiamTSCD = { id: 456 };
        const taiSanCD: ITaiSanCD = { id: 55262 };
        chiTietGiamTSCD.taiSanCD = taiSanCD;

        activatedRoute.data = of({ chiTietGiamTSCD });
        comp.ngOnInit();

        expect(comp.editForm.value).toEqual(expect.objectContaining(chiTietGiamTSCD));
        expect(comp.taiSanCDSSharedCollection).toContain(taiSanCD);
      });
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', () => {
        // GIVEN
        const saveSubject = new Subject();
        const chiTietGiamTSCD = { id: 123 };
        spyOn(chiTietGiamTSCDService, 'update').and.returnValue(saveSubject);
        spyOn(comp, 'previousState');
        activatedRoute.data = of({ chiTietGiamTSCD });
        comp.ngOnInit();

        // WHEN
        comp.save();
        expect(comp.isSaving).toEqual(true);
        saveSubject.next(new HttpResponse({ body: chiTietGiamTSCD }));
        saveSubject.complete();

        // THEN
        expect(comp.previousState).toHaveBeenCalled();
        expect(chiTietGiamTSCDService.update).toHaveBeenCalledWith(chiTietGiamTSCD);
        expect(comp.isSaving).toEqual(false);
      });

      it('Should call create service on save for new entity', () => {
        // GIVEN
        const saveSubject = new Subject();
        const chiTietGiamTSCD = new ChiTietGiamTSCD();
        spyOn(chiTietGiamTSCDService, 'create').and.returnValue(saveSubject);
        spyOn(comp, 'previousState');
        activatedRoute.data = of({ chiTietGiamTSCD });
        comp.ngOnInit();

        // WHEN
        comp.save();
        expect(comp.isSaving).toEqual(true);
        saveSubject.next(new HttpResponse({ body: chiTietGiamTSCD }));
        saveSubject.complete();

        // THEN
        expect(chiTietGiamTSCDService.create).toHaveBeenCalledWith(chiTietGiamTSCD);
        expect(comp.isSaving).toEqual(false);
        expect(comp.previousState).toHaveBeenCalled();
      });

      it('Should set isSaving to false on error', () => {
        // GIVEN
        const saveSubject = new Subject();
        const chiTietGiamTSCD = { id: 123 };
        spyOn(chiTietGiamTSCDService, 'update').and.returnValue(saveSubject);
        spyOn(comp, 'previousState');
        activatedRoute.data = of({ chiTietGiamTSCD });
        comp.ngOnInit();

        // WHEN
        comp.save();
        expect(comp.isSaving).toEqual(true);
        saveSubject.error('This is an error!');

        // THEN
        expect(chiTietGiamTSCDService.update).toHaveBeenCalledWith(chiTietGiamTSCD);
        expect(comp.isSaving).toEqual(false);
        expect(comp.previousState).not.toHaveBeenCalled();
      });
    });

    describe('Tracking relationships identifiers', () => {
      describe('trackTaiSanCDById', () => {
        it('Should return tracked TaiSanCD primary key', () => {
          const entity = { id: 123 };
          const trackResult = comp.trackTaiSanCDById(0, entity);
          expect(trackResult).toEqual(entity.id);
        });
      });
    });
  });
});
