package com.group3.finmansys.service.mapper;

import com.group3.finmansys.domain.*;
import com.group3.finmansys.service.dto.PhongDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link Phong} and its DTO {@link PhongDTO}.
 */
@Mapper(componentModel = "spring", uses = { KhachSanMapper.class })
public interface PhongMapper extends EntityMapper<PhongDTO, Phong> {
    @Mapping(target = "khachSan", source = "khachSan", qualifiedByName = "ten")
    PhongDTO toDto(Phong s);

    @Named("ten")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    @Mapping(target = "ten", source = "ten")
    PhongDTO toDtoTen(Phong phong);
}
