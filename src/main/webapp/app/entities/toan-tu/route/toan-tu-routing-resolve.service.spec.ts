jest.mock('@angular/router');

import { TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ActivatedRouteSnapshot, Router } from '@angular/router';
import { of } from 'rxjs';

import { IToanTu, ToanTu } from '../toan-tu.model';
import { ToanTuService } from '../service/toan-tu.service';

import { ToanTuRoutingResolveService } from './toan-tu-routing-resolve.service';

describe('Service Tests', () => {
  describe('ToanTu routing resolve service', () => {
    let mockRouter: Router;
    let mockActivatedRouteSnapshot: ActivatedRouteSnapshot;
    let routingResolveService: ToanTuRoutingResolveService;
    let service: ToanTuService;
    let resultToanTu: IToanTu | undefined;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule],
        providers: [Router, ActivatedRouteSnapshot],
      });
      mockRouter = TestBed.inject(Router);
      mockActivatedRouteSnapshot = TestBed.inject(ActivatedRouteSnapshot);
      routingResolveService = TestBed.inject(ToanTuRoutingResolveService);
      service = TestBed.inject(ToanTuService);
      resultToanTu = undefined;
    });

    describe('resolve', () => {
      it('should return IToanTu returned by find', () => {
        // GIVEN
        service.find = jest.fn(id => of(new HttpResponse({ body: { id } })));
        mockActivatedRouteSnapshot.params = { id: 123 };

        // WHEN
        routingResolveService.resolve(mockActivatedRouteSnapshot).subscribe(result => {
          resultToanTu = result;
        });

        // THEN
        expect(service.find).toBeCalledWith(123);
        expect(resultToanTu).toEqual({ id: 123 });
      });

      it('should return new IToanTu if id is not provided', () => {
        // GIVEN
        service.find = jest.fn();
        mockActivatedRouteSnapshot.params = {};

        // WHEN
        routingResolveService.resolve(mockActivatedRouteSnapshot).subscribe(result => {
          resultToanTu = result;
        });

        // THEN
        expect(service.find).not.toBeCalled();
        expect(resultToanTu).toEqual(new ToanTu());
      });

      it('should route to 404 page if data not found in server', () => {
        // GIVEN
        spyOn(service, 'find').and.returnValue(of(new HttpResponse({ body: null })));
        mockActivatedRouteSnapshot.params = { id: 123 };

        // WHEN
        routingResolveService.resolve(mockActivatedRouteSnapshot).subscribe(result => {
          resultToanTu = result;
        });

        // THEN
        expect(service.find).toBeCalledWith(123);
        expect(resultToanTu).toEqual(undefined);
        expect(mockRouter.navigate).toHaveBeenCalledWith(['404']);
      });
    });
  });
});
