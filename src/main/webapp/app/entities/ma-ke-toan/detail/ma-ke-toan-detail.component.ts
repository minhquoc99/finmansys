import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IMaKeToan } from '../ma-ke-toan.model';

@Component({
  selector: 'jhi-ma-ke-toan-detail',
  templateUrl: './ma-ke-toan-detail.component.html',
})
export class MaKeToanDetailComponent implements OnInit {
  maKeToan: IMaKeToan | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ maKeToan }) => {
      this.maKeToan = maKeToan;
    });
  }

  previousState(): void {
    window.history.back();
  }
}
