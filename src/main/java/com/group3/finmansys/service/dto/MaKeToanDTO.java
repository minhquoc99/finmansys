package com.group3.finmansys.service.dto;

import java.io.Serializable;
import java.util.Objects;
import javax.validation.constraints.*;

/**
 * A DTO for the {@link com.group3.finmansys.domain.MaKeToan} entity.
 */
public class MaKeToanDTO implements Serializable {

    private Long id;

    @NotNull
    @DecimalMin(value = "1")
    private Double soHieu;

    @NotNull
    private String ten;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Double getSoHieu() {
        return soHieu;
    }

    public void setSoHieu(Double soHieu) {
        this.soHieu = soHieu;
    }

    public String getTen() {
        return ten;
    }

    public void setTen(String ten) {
        this.ten = ten;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof MaKeToanDTO)) {
            return false;
        }

        MaKeToanDTO maKeToanDTO = (MaKeToanDTO) o;
        if (this.id == null) {
            return false;
        }
        return Objects.equals(this.id, maKeToanDTO.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.id);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "MaKeToanDTO{" +
            "id=" + getId() +
            ", soHieu=" + getSoHieu() +
            ", ten='" + getTen() + "'" +
            "}";
    }
}
