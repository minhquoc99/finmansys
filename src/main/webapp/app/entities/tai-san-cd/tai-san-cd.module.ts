import { NgModule } from '@angular/core';

import { SharedModule } from 'app/shared/shared.module';
import { TaiSanCDComponent } from './list/tai-san-cd.component';
import { TaiSanCDDetailComponent } from './detail/tai-san-cd-detail.component';
import { TaiSanCDUpdateComponent } from './update/tai-san-cd-update.component';
import { TaiSanCDDeleteDialogComponent } from './delete/tai-san-cd-delete-dialog.component';
import { TaiSanCDRoutingModule } from './route/tai-san-cd-routing.module';

@NgModule({
  imports: [SharedModule, TaiSanCDRoutingModule],
  declarations: [TaiSanCDComponent, TaiSanCDDetailComponent, TaiSanCDUpdateComponent, TaiSanCDDeleteDialogComponent],
  entryComponents: [TaiSanCDDeleteDialogComponent],
})
export class TaiSanCDModule {}
