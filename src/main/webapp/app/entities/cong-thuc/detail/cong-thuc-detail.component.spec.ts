import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { CongThucDetailComponent } from './cong-thuc-detail.component';

describe('Component Tests', () => {
  describe('CongThuc Management Detail Component', () => {
    let comp: CongThucDetailComponent;
    let fixture: ComponentFixture<CongThucDetailComponent>;

    beforeEach(() => {
      TestBed.configureTestingModule({
        declarations: [CongThucDetailComponent],
        providers: [
          {
            provide: ActivatedRoute,
            useValue: { data: of({ congThuc: { id: 123 } }) },
          },
        ],
      })
        .overrideTemplate(CongThucDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(CongThucDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should load congThuc on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.congThuc).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
