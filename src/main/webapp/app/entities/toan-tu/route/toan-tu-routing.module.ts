import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { UserRouteAccessService } from 'app/core/auth/user-route-access.service';
import { ToanTuComponent } from '../list/toan-tu.component';
import { ToanTuDetailComponent } from '../detail/toan-tu-detail.component';
import { ToanTuUpdateComponent } from '../update/toan-tu-update.component';
import { ToanTuRoutingResolveService } from './toan-tu-routing-resolve.service';

const toanTuRoute: Routes = [
  {
    path: '',
    component: ToanTuComponent,
    data: {
      defaultSort: 'id,asc',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: ToanTuDetailComponent,
    resolve: {
      toanTu: ToanTuRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: ToanTuUpdateComponent,
    resolve: {
      toanTu: ToanTuRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: ToanTuUpdateComponent,
    resolve: {
      toanTu: ToanTuRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
];

@NgModule({
  imports: [RouterModule.forChild(toanTuRoute)],
  exports: [RouterModule],
})
export class ToanTuRoutingModule {}
