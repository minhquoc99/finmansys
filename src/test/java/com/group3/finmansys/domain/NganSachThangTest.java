package com.group3.finmansys.domain;

import static org.assertj.core.api.Assertions.assertThat;

import com.group3.finmansys.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class NganSachThangTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(NganSachThang.class);
        NganSachThang nganSachThang1 = new NganSachThang();
        nganSachThang1.setId(1L);
        NganSachThang nganSachThang2 = new NganSachThang();
        nganSachThang2.setId(nganSachThang1.getId());
        assertThat(nganSachThang1).isEqualTo(nganSachThang2);
        nganSachThang2.setId(2L);
        assertThat(nganSachThang1).isNotEqualTo(nganSachThang2);
        nganSachThang1.setId(null);
        assertThat(nganSachThang1).isNotEqualTo(nganSachThang2);
    }
}
