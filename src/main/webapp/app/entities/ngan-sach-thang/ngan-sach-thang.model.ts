import { INganSachNam } from 'app/entities/ngan-sach-nam/ngan-sach-nam.model';
import { IKhoanMucChiPhi } from 'app/entities/khoan-muc-chi-phi/khoan-muc-chi-phi.model';

export interface INganSachThang {
  id?: number;
  thang?: number;
  soTien?: number;
  nganSachNam?: INganSachNam;
  khoanMucChiPhi?: IKhoanMucChiPhi;
}

export class NganSachThang implements INganSachThang {
  constructor(
    public id?: number,
    public thang?: number,
    public soTien?: number,
    public nganSachNam?: INganSachNam,
    public khoanMucChiPhi?: IKhoanMucChiPhi
  ) {}
}

export function getNganSachThangIdentifier(nganSachThang: INganSachThang): number | undefined {
  return nganSachThang.id;
}
