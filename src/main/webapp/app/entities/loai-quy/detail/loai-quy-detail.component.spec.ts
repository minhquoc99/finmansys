import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { LoaiQuyDetailComponent } from './loai-quy-detail.component';

describe('Component Tests', () => {
  describe('LoaiQuy Management Detail Component', () => {
    let comp: LoaiQuyDetailComponent;
    let fixture: ComponentFixture<LoaiQuyDetailComponent>;

    beforeEach(() => {
      TestBed.configureTestingModule({
        declarations: [LoaiQuyDetailComponent],
        providers: [
          {
            provide: ActivatedRoute,
            useValue: { data: of({ loaiQuy: { id: 123 } }) },
          },
        ],
      })
        .overrideTemplate(LoaiQuyDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(LoaiQuyDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should load loaiQuy on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.loaiQuy).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
