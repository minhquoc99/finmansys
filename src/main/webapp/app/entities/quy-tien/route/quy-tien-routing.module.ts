import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { UserRouteAccessService } from 'app/core/auth/user-route-access.service';
import { QuyTienComponent } from '../list/quy-tien.component';
import { QuyTienDetailComponent } from '../detail/quy-tien-detail.component';
import { QuyTienUpdateComponent } from '../update/quy-tien-update.component';
import { QuyTienRoutingResolveService } from './quy-tien-routing-resolve.service';

const quyTienRoute: Routes = [
  {
    path: '',
    component: QuyTienComponent,
    data: {
      defaultSort: 'id,asc',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: QuyTienDetailComponent,
    resolve: {
      quyTien: QuyTienRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: QuyTienUpdateComponent,
    resolve: {
      quyTien: QuyTienRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: QuyTienUpdateComponent,
    resolve: {
      quyTien: QuyTienRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
];

@NgModule({
  imports: [RouterModule.forChild(quyTienRoute)],
  exports: [RouterModule],
})
export class QuyTienRoutingModule {}
