import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { UserRouteAccessService } from 'app/core/auth/user-route-access.service';
import { KhoanPhaiTraComponent } from '../list/khoan-phai-tra.component';
import { KhoanPhaiTraDetailComponent } from '../detail/khoan-phai-tra-detail.component';
import { KhoanPhaiTraUpdateComponent } from '../update/khoan-phai-tra-update.component';
import { KhoanPhaiTraRoutingResolveService } from './khoan-phai-tra-routing-resolve.service';

const khoanPhaiTraRoute: Routes = [
  {
    path: '',
    component: KhoanPhaiTraComponent,
    data: {
      defaultSort: 'id,asc',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: KhoanPhaiTraDetailComponent,
    resolve: {
      khoanPhaiTra: KhoanPhaiTraRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: KhoanPhaiTraUpdateComponent,
    resolve: {
      khoanPhaiTra: KhoanPhaiTraRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: KhoanPhaiTraUpdateComponent,
    resolve: {
      khoanPhaiTra: KhoanPhaiTraRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
];

@NgModule({
  imports: [RouterModule.forChild(khoanPhaiTraRoute)],
  exports: [RouterModule],
})
export class KhoanPhaiTraRoutingModule {}
