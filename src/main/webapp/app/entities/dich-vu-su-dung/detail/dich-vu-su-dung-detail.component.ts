import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IDichVuSuDung } from '../dich-vu-su-dung.model';

@Component({
  selector: 'jhi-dich-vu-su-dung-detail',
  templateUrl: './dich-vu-su-dung-detail.component.html',
})
export class DichVuSuDungDetailComponent implements OnInit {
  dichVuSuDung: IDichVuSuDung | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ dichVuSuDung }) => {
      this.dichVuSuDung = dichVuSuDung;
    });
  }

  previousState(): void {
    window.history.back();
  }
}
