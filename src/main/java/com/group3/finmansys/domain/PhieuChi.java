package com.group3.finmansys.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.time.Instant;
import javax.persistence.*;
import javax.validation.constraints.*;

/**
 * A PhieuChi.
 */
@Entity
@Table(name = "phieu_chi")
public class PhieuChi implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(name = "ngay_chung_tu", nullable = false)
    private Instant ngayChungTu;

    @NotNull
    @DecimalMin(value = "1")
    @Column(name = "so_tien", nullable = false)
    private Double soTien;

    @ManyToOne(optional = false)
    @NotNull
    private KhachHang khachHang;

    @ManyToOne(optional = false)
    @NotNull
    private KhoanMucChiPhi khoanMucChiPhi;

    @ManyToOne
    @JsonIgnoreProperties(value = { "khachHang", "maKeToan", "dsPhieuChis" }, allowSetters = true)
    private KhoanPhaiTra khoanPhaiTra;

    @ManyToOne(optional = false)
    @NotNull
    @JsonIgnoreProperties(value = { "loaiQuy", "maKeToan", "khachSan", "phieuThus", "phieuChis" }, allowSetters = true)
    private QuyTien quyTien;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public PhieuChi id(Long id) {
        this.id = id;
        return this;
    }

    public Instant getNgayChungTu() {
        return this.ngayChungTu;
    }

    public PhieuChi ngayChungTu(Instant ngayChungTu) {
        this.ngayChungTu = ngayChungTu;
        return this;
    }

    public void setNgayChungTu(Instant ngayChungTu) {
        this.ngayChungTu = ngayChungTu;
    }

    public Double getSoTien() {
        return this.soTien;
    }

    public PhieuChi soTien(Double soTien) {
        this.soTien = soTien;
        return this;
    }

    public void setSoTien(Double soTien) {
        this.soTien = soTien;
    }

    public KhachHang getKhachHang() {
        return this.khachHang;
    }

    public PhieuChi khachHang(KhachHang khachHang) {
        this.setKhachHang(khachHang);
        return this;
    }

    public void setKhachHang(KhachHang khachHang) {
        this.khachHang = khachHang;
    }

    public KhoanMucChiPhi getKhoanMucChiPhi() {
        return this.khoanMucChiPhi;
    }

    public PhieuChi khoanMucChiPhi(KhoanMucChiPhi khoanMucChiPhi) {
        this.setKhoanMucChiPhi(khoanMucChiPhi);
        return this;
    }

    public void setKhoanMucChiPhi(KhoanMucChiPhi khoanMucChiPhi) {
        this.khoanMucChiPhi = khoanMucChiPhi;
    }

    public KhoanPhaiTra getKhoanPhaiTra() {
        return this.khoanPhaiTra;
    }

    public PhieuChi khoanPhaiTra(KhoanPhaiTra khoanPhaiTra) {
        this.setKhoanPhaiTra(khoanPhaiTra);
        return this;
    }

    public void setKhoanPhaiTra(KhoanPhaiTra khoanPhaiTra) {
        this.khoanPhaiTra = khoanPhaiTra;
    }

    public QuyTien getQuyTien() {
        return this.quyTien;
    }

    public PhieuChi quyTien(QuyTien quyTien) {
        this.setQuyTien(quyTien);
        return this;
    }

    public void setQuyTien(QuyTien quyTien) {
        this.quyTien = quyTien;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof PhieuChi)) {
            return false;
        }
        return id != null && id.equals(((PhieuChi) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "PhieuChi{" +
            "id=" + getId() +
            ", ngayChungTu='" + getNgayChungTu() + "'" +
            ", soTien=" + getSoTien() +
            "}";
    }
}
