package com.group3.finmansys.repository;

import com.group3.finmansys.domain.ChiTietGiamTSCD;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data SQL repository for the ChiTietGiamTSCD entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ChiTietGiamTSCDRepository extends JpaRepository<ChiTietGiamTSCD, Long>, JpaSpecificationExecutor<ChiTietGiamTSCD> {}
