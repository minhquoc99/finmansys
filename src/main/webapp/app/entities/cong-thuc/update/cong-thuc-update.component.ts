import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize, map } from 'rxjs/operators';

import { ICongThuc, CongThuc } from '../cong-thuc.model';
import { CongThucService } from '../service/cong-thuc.service';

@Component({
  selector: 'jhi-cong-thuc-update',
  templateUrl: './cong-thuc-update.component.html',
})
export class CongThucUpdateComponent implements OnInit {
  isSaving = false;

  congThucsSharedCollection: ICongThuc[] = [];

  editForm = this.fb.group({
    id: [],
    maSo: [null, [Validators.required]],
    tenChiTieu: [null, [Validators.required]],
    cap: [null, [Validators.required]],
    soDauKy: [],
    soCuoiKy: [],
    capCha: [],
  });

  constructor(protected congThucService: CongThucService, protected activatedRoute: ActivatedRoute, protected fb: FormBuilder) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ congThuc }) => {
      this.updateForm(congThuc);

      this.loadRelationshipsOptions();
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const congThuc = this.createFromForm();
    if (congThuc.id !== undefined) {
      this.subscribeToSaveResponse(this.congThucService.update(congThuc));
    } else {
      this.subscribeToSaveResponse(this.congThucService.create(congThuc));
    }
  }

  trackCongThucById(index: number, item: ICongThuc): number {
    return item.id!;
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<ICongThuc>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(congThuc: ICongThuc): void {
    this.editForm.patchValue({
      id: congThuc.id,
      maSo: congThuc.maSo,
      tenChiTieu: congThuc.tenChiTieu,
      cap: congThuc.cap,
      soDauKy: congThuc.soDauKy,
      soCuoiKy: congThuc.soCuoiKy,
      capCha: congThuc.capCha,
    });

    this.congThucsSharedCollection = this.congThucService.addCongThucToCollectionIfMissing(this.congThucsSharedCollection, congThuc.capCha);
  }

  protected loadRelationshipsOptions(): void {
    this.congThucService
      .query({
        page: 0,
        size: 10000,
        sort: ['id,asc'],
      })
      .pipe(map((res: HttpResponse<ICongThuc[]>) => res.body ?? []))
      .pipe(
        map((congThucs: ICongThuc[]) =>
          this.congThucService.addCongThucToCollectionIfMissing(congThucs, this.editForm.get('capCha')!.value)
        )
      )
      .subscribe((congThucs: ICongThuc[]) => (this.congThucsSharedCollection = congThucs));
  }

  protected createFromForm(): ICongThuc {
    return {
      ...new CongThuc(),
      id: this.editForm.get(['id'])!.value,
      maSo: this.editForm.get(['maSo'])!.value,
      tenChiTieu: this.editForm.get(['tenChiTieu'])!.value,
      cap: this.editForm.get(['cap'])!.value,
      soDauKy: this.editForm.get(['soDauKy'])!.value,
      soCuoiKy: this.editForm.get(['soCuoiKy'])!.value,
      capCha: this.editForm.get(['capCha'])!.value,
    };
  }
}
