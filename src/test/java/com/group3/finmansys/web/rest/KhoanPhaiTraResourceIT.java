package com.group3.finmansys.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.group3.finmansys.IntegrationTest;
import com.group3.finmansys.domain.KhachHang;
import com.group3.finmansys.domain.KhoanPhaiTra;
import com.group3.finmansys.domain.MaKeToan;
import com.group3.finmansys.domain.PhieuChi;
import com.group3.finmansys.repository.KhoanPhaiTraRepository;
import com.group3.finmansys.service.criteria.KhoanPhaiTraCriteria;
import com.group3.finmansys.service.dto.KhoanPhaiTraDTO;
import com.group3.finmansys.service.mapper.KhoanPhaiTraMapper;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link KhoanPhaiTraResource} REST controller.
 */
@IntegrationTest
@ExtendWith(MockitoExtension.class)
@AutoConfigureMockMvc
@WithMockUser
class KhoanPhaiTraResourceIT {

    private static final Instant DEFAULT_NGAY_TAO = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_NGAY_TAO = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final Instant DEFAULT_NGAY_PHAI_TRA = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_NGAY_PHAI_TRA = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final Double DEFAULT_SO_TIEN = 1D;
    private static final Double UPDATED_SO_TIEN = 2D;
    private static final Double SMALLER_SO_TIEN = 1D - 1D;

    private static final String DEFAULT_GHI_CHU = "AAAAAAAAAA";
    private static final String UPDATED_GHI_CHU = "BBBBBBBBBB";

    private static final String ENTITY_API_URL = "/api/khoan-phai-tras";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private KhoanPhaiTraRepository khoanPhaiTraRepository;

    @Autowired
    private KhoanPhaiTraMapper khoanPhaiTraMapper;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restKhoanPhaiTraMockMvc;

    private KhoanPhaiTra khoanPhaiTra;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static KhoanPhaiTra createEntity(EntityManager em) {
        KhoanPhaiTra khoanPhaiTra = new KhoanPhaiTra()
            .ngayTao(DEFAULT_NGAY_TAO)
            .ngayPhaiTra(DEFAULT_NGAY_PHAI_TRA)
            .soTien(DEFAULT_SO_TIEN)
            .ghiChu(DEFAULT_GHI_CHU);
        // Add required entity
        KhachHang khachHang;
        if (TestUtil.findAll(em, KhachHang.class).isEmpty()) {
            khachHang = KhachHangResourceIT.createEntity(em);
            em.persist(khachHang);
            em.flush();
        } else {
            khachHang = TestUtil.findAll(em, KhachHang.class).get(0);
        }
        khoanPhaiTra.setKhachHang(khachHang);
        // Add required entity
        MaKeToan maKeToan;
        if (TestUtil.findAll(em, MaKeToan.class).isEmpty()) {
            maKeToan = MaKeToanResourceIT.createEntity(em);
            em.persist(maKeToan);
            em.flush();
        } else {
            maKeToan = TestUtil.findAll(em, MaKeToan.class).get(0);
        }
        khoanPhaiTra.setMaKeToan(maKeToan);
        return khoanPhaiTra;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static KhoanPhaiTra createUpdatedEntity(EntityManager em) {
        KhoanPhaiTra khoanPhaiTra = new KhoanPhaiTra()
            .ngayTao(UPDATED_NGAY_TAO)
            .ngayPhaiTra(UPDATED_NGAY_PHAI_TRA)
            .soTien(UPDATED_SO_TIEN)
            .ghiChu(UPDATED_GHI_CHU);
        // Add required entity
        KhachHang khachHang;
        if (TestUtil.findAll(em, KhachHang.class).isEmpty()) {
            khachHang = KhachHangResourceIT.createUpdatedEntity(em);
            em.persist(khachHang);
            em.flush();
        } else {
            khachHang = TestUtil.findAll(em, KhachHang.class).get(0);
        }
        khoanPhaiTra.setKhachHang(khachHang);
        // Add required entity
        MaKeToan maKeToan;
        if (TestUtil.findAll(em, MaKeToan.class).isEmpty()) {
            maKeToan = MaKeToanResourceIT.createUpdatedEntity(em);
            em.persist(maKeToan);
            em.flush();
        } else {
            maKeToan = TestUtil.findAll(em, MaKeToan.class).get(0);
        }
        khoanPhaiTra.setMaKeToan(maKeToan);
        return khoanPhaiTra;
    }

    @BeforeEach
    public void initTest() {
        khoanPhaiTra = createEntity(em);
    }

    @Test
    @Transactional
    void createKhoanPhaiTra() throws Exception {
        int databaseSizeBeforeCreate = khoanPhaiTraRepository.findAll().size();
        // Create the KhoanPhaiTra
        KhoanPhaiTraDTO khoanPhaiTraDTO = khoanPhaiTraMapper.toDto(khoanPhaiTra);
        restKhoanPhaiTraMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(khoanPhaiTraDTO))
            )
            .andExpect(status().isCreated());

        // Validate the KhoanPhaiTra in the database
        List<KhoanPhaiTra> khoanPhaiTraList = khoanPhaiTraRepository.findAll();
        assertThat(khoanPhaiTraList).hasSize(databaseSizeBeforeCreate + 1);
        KhoanPhaiTra testKhoanPhaiTra = khoanPhaiTraList.get(khoanPhaiTraList.size() - 1);
        assertThat(testKhoanPhaiTra.getNgayTao()).isEqualTo(DEFAULT_NGAY_TAO);
        assertThat(testKhoanPhaiTra.getNgayPhaiTra()).isEqualTo(DEFAULT_NGAY_PHAI_TRA);
        assertThat(testKhoanPhaiTra.getSoTien()).isEqualTo(DEFAULT_SO_TIEN);
        assertThat(testKhoanPhaiTra.getGhiChu()).isEqualTo(DEFAULT_GHI_CHU);
    }

    @Test
    @Transactional
    void createKhoanPhaiTraWithExistingId() throws Exception {
        // Create the KhoanPhaiTra with an existing ID
        khoanPhaiTra.setId(1L);
        KhoanPhaiTraDTO khoanPhaiTraDTO = khoanPhaiTraMapper.toDto(khoanPhaiTra);

        int databaseSizeBeforeCreate = khoanPhaiTraRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restKhoanPhaiTraMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(khoanPhaiTraDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the KhoanPhaiTra in the database
        List<KhoanPhaiTra> khoanPhaiTraList = khoanPhaiTraRepository.findAll();
        assertThat(khoanPhaiTraList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void checkNgayTaoIsRequired() throws Exception {
        int databaseSizeBeforeTest = khoanPhaiTraRepository.findAll().size();
        // set the field null
        khoanPhaiTra.setNgayTao(null);

        // Create the KhoanPhaiTra, which fails.
        KhoanPhaiTraDTO khoanPhaiTraDTO = khoanPhaiTraMapper.toDto(khoanPhaiTra);

        restKhoanPhaiTraMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(khoanPhaiTraDTO))
            )
            .andExpect(status().isBadRequest());

        List<KhoanPhaiTra> khoanPhaiTraList = khoanPhaiTraRepository.findAll();
        assertThat(khoanPhaiTraList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkNgayPhaiTraIsRequired() throws Exception {
        int databaseSizeBeforeTest = khoanPhaiTraRepository.findAll().size();
        // set the field null
        khoanPhaiTra.setNgayPhaiTra(null);

        // Create the KhoanPhaiTra, which fails.
        KhoanPhaiTraDTO khoanPhaiTraDTO = khoanPhaiTraMapper.toDto(khoanPhaiTra);

        restKhoanPhaiTraMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(khoanPhaiTraDTO))
            )
            .andExpect(status().isBadRequest());

        List<KhoanPhaiTra> khoanPhaiTraList = khoanPhaiTraRepository.findAll();
        assertThat(khoanPhaiTraList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkSoTienIsRequired() throws Exception {
        int databaseSizeBeforeTest = khoanPhaiTraRepository.findAll().size();
        // set the field null
        khoanPhaiTra.setSoTien(null);

        // Create the KhoanPhaiTra, which fails.
        KhoanPhaiTraDTO khoanPhaiTraDTO = khoanPhaiTraMapper.toDto(khoanPhaiTra);

        restKhoanPhaiTraMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(khoanPhaiTraDTO))
            )
            .andExpect(status().isBadRequest());

        List<KhoanPhaiTra> khoanPhaiTraList = khoanPhaiTraRepository.findAll();
        assertThat(khoanPhaiTraList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void getAllKhoanPhaiTras() throws Exception {
        // Initialize the database
        khoanPhaiTraRepository.saveAndFlush(khoanPhaiTra);

        // Get all the khoanPhaiTraList
        restKhoanPhaiTraMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(khoanPhaiTra.getId().intValue())))
            .andExpect(jsonPath("$.[*].ngayTao").value(hasItem(DEFAULT_NGAY_TAO.toString())))
            .andExpect(jsonPath("$.[*].ngayPhaiTra").value(hasItem(DEFAULT_NGAY_PHAI_TRA.toString())))
            .andExpect(jsonPath("$.[*].soTien").value(hasItem(DEFAULT_SO_TIEN.doubleValue())))
            .andExpect(jsonPath("$.[*].ghiChu").value(hasItem(DEFAULT_GHI_CHU)));
    }

    @Test
    @Transactional
    void getKhoanPhaiTra() throws Exception {
        // Initialize the database
        khoanPhaiTraRepository.saveAndFlush(khoanPhaiTra);

        // Get the khoanPhaiTra
        restKhoanPhaiTraMockMvc
            .perform(get(ENTITY_API_URL_ID, khoanPhaiTra.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(khoanPhaiTra.getId().intValue()))
            .andExpect(jsonPath("$.ngayTao").value(DEFAULT_NGAY_TAO.toString()))
            .andExpect(jsonPath("$.ngayPhaiTra").value(DEFAULT_NGAY_PHAI_TRA.toString()))
            .andExpect(jsonPath("$.soTien").value(DEFAULT_SO_TIEN.doubleValue()))
            .andExpect(jsonPath("$.ghiChu").value(DEFAULT_GHI_CHU));
    }

    @Test
    @Transactional
    void getKhoanPhaiTrasByIdFiltering() throws Exception {
        // Initialize the database
        khoanPhaiTraRepository.saveAndFlush(khoanPhaiTra);

        Long id = khoanPhaiTra.getId();

        defaultKhoanPhaiTraShouldBeFound("id.equals=" + id);
        defaultKhoanPhaiTraShouldNotBeFound("id.notEquals=" + id);

        defaultKhoanPhaiTraShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultKhoanPhaiTraShouldNotBeFound("id.greaterThan=" + id);

        defaultKhoanPhaiTraShouldBeFound("id.lessThanOrEqual=" + id);
        defaultKhoanPhaiTraShouldNotBeFound("id.lessThan=" + id);
    }

    @Test
    @Transactional
    void getAllKhoanPhaiTrasByNgayTaoIsEqualToSomething() throws Exception {
        // Initialize the database
        khoanPhaiTraRepository.saveAndFlush(khoanPhaiTra);

        // Get all the khoanPhaiTraList where ngayTao equals to DEFAULT_NGAY_TAO
        defaultKhoanPhaiTraShouldBeFound("ngayTao.equals=" + DEFAULT_NGAY_TAO);

        // Get all the khoanPhaiTraList where ngayTao equals to UPDATED_NGAY_TAO
        defaultKhoanPhaiTraShouldNotBeFound("ngayTao.equals=" + UPDATED_NGAY_TAO);
    }

    @Test
    @Transactional
    void getAllKhoanPhaiTrasByNgayTaoIsNotEqualToSomething() throws Exception {
        // Initialize the database
        khoanPhaiTraRepository.saveAndFlush(khoanPhaiTra);

        // Get all the khoanPhaiTraList where ngayTao not equals to DEFAULT_NGAY_TAO
        defaultKhoanPhaiTraShouldNotBeFound("ngayTao.notEquals=" + DEFAULT_NGAY_TAO);

        // Get all the khoanPhaiTraList where ngayTao not equals to UPDATED_NGAY_TAO
        defaultKhoanPhaiTraShouldBeFound("ngayTao.notEquals=" + UPDATED_NGAY_TAO);
    }

    @Test
    @Transactional
    void getAllKhoanPhaiTrasByNgayTaoIsInShouldWork() throws Exception {
        // Initialize the database
        khoanPhaiTraRepository.saveAndFlush(khoanPhaiTra);

        // Get all the khoanPhaiTraList where ngayTao in DEFAULT_NGAY_TAO or UPDATED_NGAY_TAO
        defaultKhoanPhaiTraShouldBeFound("ngayTao.in=" + DEFAULT_NGAY_TAO + "," + UPDATED_NGAY_TAO);

        // Get all the khoanPhaiTraList where ngayTao equals to UPDATED_NGAY_TAO
        defaultKhoanPhaiTraShouldNotBeFound("ngayTao.in=" + UPDATED_NGAY_TAO);
    }

    @Test
    @Transactional
    void getAllKhoanPhaiTrasByNgayTaoIsNullOrNotNull() throws Exception {
        // Initialize the database
        khoanPhaiTraRepository.saveAndFlush(khoanPhaiTra);

        // Get all the khoanPhaiTraList where ngayTao is not null
        defaultKhoanPhaiTraShouldBeFound("ngayTao.specified=true");

        // Get all the khoanPhaiTraList where ngayTao is null
        defaultKhoanPhaiTraShouldNotBeFound("ngayTao.specified=false");
    }

    @Test
    @Transactional
    void getAllKhoanPhaiTrasByNgayPhaiTraIsEqualToSomething() throws Exception {
        // Initialize the database
        khoanPhaiTraRepository.saveAndFlush(khoanPhaiTra);

        // Get all the khoanPhaiTraList where ngayPhaiTra equals to DEFAULT_NGAY_PHAI_TRA
        defaultKhoanPhaiTraShouldBeFound("ngayPhaiTra.equals=" + DEFAULT_NGAY_PHAI_TRA);

        // Get all the khoanPhaiTraList where ngayPhaiTra equals to UPDATED_NGAY_PHAI_TRA
        defaultKhoanPhaiTraShouldNotBeFound("ngayPhaiTra.equals=" + UPDATED_NGAY_PHAI_TRA);
    }

    @Test
    @Transactional
    void getAllKhoanPhaiTrasByNgayPhaiTraIsNotEqualToSomething() throws Exception {
        // Initialize the database
        khoanPhaiTraRepository.saveAndFlush(khoanPhaiTra);

        // Get all the khoanPhaiTraList where ngayPhaiTra not equals to DEFAULT_NGAY_PHAI_TRA
        defaultKhoanPhaiTraShouldNotBeFound("ngayPhaiTra.notEquals=" + DEFAULT_NGAY_PHAI_TRA);

        // Get all the khoanPhaiTraList where ngayPhaiTra not equals to UPDATED_NGAY_PHAI_TRA
        defaultKhoanPhaiTraShouldBeFound("ngayPhaiTra.notEquals=" + UPDATED_NGAY_PHAI_TRA);
    }

    @Test
    @Transactional
    void getAllKhoanPhaiTrasByNgayPhaiTraIsInShouldWork() throws Exception {
        // Initialize the database
        khoanPhaiTraRepository.saveAndFlush(khoanPhaiTra);

        // Get all the khoanPhaiTraList where ngayPhaiTra in DEFAULT_NGAY_PHAI_TRA or UPDATED_NGAY_PHAI_TRA
        defaultKhoanPhaiTraShouldBeFound("ngayPhaiTra.in=" + DEFAULT_NGAY_PHAI_TRA + "," + UPDATED_NGAY_PHAI_TRA);

        // Get all the khoanPhaiTraList where ngayPhaiTra equals to UPDATED_NGAY_PHAI_TRA
        defaultKhoanPhaiTraShouldNotBeFound("ngayPhaiTra.in=" + UPDATED_NGAY_PHAI_TRA);
    }

    @Test
    @Transactional
    void getAllKhoanPhaiTrasByNgayPhaiTraIsNullOrNotNull() throws Exception {
        // Initialize the database
        khoanPhaiTraRepository.saveAndFlush(khoanPhaiTra);

        // Get all the khoanPhaiTraList where ngayPhaiTra is not null
        defaultKhoanPhaiTraShouldBeFound("ngayPhaiTra.specified=true");

        // Get all the khoanPhaiTraList where ngayPhaiTra is null
        defaultKhoanPhaiTraShouldNotBeFound("ngayPhaiTra.specified=false");
    }

    @Test
    @Transactional
    void getAllKhoanPhaiTrasBySoTienIsEqualToSomething() throws Exception {
        // Initialize the database
        khoanPhaiTraRepository.saveAndFlush(khoanPhaiTra);

        // Get all the khoanPhaiTraList where soTien equals to DEFAULT_SO_TIEN
        defaultKhoanPhaiTraShouldBeFound("soTien.equals=" + DEFAULT_SO_TIEN);

        // Get all the khoanPhaiTraList where soTien equals to UPDATED_SO_TIEN
        defaultKhoanPhaiTraShouldNotBeFound("soTien.equals=" + UPDATED_SO_TIEN);
    }

    @Test
    @Transactional
    void getAllKhoanPhaiTrasBySoTienIsNotEqualToSomething() throws Exception {
        // Initialize the database
        khoanPhaiTraRepository.saveAndFlush(khoanPhaiTra);

        // Get all the khoanPhaiTraList where soTien not equals to DEFAULT_SO_TIEN
        defaultKhoanPhaiTraShouldNotBeFound("soTien.notEquals=" + DEFAULT_SO_TIEN);

        // Get all the khoanPhaiTraList where soTien not equals to UPDATED_SO_TIEN
        defaultKhoanPhaiTraShouldBeFound("soTien.notEquals=" + UPDATED_SO_TIEN);
    }

    @Test
    @Transactional
    void getAllKhoanPhaiTrasBySoTienIsInShouldWork() throws Exception {
        // Initialize the database
        khoanPhaiTraRepository.saveAndFlush(khoanPhaiTra);

        // Get all the khoanPhaiTraList where soTien in DEFAULT_SO_TIEN or UPDATED_SO_TIEN
        defaultKhoanPhaiTraShouldBeFound("soTien.in=" + DEFAULT_SO_TIEN + "," + UPDATED_SO_TIEN);

        // Get all the khoanPhaiTraList where soTien equals to UPDATED_SO_TIEN
        defaultKhoanPhaiTraShouldNotBeFound("soTien.in=" + UPDATED_SO_TIEN);
    }

    @Test
    @Transactional
    void getAllKhoanPhaiTrasBySoTienIsNullOrNotNull() throws Exception {
        // Initialize the database
        khoanPhaiTraRepository.saveAndFlush(khoanPhaiTra);

        // Get all the khoanPhaiTraList where soTien is not null
        defaultKhoanPhaiTraShouldBeFound("soTien.specified=true");

        // Get all the khoanPhaiTraList where soTien is null
        defaultKhoanPhaiTraShouldNotBeFound("soTien.specified=false");
    }

    @Test
    @Transactional
    void getAllKhoanPhaiTrasBySoTienIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        khoanPhaiTraRepository.saveAndFlush(khoanPhaiTra);

        // Get all the khoanPhaiTraList where soTien is greater than or equal to DEFAULT_SO_TIEN
        defaultKhoanPhaiTraShouldBeFound("soTien.greaterThanOrEqual=" + DEFAULT_SO_TIEN);

        // Get all the khoanPhaiTraList where soTien is greater than or equal to UPDATED_SO_TIEN
        defaultKhoanPhaiTraShouldNotBeFound("soTien.greaterThanOrEqual=" + UPDATED_SO_TIEN);
    }

    @Test
    @Transactional
    void getAllKhoanPhaiTrasBySoTienIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        khoanPhaiTraRepository.saveAndFlush(khoanPhaiTra);

        // Get all the khoanPhaiTraList where soTien is less than or equal to DEFAULT_SO_TIEN
        defaultKhoanPhaiTraShouldBeFound("soTien.lessThanOrEqual=" + DEFAULT_SO_TIEN);

        // Get all the khoanPhaiTraList where soTien is less than or equal to SMALLER_SO_TIEN
        defaultKhoanPhaiTraShouldNotBeFound("soTien.lessThanOrEqual=" + SMALLER_SO_TIEN);
    }

    @Test
    @Transactional
    void getAllKhoanPhaiTrasBySoTienIsLessThanSomething() throws Exception {
        // Initialize the database
        khoanPhaiTraRepository.saveAndFlush(khoanPhaiTra);

        // Get all the khoanPhaiTraList where soTien is less than DEFAULT_SO_TIEN
        defaultKhoanPhaiTraShouldNotBeFound("soTien.lessThan=" + DEFAULT_SO_TIEN);

        // Get all the khoanPhaiTraList where soTien is less than UPDATED_SO_TIEN
        defaultKhoanPhaiTraShouldBeFound("soTien.lessThan=" + UPDATED_SO_TIEN);
    }

    @Test
    @Transactional
    void getAllKhoanPhaiTrasBySoTienIsGreaterThanSomething() throws Exception {
        // Initialize the database
        khoanPhaiTraRepository.saveAndFlush(khoanPhaiTra);

        // Get all the khoanPhaiTraList where soTien is greater than DEFAULT_SO_TIEN
        defaultKhoanPhaiTraShouldNotBeFound("soTien.greaterThan=" + DEFAULT_SO_TIEN);

        // Get all the khoanPhaiTraList where soTien is greater than SMALLER_SO_TIEN
        defaultKhoanPhaiTraShouldBeFound("soTien.greaterThan=" + SMALLER_SO_TIEN);
    }

    @Test
    @Transactional
    void getAllKhoanPhaiTrasByGhiChuIsEqualToSomething() throws Exception {
        // Initialize the database
        khoanPhaiTraRepository.saveAndFlush(khoanPhaiTra);

        // Get all the khoanPhaiTraList where ghiChu equals to DEFAULT_GHI_CHU
        defaultKhoanPhaiTraShouldBeFound("ghiChu.equals=" + DEFAULT_GHI_CHU);

        // Get all the khoanPhaiTraList where ghiChu equals to UPDATED_GHI_CHU
        defaultKhoanPhaiTraShouldNotBeFound("ghiChu.equals=" + UPDATED_GHI_CHU);
    }

    @Test
    @Transactional
    void getAllKhoanPhaiTrasByGhiChuIsNotEqualToSomething() throws Exception {
        // Initialize the database
        khoanPhaiTraRepository.saveAndFlush(khoanPhaiTra);

        // Get all the khoanPhaiTraList where ghiChu not equals to DEFAULT_GHI_CHU
        defaultKhoanPhaiTraShouldNotBeFound("ghiChu.notEquals=" + DEFAULT_GHI_CHU);

        // Get all the khoanPhaiTraList where ghiChu not equals to UPDATED_GHI_CHU
        defaultKhoanPhaiTraShouldBeFound("ghiChu.notEquals=" + UPDATED_GHI_CHU);
    }

    @Test
    @Transactional
    void getAllKhoanPhaiTrasByGhiChuIsInShouldWork() throws Exception {
        // Initialize the database
        khoanPhaiTraRepository.saveAndFlush(khoanPhaiTra);

        // Get all the khoanPhaiTraList where ghiChu in DEFAULT_GHI_CHU or UPDATED_GHI_CHU
        defaultKhoanPhaiTraShouldBeFound("ghiChu.in=" + DEFAULT_GHI_CHU + "," + UPDATED_GHI_CHU);

        // Get all the khoanPhaiTraList where ghiChu equals to UPDATED_GHI_CHU
        defaultKhoanPhaiTraShouldNotBeFound("ghiChu.in=" + UPDATED_GHI_CHU);
    }

    @Test
    @Transactional
    void getAllKhoanPhaiTrasByGhiChuIsNullOrNotNull() throws Exception {
        // Initialize the database
        khoanPhaiTraRepository.saveAndFlush(khoanPhaiTra);

        // Get all the khoanPhaiTraList where ghiChu is not null
        defaultKhoanPhaiTraShouldBeFound("ghiChu.specified=true");

        // Get all the khoanPhaiTraList where ghiChu is null
        defaultKhoanPhaiTraShouldNotBeFound("ghiChu.specified=false");
    }

    @Test
    @Transactional
    void getAllKhoanPhaiTrasByGhiChuContainsSomething() throws Exception {
        // Initialize the database
        khoanPhaiTraRepository.saveAndFlush(khoanPhaiTra);

        // Get all the khoanPhaiTraList where ghiChu contains DEFAULT_GHI_CHU
        defaultKhoanPhaiTraShouldBeFound("ghiChu.contains=" + DEFAULT_GHI_CHU);

        // Get all the khoanPhaiTraList where ghiChu contains UPDATED_GHI_CHU
        defaultKhoanPhaiTraShouldNotBeFound("ghiChu.contains=" + UPDATED_GHI_CHU);
    }

    @Test
    @Transactional
    void getAllKhoanPhaiTrasByGhiChuNotContainsSomething() throws Exception {
        // Initialize the database
        khoanPhaiTraRepository.saveAndFlush(khoanPhaiTra);

        // Get all the khoanPhaiTraList where ghiChu does not contain DEFAULT_GHI_CHU
        defaultKhoanPhaiTraShouldNotBeFound("ghiChu.doesNotContain=" + DEFAULT_GHI_CHU);

        // Get all the khoanPhaiTraList where ghiChu does not contain UPDATED_GHI_CHU
        defaultKhoanPhaiTraShouldBeFound("ghiChu.doesNotContain=" + UPDATED_GHI_CHU);
    }

    @Test
    @Transactional
    void getAllKhoanPhaiTrasByKhachHangIsEqualToSomething() throws Exception {
        // Initialize the database
        khoanPhaiTraRepository.saveAndFlush(khoanPhaiTra);
        KhachHang khachHang = KhachHangResourceIT.createEntity(em);
        em.persist(khachHang);
        em.flush();
        khoanPhaiTra.setKhachHang(khachHang);
        khoanPhaiTraRepository.saveAndFlush(khoanPhaiTra);
        Long khachHangId = khachHang.getId();

        // Get all the khoanPhaiTraList where khachHang equals to khachHangId
        defaultKhoanPhaiTraShouldBeFound("khachHangId.equals=" + khachHangId);

        // Get all the khoanPhaiTraList where khachHang equals to (khachHangId + 1)
        defaultKhoanPhaiTraShouldNotBeFound("khachHangId.equals=" + (khachHangId + 1));
    }

    @Test
    @Transactional
    void getAllKhoanPhaiTrasByMaKeToanIsEqualToSomething() throws Exception {
        // Initialize the database
        khoanPhaiTraRepository.saveAndFlush(khoanPhaiTra);
        MaKeToan maKeToan = MaKeToanResourceIT.createEntity(em);
        maKeToan.setSoHieu(3D);
        maKeToan = em.merge(maKeToan);
        em.flush();
        khoanPhaiTra.setMaKeToan(maKeToan);
        khoanPhaiTraRepository.saveAndFlush(khoanPhaiTra);
        Long maKeToanId = maKeToan.getId();

        // Get all the khoanPhaiTraList where maKeToan equals to maKeToanId
        defaultKhoanPhaiTraShouldBeFound("maKeToanId.equals=" + maKeToanId);

        // Get all the khoanPhaiTraList where maKeToan equals to (maKeToanId + 1)
        defaultKhoanPhaiTraShouldNotBeFound("maKeToanId.equals=" + (maKeToanId + 1));
    }

    @Test
    @Transactional
    void getAllKhoanPhaiTrasByDsPhieuChiIsEqualToSomething() throws Exception {
        // Initialize the database
        khoanPhaiTraRepository.saveAndFlush(khoanPhaiTra);
        PhieuChi dsPhieuChi = PhieuChiResourceIT.createEntity(em);
        em.persist(dsPhieuChi);
        em.flush();
        khoanPhaiTra.addDsPhieuChi(dsPhieuChi);
        khoanPhaiTraRepository.saveAndFlush(khoanPhaiTra);
        Long dsPhieuChiId = dsPhieuChi.getId();

        // Get all the khoanPhaiTraList where dsPhieuChi equals to dsPhieuChiId
        defaultKhoanPhaiTraShouldBeFound("dsPhieuChiId.equals=" + dsPhieuChiId);

        // Get all the khoanPhaiTraList where dsPhieuChi equals to (dsPhieuChiId + 1)
        defaultKhoanPhaiTraShouldNotBeFound("dsPhieuChiId.equals=" + (dsPhieuChiId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultKhoanPhaiTraShouldBeFound(String filter) throws Exception {
        restKhoanPhaiTraMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(khoanPhaiTra.getId().intValue())))
            .andExpect(jsonPath("$.[*].ngayTao").value(hasItem(DEFAULT_NGAY_TAO.toString())))
            .andExpect(jsonPath("$.[*].ngayPhaiTra").value(hasItem(DEFAULT_NGAY_PHAI_TRA.toString())))
            .andExpect(jsonPath("$.[*].soTien").value(hasItem(DEFAULT_SO_TIEN.doubleValue())))
            .andExpect(jsonPath("$.[*].ghiChu").value(hasItem(DEFAULT_GHI_CHU)));

        // Check, that the count call also returns 1
        restKhoanPhaiTraMockMvc
            .perform(get(ENTITY_API_URL + "/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultKhoanPhaiTraShouldNotBeFound(String filter) throws Exception {
        restKhoanPhaiTraMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restKhoanPhaiTraMockMvc
            .perform(get(ENTITY_API_URL + "/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }

    @Test
    @Transactional
    void getNonExistingKhoanPhaiTra() throws Exception {
        // Get the khoanPhaiTra
        restKhoanPhaiTraMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putNewKhoanPhaiTra() throws Exception {
        // Initialize the database
        khoanPhaiTraRepository.saveAndFlush(khoanPhaiTra);

        int databaseSizeBeforeUpdate = khoanPhaiTraRepository.findAll().size();

        // Update the khoanPhaiTra
        KhoanPhaiTra updatedKhoanPhaiTra = khoanPhaiTraRepository.findById(khoanPhaiTra.getId()).get();
        // Disconnect from session so that the updates on updatedKhoanPhaiTra are not directly saved in db
        em.detach(updatedKhoanPhaiTra);
        updatedKhoanPhaiTra.ngayTao(UPDATED_NGAY_TAO).ngayPhaiTra(UPDATED_NGAY_PHAI_TRA).soTien(UPDATED_SO_TIEN).ghiChu(UPDATED_GHI_CHU);
        KhoanPhaiTraDTO khoanPhaiTraDTO = khoanPhaiTraMapper.toDto(updatedKhoanPhaiTra);

        restKhoanPhaiTraMockMvc
            .perform(
                put(ENTITY_API_URL_ID, khoanPhaiTraDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(khoanPhaiTraDTO))
            )
            .andExpect(status().isOk());

        // Validate the KhoanPhaiTra in the database
        List<KhoanPhaiTra> khoanPhaiTraList = khoanPhaiTraRepository.findAll();
        assertThat(khoanPhaiTraList).hasSize(databaseSizeBeforeUpdate);
        KhoanPhaiTra testKhoanPhaiTra = khoanPhaiTraList.get(khoanPhaiTraList.size() - 1);
        assertThat(testKhoanPhaiTra.getNgayTao()).isEqualTo(UPDATED_NGAY_TAO);
        assertThat(testKhoanPhaiTra.getNgayPhaiTra()).isEqualTo(UPDATED_NGAY_PHAI_TRA);
        assertThat(testKhoanPhaiTra.getSoTien()).isEqualTo(UPDATED_SO_TIEN);
        assertThat(testKhoanPhaiTra.getGhiChu()).isEqualTo(UPDATED_GHI_CHU);
    }

    @Test
    @Transactional
    void putNonExistingKhoanPhaiTra() throws Exception {
        int databaseSizeBeforeUpdate = khoanPhaiTraRepository.findAll().size();
        khoanPhaiTra.setId(count.incrementAndGet());

        // Create the KhoanPhaiTra
        KhoanPhaiTraDTO khoanPhaiTraDTO = khoanPhaiTraMapper.toDto(khoanPhaiTra);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restKhoanPhaiTraMockMvc
            .perform(
                put(ENTITY_API_URL_ID, khoanPhaiTraDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(khoanPhaiTraDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the KhoanPhaiTra in the database
        List<KhoanPhaiTra> khoanPhaiTraList = khoanPhaiTraRepository.findAll();
        assertThat(khoanPhaiTraList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchKhoanPhaiTra() throws Exception {
        int databaseSizeBeforeUpdate = khoanPhaiTraRepository.findAll().size();
        khoanPhaiTra.setId(count.incrementAndGet());

        // Create the KhoanPhaiTra
        KhoanPhaiTraDTO khoanPhaiTraDTO = khoanPhaiTraMapper.toDto(khoanPhaiTra);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restKhoanPhaiTraMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(khoanPhaiTraDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the KhoanPhaiTra in the database
        List<KhoanPhaiTra> khoanPhaiTraList = khoanPhaiTraRepository.findAll();
        assertThat(khoanPhaiTraList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamKhoanPhaiTra() throws Exception {
        int databaseSizeBeforeUpdate = khoanPhaiTraRepository.findAll().size();
        khoanPhaiTra.setId(count.incrementAndGet());

        // Create the KhoanPhaiTra
        KhoanPhaiTraDTO khoanPhaiTraDTO = khoanPhaiTraMapper.toDto(khoanPhaiTra);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restKhoanPhaiTraMockMvc
            .perform(
                put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(khoanPhaiTraDTO))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the KhoanPhaiTra in the database
        List<KhoanPhaiTra> khoanPhaiTraList = khoanPhaiTraRepository.findAll();
        assertThat(khoanPhaiTraList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateKhoanPhaiTraWithPatch() throws Exception {
        // Initialize the database
        khoanPhaiTraRepository.saveAndFlush(khoanPhaiTra);

        int databaseSizeBeforeUpdate = khoanPhaiTraRepository.findAll().size();

        // Update the khoanPhaiTra using partial update
        KhoanPhaiTra partialUpdatedKhoanPhaiTra = new KhoanPhaiTra();
        partialUpdatedKhoanPhaiTra.setId(khoanPhaiTra.getId());

        partialUpdatedKhoanPhaiTra.ghiChu(UPDATED_GHI_CHU);

        restKhoanPhaiTraMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedKhoanPhaiTra.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedKhoanPhaiTra))
            )
            .andExpect(status().isOk());

        // Validate the KhoanPhaiTra in the database
        List<KhoanPhaiTra> khoanPhaiTraList = khoanPhaiTraRepository.findAll();
        assertThat(khoanPhaiTraList).hasSize(databaseSizeBeforeUpdate);
        KhoanPhaiTra testKhoanPhaiTra = khoanPhaiTraList.get(khoanPhaiTraList.size() - 1);
        assertThat(testKhoanPhaiTra.getNgayTao()).isEqualTo(DEFAULT_NGAY_TAO);
        assertThat(testKhoanPhaiTra.getNgayPhaiTra()).isEqualTo(DEFAULT_NGAY_PHAI_TRA);
        assertThat(testKhoanPhaiTra.getSoTien()).isEqualTo(DEFAULT_SO_TIEN);
        assertThat(testKhoanPhaiTra.getGhiChu()).isEqualTo(UPDATED_GHI_CHU);
    }

    @Test
    @Transactional
    void fullUpdateKhoanPhaiTraWithPatch() throws Exception {
        // Initialize the database
        khoanPhaiTraRepository.saveAndFlush(khoanPhaiTra);

        int databaseSizeBeforeUpdate = khoanPhaiTraRepository.findAll().size();

        // Update the khoanPhaiTra using partial update
        KhoanPhaiTra partialUpdatedKhoanPhaiTra = new KhoanPhaiTra();
        partialUpdatedKhoanPhaiTra.setId(khoanPhaiTra.getId());

        partialUpdatedKhoanPhaiTra
            .ngayTao(UPDATED_NGAY_TAO)
            .ngayPhaiTra(UPDATED_NGAY_PHAI_TRA)
            .soTien(UPDATED_SO_TIEN)
            .ghiChu(UPDATED_GHI_CHU);

        restKhoanPhaiTraMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedKhoanPhaiTra.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedKhoanPhaiTra))
            )
            .andExpect(status().isOk());

        // Validate the KhoanPhaiTra in the database
        List<KhoanPhaiTra> khoanPhaiTraList = khoanPhaiTraRepository.findAll();
        assertThat(khoanPhaiTraList).hasSize(databaseSizeBeforeUpdate);
        KhoanPhaiTra testKhoanPhaiTra = khoanPhaiTraList.get(khoanPhaiTraList.size() - 1);
        assertThat(testKhoanPhaiTra.getNgayTao()).isEqualTo(UPDATED_NGAY_TAO);
        assertThat(testKhoanPhaiTra.getNgayPhaiTra()).isEqualTo(UPDATED_NGAY_PHAI_TRA);
        assertThat(testKhoanPhaiTra.getSoTien()).isEqualTo(UPDATED_SO_TIEN);
        assertThat(testKhoanPhaiTra.getGhiChu()).isEqualTo(UPDATED_GHI_CHU);
    }

    @Test
    @Transactional
    void patchNonExistingKhoanPhaiTra() throws Exception {
        int databaseSizeBeforeUpdate = khoanPhaiTraRepository.findAll().size();
        khoanPhaiTra.setId(count.incrementAndGet());

        // Create the KhoanPhaiTra
        KhoanPhaiTraDTO khoanPhaiTraDTO = khoanPhaiTraMapper.toDto(khoanPhaiTra);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restKhoanPhaiTraMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, khoanPhaiTraDTO.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(khoanPhaiTraDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the KhoanPhaiTra in the database
        List<KhoanPhaiTra> khoanPhaiTraList = khoanPhaiTraRepository.findAll();
        assertThat(khoanPhaiTraList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchKhoanPhaiTra() throws Exception {
        int databaseSizeBeforeUpdate = khoanPhaiTraRepository.findAll().size();
        khoanPhaiTra.setId(count.incrementAndGet());

        // Create the KhoanPhaiTra
        KhoanPhaiTraDTO khoanPhaiTraDTO = khoanPhaiTraMapper.toDto(khoanPhaiTra);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restKhoanPhaiTraMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(khoanPhaiTraDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the KhoanPhaiTra in the database
        List<KhoanPhaiTra> khoanPhaiTraList = khoanPhaiTraRepository.findAll();
        assertThat(khoanPhaiTraList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamKhoanPhaiTra() throws Exception {
        int databaseSizeBeforeUpdate = khoanPhaiTraRepository.findAll().size();
        khoanPhaiTra.setId(count.incrementAndGet());

        // Create the KhoanPhaiTra
        KhoanPhaiTraDTO khoanPhaiTraDTO = khoanPhaiTraMapper.toDto(khoanPhaiTra);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restKhoanPhaiTraMockMvc
            .perform(
                patch(ENTITY_API_URL)
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(khoanPhaiTraDTO))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the KhoanPhaiTra in the database
        List<KhoanPhaiTra> khoanPhaiTraList = khoanPhaiTraRepository.findAll();
        assertThat(khoanPhaiTraList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteKhoanPhaiTra() throws Exception {
        // Initialize the database
        khoanPhaiTraRepository.saveAndFlush(khoanPhaiTra);

        int databaseSizeBeforeDelete = khoanPhaiTraRepository.findAll().size();

        // Delete the khoanPhaiTra
        restKhoanPhaiTraMockMvc
            .perform(delete(ENTITY_API_URL_ID, khoanPhaiTra.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<KhoanPhaiTra> khoanPhaiTraList = khoanPhaiTraRepository.findAll();
        assertThat(khoanPhaiTraList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
