import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize } from 'rxjs/operators';

import { IMaKeToan, MaKeToan } from '../ma-ke-toan.model';
import { MaKeToanService } from '../service/ma-ke-toan.service';

@Component({
  selector: 'jhi-ma-ke-toan-update',
  templateUrl: './ma-ke-toan-update.component.html',
})
export class MaKeToanUpdateComponent implements OnInit {
  isSaving = false;

  editForm = this.fb.group({
    id: [],
    soHieu: [null, [Validators.required, Validators.min(1)]],
    ten: [null, [Validators.required]],
  });

  constructor(protected maKeToanService: MaKeToanService, protected activatedRoute: ActivatedRoute, protected fb: FormBuilder) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ maKeToan }) => {
      this.updateForm(maKeToan);
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const maKeToan = this.createFromForm();
    if (maKeToan.id !== undefined) {
      this.subscribeToSaveResponse(this.maKeToanService.update(maKeToan));
    } else {
      this.subscribeToSaveResponse(this.maKeToanService.create(maKeToan));
    }
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IMaKeToan>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(maKeToan: IMaKeToan): void {
    this.editForm.patchValue({
      id: maKeToan.id,
      soHieu: maKeToan.soHieu,
      ten: maKeToan.ten,
    });
  }

  protected createFromForm(): IMaKeToan {
    return {
      ...new MaKeToan(),
      id: this.editForm.get(['id'])!.value,
      soHieu: this.editForm.get(['soHieu'])!.value,
      ten: this.editForm.get(['ten'])!.value,
    };
  }
}
