package com.group3.finmansys.domain;

import java.io.Serializable;
import javax.persistence.*;
import javax.validation.constraints.*;

/**
 * A DichVu.
 */
@Entity
@Table(name = "dich_vu")
public class DichVu implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(name = "ten", nullable = false)
    private String ten;

    @NotNull
    @DecimalMin(value = "1")
    @Column(name = "gia", nullable = false)
    private Double gia;

    @Column(name = "mo_ta")
    private String moTa;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public DichVu id(Long id) {
        this.id = id;
        return this;
    }

    public String getTen() {
        return this.ten;
    }

    public DichVu ten(String ten) {
        this.ten = ten;
        return this;
    }

    public void setTen(String ten) {
        this.ten = ten;
    }

    public Double getGia() {
        return this.gia;
    }

    public DichVu gia(Double gia) {
        this.gia = gia;
        return this;
    }

    public void setGia(Double gia) {
        this.gia = gia;
    }

    public String getMoTa() {
        return this.moTa;
    }

    public DichVu moTa(String moTa) {
        this.moTa = moTa;
        return this;
    }

    public void setMoTa(String moTa) {
        this.moTa = moTa;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof DichVu)) {
            return false;
        }
        return id != null && id.equals(((DichVu) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "DichVu{" +
            "id=" + getId() +
            ", ten='" + getTen() + "'" +
            ", gia=" + getGia() +
            ", moTa='" + getMoTa() + "'" +
            "}";
    }
}
