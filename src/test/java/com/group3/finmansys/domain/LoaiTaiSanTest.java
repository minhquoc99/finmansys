package com.group3.finmansys.domain;

import static org.assertj.core.api.Assertions.assertThat;

import com.group3.finmansys.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class LoaiTaiSanTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(LoaiTaiSan.class);
        LoaiTaiSan loaiTaiSan1 = new LoaiTaiSan();
        loaiTaiSan1.setId(1L);
        LoaiTaiSan loaiTaiSan2 = new LoaiTaiSan();
        loaiTaiSan2.setId(loaiTaiSan1.getId());
        assertThat(loaiTaiSan1).isEqualTo(loaiTaiSan2);
        loaiTaiSan2.setId(2L);
        assertThat(loaiTaiSan1).isNotEqualTo(loaiTaiSan2);
        loaiTaiSan1.setId(null);
        assertThat(loaiTaiSan1).isNotEqualTo(loaiTaiSan2);
    }
}
