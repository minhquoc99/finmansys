package com.group3.finmansys.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import javax.persistence.*;
import javax.validation.constraints.*;

/**
 * A NganSachThang.
 */
@Entity
@Table(name = "ngan_sach_thang")
public class NganSachThang implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Min(value = 1)
    @Max(value = 12)
    @Column(name = "thang", nullable = false)
    private Integer thang;

    @NotNull
    @Column(name = "so_tien", nullable = false)
    private Double soTien;

    @ManyToOne(optional = false)
    @NotNull
    @JsonIgnoreProperties(value = { "khachSan", "nganSachThangs" }, allowSetters = true)
    private NganSachNam nganSachNam;

    @ManyToOne(optional = false)
    @NotNull
    private KhoanMucChiPhi khoanMucChiPhi;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public NganSachThang id(Long id) {
        this.id = id;
        return this;
    }

    public Integer getThang() {
        return this.thang;
    }

    public NganSachThang thang(Integer thang) {
        this.thang = thang;
        return this;
    }

    public void setThang(Integer thang) {
        this.thang = thang;
    }

    public Double getSoTien() {
        return this.soTien;
    }

    public NganSachThang soTien(Double soTien) {
        this.soTien = soTien;
        return this;
    }

    public void setSoTien(Double soTien) {
        this.soTien = soTien;
    }

    public NganSachNam getNganSachNam() {
        return this.nganSachNam;
    }

    public NganSachThang nganSachNam(NganSachNam nganSachNam) {
        this.setNganSachNam(nganSachNam);
        return this;
    }

    public void setNganSachNam(NganSachNam nganSachNam) {
        this.nganSachNam = nganSachNam;
    }

    public KhoanMucChiPhi getKhoanMucChiPhi() {
        return this.khoanMucChiPhi;
    }

    public NganSachThang khoanMucChiPhi(KhoanMucChiPhi khoanMucChiPhi) {
        this.setKhoanMucChiPhi(khoanMucChiPhi);
        return this;
    }

    public void setKhoanMucChiPhi(KhoanMucChiPhi khoanMucChiPhi) {
        this.khoanMucChiPhi = khoanMucChiPhi;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof NganSachThang)) {
            return false;
        }
        return id != null && id.equals(((NganSachThang) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "NganSachThang{" +
            "id=" + getId() +
            ", thang=" + getThang() +
            ", soTien=" + getSoTien() +
            "}";
    }
}
