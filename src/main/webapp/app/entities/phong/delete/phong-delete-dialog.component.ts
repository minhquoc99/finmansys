import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import { IPhong } from '../phong.model';
import { PhongService } from '../service/phong.service';

@Component({
  templateUrl: './phong-delete-dialog.component.html',
})
export class PhongDeleteDialogComponent {
  phong?: IPhong;

  constructor(protected phongService: PhongService, public activeModal: NgbActiveModal) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.phongService.delete(id).subscribe(() => {
      this.activeModal.close('deleted');
    });
  }
}
