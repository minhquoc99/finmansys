package com.group3.finmansys.service;

import com.group3.finmansys.service.dto.KhachHangDTO;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Service Interface for managing {@link com.group3.finmansys.domain.KhachHang}.
 */
public interface KhachHangService {
    /**
     * Save a khachHang.
     *
     * @param khachHangDTO the entity to save.
     * @return the persisted entity.
     */
    KhachHangDTO save(KhachHangDTO khachHangDTO);

    /**
     * Partially updates a khachHang.
     *
     * @param khachHangDTO the entity to update partially.
     * @return the persisted entity.
     */
    Optional<KhachHangDTO> partialUpdate(KhachHangDTO khachHangDTO);

    /**
     * Get all the khachHangs.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<KhachHangDTO> findAll(Pageable pageable);

    /**
     * Get the "id" khachHang.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<KhachHangDTO> findOne(Long id);

    /**
     * Delete the "id" khachHang.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
