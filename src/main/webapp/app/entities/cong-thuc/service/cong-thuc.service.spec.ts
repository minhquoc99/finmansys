import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { ICongThuc, CongThuc } from '../cong-thuc.model';

import { CongThucService } from './cong-thuc.service';

describe('Service Tests', () => {
  describe('CongThuc Service', () => {
    let service: CongThucService;
    let httpMock: HttpTestingController;
    let elemDefault: ICongThuc;
    let expectedResult: ICongThuc | ICongThuc[] | boolean | null;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule],
      });
      expectedResult = null;
      service = TestBed.inject(CongThucService);
      httpMock = TestBed.inject(HttpTestingController);

      elemDefault = {
        id: 0,
        maSo: 0,
        tenChiTieu: 'AAAAAAA',
        cap: 0,
        soDauKy: 0,
        soCuoiKy: 0,
      };
    });

    describe('Service methods', () => {
      it('should find an element', () => {
        const returnedFromService = Object.assign({}, elemDefault);

        service.find(123).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(elemDefault);
      });

      it('should create a CongThuc', () => {
        const returnedFromService = Object.assign(
          {
            id: 0,
          },
          elemDefault
        );

        const expected = Object.assign({}, returnedFromService);

        service.create(new CongThuc()).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'POST' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should update a CongThuc', () => {
        const returnedFromService = Object.assign(
          {
            id: 1,
            maSo: 1,
            tenChiTieu: 'BBBBBB',
            cap: 1,
            soDauKy: 1,
            soCuoiKy: 1,
          },
          elemDefault
        );

        const expected = Object.assign({}, returnedFromService);

        service.update(expected).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'PUT' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should partial update a CongThuc', () => {
        const patchObject = Object.assign(
          {
            maSo: 1,
            soDauKy: 1,
            soCuoiKy: 1,
          },
          new CongThuc()
        );

        const returnedFromService = Object.assign(patchObject, elemDefault);

        const expected = Object.assign({}, returnedFromService);

        service.partialUpdate(patchObject).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'PATCH' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should return a list of CongThuc', () => {
        const returnedFromService = Object.assign(
          {
            id: 1,
            maSo: 1,
            tenChiTieu: 'BBBBBB',
            cap: 1,
            soDauKy: 1,
            soCuoiKy: 1,
          },
          elemDefault
        );

        const expected = Object.assign({}, returnedFromService);

        service.query().subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush([returnedFromService]);
        httpMock.verify();
        expect(expectedResult).toContainEqual(expected);
      });

      it('should delete a CongThuc', () => {
        service.delete(123).subscribe(resp => (expectedResult = resp.ok));

        const req = httpMock.expectOne({ method: 'DELETE' });
        req.flush({ status: 200 });
        expect(expectedResult);
      });

      describe('addCongThucToCollectionIfMissing', () => {
        it('should add a CongThuc to an empty array', () => {
          const congThuc: ICongThuc = { id: 123 };
          expectedResult = service.addCongThucToCollectionIfMissing([], congThuc);
          expect(expectedResult).toHaveLength(1);
          expect(expectedResult).toContain(congThuc);
        });

        it('should not add a CongThuc to an array that contains it', () => {
          const congThuc: ICongThuc = { id: 123 };
          const congThucCollection: ICongThuc[] = [
            {
              ...congThuc,
            },
            { id: 456 },
          ];
          expectedResult = service.addCongThucToCollectionIfMissing(congThucCollection, congThuc);
          expect(expectedResult).toHaveLength(2);
        });

        it("should add a CongThuc to an array that doesn't contain it", () => {
          const congThuc: ICongThuc = { id: 123 };
          const congThucCollection: ICongThuc[] = [{ id: 456 }];
          expectedResult = service.addCongThucToCollectionIfMissing(congThucCollection, congThuc);
          expect(expectedResult).toHaveLength(2);
          expect(expectedResult).toContain(congThuc);
        });

        it('should add only unique CongThuc to an array', () => {
          const congThucArray: ICongThuc[] = [{ id: 123 }, { id: 456 }, { id: 87021 }];
          const congThucCollection: ICongThuc[] = [{ id: 123 }];
          expectedResult = service.addCongThucToCollectionIfMissing(congThucCollection, ...congThucArray);
          expect(expectedResult).toHaveLength(3);
        });

        it('should accept varargs', () => {
          const congThuc: ICongThuc = { id: 123 };
          const congThuc2: ICongThuc = { id: 456 };
          expectedResult = service.addCongThucToCollectionIfMissing([], congThuc, congThuc2);
          expect(expectedResult).toHaveLength(2);
          expect(expectedResult).toContain(congThuc);
          expect(expectedResult).toContain(congThuc2);
        });

        it('should accept null and undefined values', () => {
          const congThuc: ICongThuc = { id: 123 };
          expectedResult = service.addCongThucToCollectionIfMissing([], null, congThuc, undefined);
          expect(expectedResult).toHaveLength(1);
          expect(expectedResult).toContain(congThuc);
        });
      });
    });

    afterEach(() => {
      httpMock.verify();
    });
  });
});
