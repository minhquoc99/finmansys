import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

@NgModule({
  imports: [
    RouterModule.forChild([
      {
        path: 'khach-san',
        data: { pageTitle: 'finmansysApp.khachSan.home.title' },
        loadChildren: () => import('./khach-san/khach-san.module').then(m => m.KhachSanModule),
      },
      {
        path: 'loai-tai-san',
        data: { pageTitle: 'finmansysApp.loaiTaiSan.home.title' },
        loadChildren: () => import('./loai-tai-san/loai-tai-san.module').then(m => m.LoaiTaiSanModule),
      },
      {
        path: 'tai-san-cd',
        data: { pageTitle: 'finmansysApp.taiSanCD.home.title' },
        loadChildren: () => import('./tai-san-cd/tai-san-cd.module').then(m => m.TaiSanCDModule),
      },
      {
        path: 'chi-tiet-tang-tscd',
        data: { pageTitle: 'finmansysApp.chiTietTangTSCD.home.title' },
        loadChildren: () => import('./chi-tiet-tang-tscd/chi-tiet-tang-tscd.module').then(m => m.ChiTietTangTSCDModule),
      },
      {
        path: 'chi-tiet-giam-tscd',
        data: { pageTitle: 'finmansysApp.chiTietGiamTSCD.home.title' },
        loadChildren: () => import('./chi-tiet-giam-tscd/chi-tiet-giam-tscd.module').then(m => m.ChiTietGiamTSCDModule),
      },
      {
        path: 'loai-quy',
        data: { pageTitle: 'finmansysApp.loaiQuy.home.title' },
        loadChildren: () => import('./loai-quy/loai-quy.module').then(m => m.LoaiQuyModule),
      },
      {
        path: 'ma-ke-toan',
        data: { pageTitle: 'finmansysApp.maKeToan.home.title' },
        loadChildren: () => import('./ma-ke-toan/ma-ke-toan.module').then(m => m.MaKeToanModule),
      },
      {
        path: 'quy-tien',
        data: { pageTitle: 'finmansysApp.quyTien.home.title' },
        loadChildren: () => import('./quy-tien/quy-tien.module').then(m => m.QuyTienModule),
      },
      {
        path: 'khach-hang',
        data: { pageTitle: 'finmansysApp.khachHang.home.title' },
        loadChildren: () => import('./khach-hang/khach-hang.module').then(m => m.KhachHangModule),
      },
      {
        path: 'khoan-muc-chi-phi',
        data: { pageTitle: 'finmansysApp.khoanMucChiPhi.home.title' },
        loadChildren: () => import('./khoan-muc-chi-phi/khoan-muc-chi-phi.module').then(m => m.KhoanMucChiPhiModule),
      },
      {
        path: 'phieu-chi',
        data: { pageTitle: 'finmansysApp.phieuChi.home.title' },
        loadChildren: () => import('./phieu-chi/phieu-chi.module').then(m => m.PhieuChiModule),
      },
      {
        path: 'khoan-phai-tra',
        data: { pageTitle: 'finmansysApp.khoanPhaiTra.home.title' },
        loadChildren: () => import('./khoan-phai-tra/khoan-phai-tra.module').then(m => m.KhoanPhaiTraModule),
      },
      {
        path: 'ngan-sach-nam',
        data: { pageTitle: 'finmansysApp.nganSachNam.home.title' },
        loadChildren: () => import('./ngan-sach-nam/ngan-sach-nam.module').then(m => m.NganSachNamModule),
      },
      {
        path: 'ngan-sach-thang',
        data: { pageTitle: 'finmansysApp.nganSachThang.home.title' },
        loadChildren: () => import('./ngan-sach-thang/ngan-sach-thang.module').then(m => m.NganSachThangModule),
      },
      {
        path: 'phong',
        data: { pageTitle: 'finmansysApp.phong.home.title' },
        loadChildren: () => import('./phong/phong.module').then(m => m.PhongModule),
      },
      {
        path: 'phong-duoc-dat',
        data: { pageTitle: 'finmansysApp.phongDuocDat.home.title' },
        loadChildren: () => import('./phong-duoc-dat/phong-duoc-dat.module').then(m => m.PhongDuocDatModule),
      },
      {
        path: 'phong-dat',
        data: { pageTitle: 'finmansysApp.phongDat.home.title' },
        loadChildren: () => import('./phong-dat/phong-dat.module').then(m => m.PhongDatModule),
      },
      {
        path: 'dich-vu',
        data: { pageTitle: 'finmansysApp.dichVu.home.title' },
        loadChildren: () => import('./dich-vu/dich-vu.module').then(m => m.DichVuModule),
      },
      {
        path: 'dich-vu-su-dung',
        data: { pageTitle: 'finmansysApp.dichVuSuDung.home.title' },
        loadChildren: () => import('./dich-vu-su-dung/dich-vu-su-dung.module').then(m => m.DichVuSuDungModule),
      },
      {
        path: 'phieu-thu',
        data: { pageTitle: 'finmansysApp.phieuThu.home.title' },
        loadChildren: () => import('./phieu-thu/phieu-thu.module').then(m => m.PhieuThuModule),
      },
      {
        path: 'khoan-phai-thu',
        data: { pageTitle: 'finmansysApp.khoanPhaiThu.home.title' },
        loadChildren: () => import('./khoan-phai-thu/khoan-phai-thu.module').then(m => m.KhoanPhaiThuModule),
      },
      {
        path: 'xem-bao-cao',
        data: { pageTitle: 'global.menu.xemBaoCao.main' },
        loadChildren: () => import('./xem-bao-cao/xem-bao-cao.module').then(m => m.XemBaoCaoModule),
      },
      {
        path: 'cong-thuc',
        data: { pageTitle: 'finmansysApp.congThuc.home.title' },
        loadChildren: () => import('./cong-thuc/cong-thuc.module').then(m => m.CongThucModule),
      },
      {
        path: 'toan-tu',
        data: { pageTitle: 'finmansysApp.toanTu.home.title' },
        loadChildren: () => import('./toan-tu/toan-tu.module').then(m => m.ToanTuModule),
      },
      /* jhipster-needle-add-entity-route - JHipster will add entity modules routes here */
    ]),
  ],
})
export class EntityRoutingModule {}
