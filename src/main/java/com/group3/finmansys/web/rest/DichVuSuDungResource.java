package com.group3.finmansys.web.rest;

import com.group3.finmansys.repository.DichVuSuDungRepository;
import com.group3.finmansys.service.DichVuSuDungQueryService;
import com.group3.finmansys.service.DichVuSuDungService;
import com.group3.finmansys.service.criteria.DichVuSuDungCriteria;
import com.group3.finmansys.service.dto.DichVuSuDungDTO;
import com.group3.finmansys.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.PaginationUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link com.group3.finmansys.domain.DichVuSuDung}.
 */
@RestController
@RequestMapping("/api")
public class DichVuSuDungResource {

    private final Logger log = LoggerFactory.getLogger(DichVuSuDungResource.class);

    private static final String ENTITY_NAME = "dichVuSuDung";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final DichVuSuDungService dichVuSuDungService;

    private final DichVuSuDungRepository dichVuSuDungRepository;

    private final DichVuSuDungQueryService dichVuSuDungQueryService;

    public DichVuSuDungResource(
        DichVuSuDungService dichVuSuDungService,
        DichVuSuDungRepository dichVuSuDungRepository,
        DichVuSuDungQueryService dichVuSuDungQueryService
    ) {
        this.dichVuSuDungService = dichVuSuDungService;
        this.dichVuSuDungRepository = dichVuSuDungRepository;
        this.dichVuSuDungQueryService = dichVuSuDungQueryService;
    }

    /**
     * {@code POST  /dich-vu-su-dungs} : Create a new dichVuSuDung.
     *
     * @param dichVuSuDungDTO the dichVuSuDungDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new dichVuSuDungDTO, or with status {@code 400 (Bad Request)} if the dichVuSuDung has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/dich-vu-su-dungs")
    public ResponseEntity<DichVuSuDungDTO> createDichVuSuDung(@Valid @RequestBody DichVuSuDungDTO dichVuSuDungDTO)
        throws URISyntaxException {
        log.debug("REST request to save DichVuSuDung : {}", dichVuSuDungDTO);
        if (dichVuSuDungDTO.getId() != null) {
            throw new BadRequestAlertException("A new dichVuSuDung cannot already have an ID", ENTITY_NAME, "idexists");
        }
        DichVuSuDungDTO result = dichVuSuDungService.save(dichVuSuDungDTO);
        return ResponseEntity
            .created(new URI("/api/dich-vu-su-dungs/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /dich-vu-su-dungs/:id} : Updates an existing dichVuSuDung.
     *
     * @param id the id of the dichVuSuDungDTO to save.
     * @param dichVuSuDungDTO the dichVuSuDungDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated dichVuSuDungDTO,
     * or with status {@code 400 (Bad Request)} if the dichVuSuDungDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the dichVuSuDungDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/dich-vu-su-dungs/{id}")
    public ResponseEntity<DichVuSuDungDTO> updateDichVuSuDung(
        @PathVariable(value = "id", required = false) final Long id,
        @Valid @RequestBody DichVuSuDungDTO dichVuSuDungDTO
    ) throws URISyntaxException {
        log.debug("REST request to update DichVuSuDung : {}, {}", id, dichVuSuDungDTO);
        if (dichVuSuDungDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, dichVuSuDungDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!dichVuSuDungRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        DichVuSuDungDTO result = dichVuSuDungService.save(dichVuSuDungDTO);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, dichVuSuDungDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /dich-vu-su-dungs/:id} : Partial updates given fields of an existing dichVuSuDung, field will ignore if it is null
     *
     * @param id the id of the dichVuSuDungDTO to save.
     * @param dichVuSuDungDTO the dichVuSuDungDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated dichVuSuDungDTO,
     * or with status {@code 400 (Bad Request)} if the dichVuSuDungDTO is not valid,
     * or with status {@code 404 (Not Found)} if the dichVuSuDungDTO is not found,
     * or with status {@code 500 (Internal Server Error)} if the dichVuSuDungDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/dich-vu-su-dungs/{id}", consumes = "application/merge-patch+json")
    public ResponseEntity<DichVuSuDungDTO> partialUpdateDichVuSuDung(
        @PathVariable(value = "id", required = false) final Long id,
        @NotNull @RequestBody DichVuSuDungDTO dichVuSuDungDTO
    ) throws URISyntaxException {
        log.debug("REST request to partial update DichVuSuDung partially : {}, {}", id, dichVuSuDungDTO);
        if (dichVuSuDungDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, dichVuSuDungDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!dichVuSuDungRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<DichVuSuDungDTO> result = dichVuSuDungService.partialUpdate(dichVuSuDungDTO);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, dichVuSuDungDTO.getId().toString())
        );
    }

    /**
     * {@code GET  /dich-vu-su-dungs} : get all the dichVuSuDungs.
     *
     * @param pageable the pagination information.
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of dichVuSuDungs in body.
     */
    @GetMapping("/dich-vu-su-dungs")
    public ResponseEntity<List<DichVuSuDungDTO>> getAllDichVuSuDungs(DichVuSuDungCriteria criteria, Pageable pageable) {
        log.debug("REST request to get DichVuSuDungs by criteria: {}", criteria);
        Page<DichVuSuDungDTO> page = dichVuSuDungQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /dich-vu-su-dungs/count} : count all the dichVuSuDungs.
     *
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
     */
    @GetMapping("/dich-vu-su-dungs/count")
    public ResponseEntity<Long> countDichVuSuDungs(DichVuSuDungCriteria criteria) {
        log.debug("REST request to count DichVuSuDungs by criteria: {}", criteria);
        return ResponseEntity.ok().body(dichVuSuDungQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /dich-vu-su-dungs/:id} : get the "id" dichVuSuDung.
     *
     * @param id the id of the dichVuSuDungDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the dichVuSuDungDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/dich-vu-su-dungs/{id}")
    public ResponseEntity<DichVuSuDungDTO> getDichVuSuDung(@PathVariable Long id) {
        log.debug("REST request to get DichVuSuDung : {}", id);
        Optional<DichVuSuDungDTO> dichVuSuDungDTO = dichVuSuDungService.findOne(id);
        return ResponseUtil.wrapOrNotFound(dichVuSuDungDTO);
    }

    /**
     * {@code DELETE  /dich-vu-su-dungs/:id} : delete the "id" dichVuSuDung.
     *
     * @param id the id of the dichVuSuDungDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/dich-vu-su-dungs/{id}")
    public ResponseEntity<Void> deleteDichVuSuDung(@PathVariable Long id) {
        log.debug("REST request to delete DichVuSuDung : {}", id);
        dichVuSuDungService.delete(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
            .build();
    }
}
