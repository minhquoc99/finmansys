import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { UserRouteAccessService } from 'app/core/auth/user-route-access.service';
import { PhongDatComponent } from '../list/phong-dat.component';
import { PhongDatDetailComponent } from '../detail/phong-dat-detail.component';
import { PhongDatUpdateComponent } from '../update/phong-dat-update.component';
import { PhongDatRoutingResolveService } from './phong-dat-routing-resolve.service';

const phongDatRoute: Routes = [
  {
    path: '',
    component: PhongDatComponent,
    data: {
      defaultSort: 'id,asc',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: PhongDatDetailComponent,
    resolve: {
      phongDat: PhongDatRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: PhongDatUpdateComponent,
    resolve: {
      phongDat: PhongDatRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: PhongDatUpdateComponent,
    resolve: {
      phongDat: PhongDatRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
];

@NgModule({
  imports: [RouterModule.forChild(phongDatRoute)],
  exports: [RouterModule],
})
export class PhongDatRoutingModule {}
