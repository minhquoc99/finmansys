import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize } from 'rxjs/operators';

import { IDichVu, DichVu } from '../dich-vu.model';
import { DichVuService } from '../service/dich-vu.service';

@Component({
  selector: 'jhi-dich-vu-update',
  templateUrl: './dich-vu-update.component.html',
})
export class DichVuUpdateComponent implements OnInit {
  isSaving = false;

  editForm = this.fb.group({
    id: [],
    ten: [null, [Validators.required]],
    gia: [null, [Validators.required, Validators.min(1)]],
    moTa: [],
  });

  constructor(protected dichVuService: DichVuService, protected activatedRoute: ActivatedRoute, protected fb: FormBuilder) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ dichVu }) => {
      this.updateForm(dichVu);
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const dichVu = this.createFromForm();
    if (dichVu.id !== undefined) {
      this.subscribeToSaveResponse(this.dichVuService.update(dichVu));
    } else {
      this.subscribeToSaveResponse(this.dichVuService.create(dichVu));
    }
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IDichVu>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(dichVu: IDichVu): void {
    this.editForm.patchValue({
      id: dichVu.id,
      ten: dichVu.ten,
      gia: dichVu.gia,
      moTa: dichVu.moTa,
    });
  }

  protected createFromForm(): IDichVu {
    return {
      ...new DichVu(),
      id: this.editForm.get(['id'])!.value,
      ten: this.editForm.get(['ten'])!.value,
      gia: this.editForm.get(['gia'])!.value,
      moTa: this.editForm.get(['moTa'])!.value,
    };
  }
}
