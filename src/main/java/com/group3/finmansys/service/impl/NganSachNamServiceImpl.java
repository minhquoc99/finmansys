package com.group3.finmansys.service.impl;

import com.group3.finmansys.domain.KhoanMucChiPhi;
import com.group3.finmansys.domain.NganSachNam;
import com.group3.finmansys.domain.NganSachThang;
import com.group3.finmansys.repository.KhoanMucChiPhiRepository;
import com.group3.finmansys.repository.NganSachNamRepository;
import com.group3.finmansys.repository.NganSachThangRepository;
import com.group3.finmansys.repository.PhieuChiRepository;
import com.group3.finmansys.service.NganSachNamService;
import com.group3.finmansys.service.NganSachThangService;
import com.group3.finmansys.service.dto.NganSachNamDTO;
import com.group3.finmansys.service.dto.NganSachThangDTO;
import com.group3.finmansys.service.mapper.NganSachNamMapper;
import com.group3.finmansys.service.mapper.NganSachThangMapper;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link NganSachNam}.
 */
@Service
@Transactional
public class NganSachNamServiceImpl implements NganSachNamService {

    private final Logger log = LoggerFactory.getLogger(NganSachNamServiceImpl.class);

    private final NganSachNamRepository nganSachNamRepository;

    private final NganSachNamMapper nganSachNamMapper;

    private final NganSachThangService nganSachThangService;

    private final NganSachThangMapper nganSachThangMapper;

    private final NganSachThangRepository nganSachThangRepository;

    private final PhieuChiRepository phieuChiRepository;

    private final KhoanMucChiPhiRepository khoanMucChiPhiRepository;

    public NganSachNamServiceImpl(
        NganSachNamRepository nganSachNamRepository,
        NganSachNamMapper nganSachNamMapper,
        NganSachThangService nganSachThangService,
        NganSachThangMapper nganSachThangMapper,
        NganSachThangRepository nganSachThangRepository,
        PhieuChiRepository phieuChiRepository,
        KhoanMucChiPhiRepository khoanMucChiPhiRepository
    ) {
        this.nganSachNamRepository = nganSachNamRepository;
        this.nganSachNamMapper = nganSachNamMapper;
        this.nganSachThangService = nganSachThangService;
        this.nganSachThangMapper = nganSachThangMapper;
        this.nganSachThangRepository = nganSachThangRepository;
        this.phieuChiRepository = phieuChiRepository;
        this.khoanMucChiPhiRepository = khoanMucChiPhiRepository;
    }

    @Override
    public NganSachNamDTO save(NganSachNamDTO nganSachNamDTO) {
        log.debug("Request to save NganSachNam : {}", nganSachNamDTO);
        NganSachNam nganSachNam = nganSachNamMapper.toEntity(nganSachNamDTO);
        nganSachNam = nganSachNamRepository.save(nganSachNam);
        if (nganSachNamDTO.getNganSachThangs() != null) {
            for (NganSachThangDTO nganSachThangDTO : nganSachNamDTO.getNganSachThangs()) {
                NganSachThang nganSachThang = nganSachThangMapper.toEntity(nganSachThangDTO);
                nganSachThang.setNganSachNam(nganSachNam);
                nganSachThangRepository.save(nganSachThang);
            }
        }
        return nganSachNamMapper.toDto(nganSachNam);
    }

    @Override
    public Optional<NganSachNamDTO> partialUpdate(NganSachNamDTO nganSachNamDTO) {
        log.debug("Request to partially update NganSachNam : {}", nganSachNamDTO);

        return nganSachNamRepository
            .findById(nganSachNamDTO.getId())
            .map(
                existingNganSachNam -> {
                    nganSachNamMapper.partialUpdate(existingNganSachNam, nganSachNamDTO);
                    return existingNganSachNam;
                }
            )
            .map(nganSachNamRepository::save)
            .map(nganSachNamMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<NganSachNamDTO> findAll(Pageable pageable) {
        log.debug("Request to get all NganSachNams");
        return nganSachNamRepository.findAll(pageable).map(nganSachNamMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<NganSachNamDTO> findOne(Long id) {
        log.debug("Request to get NganSachNam : {}", id);
        return nganSachNamRepository.findById(id).map(nganSachNamMapper::toDto);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete NganSachNam : {}", id);
        nganSachNamRepository.deleteById(id);
    }

    @Override
    public Optional<NganSachNamDTO> getChiTieu(Long id) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        NganSachNam nganSachNam = nganSachNamRepository.findById(id).orElse(null);
        Integer year = nganSachNam.getNam();
        Set<NganSachThang> nganSachThangs = new HashSet<>();
        for (int month = 1; month <= 12; month++) {
            String startDate = null;
            String endDate = null;
            if (month <= 9) {
                startDate = year + "-0" + month + "-01T00:00:00.00Z";
                endDate = year + "-0" + (month + 1) + "-01T00:00:00.00Z";
                if (month == 9) {
                    startDate = year + "-0" + month + "-01T00:00:00.00Z";
                    endDate = year + "-" + (month + 1) + "-01T00:00:00.00Z";
                }
            } else if (month < 12) {
                startDate = year + "-" + month + "-01T00:00:00.00Z";
                endDate = year + "-" + (month + 1) + "-01T00:00:00.00Z";
            } else {
                startDate = year + "-" + month + "-01T00:00:00.00Z";
                endDate = (year + 1) + "-0" + 1 + "-01T00:00:00.00Z";
            }

            List<Object[]> listChiTieu = phieuChiRepository.getChiTieu(Instant.parse(startDate), Instant.parse(endDate));
            for (Object[] chiTieu : listChiTieu) {
                KhoanMucChiPhi khoanMucChiPhi = khoanMucChiPhiRepository.findById((Long) chiTieu[0]).orElse(null);
                NganSachThang nganSachThang = new NganSachThang();
                nganSachThang.setKhoanMucChiPhi(khoanMucChiPhi);
                nganSachThang.setThang(month);
                nganSachThang.setSoTien((Double) chiTieu[1]);
                nganSachThangs.add(nganSachThang);
            }
        }
        NganSachNam result = new NganSachNam();
        result.setNam(year);
        result.setNganSachThangs(nganSachThangs);
        return Optional.of(nganSachNamMapper.toDto(result));
    }
}
