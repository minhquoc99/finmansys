package com.group3.finmansys.service.criteria;

import java.io.Serializable;
import java.util.Objects;
import tech.jhipster.service.Criteria;
import tech.jhipster.service.filter.BooleanFilter;
import tech.jhipster.service.filter.DoubleFilter;
import tech.jhipster.service.filter.Filter;
import tech.jhipster.service.filter.FloatFilter;
import tech.jhipster.service.filter.IntegerFilter;
import tech.jhipster.service.filter.LongFilter;
import tech.jhipster.service.filter.StringFilter;

/**
 * Criteria class for the {@link com.group3.finmansys.domain.KhachSan} entity. This class is used
 * in {@link com.group3.finmansys.web.rest.KhachSanResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /khach-sans?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class KhachSanCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter ten;

    private StringFilter diaChi;

    private StringFilter email;

    public KhachSanCriteria() {}

    public KhachSanCriteria(KhachSanCriteria other) {
        this.id = other.id == null ? null : other.id.copy();
        this.ten = other.ten == null ? null : other.ten.copy();
        this.diaChi = other.diaChi == null ? null : other.diaChi.copy();
        this.email = other.email == null ? null : other.email.copy();
    }

    @Override
    public KhachSanCriteria copy() {
        return new KhachSanCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public LongFilter id() {
        if (id == null) {
            id = new LongFilter();
        }
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getTen() {
        return ten;
    }

    public StringFilter ten() {
        if (ten == null) {
            ten = new StringFilter();
        }
        return ten;
    }

    public void setTen(StringFilter ten) {
        this.ten = ten;
    }

    public StringFilter getDiaChi() {
        return diaChi;
    }

    public StringFilter diaChi() {
        if (diaChi == null) {
            diaChi = new StringFilter();
        }
        return diaChi;
    }

    public void setDiaChi(StringFilter diaChi) {
        this.diaChi = diaChi;
    }

    public StringFilter getEmail() {
        return email;
    }

    public StringFilter email() {
        if (email == null) {
            email = new StringFilter();
        }
        return email;
    }

    public void setEmail(StringFilter email) {
        this.email = email;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final KhachSanCriteria that = (KhachSanCriteria) o;
        return (
            Objects.equals(id, that.id) &&
            Objects.equals(ten, that.ten) &&
            Objects.equals(diaChi, that.diaChi) &&
            Objects.equals(email, that.email)
        );
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, ten, diaChi, email);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "KhachSanCriteria{" +
            (id != null ? "id=" + id + ", " : "") +
            (ten != null ? "ten=" + ten + ", " : "") +
            (diaChi != null ? "diaChi=" + diaChi + ", " : "") +
            (email != null ? "email=" + email + ", " : "") +
            "}";
    }
}
