jest.mock('@angular/router');

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { of, Subject } from 'rxjs';

import { PhieuThuService } from '../service/phieu-thu.service';
import { IPhieuThu, PhieuThu } from '../phieu-thu.model';
import { IKhoanPhaiThu } from 'app/entities/khoan-phai-thu/khoan-phai-thu.model';
import { KhoanPhaiThuService } from 'app/entities/khoan-phai-thu/service/khoan-phai-thu.service';
import { IQuyTien } from 'app/entities/quy-tien/quy-tien.model';
import { QuyTienService } from 'app/entities/quy-tien/service/quy-tien.service';
import { IKhachHang } from 'app/entities/khach-hang/khach-hang.model';
import { KhachHangService } from 'app/entities/khach-hang/service/khach-hang.service';

import { PhieuThuUpdateComponent } from './phieu-thu-update.component';

describe('Component Tests', () => {
  describe('PhieuThu Management Update Component', () => {
    let comp: PhieuThuUpdateComponent;
    let fixture: ComponentFixture<PhieuThuUpdateComponent>;
    let activatedRoute: ActivatedRoute;
    let phieuThuService: PhieuThuService;
    let khoanPhaiThuService: KhoanPhaiThuService;
    let quyTienService: QuyTienService;
    let khachHangService: KhachHangService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule],
        declarations: [PhieuThuUpdateComponent],
        providers: [FormBuilder, ActivatedRoute],
      })
        .overrideTemplate(PhieuThuUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(PhieuThuUpdateComponent);
      activatedRoute = TestBed.inject(ActivatedRoute);
      phieuThuService = TestBed.inject(PhieuThuService);
      khoanPhaiThuService = TestBed.inject(KhoanPhaiThuService);
      quyTienService = TestBed.inject(QuyTienService);
      khachHangService = TestBed.inject(KhachHangService);

      comp = fixture.componentInstance;
    });

    describe('ngOnInit', () => {
      it('Should call KhoanPhaiThu query and add missing value', () => {
        const phieuThu: IPhieuThu = { id: 456 };
        const khoanPhaiThu: IKhoanPhaiThu = { id: 62454 };
        phieuThu.khoanPhaiThu = khoanPhaiThu;

        const khoanPhaiThuCollection: IKhoanPhaiThu[] = [{ id: 94811 }];
        spyOn(khoanPhaiThuService, 'query').and.returnValue(of(new HttpResponse({ body: khoanPhaiThuCollection })));
        const additionalKhoanPhaiThus = [khoanPhaiThu];
        const expectedCollection: IKhoanPhaiThu[] = [...additionalKhoanPhaiThus, ...khoanPhaiThuCollection];
        spyOn(khoanPhaiThuService, 'addKhoanPhaiThuToCollectionIfMissing').and.returnValue(expectedCollection);

        activatedRoute.data = of({ phieuThu });
        comp.ngOnInit();

        expect(khoanPhaiThuService.query).toHaveBeenCalled();
        expect(khoanPhaiThuService.addKhoanPhaiThuToCollectionIfMissing).toHaveBeenCalledWith(
          khoanPhaiThuCollection,
          ...additionalKhoanPhaiThus
        );
        expect(comp.khoanPhaiThusSharedCollection).toEqual(expectedCollection);
      });

      it('Should call QuyTien query and add missing value', () => {
        const phieuThu: IPhieuThu = { id: 456 };
        const quyTien: IQuyTien = { id: 43658 };
        phieuThu.quyTien = quyTien;

        const quyTienCollection: IQuyTien[] = [{ id: 37904 }];
        spyOn(quyTienService, 'query').and.returnValue(of(new HttpResponse({ body: quyTienCollection })));
        const additionalQuyTiens = [quyTien];
        const expectedCollection: IQuyTien[] = [...additionalQuyTiens, ...quyTienCollection];
        spyOn(quyTienService, 'addQuyTienToCollectionIfMissing').and.returnValue(expectedCollection);

        activatedRoute.data = of({ phieuThu });
        comp.ngOnInit();

        expect(quyTienService.query).toHaveBeenCalled();
        expect(quyTienService.addQuyTienToCollectionIfMissing).toHaveBeenCalledWith(quyTienCollection, ...additionalQuyTiens);
        expect(comp.quyTiensSharedCollection).toEqual(expectedCollection);
      });

      it('Should call KhachHang query and add missing value', () => {
        const phieuThu: IPhieuThu = { id: 456 };
        const khachHang: IKhachHang = { id: 71646 };
        phieuThu.khachHang = khachHang;

        const khachHangCollection: IKhachHang[] = [{ id: 29324 }];
        spyOn(khachHangService, 'query').and.returnValue(of(new HttpResponse({ body: khachHangCollection })));
        const additionalKhachHangs = [khachHang];
        const expectedCollection: IKhachHang[] = [...additionalKhachHangs, ...khachHangCollection];
        spyOn(khachHangService, 'addKhachHangToCollectionIfMissing').and.returnValue(expectedCollection);

        activatedRoute.data = of({ phieuThu });
        comp.ngOnInit();

        expect(khachHangService.query).toHaveBeenCalled();
        expect(khachHangService.addKhachHangToCollectionIfMissing).toHaveBeenCalledWith(khachHangCollection, ...additionalKhachHangs);
        expect(comp.khachHangsSharedCollection).toEqual(expectedCollection);
      });

      it('Should update editForm', () => {
        const phieuThu: IPhieuThu = { id: 456 };
        const khoanPhaiThu: IKhoanPhaiThu = { id: 16791 };
        phieuThu.khoanPhaiThu = khoanPhaiThu;
        const quyTien: IQuyTien = { id: 74086 };
        phieuThu.quyTien = quyTien;
        const khachHang: IKhachHang = { id: 89279 };
        phieuThu.khachHang = khachHang;

        activatedRoute.data = of({ phieuThu });
        comp.ngOnInit();

        expect(comp.editForm.value).toEqual(expect.objectContaining(phieuThu));
        expect(comp.khoanPhaiThusSharedCollection).toContain(khoanPhaiThu);
        expect(comp.quyTiensSharedCollection).toContain(quyTien);
        expect(comp.khachHangsSharedCollection).toContain(khachHang);
      });
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', () => {
        // GIVEN
        const saveSubject = new Subject();
        const phieuThu = { id: 123 };
        spyOn(phieuThuService, 'update').and.returnValue(saveSubject);
        spyOn(comp, 'previousState');
        activatedRoute.data = of({ phieuThu });
        comp.ngOnInit();

        // WHEN
        comp.save();
        expect(comp.isSaving).toEqual(true);
        saveSubject.next(new HttpResponse({ body: phieuThu }));
        saveSubject.complete();

        // THEN
        expect(comp.previousState).toHaveBeenCalled();
        expect(phieuThuService.update).toHaveBeenCalledWith(phieuThu);
        expect(comp.isSaving).toEqual(false);
      });

      it('Should call create service on save for new entity', () => {
        // GIVEN
        const saveSubject = new Subject();
        const phieuThu = new PhieuThu();
        spyOn(phieuThuService, 'create').and.returnValue(saveSubject);
        spyOn(comp, 'previousState');
        activatedRoute.data = of({ phieuThu });
        comp.ngOnInit();

        // WHEN
        comp.save();
        expect(comp.isSaving).toEqual(true);
        saveSubject.next(new HttpResponse({ body: phieuThu }));
        saveSubject.complete();

        // THEN
        expect(phieuThuService.create).toHaveBeenCalledWith(phieuThu);
        expect(comp.isSaving).toEqual(false);
        expect(comp.previousState).toHaveBeenCalled();
      });

      it('Should set isSaving to false on error', () => {
        // GIVEN
        const saveSubject = new Subject();
        const phieuThu = { id: 123 };
        spyOn(phieuThuService, 'update').and.returnValue(saveSubject);
        spyOn(comp, 'previousState');
        activatedRoute.data = of({ phieuThu });
        comp.ngOnInit();

        // WHEN
        comp.save();
        expect(comp.isSaving).toEqual(true);
        saveSubject.error('This is an error!');

        // THEN
        expect(phieuThuService.update).toHaveBeenCalledWith(phieuThu);
        expect(comp.isSaving).toEqual(false);
        expect(comp.previousState).not.toHaveBeenCalled();
      });
    });

    describe('Tracking relationships identifiers', () => {
      describe('trackKhoanPhaiThuById', () => {
        it('Should return tracked KhoanPhaiThu primary key', () => {
          const entity = { id: 123 };
          const trackResult = comp.trackKhoanPhaiThuById(0, entity);
          expect(trackResult).toEqual(entity.id);
        });
      });

      describe('trackQuyTienById', () => {
        it('Should return tracked QuyTien primary key', () => {
          const entity = { id: 123 };
          const trackResult = comp.trackQuyTienById(0, entity);
          expect(trackResult).toEqual(entity.id);
        });
      });

      describe('trackKhachHangById', () => {
        it('Should return tracked KhachHang primary key', () => {
          const entity = { id: 123 };
          const trackResult = comp.trackKhachHangById(0, entity);
          expect(trackResult).toEqual(entity.id);
        });
      });
    });
  });
});
