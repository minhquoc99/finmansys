package com.group3.finmansys.service.impl;

import com.group3.finmansys.domain.KhachHang;
import com.group3.finmansys.repository.KhachHangRepository;
import com.group3.finmansys.service.KhachHangService;
import com.group3.finmansys.service.dto.KhachHangDTO;
import com.group3.finmansys.service.mapper.KhachHangMapper;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link KhachHang}.
 */
@Service
@Transactional
public class KhachHangServiceImpl implements KhachHangService {

    private final Logger log = LoggerFactory.getLogger(KhachHangServiceImpl.class);

    private final KhachHangRepository khachHangRepository;

    private final KhachHangMapper khachHangMapper;

    public KhachHangServiceImpl(KhachHangRepository khachHangRepository, KhachHangMapper khachHangMapper) {
        this.khachHangRepository = khachHangRepository;
        this.khachHangMapper = khachHangMapper;
    }

    @Override
    public KhachHangDTO save(KhachHangDTO khachHangDTO) {
        log.debug("Request to save KhachHang : {}", khachHangDTO);
        KhachHang khachHang = khachHangMapper.toEntity(khachHangDTO);
        khachHang = khachHangRepository.save(khachHang);
        return khachHangMapper.toDto(khachHang);
    }

    @Override
    public Optional<KhachHangDTO> partialUpdate(KhachHangDTO khachHangDTO) {
        log.debug("Request to partially update KhachHang : {}", khachHangDTO);

        return khachHangRepository
            .findById(khachHangDTO.getId())
            .map(
                existingKhachHang -> {
                    khachHangMapper.partialUpdate(existingKhachHang, khachHangDTO);
                    return existingKhachHang;
                }
            )
            .map(khachHangRepository::save)
            .map(khachHangMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<KhachHangDTO> findAll(Pageable pageable) {
        log.debug("Request to get all KhachHangs");
        return khachHangRepository.findAll(pageable).map(khachHangMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<KhachHangDTO> findOne(Long id) {
        log.debug("Request to get KhachHang : {}", id);
        return khachHangRepository.findById(id).map(khachHangMapper::toDto);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete KhachHang : {}", id);
        khachHangRepository.deleteById(id);
    }
}
