package com.group3.finmansys.service.impl;

import com.group3.finmansys.domain.Phong;
import com.group3.finmansys.repository.PhongRepository;
import com.group3.finmansys.service.PhongService;
import com.group3.finmansys.service.dto.PhongDTO;
import com.group3.finmansys.service.mapper.PhongMapper;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link Phong}.
 */
@Service
@Transactional
public class PhongServiceImpl implements PhongService {

    private final Logger log = LoggerFactory.getLogger(PhongServiceImpl.class);

    private final PhongRepository phongRepository;

    private final PhongMapper phongMapper;

    public PhongServiceImpl(PhongRepository phongRepository, PhongMapper phongMapper) {
        this.phongRepository = phongRepository;
        this.phongMapper = phongMapper;
    }

    @Override
    public PhongDTO save(PhongDTO phongDTO) {
        log.debug("Request to save Phong : {}", phongDTO);
        Phong phong = phongMapper.toEntity(phongDTO);
        phong = phongRepository.save(phong);
        return phongMapper.toDto(phong);
    }

    @Override
    public Optional<PhongDTO> partialUpdate(PhongDTO phongDTO) {
        log.debug("Request to partially update Phong : {}", phongDTO);

        return phongRepository
            .findById(phongDTO.getId())
            .map(
                existingPhong -> {
                    phongMapper.partialUpdate(existingPhong, phongDTO);
                    return existingPhong;
                }
            )
            .map(phongRepository::save)
            .map(phongMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<PhongDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Phongs");
        return phongRepository.findAll(pageable).map(phongMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<PhongDTO> findOne(Long id) {
        log.debug("Request to get Phong : {}", id);
        return phongRepository.findById(id).map(phongMapper::toDto);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete Phong : {}", id);
        phongRepository.deleteById(id);
    }
}
