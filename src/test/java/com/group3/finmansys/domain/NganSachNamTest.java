package com.group3.finmansys.domain;

import static org.assertj.core.api.Assertions.assertThat;

import com.group3.finmansys.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class NganSachNamTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(NganSachNam.class);
        NganSachNam nganSachNam1 = new NganSachNam();
        nganSachNam1.setId(1L);
        NganSachNam nganSachNam2 = new NganSachNam();
        nganSachNam2.setId(nganSachNam1.getId());
        assertThat(nganSachNam1).isEqualTo(nganSachNam2);
        nganSachNam2.setId(2L);
        assertThat(nganSachNam1).isNotEqualTo(nganSachNam2);
        nganSachNam1.setId(null);
        assertThat(nganSachNam1).isNotEqualTo(nganSachNam2);
    }
}
