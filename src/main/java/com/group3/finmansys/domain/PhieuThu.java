package com.group3.finmansys.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.time.Instant;
import javax.persistence.*;
import javax.validation.constraints.*;

/**
 * A PhieuThu.
 */
@Entity
@Table(name = "phieu_thu")
public class PhieuThu implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(name = "ngay_chung_tu", nullable = false)
    private Instant ngayChungTu;

    @NotNull
    @DecimalMin(value = "1")
    @Column(name = "so_tien", nullable = false)
    private Double soTien;

    @ManyToOne(optional = false)
    @NotNull
    @JsonIgnoreProperties(value = { "khachHang", "maKeToan", "phieuThus", "phongDuocDat" }, allowSetters = true)
    private KhoanPhaiThu khoanPhaiThu;

    @ManyToOne(optional = false)
    @NotNull
    @JsonIgnoreProperties(value = { "loaiQuy", "maKeToan", "khachSan", "phieuThus", "phieuChis" }, allowSetters = true)
    private QuyTien quyTien;

    @ManyToOne(optional = false)
    @NotNull
    private KhachHang khachHang;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public PhieuThu id(Long id) {
        this.id = id;
        return this;
    }

    public Instant getNgayChungTu() {
        return this.ngayChungTu;
    }

    public PhieuThu ngayChungTu(Instant ngayChungTu) {
        this.ngayChungTu = ngayChungTu;
        return this;
    }

    public void setNgayChungTu(Instant ngayChungTu) {
        this.ngayChungTu = ngayChungTu;
    }

    public Double getSoTien() {
        return this.soTien;
    }

    public PhieuThu soTien(Double soTien) {
        this.soTien = soTien;
        return this;
    }

    public void setSoTien(Double soTien) {
        this.soTien = soTien;
    }

    public KhoanPhaiThu getKhoanPhaiThu() {
        return this.khoanPhaiThu;
    }

    public PhieuThu khoanPhaiThu(KhoanPhaiThu khoanPhaiThu) {
        this.setKhoanPhaiThu(khoanPhaiThu);
        return this;
    }

    public void setKhoanPhaiThu(KhoanPhaiThu khoanPhaiThu) {
        this.khoanPhaiThu = khoanPhaiThu;
    }

    public QuyTien getQuyTien() {
        return this.quyTien;
    }

    public PhieuThu quyTien(QuyTien quyTien) {
        this.setQuyTien(quyTien);
        return this;
    }

    public void setQuyTien(QuyTien quyTien) {
        this.quyTien = quyTien;
    }

    public KhachHang getKhachHang() {
        return this.khachHang;
    }

    public PhieuThu khachHang(KhachHang khachHang) {
        this.setKhachHang(khachHang);
        return this;
    }

    public void setKhachHang(KhachHang khachHang) {
        this.khachHang = khachHang;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof PhieuThu)) {
            return false;
        }
        return id != null && id.equals(((PhieuThu) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "PhieuThu{" +
            "id=" + getId() +
            ", ngayChungTu='" + getNgayChungTu() + "'" +
            ", soTien=" + getSoTien() +
            "}";
    }
}
