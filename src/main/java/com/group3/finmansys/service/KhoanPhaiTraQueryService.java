package com.group3.finmansys.service;

import com.group3.finmansys.domain.*; // for static metamodels
import com.group3.finmansys.domain.KhoanPhaiTra;
import com.group3.finmansys.repository.KhoanPhaiTraRepository;
import com.group3.finmansys.service.criteria.KhoanPhaiTraCriteria;
import com.group3.finmansys.service.dto.KhoanPhaiTraDTO;
import com.group3.finmansys.service.mapper.KhoanPhaiTraMapper;
import java.util.List;
import javax.persistence.criteria.JoinType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tech.jhipster.service.QueryService;

/**
 * Service for executing complex queries for {@link KhoanPhaiTra} entities in the database.
 * The main input is a {@link KhoanPhaiTraCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link KhoanPhaiTraDTO} or a {@link Page} of {@link KhoanPhaiTraDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class KhoanPhaiTraQueryService extends QueryService<KhoanPhaiTra> {

    private final Logger log = LoggerFactory.getLogger(KhoanPhaiTraQueryService.class);

    private final KhoanPhaiTraRepository khoanPhaiTraRepository;

    private final KhoanPhaiTraMapper khoanPhaiTraMapper;

    public KhoanPhaiTraQueryService(KhoanPhaiTraRepository khoanPhaiTraRepository, KhoanPhaiTraMapper khoanPhaiTraMapper) {
        this.khoanPhaiTraRepository = khoanPhaiTraRepository;
        this.khoanPhaiTraMapper = khoanPhaiTraMapper;
    }

    /**
     * Return a {@link List} of {@link KhoanPhaiTraDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<KhoanPhaiTraDTO> findByCriteria(KhoanPhaiTraCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<KhoanPhaiTra> specification = createSpecification(criteria);
        return khoanPhaiTraMapper.toDto(khoanPhaiTraRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link KhoanPhaiTraDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<KhoanPhaiTraDTO> findByCriteria(KhoanPhaiTraCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<KhoanPhaiTra> specification = createSpecification(criteria);
        return khoanPhaiTraRepository.findAll(specification, page).map(khoanPhaiTraMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(KhoanPhaiTraCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<KhoanPhaiTra> specification = createSpecification(criteria);
        return khoanPhaiTraRepository.count(specification);
    }

    /**
     * Function to convert {@link KhoanPhaiTraCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<KhoanPhaiTra> createSpecification(KhoanPhaiTraCriteria criteria) {
        Specification<KhoanPhaiTra> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getId(), KhoanPhaiTra_.id));
            }
            if (criteria.getNgayTao() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getNgayTao(), KhoanPhaiTra_.ngayTao));
            }
            if (criteria.getNgayPhaiTra() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getNgayPhaiTra(), KhoanPhaiTra_.ngayPhaiTra));
            }
            if (criteria.getSoTien() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getSoTien(), KhoanPhaiTra_.soTien));
            }
            if (criteria.getGhiChu() != null) {
                specification = specification.and(buildStringSpecification(criteria.getGhiChu(), KhoanPhaiTra_.ghiChu));
            }
            if (criteria.getKhachHangId() != null) {
                specification =
                    specification.and(
                        buildSpecification(
                            criteria.getKhachHangId(),
                            root -> root.join(KhoanPhaiTra_.khachHang, JoinType.LEFT).get(KhachHang_.id)
                        )
                    );
            }
            if (criteria.getMaKeToanId() != null) {
                specification =
                    specification.and(
                        buildSpecification(
                            criteria.getMaKeToanId(),
                            root -> root.join(KhoanPhaiTra_.maKeToan, JoinType.LEFT).get(MaKeToan_.id)
                        )
                    );
            }
            if (criteria.getDsPhieuChiId() != null) {
                specification =
                    specification.and(
                        buildSpecification(
                            criteria.getDsPhieuChiId(),
                            root -> root.join(KhoanPhaiTra_.dsPhieuChis, JoinType.LEFT).get(PhieuChi_.id)
                        )
                    );
            }
        }
        return specification;
    }
}
