import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { mergeMap } from 'rxjs/operators';

import { IKhoanPhaiThu, KhoanPhaiThu } from '../khoan-phai-thu.model';
import { KhoanPhaiThuService } from '../service/khoan-phai-thu.service';

@Injectable({ providedIn: 'root' })
export class KhoanPhaiThuRoutingResolveService implements Resolve<IKhoanPhaiThu> {
  constructor(protected service: KhoanPhaiThuService, protected router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IKhoanPhaiThu> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        mergeMap((khoanPhaiThu: HttpResponse<KhoanPhaiThu>) => {
          if (khoanPhaiThu.body) {
            return of(khoanPhaiThu.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new KhoanPhaiThu());
  }
}
