package com.group3.finmansys.service;

import com.group3.finmansys.service.dto.DichVuSuDungDTO;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Service Interface for managing {@link com.group3.finmansys.domain.DichVuSuDung}.
 */
public interface DichVuSuDungService {
    /**
     * Save a dichVuSuDung.
     *
     * @param dichVuSuDungDTO the entity to save.
     * @return the persisted entity.
     */
    DichVuSuDungDTO save(DichVuSuDungDTO dichVuSuDungDTO);

    /**
     * Partially updates a dichVuSuDung.
     *
     * @param dichVuSuDungDTO the entity to update partially.
     * @return the persisted entity.
     */
    Optional<DichVuSuDungDTO> partialUpdate(DichVuSuDungDTO dichVuSuDungDTO);

    /**
     * Get all the dichVuSuDungs.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<DichVuSuDungDTO> findAll(Pageable pageable);

    /**
     * Get the "id" dichVuSuDung.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<DichVuSuDungDTO> findOne(Long id);

    /**
     * Delete the "id" dichVuSuDung.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
