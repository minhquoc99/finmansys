package com.group3.finmansys.service.dto;

import static org.assertj.core.api.Assertions.assertThat;

import com.group3.finmansys.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class MaKeToanDTOTest {

    @Test
    void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(MaKeToanDTO.class);
        MaKeToanDTO maKeToanDTO1 = new MaKeToanDTO();
        maKeToanDTO1.setId(1L);
        MaKeToanDTO maKeToanDTO2 = new MaKeToanDTO();
        assertThat(maKeToanDTO1).isNotEqualTo(maKeToanDTO2);
        maKeToanDTO2.setId(maKeToanDTO1.getId());
        assertThat(maKeToanDTO1).isEqualTo(maKeToanDTO2);
        maKeToanDTO2.setId(2L);
        assertThat(maKeToanDTO1).isNotEqualTo(maKeToanDTO2);
        maKeToanDTO1.setId(null);
        assertThat(maKeToanDTO1).isNotEqualTo(maKeToanDTO2);
    }
}
