package com.group3.finmansys.web.rest;

import com.group3.finmansys.repository.NganSachNamRepository;
import com.group3.finmansys.service.NganSachNamQueryService;
import com.group3.finmansys.service.NganSachNamService;
import com.group3.finmansys.service.criteria.NganSachNamCriteria;
import com.group3.finmansys.service.dto.NganSachNamDTO;
import com.group3.finmansys.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.PaginationUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link com.group3.finmansys.domain.NganSachNam}.
 */
@RestController
@RequestMapping("/api")
public class NganSachNamResource {

    private final Logger log = LoggerFactory.getLogger(NganSachNamResource.class);

    private static final String ENTITY_NAME = "nganSachNam";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final NganSachNamService nganSachNamService;

    private final NganSachNamRepository nganSachNamRepository;

    private final NganSachNamQueryService nganSachNamQueryService;

    public NganSachNamResource(
        NganSachNamService nganSachNamService,
        NganSachNamRepository nganSachNamRepository,
        NganSachNamQueryService nganSachNamQueryService
    ) {
        this.nganSachNamService = nganSachNamService;
        this.nganSachNamRepository = nganSachNamRepository;
        this.nganSachNamQueryService = nganSachNamQueryService;
    }

    /**
     * {@code POST  /ngan-sach-nams} : Create a new nganSachNam.
     *
     * @param nganSachNamDTO the nganSachNamDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new nganSachNamDTO, or with status {@code 400 (Bad Request)} if the nganSachNam has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/ngan-sach-nams")
    public ResponseEntity<NganSachNamDTO> createNganSachNam(@Valid @RequestBody NganSachNamDTO nganSachNamDTO) throws URISyntaxException {
        log.debug("REST request to save NganSachNam : {}", nganSachNamDTO);
        if (nganSachNamDTO.getId() != null) {
            throw new BadRequestAlertException("A new nganSachNam cannot already have an ID", ENTITY_NAME, "idexists");
        }
        NganSachNamDTO result = nganSachNamService.save(nganSachNamDTO);
        return ResponseEntity
            .created(new URI("/api/ngan-sach-nams/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /ngan-sach-nams/:id} : Updates an existing nganSachNam.
     *
     * @param id the id of the nganSachNamDTO to save.
     * @param nganSachNamDTO the nganSachNamDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated nganSachNamDTO,
     * or with status {@code 400 (Bad Request)} if the nganSachNamDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the nganSachNamDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/ngan-sach-nams/{id}")
    public ResponseEntity<NganSachNamDTO> updateNganSachNam(
        @PathVariable(value = "id", required = false) final Long id,
        @Valid @RequestBody NganSachNamDTO nganSachNamDTO
    ) throws URISyntaxException {
        log.debug("REST request to update NganSachNam : {}, {}", id, nganSachNamDTO);
        if (nganSachNamDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, nganSachNamDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!nganSachNamRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        NganSachNamDTO result = nganSachNamService.save(nganSachNamDTO);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, nganSachNamDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /ngan-sach-nams/:id} : Partial updates given fields of an existing nganSachNam, field will ignore if it is null
     *
     * @param id the id of the nganSachNamDTO to save.
     * @param nganSachNamDTO the nganSachNamDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated nganSachNamDTO,
     * or with status {@code 400 (Bad Request)} if the nganSachNamDTO is not valid,
     * or with status {@code 404 (Not Found)} if the nganSachNamDTO is not found,
     * or with status {@code 500 (Internal Server Error)} if the nganSachNamDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/ngan-sach-nams/{id}", consumes = "application/merge-patch+json")
    public ResponseEntity<NganSachNamDTO> partialUpdateNganSachNam(
        @PathVariable(value = "id", required = false) final Long id,
        @NotNull @RequestBody NganSachNamDTO nganSachNamDTO
    ) throws URISyntaxException {
        log.debug("REST request to partial update NganSachNam partially : {}, {}", id, nganSachNamDTO);
        if (nganSachNamDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, nganSachNamDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!nganSachNamRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<NganSachNamDTO> result = nganSachNamService.partialUpdate(nganSachNamDTO);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, nganSachNamDTO.getId().toString())
        );
    }

    /**
     * {@code GET  /ngan-sach-nams} : get all the nganSachNams.
     *
     * @param pageable the pagination information.
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of nganSachNams in body.
     */
    @GetMapping("/ngan-sach-nams")
    public ResponseEntity<List<NganSachNamDTO>> getAllNganSachNams(NganSachNamCriteria criteria, Pageable pageable) {
        log.debug("REST request to get NganSachNams by criteria: {}", criteria);
        Page<NganSachNamDTO> page = nganSachNamQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /ngan-sach-nams/count} : count all the nganSachNams.
     *
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
     */
    @GetMapping("/ngan-sach-nams/count")
    public ResponseEntity<Long> countNganSachNams(NganSachNamCriteria criteria) {
        log.debug("REST request to count NganSachNams by criteria: {}", criteria);
        return ResponseEntity.ok().body(nganSachNamQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /ngan-sach-nams/:id} : get the "id" nganSachNam.
     *
     * @param id the id of the nganSachNamDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the nganSachNamDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/ngan-sach-nams/{id}")
    public ResponseEntity<NganSachNamDTO> getNganSachNam(@PathVariable Long id) {
        log.debug("REST request to get NganSachNam : {}", id);
        Optional<NganSachNamDTO> nganSachNamDTO = nganSachNamService.findOne(id);
        return ResponseUtil.wrapOrNotFound(nganSachNamDTO);
    }

    /**
     * {@code GET  /ngan-sach-nams/:id} : get the "id" nganSachNam.
     *
     * @param id the id of the nganSachNamDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the nganSachNamDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/ngan-sach-nams/chi-tieu/{id}")
    public ResponseEntity<NganSachNamDTO> getChiTieu(@PathVariable Long id) {
        log.debug("REST request to get NganSachNam : {}", id);
        Optional<NganSachNamDTO> nganSachNamDTO = nganSachNamService.getChiTieu(id);
        return ResponseUtil.wrapOrNotFound(nganSachNamDTO);
    }

    /**
     * {@code DELETE  /ngan-sach-nams/:id} : delete the "id" nganSachNam.
     *
     * @param id the id of the nganSachNamDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/ngan-sach-nams/{id}")
    public ResponseEntity<Void> deleteNganSachNam(@PathVariable Long id) {
        log.debug("REST request to delete NganSachNam : {}", id);
        nganSachNamService.delete(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
            .build();
    }
}
