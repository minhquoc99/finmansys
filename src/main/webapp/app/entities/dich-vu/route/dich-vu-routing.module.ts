import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { UserRouteAccessService } from 'app/core/auth/user-route-access.service';
import { DichVuComponent } from '../list/dich-vu.component';
import { DichVuDetailComponent } from '../detail/dich-vu-detail.component';
import { DichVuUpdateComponent } from '../update/dich-vu-update.component';
import { DichVuRoutingResolveService } from './dich-vu-routing-resolve.service';

const dichVuRoute: Routes = [
  {
    path: '',
    component: DichVuComponent,
    data: {
      defaultSort: 'id,asc',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: DichVuDetailComponent,
    resolve: {
      dichVu: DichVuRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: DichVuUpdateComponent,
    resolve: {
      dichVu: DichVuRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: DichVuUpdateComponent,
    resolve: {
      dichVu: DichVuRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
];

@NgModule({
  imports: [RouterModule.forChild(dichVuRoute)],
  exports: [RouterModule],
})
export class DichVuRoutingModule {}
