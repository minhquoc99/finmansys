package com.group3.finmansys.web.rest;

import com.group3.finmansys.repository.KhoanPhaiTraRepository;
import com.group3.finmansys.service.KhoanPhaiTraQueryService;
import com.group3.finmansys.service.KhoanPhaiTraService;
import com.group3.finmansys.service.criteria.KhoanPhaiTraCriteria;
import com.group3.finmansys.service.dto.KhoanPhaiTraDTO;
import com.group3.finmansys.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.PaginationUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link com.group3.finmansys.domain.KhoanPhaiTra}.
 */
@RestController
@RequestMapping("/api")
public class KhoanPhaiTraResource {

    private final Logger log = LoggerFactory.getLogger(KhoanPhaiTraResource.class);

    private static final String ENTITY_NAME = "khoanPhaiTra";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final KhoanPhaiTraService khoanPhaiTraService;

    private final KhoanPhaiTraRepository khoanPhaiTraRepository;

    private final KhoanPhaiTraQueryService khoanPhaiTraQueryService;

    public KhoanPhaiTraResource(
        KhoanPhaiTraService khoanPhaiTraService,
        KhoanPhaiTraRepository khoanPhaiTraRepository,
        KhoanPhaiTraQueryService khoanPhaiTraQueryService
    ) {
        this.khoanPhaiTraService = khoanPhaiTraService;
        this.khoanPhaiTraRepository = khoanPhaiTraRepository;
        this.khoanPhaiTraQueryService = khoanPhaiTraQueryService;
    }

    /**
     * {@code POST  /khoan-phai-tras} : Create a new khoanPhaiTra.
     *
     * @param khoanPhaiTraDTO the khoanPhaiTraDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new khoanPhaiTraDTO, or with status {@code 400 (Bad Request)} if the khoanPhaiTra has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/khoan-phai-tras")
    public ResponseEntity<KhoanPhaiTraDTO> createKhoanPhaiTra(@Valid @RequestBody KhoanPhaiTraDTO khoanPhaiTraDTO)
        throws URISyntaxException {
        log.debug("REST request to save KhoanPhaiTra : {}", khoanPhaiTraDTO);
        if (khoanPhaiTraDTO.getId() != null) {
            throw new BadRequestAlertException("A new khoanPhaiTra cannot already have an ID", ENTITY_NAME, "idexists");
        }
        KhoanPhaiTraDTO result = khoanPhaiTraService.save(khoanPhaiTraDTO);
        return ResponseEntity
            .created(new URI("/api/khoan-phai-tras/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /khoan-phai-tras/:id} : Updates an existing khoanPhaiTra.
     *
     * @param id the id of the khoanPhaiTraDTO to save.
     * @param khoanPhaiTraDTO the khoanPhaiTraDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated khoanPhaiTraDTO,
     * or with status {@code 400 (Bad Request)} if the khoanPhaiTraDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the khoanPhaiTraDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/khoan-phai-tras/{id}")
    public ResponseEntity<KhoanPhaiTraDTO> updateKhoanPhaiTra(
        @PathVariable(value = "id", required = false) final Long id,
        @Valid @RequestBody KhoanPhaiTraDTO khoanPhaiTraDTO
    ) throws URISyntaxException {
        log.debug("REST request to update KhoanPhaiTra : {}, {}", id, khoanPhaiTraDTO);
        if (khoanPhaiTraDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, khoanPhaiTraDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!khoanPhaiTraRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        KhoanPhaiTraDTO result = khoanPhaiTraService.save(khoanPhaiTraDTO);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, khoanPhaiTraDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /khoan-phai-tras/:id} : Partial updates given fields of an existing khoanPhaiTra, field will ignore if it is null
     *
     * @param id the id of the khoanPhaiTraDTO to save.
     * @param khoanPhaiTraDTO the khoanPhaiTraDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated khoanPhaiTraDTO,
     * or with status {@code 400 (Bad Request)} if the khoanPhaiTraDTO is not valid,
     * or with status {@code 404 (Not Found)} if the khoanPhaiTraDTO is not found,
     * or with status {@code 500 (Internal Server Error)} if the khoanPhaiTraDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/khoan-phai-tras/{id}", consumes = "application/merge-patch+json")
    public ResponseEntity<KhoanPhaiTraDTO> partialUpdateKhoanPhaiTra(
        @PathVariable(value = "id", required = false) final Long id,
        @NotNull @RequestBody KhoanPhaiTraDTO khoanPhaiTraDTO
    ) throws URISyntaxException {
        log.debug("REST request to partial update KhoanPhaiTra partially : {}, {}", id, khoanPhaiTraDTO);
        if (khoanPhaiTraDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, khoanPhaiTraDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!khoanPhaiTraRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<KhoanPhaiTraDTO> result = khoanPhaiTraService.partialUpdate(khoanPhaiTraDTO);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, khoanPhaiTraDTO.getId().toString())
        );
    }

    /**
     * {@code GET  /khoan-phai-tras} : get all the khoanPhaiTras.
     *
     * @param pageable the pagination information.
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of khoanPhaiTras in body.
     */
    @GetMapping("/khoan-phai-tras")
    public ResponseEntity<List<KhoanPhaiTraDTO>> getAllKhoanPhaiTras(KhoanPhaiTraCriteria criteria, Pageable pageable) {
        log.debug("REST request to get KhoanPhaiTras by criteria: {}", criteria);
        Page<KhoanPhaiTraDTO> page = khoanPhaiTraQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /khoan-phai-tras/count} : count all the khoanPhaiTras.
     *
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
     */
    @GetMapping("/khoan-phai-tras/count")
    public ResponseEntity<Long> countKhoanPhaiTras(KhoanPhaiTraCriteria criteria) {
        log.debug("REST request to count KhoanPhaiTras by criteria: {}", criteria);
        return ResponseEntity.ok().body(khoanPhaiTraQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /khoan-phai-tras/:id} : get the "id" khoanPhaiTra.
     *
     * @param id the id of the khoanPhaiTraDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the khoanPhaiTraDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/khoan-phai-tras/{id}")
    public ResponseEntity<KhoanPhaiTraDTO> getKhoanPhaiTra(@PathVariable Long id) {
        log.debug("REST request to get KhoanPhaiTra : {}", id);
        Optional<KhoanPhaiTraDTO> khoanPhaiTraDTO = khoanPhaiTraService.findOne(id);
        return ResponseUtil.wrapOrNotFound(khoanPhaiTraDTO);
    }

    /**
     * {@code DELETE  /khoan-phai-tras/:id} : delete the "id" khoanPhaiTra.
     *
     * @param id the id of the khoanPhaiTraDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/khoan-phai-tras/{id}")
    public ResponseEntity<Void> deleteKhoanPhaiTra(@PathVariable Long id) {
        log.debug("REST request to delete KhoanPhaiTra : {}", id);
        khoanPhaiTraService.delete(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
            .build();
    }
}
