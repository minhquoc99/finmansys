package com.group3.finmansys.service.dto;

import java.io.Serializable;
import java.time.Instant;
import java.util.Objects;
import javax.validation.constraints.*;

/**
 * A DTO for the {@link com.group3.finmansys.domain.PhieuChi} entity.
 */
public class PhieuChiDTO implements Serializable {

    private Long id;

    @NotNull
    private Instant ngayChungTu;

    @NotNull
    @DecimalMin(value = "1")
    private Double soTien;

    private KhachHangDTO khachHang;

    private KhoanMucChiPhiDTO khoanMucChiPhi;

    private KhoanPhaiTraDTO khoanPhaiTra;

    private QuyTienDTO quyTien;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Instant getNgayChungTu() {
        return ngayChungTu;
    }

    public void setNgayChungTu(Instant ngayChungTu) {
        this.ngayChungTu = ngayChungTu;
    }

    public Double getSoTien() {
        return soTien;
    }

    public void setSoTien(Double soTien) {
        this.soTien = soTien;
    }

    public KhachHangDTO getKhachHang() {
        return khachHang;
    }

    public void setKhachHang(KhachHangDTO khachHang) {
        this.khachHang = khachHang;
    }

    public KhoanMucChiPhiDTO getKhoanMucChiPhi() {
        return khoanMucChiPhi;
    }

    public void setKhoanMucChiPhi(KhoanMucChiPhiDTO khoanMucChiPhi) {
        this.khoanMucChiPhi = khoanMucChiPhi;
    }

    public KhoanPhaiTraDTO getKhoanPhaiTra() {
        return khoanPhaiTra;
    }

    public void setKhoanPhaiTra(KhoanPhaiTraDTO khoanPhaiTra) {
        this.khoanPhaiTra = khoanPhaiTra;
    }

    public QuyTienDTO getQuyTien() {
        return quyTien;
    }

    public void setQuyTien(QuyTienDTO quyTien) {
        this.quyTien = quyTien;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof PhieuChiDTO)) {
            return false;
        }

        PhieuChiDTO phieuChiDTO = (PhieuChiDTO) o;
        if (this.id == null) {
            return false;
        }
        return Objects.equals(this.id, phieuChiDTO.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.id);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "PhieuChiDTO{" +
            "id=" + getId() +
            ", ngayChungTu='" + getNgayChungTu() + "'" +
            ", soTien=" + getSoTien() +
            ", khachHang=" + getKhachHang() +
            ", khoanMucChiPhi=" + getKhoanMucChiPhi() +
            ", khoanPhaiTra=" + getKhoanPhaiTra() +
            ", quyTien=" + getQuyTien() +
            "}";
    }
}
