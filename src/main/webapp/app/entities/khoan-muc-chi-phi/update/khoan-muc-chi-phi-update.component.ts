import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize } from 'rxjs/operators';

import { IKhoanMucChiPhi, KhoanMucChiPhi } from '../khoan-muc-chi-phi.model';
import { KhoanMucChiPhiService } from '../service/khoan-muc-chi-phi.service';

@Component({
  selector: 'jhi-khoan-muc-chi-phi-update',
  templateUrl: './khoan-muc-chi-phi-update.component.html',
})
export class KhoanMucChiPhiUpdateComponent implements OnInit {
  isSaving = false;

  editForm = this.fb.group({
    id: [],
    ten: [null, [Validators.required]],
    moTa: [],
  });

  constructor(
    protected khoanMucChiPhiService: KhoanMucChiPhiService,
    protected activatedRoute: ActivatedRoute,
    protected fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ khoanMucChiPhi }) => {
      this.updateForm(khoanMucChiPhi);
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const khoanMucChiPhi = this.createFromForm();
    if (khoanMucChiPhi.id !== undefined) {
      this.subscribeToSaveResponse(this.khoanMucChiPhiService.update(khoanMucChiPhi));
    } else {
      this.subscribeToSaveResponse(this.khoanMucChiPhiService.create(khoanMucChiPhi));
    }
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IKhoanMucChiPhi>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(khoanMucChiPhi: IKhoanMucChiPhi): void {
    this.editForm.patchValue({
      id: khoanMucChiPhi.id,
      ten: khoanMucChiPhi.ten,
      moTa: khoanMucChiPhi.moTa,
    });
  }

  protected createFromForm(): IKhoanMucChiPhi {
    return {
      ...new KhoanMucChiPhi(),
      id: this.editForm.get(['id'])!.value,
      ten: this.editForm.get(['ten'])!.value,
      moTa: this.editForm.get(['moTa'])!.value,
    };
  }
}
