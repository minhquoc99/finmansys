package com.group3.finmansys.service.dto;

import java.io.Serializable;
import java.time.Instant;
import java.util.Objects;
import javax.validation.constraints.*;

/**
 * A DTO for the {@link com.group3.finmansys.domain.ChiTietTangTSCD} entity.
 */
public class ChiTietTangTSCDDTO implements Serializable {

    private Long id;

    @NotNull
    private Instant ngayThuMua;

    @NotNull
    @Min(value = 1)
    private Integer soLuong;

    @NotNull
    @DecimalMin(value = "1")
    private Double chiPhiMua;

    private TaiSanCDDTO taiSanCD;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Instant getNgayThuMua() {
        return ngayThuMua;
    }

    public void setNgayThuMua(Instant ngayThuMua) {
        this.ngayThuMua = ngayThuMua;
    }

    public Integer getSoLuong() {
        return soLuong;
    }

    public void setSoLuong(Integer soLuong) {
        this.soLuong = soLuong;
    }

    public Double getChiPhiMua() {
        return chiPhiMua;
    }

    public void setChiPhiMua(Double chiPhiMua) {
        this.chiPhiMua = chiPhiMua;
    }

    public TaiSanCDDTO getTaiSanCD() {
        return taiSanCD;
    }

    public void setTaiSanCD(TaiSanCDDTO taiSanCD) {
        this.taiSanCD = taiSanCD;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof ChiTietTangTSCDDTO)) {
            return false;
        }

        ChiTietTangTSCDDTO chiTietTangTSCDDTO = (ChiTietTangTSCDDTO) o;
        if (this.id == null) {
            return false;
        }
        return Objects.equals(this.id, chiTietTangTSCDDTO.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.id);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "ChiTietTangTSCDDTO{" +
            "id=" + getId() +
            ", ngayThuMua='" + getNgayThuMua() + "'" +
            ", soLuong=" + getSoLuong() +
            ", chiPhiMua=" + getChiPhiMua() +
            ", taiSanCD=" + getTaiSanCD() +
            "}";
    }
}
