import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize, map } from 'rxjs/operators';

import * as dayjs from 'dayjs';
import { DATE_TIME_FORMAT } from 'app/config/input.constants';

import { IChiTietTangTSCD, ChiTietTangTSCD } from '../chi-tiet-tang-tscd.model';
import { ChiTietTangTSCDService } from '../service/chi-tiet-tang-tscd.service';
import { ITaiSanCD } from 'app/entities/tai-san-cd/tai-san-cd.model';
import { TaiSanCDService } from 'app/entities/tai-san-cd/service/tai-san-cd.service';

@Component({
  selector: 'jhi-chi-tiet-tang-tscd-update',
  templateUrl: './chi-tiet-tang-tscd-update.component.html',
})
export class ChiTietTangTSCDUpdateComponent implements OnInit {
  isSaving = false;

  taiSanCDSSharedCollection: ITaiSanCD[] = [];

  editForm = this.fb.group({
    id: [],
    ngayThuMua: [null, [Validators.required]],
    soLuong: [null, [Validators.required, Validators.min(1)]],
    chiPhiMua: [null, [Validators.required, Validators.min(1)]],
    taiSanCD: [null, Validators.required],
  });

  constructor(
    protected chiTietTangTSCDService: ChiTietTangTSCDService,
    protected taiSanCDService: TaiSanCDService,
    protected activatedRoute: ActivatedRoute,
    protected fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ chiTietTangTSCD }) => {
      if (chiTietTangTSCD.id === undefined) {
        const today = dayjs().startOf('day');
        chiTietTangTSCD.ngayThuMua = today;
      }

      this.updateForm(chiTietTangTSCD);

      this.loadRelationshipsOptions();
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const chiTietTangTSCD = this.createFromForm();
    if (chiTietTangTSCD.id !== undefined) {
      this.subscribeToSaveResponse(this.chiTietTangTSCDService.update(chiTietTangTSCD));
    } else {
      this.subscribeToSaveResponse(this.chiTietTangTSCDService.create(chiTietTangTSCD));
    }
  }

  trackTaiSanCDById(index: number, item: ITaiSanCD): number {
    return item.id!;
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IChiTietTangTSCD>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(chiTietTangTSCD: IChiTietTangTSCD): void {
    this.editForm.patchValue({
      id: chiTietTangTSCD.id,
      ngayThuMua: chiTietTangTSCD.ngayThuMua ? chiTietTangTSCD.ngayThuMua.format(DATE_TIME_FORMAT) : null,
      soLuong: chiTietTangTSCD.soLuong,
      chiPhiMua: chiTietTangTSCD.chiPhiMua,
      taiSanCD: chiTietTangTSCD.taiSanCD,
    });

    this.taiSanCDSSharedCollection = this.taiSanCDService.addTaiSanCDToCollectionIfMissing(
      this.taiSanCDSSharedCollection,
      chiTietTangTSCD.taiSanCD
    );
  }

  protected loadRelationshipsOptions(): void {
    this.taiSanCDService
      .query()
      .pipe(map((res: HttpResponse<ITaiSanCD[]>) => res.body ?? []))
      .pipe(
        map((taiSanCDS: ITaiSanCD[]) =>
          this.taiSanCDService.addTaiSanCDToCollectionIfMissing(taiSanCDS, this.editForm.get('taiSanCD')!.value)
        )
      )
      .subscribe((taiSanCDS: ITaiSanCD[]) => (this.taiSanCDSSharedCollection = taiSanCDS));
  }

  protected createFromForm(): IChiTietTangTSCD {
    return {
      ...new ChiTietTangTSCD(),
      id: this.editForm.get(['id'])!.value,
      ngayThuMua: this.editForm.get(['ngayThuMua'])!.value ? dayjs(this.editForm.get(['ngayThuMua'])!.value, DATE_TIME_FORMAT) : undefined,
      soLuong: this.editForm.get(['soLuong'])!.value,
      chiPhiMua: this.editForm.get(['chiPhiMua'])!.value,
      taiSanCD: this.editForm.get(['taiSanCD'])!.value,
    };
  }
}
