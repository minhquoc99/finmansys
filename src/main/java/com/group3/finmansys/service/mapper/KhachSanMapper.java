package com.group3.finmansys.service.mapper;

import com.group3.finmansys.domain.*;
import com.group3.finmansys.service.dto.KhachSanDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link KhachSan} and its DTO {@link KhachSanDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface KhachSanMapper extends EntityMapper<KhachSanDTO, KhachSan> {
    @Named("ten")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    @Mapping(target = "ten", source = "ten")
    KhachSanDTO toDtoTen(KhachSan khachSan);
}
