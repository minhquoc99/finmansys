package com.group3.finmansys.domain;

import static org.assertj.core.api.Assertions.assertThat;

import com.group3.finmansys.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class LoaiQuyTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(LoaiQuy.class);
        LoaiQuy loaiQuy1 = new LoaiQuy();
        loaiQuy1.setId(1L);
        LoaiQuy loaiQuy2 = new LoaiQuy();
        loaiQuy2.setId(loaiQuy1.getId());
        assertThat(loaiQuy1).isEqualTo(loaiQuy2);
        loaiQuy2.setId(2L);
        assertThat(loaiQuy1).isNotEqualTo(loaiQuy2);
        loaiQuy1.setId(null);
        assertThat(loaiQuy1).isNotEqualTo(loaiQuy2);
    }
}
