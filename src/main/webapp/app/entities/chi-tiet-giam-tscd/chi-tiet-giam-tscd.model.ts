import * as dayjs from 'dayjs';
import { ITaiSanCD } from 'app/entities/tai-san-cd/tai-san-cd.model';

export interface IChiTietGiamTSCD {
  id?: number;
  ngayMua?: dayjs.Dayjs;
  soLuong?: number;
  khauHao?: number | null;
  soTienThanhLy?: number;
  chiPhiMua?: number;
  taiSanCD?: ITaiSanCD;
}

export class ChiTietGiamTSCD implements IChiTietGiamTSCD {
  constructor(
    public id?: number,
    public ngayMua?: dayjs.Dayjs,
    public soLuong?: number,
    public khauHao?: number | null,
    public soTienThanhLy?: number,
    public chiPhiMua?: number,
    public taiSanCD?: ITaiSanCD
  ) {}
}

export function getChiTietGiamTSCDIdentifier(chiTietGiamTSCD: IChiTietGiamTSCD): number | undefined {
  return chiTietGiamTSCD.id;
}
