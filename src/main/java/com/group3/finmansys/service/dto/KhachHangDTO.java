package com.group3.finmansys.service.dto;

import java.io.Serializable;
import java.util.Objects;
import javax.validation.constraints.*;

/**
 * A DTO for the {@link com.group3.finmansys.domain.KhachHang} entity.
 */
public class KhachHangDTO implements Serializable {

    private Long id;

    @NotNull
    private String ten;

    private String diaChi;

    private String email;

    private String sdt;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTen() {
        return ten;
    }

    public void setTen(String ten) {
        this.ten = ten;
    }

    public String getDiaChi() {
        return diaChi;
    }

    public void setDiaChi(String diaChi) {
        this.diaChi = diaChi;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getSdt() {
        return sdt;
    }

    public void setSdt(String sdt) {
        this.sdt = sdt;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof KhachHangDTO)) {
            return false;
        }

        KhachHangDTO khachHangDTO = (KhachHangDTO) o;
        if (this.id == null) {
            return false;
        }
        return Objects.equals(this.id, khachHangDTO.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.id);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "KhachHangDTO{" +
            "id=" + getId() +
            ", ten='" + getTen() + "'" +
            ", diaChi='" + getDiaChi() + "'" +
            ", email='" + getEmail() + "'" +
            ", sdt='" + getSdt() + "'" +
            "}";
    }
}
