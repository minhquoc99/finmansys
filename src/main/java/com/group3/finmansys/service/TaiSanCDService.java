package com.group3.finmansys.service;

import com.group3.finmansys.service.dto.TaiSanCDDTO;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Service Interface for managing {@link com.group3.finmansys.domain.TaiSanCD}.
 */
public interface TaiSanCDService {
    /**
     * Save a taiSanCD.
     *
     * @param taiSanCDDTO the entity to save.
     * @return the persisted entity.
     */
    TaiSanCDDTO save(TaiSanCDDTO taiSanCDDTO);

    /**
     * Partially updates a taiSanCD.
     *
     * @param taiSanCDDTO the entity to update partially.
     * @return the persisted entity.
     */
    Optional<TaiSanCDDTO> partialUpdate(TaiSanCDDTO taiSanCDDTO);

    /**
     * Get all the taiSanCDS.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<TaiSanCDDTO> findAll(Pageable pageable);

    /**
     * Get the "id" taiSanCD.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<TaiSanCDDTO> findOne(Long id);

    /**
     * Delete the "id" taiSanCD.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
