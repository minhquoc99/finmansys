import { Component, Input } from '@angular/core';

@Component({
  selector: 'jhi-my-tab',
  templateUrl: './my-tab.component.html',
  styleUrls: ['./my-tab.component.scss'],
})
export class MyTabComponent {
  @Input() tabTitle: string | undefined;
  @Input() active = false;
}
