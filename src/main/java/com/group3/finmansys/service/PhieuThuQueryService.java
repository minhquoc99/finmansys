package com.group3.finmansys.service;

import com.group3.finmansys.domain.*; // for static metamodels
import com.group3.finmansys.domain.PhieuThu;
import com.group3.finmansys.repository.PhieuThuRepository;
import com.group3.finmansys.service.criteria.PhieuThuCriteria;
import com.group3.finmansys.service.dto.PhieuThuDTO;
import com.group3.finmansys.service.mapper.PhieuThuMapper;
import java.util.List;
import javax.persistence.criteria.JoinType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tech.jhipster.service.QueryService;

/**
 * Service for executing complex queries for {@link PhieuThu} entities in the database.
 * The main input is a {@link PhieuThuCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link PhieuThuDTO} or a {@link Page} of {@link PhieuThuDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class PhieuThuQueryService extends QueryService<PhieuThu> {

    private final Logger log = LoggerFactory.getLogger(PhieuThuQueryService.class);

    private final PhieuThuRepository phieuThuRepository;

    private final PhieuThuMapper phieuThuMapper;

    public PhieuThuQueryService(PhieuThuRepository phieuThuRepository, PhieuThuMapper phieuThuMapper) {
        this.phieuThuRepository = phieuThuRepository;
        this.phieuThuMapper = phieuThuMapper;
    }

    /**
     * Return a {@link List} of {@link PhieuThuDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<PhieuThuDTO> findByCriteria(PhieuThuCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<PhieuThu> specification = createSpecification(criteria);
        return phieuThuMapper.toDto(phieuThuRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link PhieuThuDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<PhieuThuDTO> findByCriteria(PhieuThuCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<PhieuThu> specification = createSpecification(criteria);
        return phieuThuRepository.findAll(specification, page).map(phieuThuMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(PhieuThuCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<PhieuThu> specification = createSpecification(criteria);
        return phieuThuRepository.count(specification);
    }

    /**
     * Function to convert {@link PhieuThuCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<PhieuThu> createSpecification(PhieuThuCriteria criteria) {
        Specification<PhieuThu> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getId(), PhieuThu_.id));
            }
            if (criteria.getNgayChungTu() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getNgayChungTu(), PhieuThu_.ngayChungTu));
            }
            if (criteria.getSoTien() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getSoTien(), PhieuThu_.soTien));
            }
            if (criteria.getKhoanPhaiThuId() != null) {
                specification =
                    specification.and(
                        buildSpecification(
                            criteria.getKhoanPhaiThuId(),
                            root -> root.join(PhieuThu_.khoanPhaiThu, JoinType.LEFT).get(KhoanPhaiThu_.id)
                        )
                    );
            }
            if (criteria.getQuyTienId() != null) {
                specification =
                    specification.and(
                        buildSpecification(criteria.getQuyTienId(), root -> root.join(PhieuThu_.quyTien, JoinType.LEFT).get(QuyTien_.id))
                    );
            }
            if (criteria.getKhachHangId() != null) {
                specification =
                    specification.and(
                        buildSpecification(
                            criteria.getKhachHangId(),
                            root -> root.join(PhieuThu_.khachHang, JoinType.LEFT).get(KhachHang_.id)
                        )
                    );
            }
        }
        return specification;
    }
}
