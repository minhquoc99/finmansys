import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { UserRouteAccessService } from 'app/core/auth/user-route-access.service';
import { KhoanMucChiPhiComponent } from '../list/khoan-muc-chi-phi.component';
import { KhoanMucChiPhiDetailComponent } from '../detail/khoan-muc-chi-phi-detail.component';
import { KhoanMucChiPhiUpdateComponent } from '../update/khoan-muc-chi-phi-update.component';
import { KhoanMucChiPhiRoutingResolveService } from './khoan-muc-chi-phi-routing-resolve.service';

const khoanMucChiPhiRoute: Routes = [
  {
    path: '',
    component: KhoanMucChiPhiComponent,
    data: {
      defaultSort: 'id,asc',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: KhoanMucChiPhiDetailComponent,
    resolve: {
      khoanMucChiPhi: KhoanMucChiPhiRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: KhoanMucChiPhiUpdateComponent,
    resolve: {
      khoanMucChiPhi: KhoanMucChiPhiRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: KhoanMucChiPhiUpdateComponent,
    resolve: {
      khoanMucChiPhi: KhoanMucChiPhiRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
];

@NgModule({
  imports: [RouterModule.forChild(khoanMucChiPhiRoute)],
  exports: [RouterModule],
})
export class KhoanMucChiPhiRoutingModule {}
