import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { mergeMap } from 'rxjs/operators';

import { ICongThuc, CongThuc } from '../cong-thuc.model';
import { CongThucService } from '../service/cong-thuc.service';

@Injectable({ providedIn: 'root' })
export class CongThucRoutingResolveService implements Resolve<ICongThuc> {
  constructor(protected service: CongThucService, protected router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<ICongThuc> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        mergeMap((congThuc: HttpResponse<CongThuc>) => {
          if (congThuc.body) {
            return of(congThuc.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new CongThuc());
  }
}
