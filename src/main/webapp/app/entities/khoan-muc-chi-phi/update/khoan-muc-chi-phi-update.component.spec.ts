jest.mock('@angular/router');

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { of, Subject } from 'rxjs';

import { KhoanMucChiPhiService } from '../service/khoan-muc-chi-phi.service';
import { IKhoanMucChiPhi, KhoanMucChiPhi } from '../khoan-muc-chi-phi.model';

import { KhoanMucChiPhiUpdateComponent } from './khoan-muc-chi-phi-update.component';

describe('Component Tests', () => {
  describe('KhoanMucChiPhi Management Update Component', () => {
    let comp: KhoanMucChiPhiUpdateComponent;
    let fixture: ComponentFixture<KhoanMucChiPhiUpdateComponent>;
    let activatedRoute: ActivatedRoute;
    let khoanMucChiPhiService: KhoanMucChiPhiService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule],
        declarations: [KhoanMucChiPhiUpdateComponent],
        providers: [FormBuilder, ActivatedRoute],
      })
        .overrideTemplate(KhoanMucChiPhiUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(KhoanMucChiPhiUpdateComponent);
      activatedRoute = TestBed.inject(ActivatedRoute);
      khoanMucChiPhiService = TestBed.inject(KhoanMucChiPhiService);

      comp = fixture.componentInstance;
    });

    describe('ngOnInit', () => {
      it('Should update editForm', () => {
        const khoanMucChiPhi: IKhoanMucChiPhi = { id: 456 };

        activatedRoute.data = of({ khoanMucChiPhi });
        comp.ngOnInit();

        expect(comp.editForm.value).toEqual(expect.objectContaining(khoanMucChiPhi));
      });
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', () => {
        // GIVEN
        const saveSubject = new Subject();
        const khoanMucChiPhi = { id: 123 };
        spyOn(khoanMucChiPhiService, 'update').and.returnValue(saveSubject);
        spyOn(comp, 'previousState');
        activatedRoute.data = of({ khoanMucChiPhi });
        comp.ngOnInit();

        // WHEN
        comp.save();
        expect(comp.isSaving).toEqual(true);
        saveSubject.next(new HttpResponse({ body: khoanMucChiPhi }));
        saveSubject.complete();

        // THEN
        expect(comp.previousState).toHaveBeenCalled();
        expect(khoanMucChiPhiService.update).toHaveBeenCalledWith(khoanMucChiPhi);
        expect(comp.isSaving).toEqual(false);
      });

      it('Should call create service on save for new entity', () => {
        // GIVEN
        const saveSubject = new Subject();
        const khoanMucChiPhi = new KhoanMucChiPhi();
        spyOn(khoanMucChiPhiService, 'create').and.returnValue(saveSubject);
        spyOn(comp, 'previousState');
        activatedRoute.data = of({ khoanMucChiPhi });
        comp.ngOnInit();

        // WHEN
        comp.save();
        expect(comp.isSaving).toEqual(true);
        saveSubject.next(new HttpResponse({ body: khoanMucChiPhi }));
        saveSubject.complete();

        // THEN
        expect(khoanMucChiPhiService.create).toHaveBeenCalledWith(khoanMucChiPhi);
        expect(comp.isSaving).toEqual(false);
        expect(comp.previousState).toHaveBeenCalled();
      });

      it('Should set isSaving to false on error', () => {
        // GIVEN
        const saveSubject = new Subject();
        const khoanMucChiPhi = { id: 123 };
        spyOn(khoanMucChiPhiService, 'update').and.returnValue(saveSubject);
        spyOn(comp, 'previousState');
        activatedRoute.data = of({ khoanMucChiPhi });
        comp.ngOnInit();

        // WHEN
        comp.save();
        expect(comp.isSaving).toEqual(true);
        saveSubject.error('This is an error!');

        // THEN
        expect(khoanMucChiPhiService.update).toHaveBeenCalledWith(khoanMucChiPhi);
        expect(comp.isSaving).toEqual(false);
        expect(comp.previousState).not.toHaveBeenCalled();
      });
    });
  });
});
