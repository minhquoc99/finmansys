jest.mock('@angular/router');

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { of, Subject } from 'rxjs';

import { ChiTietTangTSCDService } from '../service/chi-tiet-tang-tscd.service';
import { IChiTietTangTSCD, ChiTietTangTSCD } from '../chi-tiet-tang-tscd.model';
import { ITaiSanCD } from 'app/entities/tai-san-cd/tai-san-cd.model';
import { TaiSanCDService } from 'app/entities/tai-san-cd/service/tai-san-cd.service';

import { ChiTietTangTSCDUpdateComponent } from './chi-tiet-tang-tscd-update.component';

describe('Component Tests', () => {
  describe('ChiTietTangTSCD Management Update Component', () => {
    let comp: ChiTietTangTSCDUpdateComponent;
    let fixture: ComponentFixture<ChiTietTangTSCDUpdateComponent>;
    let activatedRoute: ActivatedRoute;
    let chiTietTangTSCDService: ChiTietTangTSCDService;
    let taiSanCDService: TaiSanCDService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule],
        declarations: [ChiTietTangTSCDUpdateComponent],
        providers: [FormBuilder, ActivatedRoute],
      })
        .overrideTemplate(ChiTietTangTSCDUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(ChiTietTangTSCDUpdateComponent);
      activatedRoute = TestBed.inject(ActivatedRoute);
      chiTietTangTSCDService = TestBed.inject(ChiTietTangTSCDService);
      taiSanCDService = TestBed.inject(TaiSanCDService);

      comp = fixture.componentInstance;
    });

    describe('ngOnInit', () => {
      it('Should call TaiSanCD query and add missing value', () => {
        const chiTietTangTSCD: IChiTietTangTSCD = { id: 456 };
        const taiSanCD: ITaiSanCD = { id: 85303 };
        chiTietTangTSCD.taiSanCD = taiSanCD;

        const taiSanCDCollection: ITaiSanCD[] = [{ id: 59674 }];
        spyOn(taiSanCDService, 'query').and.returnValue(of(new HttpResponse({ body: taiSanCDCollection })));
        const additionalTaiSanCDS = [taiSanCD];
        const expectedCollection: ITaiSanCD[] = [...additionalTaiSanCDS, ...taiSanCDCollection];
        spyOn(taiSanCDService, 'addTaiSanCDToCollectionIfMissing').and.returnValue(expectedCollection);

        activatedRoute.data = of({ chiTietTangTSCD });
        comp.ngOnInit();

        expect(taiSanCDService.query).toHaveBeenCalled();
        expect(taiSanCDService.addTaiSanCDToCollectionIfMissing).toHaveBeenCalledWith(taiSanCDCollection, ...additionalTaiSanCDS);
        expect(comp.taiSanCDSSharedCollection).toEqual(expectedCollection);
      });

      it('Should update editForm', () => {
        const chiTietTangTSCD: IChiTietTangTSCD = { id: 456 };
        const taiSanCD: ITaiSanCD = { id: 32931 };
        chiTietTangTSCD.taiSanCD = taiSanCD;

        activatedRoute.data = of({ chiTietTangTSCD });
        comp.ngOnInit();

        expect(comp.editForm.value).toEqual(expect.objectContaining(chiTietTangTSCD));
        expect(comp.taiSanCDSSharedCollection).toContain(taiSanCD);
      });
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', () => {
        // GIVEN
        const saveSubject = new Subject();
        const chiTietTangTSCD = { id: 123 };
        spyOn(chiTietTangTSCDService, 'update').and.returnValue(saveSubject);
        spyOn(comp, 'previousState');
        activatedRoute.data = of({ chiTietTangTSCD });
        comp.ngOnInit();

        // WHEN
        comp.save();
        expect(comp.isSaving).toEqual(true);
        saveSubject.next(new HttpResponse({ body: chiTietTangTSCD }));
        saveSubject.complete();

        // THEN
        expect(comp.previousState).toHaveBeenCalled();
        expect(chiTietTangTSCDService.update).toHaveBeenCalledWith(chiTietTangTSCD);
        expect(comp.isSaving).toEqual(false);
      });

      it('Should call create service on save for new entity', () => {
        // GIVEN
        const saveSubject = new Subject();
        const chiTietTangTSCD = new ChiTietTangTSCD();
        spyOn(chiTietTangTSCDService, 'create').and.returnValue(saveSubject);
        spyOn(comp, 'previousState');
        activatedRoute.data = of({ chiTietTangTSCD });
        comp.ngOnInit();

        // WHEN
        comp.save();
        expect(comp.isSaving).toEqual(true);
        saveSubject.next(new HttpResponse({ body: chiTietTangTSCD }));
        saveSubject.complete();

        // THEN
        expect(chiTietTangTSCDService.create).toHaveBeenCalledWith(chiTietTangTSCD);
        expect(comp.isSaving).toEqual(false);
        expect(comp.previousState).toHaveBeenCalled();
      });

      it('Should set isSaving to false on error', () => {
        // GIVEN
        const saveSubject = new Subject();
        const chiTietTangTSCD = { id: 123 };
        spyOn(chiTietTangTSCDService, 'update').and.returnValue(saveSubject);
        spyOn(comp, 'previousState');
        activatedRoute.data = of({ chiTietTangTSCD });
        comp.ngOnInit();

        // WHEN
        comp.save();
        expect(comp.isSaving).toEqual(true);
        saveSubject.error('This is an error!');

        // THEN
        expect(chiTietTangTSCDService.update).toHaveBeenCalledWith(chiTietTangTSCD);
        expect(comp.isSaving).toEqual(false);
        expect(comp.previousState).not.toHaveBeenCalled();
      });
    });

    describe('Tracking relationships identifiers', () => {
      describe('trackTaiSanCDById', () => {
        it('Should return tracked TaiSanCD primary key', () => {
          const entity = { id: 123 };
          const trackResult = comp.trackTaiSanCDById(0, entity);
          expect(trackResult).toEqual(entity.id);
        });
      });
    });
  });
});
