import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { mergeMap } from 'rxjs/operators';

import { ILoaiTaiSan, LoaiTaiSan } from '../loai-tai-san.model';
import { LoaiTaiSanService } from '../service/loai-tai-san.service';

@Injectable({ providedIn: 'root' })
export class LoaiTaiSanRoutingResolveService implements Resolve<ILoaiTaiSan> {
  constructor(protected service: LoaiTaiSanService, protected router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<ILoaiTaiSan> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        mergeMap((loaiTaiSan: HttpResponse<LoaiTaiSan>) => {
          if (loaiTaiSan.body) {
            return of(loaiTaiSan.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new LoaiTaiSan());
  }
}
