import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import * as dayjs from 'dayjs';

import { DATE_TIME_FORMAT } from 'app/config/input.constants';
import { IKhoanPhaiTra, KhoanPhaiTra } from '../khoan-phai-tra.model';

import { KhoanPhaiTraService } from './khoan-phai-tra.service';

describe('Service Tests', () => {
  describe('KhoanPhaiTra Service', () => {
    let service: KhoanPhaiTraService;
    let httpMock: HttpTestingController;
    let elemDefault: IKhoanPhaiTra;
    let expectedResult: IKhoanPhaiTra | IKhoanPhaiTra[] | boolean | null;
    let currentDate: dayjs.Dayjs;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule],
      });
      expectedResult = null;
      service = TestBed.inject(KhoanPhaiTraService);
      httpMock = TestBed.inject(HttpTestingController);
      currentDate = dayjs();

      elemDefault = {
        id: 0,
        ngayTao: currentDate,
        ngayPhaiTra: currentDate,
        soTien: 0,
        ghiChu: 'AAAAAAA',
      };
    });

    describe('Service methods', () => {
      it('should find an element', () => {
        const returnedFromService = Object.assign(
          {
            ngayTao: currentDate.format(DATE_TIME_FORMAT),
            ngayPhaiTra: currentDate.format(DATE_TIME_FORMAT),
          },
          elemDefault
        );

        service.find(123).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(elemDefault);
      });

      it('should create a KhoanPhaiTra', () => {
        const returnedFromService = Object.assign(
          {
            id: 0,
            ngayTao: currentDate.format(DATE_TIME_FORMAT),
            ngayPhaiTra: currentDate.format(DATE_TIME_FORMAT),
          },
          elemDefault
        );

        const expected = Object.assign(
          {
            ngayTao: currentDate,
            ngayPhaiTra: currentDate,
          },
          returnedFromService
        );

        service.create(new KhoanPhaiTra()).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'POST' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should update a KhoanPhaiTra', () => {
        const returnedFromService = Object.assign(
          {
            id: 1,
            ngayTao: currentDate.format(DATE_TIME_FORMAT),
            ngayPhaiTra: currentDate.format(DATE_TIME_FORMAT),
            soTien: 1,
            ghiChu: 'BBBBBB',
          },
          elemDefault
        );

        const expected = Object.assign(
          {
            ngayTao: currentDate,
            ngayPhaiTra: currentDate,
          },
          returnedFromService
        );

        service.update(expected).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'PUT' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should partial update a KhoanPhaiTra', () => {
        const patchObject = Object.assign(
          {
            ghiChu: 'BBBBBB',
          },
          new KhoanPhaiTra()
        );

        const returnedFromService = Object.assign(patchObject, elemDefault);

        const expected = Object.assign(
          {
            ngayTao: currentDate,
            ngayPhaiTra: currentDate,
          },
          returnedFromService
        );

        service.partialUpdate(patchObject).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'PATCH' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should return a list of KhoanPhaiTra', () => {
        const returnedFromService = Object.assign(
          {
            id: 1,
            ngayTao: currentDate.format(DATE_TIME_FORMAT),
            ngayPhaiTra: currentDate.format(DATE_TIME_FORMAT),
            soTien: 1,
            ghiChu: 'BBBBBB',
          },
          elemDefault
        );

        const expected = Object.assign(
          {
            ngayTao: currentDate,
            ngayPhaiTra: currentDate,
          },
          returnedFromService
        );

        service.query().subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush([returnedFromService]);
        httpMock.verify();
        expect(expectedResult).toContainEqual(expected);
      });

      it('should delete a KhoanPhaiTra', () => {
        service.delete(123).subscribe(resp => (expectedResult = resp.ok));

        const req = httpMock.expectOne({ method: 'DELETE' });
        req.flush({ status: 200 });
        expect(expectedResult);
      });

      describe('addKhoanPhaiTraToCollectionIfMissing', () => {
        it('should add a KhoanPhaiTra to an empty array', () => {
          const khoanPhaiTra: IKhoanPhaiTra = { id: 123 };
          expectedResult = service.addKhoanPhaiTraToCollectionIfMissing([], khoanPhaiTra);
          expect(expectedResult).toHaveLength(1);
          expect(expectedResult).toContain(khoanPhaiTra);
        });

        it('should not add a KhoanPhaiTra to an array that contains it', () => {
          const khoanPhaiTra: IKhoanPhaiTra = { id: 123 };
          const khoanPhaiTraCollection: IKhoanPhaiTra[] = [
            {
              ...khoanPhaiTra,
            },
            { id: 456 },
          ];
          expectedResult = service.addKhoanPhaiTraToCollectionIfMissing(khoanPhaiTraCollection, khoanPhaiTra);
          expect(expectedResult).toHaveLength(2);
        });

        it("should add a KhoanPhaiTra to an array that doesn't contain it", () => {
          const khoanPhaiTra: IKhoanPhaiTra = { id: 123 };
          const khoanPhaiTraCollection: IKhoanPhaiTra[] = [{ id: 456 }];
          expectedResult = service.addKhoanPhaiTraToCollectionIfMissing(khoanPhaiTraCollection, khoanPhaiTra);
          expect(expectedResult).toHaveLength(2);
          expect(expectedResult).toContain(khoanPhaiTra);
        });

        it('should add only unique KhoanPhaiTra to an array', () => {
          const khoanPhaiTraArray: IKhoanPhaiTra[] = [{ id: 123 }, { id: 456 }, { id: 15270 }];
          const khoanPhaiTraCollection: IKhoanPhaiTra[] = [{ id: 123 }];
          expectedResult = service.addKhoanPhaiTraToCollectionIfMissing(khoanPhaiTraCollection, ...khoanPhaiTraArray);
          expect(expectedResult).toHaveLength(3);
        });

        it('should accept varargs', () => {
          const khoanPhaiTra: IKhoanPhaiTra = { id: 123 };
          const khoanPhaiTra2: IKhoanPhaiTra = { id: 456 };
          expectedResult = service.addKhoanPhaiTraToCollectionIfMissing([], khoanPhaiTra, khoanPhaiTra2);
          expect(expectedResult).toHaveLength(2);
          expect(expectedResult).toContain(khoanPhaiTra);
          expect(expectedResult).toContain(khoanPhaiTra2);
        });

        it('should accept null and undefined values', () => {
          const khoanPhaiTra: IKhoanPhaiTra = { id: 123 };
          expectedResult = service.addKhoanPhaiTraToCollectionIfMissing([], null, khoanPhaiTra, undefined);
          expect(expectedResult).toHaveLength(1);
          expect(expectedResult).toContain(khoanPhaiTra);
        });
      });
    });

    afterEach(() => {
      httpMock.verify();
    });
  });
});
