import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { UserRouteAccessService } from 'app/core/auth/user-route-access.service';
import { MaKeToanComponent } from '../list/ma-ke-toan.component';
import { MaKeToanDetailComponent } from '../detail/ma-ke-toan-detail.component';
import { MaKeToanUpdateComponent } from '../update/ma-ke-toan-update.component';
import { MaKeToanRoutingResolveService } from './ma-ke-toan-routing-resolve.service';

const maKeToanRoute: Routes = [
  {
    path: '',
    component: MaKeToanComponent,
    data: {
      defaultSort: 'id,asc',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: MaKeToanDetailComponent,
    resolve: {
      maKeToan: MaKeToanRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: MaKeToanUpdateComponent,
    resolve: {
      maKeToan: MaKeToanRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: MaKeToanUpdateComponent,
    resolve: {
      maKeToan: MaKeToanRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
];

@NgModule({
  imports: [RouterModule.forChild(maKeToanRoute)],
  exports: [RouterModule],
})
export class MaKeToanRoutingModule {}
