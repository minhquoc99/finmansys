jest.mock('@angular/router');

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { of, Subject } from 'rxjs';

import { LoaiTaiSanService } from '../service/loai-tai-san.service';
import { ILoaiTaiSan, LoaiTaiSan } from '../loai-tai-san.model';
import { IMaKeToan } from 'app/entities/ma-ke-toan/ma-ke-toan.model';
import { MaKeToanService } from 'app/entities/ma-ke-toan/service/ma-ke-toan.service';

import { LoaiTaiSanUpdateComponent } from './loai-tai-san-update.component';

describe('Component Tests', () => {
  describe('LoaiTaiSan Management Update Component', () => {
    let comp: LoaiTaiSanUpdateComponent;
    let fixture: ComponentFixture<LoaiTaiSanUpdateComponent>;
    let activatedRoute: ActivatedRoute;
    let loaiTaiSanService: LoaiTaiSanService;
    let maKeToanService: MaKeToanService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule],
        declarations: [LoaiTaiSanUpdateComponent],
        providers: [FormBuilder, ActivatedRoute],
      })
        .overrideTemplate(LoaiTaiSanUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(LoaiTaiSanUpdateComponent);
      activatedRoute = TestBed.inject(ActivatedRoute);
      loaiTaiSanService = TestBed.inject(LoaiTaiSanService);
      maKeToanService = TestBed.inject(MaKeToanService);

      comp = fixture.componentInstance;
    });

    describe('ngOnInit', () => {
      it('Should call MaKeToan query and add missing value', () => {
        const loaiTaiSan: ILoaiTaiSan = { id: 456 };
        const maKeToan: IMaKeToan = { id: 53263 };
        loaiTaiSan.maKeToan = maKeToan;

        const maKeToanCollection: IMaKeToan[] = [{ id: 10718 }];
        spyOn(maKeToanService, 'query').and.returnValue(of(new HttpResponse({ body: maKeToanCollection })));
        const additionalMaKeToans = [maKeToan];
        const expectedCollection: IMaKeToan[] = [...additionalMaKeToans, ...maKeToanCollection];
        spyOn(maKeToanService, 'addMaKeToanToCollectionIfMissing').and.returnValue(expectedCollection);

        activatedRoute.data = of({ loaiTaiSan });
        comp.ngOnInit();

        expect(maKeToanService.query).toHaveBeenCalled();
        expect(maKeToanService.addMaKeToanToCollectionIfMissing).toHaveBeenCalledWith(maKeToanCollection, ...additionalMaKeToans);
        expect(comp.maKeToansSharedCollection).toEqual(expectedCollection);
      });

      it('Should update editForm', () => {
        const loaiTaiSan: ILoaiTaiSan = { id: 456 };
        const maKeToan: IMaKeToan = { id: 25204 };
        loaiTaiSan.maKeToan = maKeToan;

        activatedRoute.data = of({ loaiTaiSan });
        comp.ngOnInit();

        expect(comp.editForm.value).toEqual(expect.objectContaining(loaiTaiSan));
        expect(comp.maKeToansSharedCollection).toContain(maKeToan);
      });
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', () => {
        // GIVEN
        const saveSubject = new Subject();
        const loaiTaiSan = { id: 123 };
        spyOn(loaiTaiSanService, 'update').and.returnValue(saveSubject);
        spyOn(comp, 'previousState');
        activatedRoute.data = of({ loaiTaiSan });
        comp.ngOnInit();

        // WHEN
        comp.save();
        expect(comp.isSaving).toEqual(true);
        saveSubject.next(new HttpResponse({ body: loaiTaiSan }));
        saveSubject.complete();

        // THEN
        expect(comp.previousState).toHaveBeenCalled();
        expect(loaiTaiSanService.update).toHaveBeenCalledWith(loaiTaiSan);
        expect(comp.isSaving).toEqual(false);
      });

      it('Should call create service on save for new entity', () => {
        // GIVEN
        const saveSubject = new Subject();
        const loaiTaiSan = new LoaiTaiSan();
        spyOn(loaiTaiSanService, 'create').and.returnValue(saveSubject);
        spyOn(comp, 'previousState');
        activatedRoute.data = of({ loaiTaiSan });
        comp.ngOnInit();

        // WHEN
        comp.save();
        expect(comp.isSaving).toEqual(true);
        saveSubject.next(new HttpResponse({ body: loaiTaiSan }));
        saveSubject.complete();

        // THEN
        expect(loaiTaiSanService.create).toHaveBeenCalledWith(loaiTaiSan);
        expect(comp.isSaving).toEqual(false);
        expect(comp.previousState).toHaveBeenCalled();
      });

      it('Should set isSaving to false on error', () => {
        // GIVEN
        const saveSubject = new Subject();
        const loaiTaiSan = { id: 123 };
        spyOn(loaiTaiSanService, 'update').and.returnValue(saveSubject);
        spyOn(comp, 'previousState');
        activatedRoute.data = of({ loaiTaiSan });
        comp.ngOnInit();

        // WHEN
        comp.save();
        expect(comp.isSaving).toEqual(true);
        saveSubject.error('This is an error!');

        // THEN
        expect(loaiTaiSanService.update).toHaveBeenCalledWith(loaiTaiSan);
        expect(comp.isSaving).toEqual(false);
        expect(comp.previousState).not.toHaveBeenCalled();
      });
    });

    describe('Tracking relationships identifiers', () => {
      describe('trackMaKeToanById', () => {
        it('Should return tracked MaKeToan primary key', () => {
          const entity = { id: 123 };
          const trackResult = comp.trackMaKeToanById(0, entity);
          expect(trackResult).toEqual(entity.id);
        });
      });
    });
  });
});
