package com.group3.finmansys.domain;

import static org.assertj.core.api.Assertions.assertThat;

import com.group3.finmansys.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class DichVuSuDungTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(DichVuSuDung.class);
        DichVuSuDung dichVuSuDung1 = new DichVuSuDung();
        dichVuSuDung1.setId(1L);
        DichVuSuDung dichVuSuDung2 = new DichVuSuDung();
        dichVuSuDung2.setId(dichVuSuDung1.getId());
        assertThat(dichVuSuDung1).isEqualTo(dichVuSuDung2);
        dichVuSuDung2.setId(2L);
        assertThat(dichVuSuDung1).isNotEqualTo(dichVuSuDung2);
        dichVuSuDung1.setId(null);
        assertThat(dichVuSuDung1).isNotEqualTo(dichVuSuDung2);
    }
}
