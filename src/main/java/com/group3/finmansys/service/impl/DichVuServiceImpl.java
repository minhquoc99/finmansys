package com.group3.finmansys.service.impl;

import com.group3.finmansys.domain.DichVu;
import com.group3.finmansys.repository.DichVuRepository;
import com.group3.finmansys.service.DichVuService;
import com.group3.finmansys.service.dto.DichVuDTO;
import com.group3.finmansys.service.mapper.DichVuMapper;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link DichVu}.
 */
@Service
@Transactional
public class DichVuServiceImpl implements DichVuService {

    private final Logger log = LoggerFactory.getLogger(DichVuServiceImpl.class);

    private final DichVuRepository dichVuRepository;

    private final DichVuMapper dichVuMapper;

    public DichVuServiceImpl(DichVuRepository dichVuRepository, DichVuMapper dichVuMapper) {
        this.dichVuRepository = dichVuRepository;
        this.dichVuMapper = dichVuMapper;
    }

    @Override
    public DichVuDTO save(DichVuDTO dichVuDTO) {
        log.debug("Request to save DichVu : {}", dichVuDTO);
        DichVu dichVu = dichVuMapper.toEntity(dichVuDTO);
        dichVu = dichVuRepository.save(dichVu);
        return dichVuMapper.toDto(dichVu);
    }

    @Override
    public Optional<DichVuDTO> partialUpdate(DichVuDTO dichVuDTO) {
        log.debug("Request to partially update DichVu : {}", dichVuDTO);

        return dichVuRepository
            .findById(dichVuDTO.getId())
            .map(
                existingDichVu -> {
                    dichVuMapper.partialUpdate(existingDichVu, dichVuDTO);
                    return existingDichVu;
                }
            )
            .map(dichVuRepository::save)
            .map(dichVuMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<DichVuDTO> findAll(Pageable pageable) {
        log.debug("Request to get all DichVus");
        return dichVuRepository.findAll(pageable).map(dichVuMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<DichVuDTO> findOne(Long id) {
        log.debug("Request to get DichVu : {}", id);
        return dichVuRepository.findById(id).map(dichVuMapper::toDto);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete DichVu : {}", id);
        dichVuRepository.deleteById(id);
    }
}
