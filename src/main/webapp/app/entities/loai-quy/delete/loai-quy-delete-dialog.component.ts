import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import { ILoaiQuy } from '../loai-quy.model';
import { LoaiQuyService } from '../service/loai-quy.service';

@Component({
  templateUrl: './loai-quy-delete-dialog.component.html',
})
export class LoaiQuyDeleteDialogComponent {
  loaiQuy?: ILoaiQuy;

  constructor(protected loaiQuyService: LoaiQuyService, public activeModal: NgbActiveModal) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.loaiQuyService.delete(id).subscribe(() => {
      this.activeModal.close('deleted');
    });
  }
}
