package com.group3.finmansys.repository;

import com.group3.finmansys.domain.QuyTien;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data SQL repository for the QuyTien entity.
 */
@SuppressWarnings("unused")
@Repository
public interface QuyTienRepository extends JpaRepository<QuyTien, Long>, JpaSpecificationExecutor<QuyTien> {}
