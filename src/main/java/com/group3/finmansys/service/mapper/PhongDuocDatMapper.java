package com.group3.finmansys.service.mapper;

import com.group3.finmansys.domain.*;
import com.group3.finmansys.service.dto.PhongDuocDatDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link PhongDuocDat} and its DTO {@link PhongDuocDatDTO}.
 */
@Mapper(componentModel = "spring", uses = { KhachHangMapper.class, PhongDatMapper.class, DichVuSuDungMapper.class })
public interface PhongDuocDatMapper extends EntityMapper<PhongDuocDatDTO, PhongDuocDat> {
    @Mapping(target = "khachHang", source = "khachHang", qualifiedByName = "ten")
    PhongDuocDatDTO toDto(PhongDuocDat s);

    @Named("id")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    PhongDuocDatDTO toDtoId(PhongDuocDat phongDuocDat);
}
