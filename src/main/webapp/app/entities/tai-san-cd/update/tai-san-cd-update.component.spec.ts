jest.mock('@angular/router');

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { of, Subject } from 'rxjs';

import { TaiSanCDService } from '../service/tai-san-cd.service';
import { ITaiSanCD, TaiSanCD } from '../tai-san-cd.model';
import { IKhachSan } from 'app/entities/khach-san/khach-san.model';
import { KhachSanService } from 'app/entities/khach-san/service/khach-san.service';
import { ILoaiTaiSan } from 'app/entities/loai-tai-san/loai-tai-san.model';
import { LoaiTaiSanService } from 'app/entities/loai-tai-san/service/loai-tai-san.service';

import { TaiSanCDUpdateComponent } from './tai-san-cd-update.component';

describe('Component Tests', () => {
  describe('TaiSanCD Management Update Component', () => {
    let comp: TaiSanCDUpdateComponent;
    let fixture: ComponentFixture<TaiSanCDUpdateComponent>;
    let activatedRoute: ActivatedRoute;
    let taiSanCDService: TaiSanCDService;
    let khachSanService: KhachSanService;
    let loaiTaiSanService: LoaiTaiSanService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule],
        declarations: [TaiSanCDUpdateComponent],
        providers: [FormBuilder, ActivatedRoute],
      })
        .overrideTemplate(TaiSanCDUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(TaiSanCDUpdateComponent);
      activatedRoute = TestBed.inject(ActivatedRoute);
      taiSanCDService = TestBed.inject(TaiSanCDService);
      khachSanService = TestBed.inject(KhachSanService);
      loaiTaiSanService = TestBed.inject(LoaiTaiSanService);

      comp = fixture.componentInstance;
    });

    describe('ngOnInit', () => {
      it('Should call KhachSan query and add missing value', () => {
        const taiSanCD: ITaiSanCD = { id: 456 };
        const khachSan: IKhachSan = { id: 93831 };
        taiSanCD.khachSan = khachSan;

        const khachSanCollection: IKhachSan[] = [{ id: 19492 }];
        spyOn(khachSanService, 'query').and.returnValue(of(new HttpResponse({ body: khachSanCollection })));
        const additionalKhachSans = [khachSan];
        const expectedCollection: IKhachSan[] = [...additionalKhachSans, ...khachSanCollection];
        spyOn(khachSanService, 'addKhachSanToCollectionIfMissing').and.returnValue(expectedCollection);

        activatedRoute.data = of({ taiSanCD });
        comp.ngOnInit();

        expect(khachSanService.query).toHaveBeenCalled();
        expect(khachSanService.addKhachSanToCollectionIfMissing).toHaveBeenCalledWith(khachSanCollection, ...additionalKhachSans);
        expect(comp.khachSansSharedCollection).toEqual(expectedCollection);
      });

      it('Should call LoaiTaiSan query and add missing value', () => {
        const taiSanCD: ITaiSanCD = { id: 456 };
        const loaiTaiSan: ILoaiTaiSan = { id: 62822 };
        taiSanCD.loaiTaiSan = loaiTaiSan;

        const loaiTaiSanCollection: ILoaiTaiSan[] = [{ id: 23029 }];
        spyOn(loaiTaiSanService, 'query').and.returnValue(of(new HttpResponse({ body: loaiTaiSanCollection })));
        const additionalLoaiTaiSans = [loaiTaiSan];
        const expectedCollection: ILoaiTaiSan[] = [...additionalLoaiTaiSans, ...loaiTaiSanCollection];
        spyOn(loaiTaiSanService, 'addLoaiTaiSanToCollectionIfMissing').and.returnValue(expectedCollection);

        activatedRoute.data = of({ taiSanCD });
        comp.ngOnInit();

        expect(loaiTaiSanService.query).toHaveBeenCalled();
        expect(loaiTaiSanService.addLoaiTaiSanToCollectionIfMissing).toHaveBeenCalledWith(loaiTaiSanCollection, ...additionalLoaiTaiSans);
        expect(comp.loaiTaiSansSharedCollection).toEqual(expectedCollection);
      });

      it('Should update editForm', () => {
        const taiSanCD: ITaiSanCD = { id: 456 };
        const khachSan: IKhachSan = { id: 52768 };
        taiSanCD.khachSan = khachSan;
        const loaiTaiSan: ILoaiTaiSan = { id: 67382 };
        taiSanCD.loaiTaiSan = loaiTaiSan;

        activatedRoute.data = of({ taiSanCD });
        comp.ngOnInit();

        expect(comp.editForm.value).toEqual(expect.objectContaining(taiSanCD));
        expect(comp.khachSansSharedCollection).toContain(khachSan);
        expect(comp.loaiTaiSansSharedCollection).toContain(loaiTaiSan);
      });
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', () => {
        // GIVEN
        const saveSubject = new Subject();
        const taiSanCD = { id: 123 };
        spyOn(taiSanCDService, 'update').and.returnValue(saveSubject);
        spyOn(comp, 'previousState');
        activatedRoute.data = of({ taiSanCD });
        comp.ngOnInit();

        // WHEN
        comp.save();
        expect(comp.isSaving).toEqual(true);
        saveSubject.next(new HttpResponse({ body: taiSanCD }));
        saveSubject.complete();

        // THEN
        expect(comp.previousState).toHaveBeenCalled();
        expect(taiSanCDService.update).toHaveBeenCalledWith(taiSanCD);
        expect(comp.isSaving).toEqual(false);
      });

      it('Should call create service on save for new entity', () => {
        // GIVEN
        const saveSubject = new Subject();
        const taiSanCD = new TaiSanCD();
        spyOn(taiSanCDService, 'create').and.returnValue(saveSubject);
        spyOn(comp, 'previousState');
        activatedRoute.data = of({ taiSanCD });
        comp.ngOnInit();

        // WHEN
        comp.save();
        expect(comp.isSaving).toEqual(true);
        saveSubject.next(new HttpResponse({ body: taiSanCD }));
        saveSubject.complete();

        // THEN
        expect(taiSanCDService.create).toHaveBeenCalledWith(taiSanCD);
        expect(comp.isSaving).toEqual(false);
        expect(comp.previousState).toHaveBeenCalled();
      });

      it('Should set isSaving to false on error', () => {
        // GIVEN
        const saveSubject = new Subject();
        const taiSanCD = { id: 123 };
        spyOn(taiSanCDService, 'update').and.returnValue(saveSubject);
        spyOn(comp, 'previousState');
        activatedRoute.data = of({ taiSanCD });
        comp.ngOnInit();

        // WHEN
        comp.save();
        expect(comp.isSaving).toEqual(true);
        saveSubject.error('This is an error!');

        // THEN
        expect(taiSanCDService.update).toHaveBeenCalledWith(taiSanCD);
        expect(comp.isSaving).toEqual(false);
        expect(comp.previousState).not.toHaveBeenCalled();
      });
    });

    describe('Tracking relationships identifiers', () => {
      describe('trackKhachSanById', () => {
        it('Should return tracked KhachSan primary key', () => {
          const entity = { id: 123 };
          const trackResult = comp.trackKhachSanById(0, entity);
          expect(trackResult).toEqual(entity.id);
        });
      });

      describe('trackLoaiTaiSanById', () => {
        it('Should return tracked LoaiTaiSan primary key', () => {
          const entity = { id: 123 };
          const trackResult = comp.trackLoaiTaiSanById(0, entity);
          expect(trackResult).toEqual(entity.id);
        });
      });
    });
  });
});
