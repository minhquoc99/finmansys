import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IQuyTien } from '../quy-tien.model';

@Component({
  selector: 'jhi-quy-tien-detail',
  templateUrl: './quy-tien-detail.component.html',
})
export class QuyTienDetailComponent implements OnInit {
  quyTien: IQuyTien | null = null;

  soTienThu = 0;

  soTienChi = 0;

  soTienHienTai = 0;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ quyTien }) => {
      this.quyTien = quyTien;
      for (const phieuThu of quyTien.phieuThus) {
        this.soTienThu = this.soTienThu + Number(phieuThu.soTien);
      }

      for (const phieuChi of quyTien.phieuChis) {
        this.soTienChi = this.soTienChi + Number(phieuChi.soTien);
      }

      this.soTienHienTai = Number(quyTien.soTien) + this.soTienThu - this.soTienChi;
    });
  }

  previousState(): void {
    window.history.back();
  }
}
