import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { NganSachNamDetailComponent } from './ngan-sach-nam-detail.component';

describe('Component Tests', () => {
  describe('NganSachNam Management Detail Component', () => {
    let comp: NganSachNamDetailComponent;
    let fixture: ComponentFixture<NganSachNamDetailComponent>;

    beforeEach(() => {
      TestBed.configureTestingModule({
        declarations: [NganSachNamDetailComponent],
        providers: [
          {
            provide: ActivatedRoute,
            useValue: { data: of({ nganSachNam: { id: 123 } }) },
          },
        ],
      })
        .overrideTemplate(NganSachNamDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(NganSachNamDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should load nganSachNam on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.nganSachNam).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
