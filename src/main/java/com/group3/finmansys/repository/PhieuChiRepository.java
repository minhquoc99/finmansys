package com.group3.finmansys.repository;

import com.group3.finmansys.domain.PhieuChi;
import java.time.Instant;
import java.util.Date;
import java.util.List;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data SQL repository for the PhieuChi entity.
 */
@SuppressWarnings("unused")
@Repository
public interface PhieuChiRepository extends JpaRepository<PhieuChi, Long>, JpaSpecificationExecutor<PhieuChi> {
    @Query(
        "SELECT pc.khoanMucChiPhi.id, SUM(pc.soTien) as tongTien " +
        "   FROM PhieuChi pc " +
        "   WHERE pc.ngayChungTu >= ?1  " +
        "   AND pc.ngayChungTu <= ?2 " +
        "   GROUP BY pc.khoanMucChiPhi.id"
    )
    List<Object[]> getChiTieu(Instant startDate, Instant endDate);
}
