jest.mock('@angular/router');

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { of, Subject } from 'rxjs';

import { DichVuSuDungService } from '../service/dich-vu-su-dung.service';
import { IDichVuSuDung, DichVuSuDung } from '../dich-vu-su-dung.model';
import { IDichVu } from 'app/entities/dich-vu/dich-vu.model';
import { DichVuService } from 'app/entities/dich-vu/service/dich-vu.service';
import { IPhongDuocDat } from 'app/entities/phong-duoc-dat/phong-duoc-dat.model';
import { PhongDuocDatService } from 'app/entities/phong-duoc-dat/service/phong-duoc-dat.service';

import { DichVuSuDungUpdateComponent } from './dich-vu-su-dung-update.component';

describe('Component Tests', () => {
  describe('DichVuSuDung Management Update Component', () => {
    let comp: DichVuSuDungUpdateComponent;
    let fixture: ComponentFixture<DichVuSuDungUpdateComponent>;
    let activatedRoute: ActivatedRoute;
    let dichVuSuDungService: DichVuSuDungService;
    let dichVuService: DichVuService;
    let phongDuocDatService: PhongDuocDatService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule],
        declarations: [DichVuSuDungUpdateComponent],
        providers: [FormBuilder, ActivatedRoute],
      })
        .overrideTemplate(DichVuSuDungUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(DichVuSuDungUpdateComponent);
      activatedRoute = TestBed.inject(ActivatedRoute);
      dichVuSuDungService = TestBed.inject(DichVuSuDungService);
      dichVuService = TestBed.inject(DichVuService);
      phongDuocDatService = TestBed.inject(PhongDuocDatService);

      comp = fixture.componentInstance;
    });

    describe('ngOnInit', () => {
      it('Should call DichVu query and add missing value', () => {
        const dichVuSuDung: IDichVuSuDung = { id: 456 };
        const dichVu: IDichVu = { id: 56316 };
        dichVuSuDung.dichVu = dichVu;

        const dichVuCollection: IDichVu[] = [{ id: 7948 }];
        spyOn(dichVuService, 'query').and.returnValue(of(new HttpResponse({ body: dichVuCollection })));
        const additionalDichVus = [dichVu];
        const expectedCollection: IDichVu[] = [...additionalDichVus, ...dichVuCollection];
        spyOn(dichVuService, 'addDichVuToCollectionIfMissing').and.returnValue(expectedCollection);

        activatedRoute.data = of({ dichVuSuDung });
        comp.ngOnInit();

        expect(dichVuService.query).toHaveBeenCalled();
        expect(dichVuService.addDichVuToCollectionIfMissing).toHaveBeenCalledWith(dichVuCollection, ...additionalDichVus);
        expect(comp.dichVusSharedCollection).toEqual(expectedCollection);
      });

      it('Should call PhongDuocDat query and add missing value', () => {
        const dichVuSuDung: IDichVuSuDung = { id: 456 };
        const phongDuocDat: IPhongDuocDat = { id: 94550 };
        dichVuSuDung.phongDuocDat = phongDuocDat;

        const phongDuocDatCollection: IPhongDuocDat[] = [{ id: 61880 }];
        spyOn(phongDuocDatService, 'query').and.returnValue(of(new HttpResponse({ body: phongDuocDatCollection })));
        const additionalPhongDuocDats = [phongDuocDat];
        const expectedCollection: IPhongDuocDat[] = [...additionalPhongDuocDats, ...phongDuocDatCollection];
        spyOn(phongDuocDatService, 'addPhongDuocDatToCollectionIfMissing').and.returnValue(expectedCollection);

        activatedRoute.data = of({ dichVuSuDung });
        comp.ngOnInit();

        expect(phongDuocDatService.query).toHaveBeenCalled();
        expect(phongDuocDatService.addPhongDuocDatToCollectionIfMissing).toHaveBeenCalledWith(
          phongDuocDatCollection,
          ...additionalPhongDuocDats
        );
        expect(comp.phongDuocDatsSharedCollection).toEqual(expectedCollection);
      });

      it('Should update editForm', () => {
        const dichVuSuDung: IDichVuSuDung = { id: 456 };
        const dichVu: IDichVu = { id: 61517 };
        dichVuSuDung.dichVu = dichVu;
        const phongDuocDat: IPhongDuocDat = { id: 38824 };
        dichVuSuDung.phongDuocDat = phongDuocDat;

        activatedRoute.data = of({ dichVuSuDung });
        comp.ngOnInit();

        expect(comp.editForm.value).toEqual(expect.objectContaining(dichVuSuDung));
        expect(comp.dichVusSharedCollection).toContain(dichVu);
        expect(comp.phongDuocDatsSharedCollection).toContain(phongDuocDat);
      });
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', () => {
        // GIVEN
        const saveSubject = new Subject();
        const dichVuSuDung = { id: 123 };
        spyOn(dichVuSuDungService, 'update').and.returnValue(saveSubject);
        spyOn(comp, 'previousState');
        activatedRoute.data = of({ dichVuSuDung });
        comp.ngOnInit();

        // WHEN
        comp.save();
        expect(comp.isSaving).toEqual(true);
        saveSubject.next(new HttpResponse({ body: dichVuSuDung }));
        saveSubject.complete();

        // THEN
        expect(comp.previousState).toHaveBeenCalled();
        expect(dichVuSuDungService.update).toHaveBeenCalledWith(dichVuSuDung);
        expect(comp.isSaving).toEqual(false);
      });

      it('Should call create service on save for new entity', () => {
        // GIVEN
        const saveSubject = new Subject();
        const dichVuSuDung = new DichVuSuDung();
        spyOn(dichVuSuDungService, 'create').and.returnValue(saveSubject);
        spyOn(comp, 'previousState');
        activatedRoute.data = of({ dichVuSuDung });
        comp.ngOnInit();

        // WHEN
        comp.save();
        expect(comp.isSaving).toEqual(true);
        saveSubject.next(new HttpResponse({ body: dichVuSuDung }));
        saveSubject.complete();

        // THEN
        expect(dichVuSuDungService.create).toHaveBeenCalledWith(dichVuSuDung);
        expect(comp.isSaving).toEqual(false);
        expect(comp.previousState).toHaveBeenCalled();
      });

      it('Should set isSaving to false on error', () => {
        // GIVEN
        const saveSubject = new Subject();
        const dichVuSuDung = { id: 123 };
        spyOn(dichVuSuDungService, 'update').and.returnValue(saveSubject);
        spyOn(comp, 'previousState');
        activatedRoute.data = of({ dichVuSuDung });
        comp.ngOnInit();

        // WHEN
        comp.save();
        expect(comp.isSaving).toEqual(true);
        saveSubject.error('This is an error!');

        // THEN
        expect(dichVuSuDungService.update).toHaveBeenCalledWith(dichVuSuDung);
        expect(comp.isSaving).toEqual(false);
        expect(comp.previousState).not.toHaveBeenCalled();
      });
    });

    describe('Tracking relationships identifiers', () => {
      describe('trackDichVuById', () => {
        it('Should return tracked DichVu primary key', () => {
          const entity = { id: 123 };
          const trackResult = comp.trackDichVuById(0, entity);
          expect(trackResult).toEqual(entity.id);
        });
      });

      describe('trackPhongDuocDatById', () => {
        it('Should return tracked PhongDuocDat primary key', () => {
          const entity = { id: 123 };
          const trackResult = comp.trackPhongDuocDatById(0, entity);
          expect(trackResult).toEqual(entity.id);
        });
      });
    });
  });
});
