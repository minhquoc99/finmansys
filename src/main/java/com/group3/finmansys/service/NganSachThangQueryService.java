package com.group3.finmansys.service;

import com.group3.finmansys.domain.*; // for static metamodels
import com.group3.finmansys.domain.NganSachThang;
import com.group3.finmansys.repository.NganSachThangRepository;
import com.group3.finmansys.service.criteria.NganSachThangCriteria;
import com.group3.finmansys.service.dto.NganSachThangDTO;
import com.group3.finmansys.service.mapper.NganSachThangMapper;
import java.util.List;
import javax.persistence.criteria.JoinType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tech.jhipster.service.QueryService;

/**
 * Service for executing complex queries for {@link NganSachThang} entities in the database.
 * The main input is a {@link NganSachThangCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link NganSachThangDTO} or a {@link Page} of {@link NganSachThangDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class NganSachThangQueryService extends QueryService<NganSachThang> {

    private final Logger log = LoggerFactory.getLogger(NganSachThangQueryService.class);

    private final NganSachThangRepository nganSachThangRepository;

    private final NganSachThangMapper nganSachThangMapper;

    public NganSachThangQueryService(NganSachThangRepository nganSachThangRepository, NganSachThangMapper nganSachThangMapper) {
        this.nganSachThangRepository = nganSachThangRepository;
        this.nganSachThangMapper = nganSachThangMapper;
    }

    /**
     * Return a {@link List} of {@link NganSachThangDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<NganSachThangDTO> findByCriteria(NganSachThangCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<NganSachThang> specification = createSpecification(criteria);
        return nganSachThangMapper.toDto(nganSachThangRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link NganSachThangDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<NganSachThangDTO> findByCriteria(NganSachThangCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<NganSachThang> specification = createSpecification(criteria);
        return nganSachThangRepository.findAll(specification, page).map(nganSachThangMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(NganSachThangCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<NganSachThang> specification = createSpecification(criteria);
        return nganSachThangRepository.count(specification);
    }

    /**
     * Function to convert {@link NganSachThangCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<NganSachThang> createSpecification(NganSachThangCriteria criteria) {
        Specification<NganSachThang> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getId(), NganSachThang_.id));
            }
            if (criteria.getThang() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getThang(), NganSachThang_.thang));
            }
            if (criteria.getSoTien() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getSoTien(), NganSachThang_.soTien));
            }
            if (criteria.getNganSachNamId() != null) {
                specification =
                    specification.and(
                        buildSpecification(
                            criteria.getNganSachNamId(),
                            root -> root.join(NganSachThang_.nganSachNam, JoinType.LEFT).get(NganSachNam_.id)
                        )
                    );
            }
            if (criteria.getKhoanMucChiPhiId() != null) {
                specification =
                    specification.and(
                        buildSpecification(
                            criteria.getKhoanMucChiPhiId(),
                            root -> root.join(NganSachThang_.khoanMucChiPhi, JoinType.LEFT).get(KhoanMucChiPhi_.id)
                        )
                    );
            }
        }
        return specification;
    }
}
