import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize, map } from 'rxjs/operators';

import * as dayjs from 'dayjs';
import { DATE_TIME_FORMAT } from 'app/config/input.constants';

import { IPhongDat, PhongDat } from '../phong-dat.model';
import { PhongDatService } from '../service/phong-dat.service';
import { IPhong } from 'app/entities/phong/phong.model';
import { PhongService } from 'app/entities/phong/service/phong.service';
import { IPhongDuocDat } from 'app/entities/phong-duoc-dat/phong-duoc-dat.model';
import { PhongDuocDatService } from 'app/entities/phong-duoc-dat/service/phong-duoc-dat.service';

@Component({
  selector: 'jhi-phong-dat-update',
  templateUrl: './phong-dat-update.component.html',
})
export class PhongDatUpdateComponent implements OnInit {
  isSaving = false;

  phongsSharedCollection: IPhong[] = [];
  phongDuocDatsSharedCollection: IPhongDuocDat[] = [];

  editForm = this.fb.group({
    id: [],
    ngayDat: [null, [Validators.required]],
    phong: [null, Validators.required],
    phongDuocDat: [null, Validators.required],
  });

  constructor(
    protected phongDatService: PhongDatService,
    protected phongService: PhongService,
    protected phongDuocDatService: PhongDuocDatService,
    protected activatedRoute: ActivatedRoute,
    protected fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ phongDat }) => {
      if (phongDat.id === undefined) {
        const today = dayjs().startOf('day');
        phongDat.ngayDat = today;
      }

      this.updateForm(phongDat);

      this.loadRelationshipsOptions();
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const phongDat = this.createFromForm();
    if (phongDat.id !== undefined) {
      this.subscribeToSaveResponse(this.phongDatService.update(phongDat));
    } else {
      this.subscribeToSaveResponse(this.phongDatService.create(phongDat));
    }
  }

  trackPhongById(index: number, item: IPhong): number {
    return item.id!;
  }

  trackPhongDuocDatById(index: number, item: IPhongDuocDat): number {
    return item.id!;
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IPhongDat>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(phongDat: IPhongDat): void {
    this.editForm.patchValue({
      id: phongDat.id,
      ngayDat: phongDat.ngayDat ? phongDat.ngayDat.format(DATE_TIME_FORMAT) : null,
      phong: phongDat.phong,
      phongDuocDat: phongDat.phongDuocDat,
    });

    this.phongsSharedCollection = this.phongService.addPhongToCollectionIfMissing(this.phongsSharedCollection, phongDat.phong);
    this.phongDuocDatsSharedCollection = this.phongDuocDatService.addPhongDuocDatToCollectionIfMissing(
      this.phongDuocDatsSharedCollection,
      phongDat.phongDuocDat
    );
  }

  protected loadRelationshipsOptions(): void {
    this.phongService
      .query()
      .pipe(map((res: HttpResponse<IPhong[]>) => res.body ?? []))
      .pipe(map((phongs: IPhong[]) => this.phongService.addPhongToCollectionIfMissing(phongs, this.editForm.get('phong')!.value)))
      .subscribe((phongs: IPhong[]) => (this.phongsSharedCollection = phongs));

    this.phongDuocDatService
      .query()
      .pipe(map((res: HttpResponse<IPhongDuocDat[]>) => res.body ?? []))
      .pipe(
        map((phongDuocDats: IPhongDuocDat[]) =>
          this.phongDuocDatService.addPhongDuocDatToCollectionIfMissing(phongDuocDats, this.editForm.get('phongDuocDat')!.value)
        )
      )
      .subscribe((phongDuocDats: IPhongDuocDat[]) => (this.phongDuocDatsSharedCollection = phongDuocDats));
  }

  protected createFromForm(): IPhongDat {
    return {
      ...new PhongDat(),
      id: this.editForm.get(['id'])!.value,
      ngayDat: this.editForm.get(['ngayDat'])!.value ? dayjs(this.editForm.get(['ngayDat'])!.value, DATE_TIME_FORMAT) : undefined,
      phong: this.editForm.get(['phong'])!.value,
      phongDuocDat: this.editForm.get(['phongDuocDat'])!.value,
    };
  }
}
