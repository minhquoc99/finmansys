import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize, map } from 'rxjs/operators';

import * as dayjs from 'dayjs';
import { DATE_TIME_FORMAT } from 'app/config/input.constants';

import { IPhieuThu, PhieuThu } from '../phieu-thu.model';
import { PhieuThuService } from '../service/phieu-thu.service';
import { IKhoanPhaiThu } from 'app/entities/khoan-phai-thu/khoan-phai-thu.model';
import { KhoanPhaiThuService } from 'app/entities/khoan-phai-thu/service/khoan-phai-thu.service';
import { IQuyTien } from 'app/entities/quy-tien/quy-tien.model';
import { QuyTienService } from 'app/entities/quy-tien/service/quy-tien.service';
import { IKhachHang } from 'app/entities/khach-hang/khach-hang.model';
import { KhachHangService } from 'app/entities/khach-hang/service/khach-hang.service';

@Component({
  selector: 'jhi-phieu-thu-update',
  templateUrl: './phieu-thu-update.component.html',
})
export class PhieuThuUpdateComponent implements OnInit {
  isSaving = false;

  khoanPhaiThusSharedCollection: IKhoanPhaiThu[] = [];
  quyTiensSharedCollection: IQuyTien[] = [];
  khachHangsSharedCollection: IKhachHang[] = [];
  khoanPhaiThuViews: IKhoanPhaiThu[] = [];

  editForm = this.fb.group({
    id: [],
    ngayChungTu: [null, [Validators.required]],
    soTien: [null, [Validators.required, Validators.min(1)]],
    khoanPhaiThu: [null, Validators.required],
    quyTien: [null, Validators.required],
    khachHang: [null, Validators.required],
  });

  constructor(
    protected phieuThuService: PhieuThuService,
    protected khoanPhaiThuService: KhoanPhaiThuService,
    protected quyTienService: QuyTienService,
    protected khachHangService: KhachHangService,
    protected activatedRoute: ActivatedRoute,
    protected fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ phieuThu }) => {
      if (phieuThu.id === undefined) {
        const today = dayjs().startOf('day');
        phieuThu.ngayChungTu = today;
      }

      this.updateForm(phieuThu);

      this.loadRelationshipsOptions();
    });
  }

  onChangeKhachHang(): void {
    this.khoanPhaiThuViews = [];
    let checked = true;
    for (const khoanPhaiThu of this.khoanPhaiThusSharedCollection) {
      if (khoanPhaiThu.khachHang?.id === this.editForm.get('khachHang')!.value.id) {
        this.khoanPhaiThuViews.push(khoanPhaiThu);
        if (!this.editForm.get('khoanPhaiThu')!.value?.id) {
          checked = false;
        } else {
          if (khoanPhaiThu.id === this.editForm.get('khoanPhaiThu')!.value.id) {
            checked = false;
          }
        }
      }
    }
    if (this.editForm.get('khoanPhaiThu') && checked) {
      this.editForm.get('khoanPhaiThu')?.patchValue(this.khoanPhaiThuViews[0]);
    }
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const phieuThu = this.createFromForm();
    if (phieuThu.id !== undefined) {
      this.subscribeToSaveResponse(this.phieuThuService.update(phieuThu));
    } else {
      this.subscribeToSaveResponse(this.phieuThuService.create(phieuThu));
    }
  }

  trackKhoanPhaiThuById(index: number, item: IKhoanPhaiThu): number {
    return item.id!;
  }

  trackQuyTienById(index: number, item: IQuyTien): number {
    return item.id!;
  }

  trackKhachHangById(index: number, item: IKhachHang): number {
    return item.id!;
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IPhieuThu>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(phieuThu: IPhieuThu): void {
    this.editForm.patchValue({
      id: phieuThu.id,
      ngayChungTu: phieuThu.ngayChungTu ? phieuThu.ngayChungTu.format(DATE_TIME_FORMAT) : null,
      soTien: phieuThu.soTien,
      khoanPhaiThu: phieuThu.khoanPhaiThu,
      quyTien: phieuThu.quyTien,
      khachHang: phieuThu.khachHang,
    });

    this.khoanPhaiThusSharedCollection = this.khoanPhaiThuService.addKhoanPhaiThuToCollectionIfMissing(
      this.khoanPhaiThusSharedCollection,
      phieuThu.khoanPhaiThu
    );
    this.quyTiensSharedCollection = this.quyTienService.addQuyTienToCollectionIfMissing(this.quyTiensSharedCollection, phieuThu.quyTien);
    this.khachHangsSharedCollection = this.khachHangService.addKhachHangToCollectionIfMissing(
      this.khachHangsSharedCollection,
      phieuThu.khachHang
    );
  }

  protected loadRelationshipsOptions(): void {
    this.quyTienService
      .query()
      .pipe(map((res: HttpResponse<IQuyTien[]>) => res.body ?? []))
      .pipe(
        map((quyTiens: IQuyTien[]) => this.quyTienService.addQuyTienToCollectionIfMissing(quyTiens, this.editForm.get('quyTien')!.value))
      )
      .subscribe((quyTiens: IQuyTien[]) => (this.quyTiensSharedCollection = quyTiens));

    this.khachHangService
      .query({ page: 0, size: 10000, sort: ['id,asc'] })
      .pipe(map((res: HttpResponse<IKhachHang[]>) => res.body ?? []))
      .pipe(
        map((khachHangs: IKhachHang[]) =>
          this.khachHangService.addKhachHangToCollectionIfMissing(khachHangs, this.editForm.get('khachHang')!.value)
        )
      )
      .subscribe((khachHangs: IKhachHang[]) => {
        this.khachHangsSharedCollection = khachHangs;
        if (!this.editForm.get('khachHang')!.value) {
          this.editForm.get('khachHang')?.patchValue(this.khachHangsSharedCollection[0]);
        }
        this.khoanPhaiThuService
          .query()
          .pipe(map((res: HttpResponse<IKhoanPhaiThu[]>) => res.body ?? []))
          .pipe(
            map((khoanPhaiThus: IKhoanPhaiThu[]) =>
              this.khoanPhaiThuService.addKhoanPhaiThuToCollectionIfMissing(khoanPhaiThus, this.editForm.get('khoanPhaiThu')!.value)
            )
          )
          .subscribe((khoanPhaiThus: IKhoanPhaiThu[]) => {
            this.khoanPhaiThusSharedCollection = khoanPhaiThus;
            this.onChangeKhachHang();
          });
      });
  }

  protected createFromForm(): IPhieuThu {
    return {
      ...new PhieuThu(),
      id: this.editForm.get(['id'])!.value,
      ngayChungTu: this.editForm.get(['ngayChungTu'])!.value
        ? dayjs(this.editForm.get(['ngayChungTu'])!.value, DATE_TIME_FORMAT)
        : undefined,
      soTien: this.editForm.get(['soTien'])!.value,
      khoanPhaiThu: this.editForm.get(['khoanPhaiThu'])!.value,
      quyTien: this.editForm.get(['quyTien'])!.value,
      khachHang: this.editForm.get(['khachHang'])!.value,
    };
  }
}
