import { Component, OnInit, ElementRef } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize, map } from 'rxjs/operators';

import { ITaiSanCD, TaiSanCD } from '../tai-san-cd.model';
import { TaiSanCDService } from '../service/tai-san-cd.service';
import { AlertError } from 'app/shared/alert/alert-error.model';
import { EventManager, EventWithContent } from 'app/core/util/event-manager.service';
import { DataUtils, FileLoadError } from 'app/core/util/data-util.service';
import { IKhachSan } from 'app/entities/khach-san/khach-san.model';
import { KhachSanService } from 'app/entities/khach-san/service/khach-san.service';
import { ILoaiTaiSan } from 'app/entities/loai-tai-san/loai-tai-san.model';
import { LoaiTaiSanService } from 'app/entities/loai-tai-san/service/loai-tai-san.service';

@Component({
  selector: 'jhi-tai-san-cd-update',
  templateUrl: './tai-san-cd-update.component.html',
})
export class TaiSanCDUpdateComponent implements OnInit {
  isSaving = false;

  khachSansSharedCollection: IKhachSan[] = [];
  loaiTaiSansSharedCollection: ILoaiTaiSan[] = [];

  editForm = this.fb.group({
    id: [],
    ten: [null, [Validators.required, Validators.maxLength(50)]],
    donViTuoiTho: [null, [Validators.required]],
    thoiGianSuDung: [null, [Validators.required, Validators.min(1)]],
    hinhAnh: [],
    hinhAnhContentType: [],
    khachSan: [null, Validators.required],
    loaiTaiSan: [null, Validators.required],
  });

  constructor(
    protected dataUtils: DataUtils,
    protected eventManager: EventManager,
    protected taiSanCDService: TaiSanCDService,
    protected khachSanService: KhachSanService,
    protected loaiTaiSanService: LoaiTaiSanService,
    protected elementRef: ElementRef,
    protected activatedRoute: ActivatedRoute,
    protected fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ taiSanCD }) => {
      this.updateForm(taiSanCD);

      this.loadRelationshipsOptions();
    });
  }

  byteSize(base64String: string): string {
    return this.dataUtils.byteSize(base64String);
  }

  openFile(base64String: string, contentType: string | null | undefined): void {
    this.dataUtils.openFile(base64String, contentType);
  }

  setFileData(event: Event, field: string, isImage: boolean): void {
    this.dataUtils.loadFileToForm(event, this.editForm, field, isImage).subscribe({
      error: (err: FileLoadError) =>
        this.eventManager.broadcast(
          new EventWithContent<AlertError>('finmansysApp.error', { ...err, key: 'error.file.' + err.key })
        ),
    });
  }

  clearInputImage(field: string, fieldContentType: string, idInput: string): void {
    this.editForm.patchValue({
      [field]: null,
      [fieldContentType]: null,
    });
    if (idInput && this.elementRef.nativeElement.querySelector('#' + idInput)) {
      this.elementRef.nativeElement.querySelector('#' + idInput).value = null;
    }
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const taiSanCD = this.createFromForm();
    if (taiSanCD.id !== undefined) {
      this.subscribeToSaveResponse(this.taiSanCDService.update(taiSanCD));
    } else {
      this.subscribeToSaveResponse(this.taiSanCDService.create(taiSanCD));
    }
  }

  trackKhachSanById(index: number, item: IKhachSan): number {
    return item.id!;
  }

  trackLoaiTaiSanById(index: number, item: ILoaiTaiSan): number {
    return item.id!;
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<ITaiSanCD>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(taiSanCD: ITaiSanCD): void {
    this.editForm.patchValue({
      id: taiSanCD.id,
      ten: taiSanCD.ten,
      donViTuoiTho: taiSanCD.donViTuoiTho,
      thoiGianSuDung: taiSanCD.thoiGianSuDung,
      hinhAnh: taiSanCD.hinhAnh,
      hinhAnhContentType: taiSanCD.hinhAnhContentType,
      khachSan: taiSanCD.khachSan,
      loaiTaiSan: taiSanCD.loaiTaiSan,
    });

    this.khachSansSharedCollection = this.khachSanService.addKhachSanToCollectionIfMissing(
      this.khachSansSharedCollection,
      taiSanCD.khachSan
    );
    this.loaiTaiSansSharedCollection = this.loaiTaiSanService.addLoaiTaiSanToCollectionIfMissing(
      this.loaiTaiSansSharedCollection,
      taiSanCD.loaiTaiSan
    );
  }

  protected loadRelationshipsOptions(): void {
    this.khachSanService
      .query()
      .pipe(map((res: HttpResponse<IKhachSan[]>) => res.body ?? []))
      .pipe(
        map((khachSans: IKhachSan[]) =>
          this.khachSanService.addKhachSanToCollectionIfMissing(khachSans, this.editForm.get('khachSan')!.value)
        )
      )
      .subscribe((khachSans: IKhachSan[]) => (this.khachSansSharedCollection = khachSans));

    this.loaiTaiSanService
      .query()
      .pipe(map((res: HttpResponse<ILoaiTaiSan[]>) => res.body ?? []))
      .pipe(
        map((loaiTaiSans: ILoaiTaiSan[]) =>
          this.loaiTaiSanService.addLoaiTaiSanToCollectionIfMissing(loaiTaiSans, this.editForm.get('loaiTaiSan')!.value)
        )
      )
      .subscribe((loaiTaiSans: ILoaiTaiSan[]) => (this.loaiTaiSansSharedCollection = loaiTaiSans));
  }

  protected createFromForm(): ITaiSanCD {
    return {
      ...new TaiSanCD(),
      id: this.editForm.get(['id'])!.value,
      ten: this.editForm.get(['ten'])!.value,
      donViTuoiTho: this.editForm.get(['donViTuoiTho'])!.value,
      thoiGianSuDung: this.editForm.get(['thoiGianSuDung'])!.value,
      hinhAnhContentType: this.editForm.get(['hinhAnhContentType'])!.value,
      hinhAnh: this.editForm.get(['hinhAnh'])!.value,
      khachSan: this.editForm.get(['khachSan'])!.value,
      loaiTaiSan: this.editForm.get(['loaiTaiSan'])!.value,
    };
  }
}
