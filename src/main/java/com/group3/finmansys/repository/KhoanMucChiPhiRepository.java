package com.group3.finmansys.repository;

import com.group3.finmansys.domain.KhoanMucChiPhi;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data SQL repository for the KhoanMucChiPhi entity.
 */
@SuppressWarnings("unused")
@Repository
public interface KhoanMucChiPhiRepository extends JpaRepository<KhoanMucChiPhi, Long>, JpaSpecificationExecutor<KhoanMucChiPhi> {}
