package com.group3.finmansys.repository;

import com.group3.finmansys.domain.DichVuSuDung;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data SQL repository for the DichVuSuDung entity.
 */
@SuppressWarnings("unused")
@Repository
public interface DichVuSuDungRepository extends JpaRepository<DichVuSuDung, Long>, JpaSpecificationExecutor<DichVuSuDung> {}
