package com.group3.finmansys.web.rest;

import com.group3.finmansys.repository.TaiSanCDRepository;
import com.group3.finmansys.service.TaiSanCDQueryService;
import com.group3.finmansys.service.TaiSanCDService;
import com.group3.finmansys.service.criteria.TaiSanCDCriteria;
import com.group3.finmansys.service.dto.TaiSanCDDTO;
import com.group3.finmansys.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.PaginationUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link com.group3.finmansys.domain.TaiSanCD}.
 */
@RestController
@RequestMapping("/api")
public class TaiSanCDResource {

    private final Logger log = LoggerFactory.getLogger(TaiSanCDResource.class);

    private static final String ENTITY_NAME = "taiSanCD";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final TaiSanCDService taiSanCDService;

    private final TaiSanCDRepository taiSanCDRepository;

    private final TaiSanCDQueryService taiSanCDQueryService;

    public TaiSanCDResource(
        TaiSanCDService taiSanCDService,
        TaiSanCDRepository taiSanCDRepository,
        TaiSanCDQueryService taiSanCDQueryService
    ) {
        this.taiSanCDService = taiSanCDService;
        this.taiSanCDRepository = taiSanCDRepository;
        this.taiSanCDQueryService = taiSanCDQueryService;
    }

    /**
     * {@code POST  /tai-san-cds} : Create a new taiSanCD.
     *
     * @param taiSanCDDTO the taiSanCDDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new taiSanCDDTO, or with status {@code 400 (Bad Request)} if the taiSanCD has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/tai-san-cds")
    public ResponseEntity<TaiSanCDDTO> createTaiSanCD(@Valid @RequestBody TaiSanCDDTO taiSanCDDTO) throws URISyntaxException {
        log.debug("REST request to save TaiSanCD : {}", taiSanCDDTO);
        if (taiSanCDDTO.getId() != null) {
            throw new BadRequestAlertException("A new taiSanCD cannot already have an ID", ENTITY_NAME, "idexists");
        }
        TaiSanCDDTO result = taiSanCDService.save(taiSanCDDTO);
        return ResponseEntity
            .created(new URI("/api/tai-san-cds/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /tai-san-cds/:id} : Updates an existing taiSanCD.
     *
     * @param id the id of the taiSanCDDTO to save.
     * @param taiSanCDDTO the taiSanCDDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated taiSanCDDTO,
     * or with status {@code 400 (Bad Request)} if the taiSanCDDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the taiSanCDDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/tai-san-cds/{id}")
    public ResponseEntity<TaiSanCDDTO> updateTaiSanCD(
        @PathVariable(value = "id", required = false) final Long id,
        @Valid @RequestBody TaiSanCDDTO taiSanCDDTO
    ) throws URISyntaxException {
        log.debug("REST request to update TaiSanCD : {}, {}", id, taiSanCDDTO);
        if (taiSanCDDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, taiSanCDDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!taiSanCDRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        TaiSanCDDTO result = taiSanCDService.save(taiSanCDDTO);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, taiSanCDDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /tai-san-cds/:id} : Partial updates given fields of an existing taiSanCD, field will ignore if it is null
     *
     * @param id the id of the taiSanCDDTO to save.
     * @param taiSanCDDTO the taiSanCDDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated taiSanCDDTO,
     * or with status {@code 400 (Bad Request)} if the taiSanCDDTO is not valid,
     * or with status {@code 404 (Not Found)} if the taiSanCDDTO is not found,
     * or with status {@code 500 (Internal Server Error)} if the taiSanCDDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/tai-san-cds/{id}", consumes = "application/merge-patch+json")
    public ResponseEntity<TaiSanCDDTO> partialUpdateTaiSanCD(
        @PathVariable(value = "id", required = false) final Long id,
        @NotNull @RequestBody TaiSanCDDTO taiSanCDDTO
    ) throws URISyntaxException {
        log.debug("REST request to partial update TaiSanCD partially : {}, {}", id, taiSanCDDTO);
        if (taiSanCDDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, taiSanCDDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!taiSanCDRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<TaiSanCDDTO> result = taiSanCDService.partialUpdate(taiSanCDDTO);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, taiSanCDDTO.getId().toString())
        );
    }

    /**
     * {@code GET  /tai-san-cds} : get all the taiSanCDS.
     *
     * @param pageable the pagination information.
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of taiSanCDS in body.
     */
    @GetMapping("/tai-san-cds")
    public ResponseEntity<List<TaiSanCDDTO>> getAllTaiSanCDS(TaiSanCDCriteria criteria, Pageable pageable) {
        log.debug("REST request to get TaiSanCDS by criteria: {}", criteria);
        Page<TaiSanCDDTO> page = taiSanCDQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /tai-san-cds/count} : count all the taiSanCDS.
     *
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
     */
    @GetMapping("/tai-san-cds/count")
    public ResponseEntity<Long> countTaiSanCDS(TaiSanCDCriteria criteria) {
        log.debug("REST request to count TaiSanCDS by criteria: {}", criteria);
        return ResponseEntity.ok().body(taiSanCDQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /tai-san-cds/:id} : get the "id" taiSanCD.
     *
     * @param id the id of the taiSanCDDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the taiSanCDDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/tai-san-cds/{id}")
    public ResponseEntity<TaiSanCDDTO> getTaiSanCD(@PathVariable Long id) {
        log.debug("REST request to get TaiSanCD : {}", id);
        Optional<TaiSanCDDTO> taiSanCDDTO = taiSanCDService.findOne(id);
        return ResponseUtil.wrapOrNotFound(taiSanCDDTO);
    }

    /**
     * {@code DELETE  /tai-san-cds/:id} : delete the "id" taiSanCD.
     *
     * @param id the id of the taiSanCDDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/tai-san-cds/{id}")
    public ResponseEntity<Void> deleteTaiSanCD(@PathVariable Long id) {
        log.debug("REST request to delete TaiSanCD : {}", id);
        taiSanCDService.delete(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
            .build();
    }
}
