package com.group3.finmansys.service.dto;

import static org.assertj.core.api.Assertions.assertThat;

import com.group3.finmansys.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class CongThucDTOTest {

    @Test
    void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(CongThucDTO.class);
        CongThucDTO congThucDTO1 = new CongThucDTO();
        congThucDTO1.setId(1L);
        CongThucDTO congThucDTO2 = new CongThucDTO();
        assertThat(congThucDTO1).isNotEqualTo(congThucDTO2);
        congThucDTO2.setId(congThucDTO1.getId());
        assertThat(congThucDTO1).isEqualTo(congThucDTO2);
        congThucDTO2.setId(2L);
        assertThat(congThucDTO1).isNotEqualTo(congThucDTO2);
        congThucDTO1.setId(null);
        assertThat(congThucDTO1).isNotEqualTo(congThucDTO2);
    }
}
