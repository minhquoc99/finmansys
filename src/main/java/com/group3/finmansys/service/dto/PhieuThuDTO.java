package com.group3.finmansys.service.dto;

import java.io.Serializable;
import java.time.Instant;
import java.util.Objects;
import javax.validation.constraints.*;

/**
 * A DTO for the {@link com.group3.finmansys.domain.PhieuThu} entity.
 */
public class PhieuThuDTO implements Serializable {

    private Long id;

    @NotNull
    private Instant ngayChungTu;

    @NotNull
    @DecimalMin(value = "1")
    private Double soTien;

    private KhoanPhaiThuDTO khoanPhaiThu;

    private QuyTienDTO quyTien;

    private KhachHangDTO khachHang;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Instant getNgayChungTu() {
        return ngayChungTu;
    }

    public void setNgayChungTu(Instant ngayChungTu) {
        this.ngayChungTu = ngayChungTu;
    }

    public Double getSoTien() {
        return soTien;
    }

    public void setSoTien(Double soTien) {
        this.soTien = soTien;
    }

    public KhoanPhaiThuDTO getKhoanPhaiThu() {
        return khoanPhaiThu;
    }

    public void setKhoanPhaiThu(KhoanPhaiThuDTO khoanPhaiThu) {
        this.khoanPhaiThu = khoanPhaiThu;
    }

    public QuyTienDTO getQuyTien() {
        return quyTien;
    }

    public void setQuyTien(QuyTienDTO quyTien) {
        this.quyTien = quyTien;
    }

    public KhachHangDTO getKhachHang() {
        return khachHang;
    }

    public void setKhachHang(KhachHangDTO khachHang) {
        this.khachHang = khachHang;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof PhieuThuDTO)) {
            return false;
        }

        PhieuThuDTO phieuThuDTO = (PhieuThuDTO) o;
        if (this.id == null) {
            return false;
        }
        return Objects.equals(this.id, phieuThuDTO.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.id);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "PhieuThuDTO{" +
            "id=" + getId() +
            ", ngayChungTu='" + getNgayChungTu() + "'" +
            ", soTien=" + getSoTien() +
            ", khoanPhaiThu=" + getKhoanPhaiThu() +
            ", quyTien=" + getQuyTien() +
            ", khachHang=" + getKhachHang() +
            "}";
    }
}
