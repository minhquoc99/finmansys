import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { mergeMap } from 'rxjs/operators';

import { IMaKeToan, MaKeToan } from '../ma-ke-toan.model';
import { MaKeToanService } from '../service/ma-ke-toan.service';

@Injectable({ providedIn: 'root' })
export class MaKeToanRoutingResolveService implements Resolve<IMaKeToan> {
  constructor(protected service: MaKeToanService, protected router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IMaKeToan> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        mergeMap((maKeToan: HttpResponse<MaKeToan>) => {
          if (maKeToan.body) {
            return of(maKeToan.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new MaKeToan());
  }
}
