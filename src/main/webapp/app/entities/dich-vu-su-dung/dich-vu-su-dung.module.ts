import { NgModule } from '@angular/core';

import { SharedModule } from 'app/shared/shared.module';
import { DichVuSuDungComponent } from './list/dich-vu-su-dung.component';
import { DichVuSuDungDetailComponent } from './detail/dich-vu-su-dung-detail.component';
import { DichVuSuDungUpdateComponent } from './update/dich-vu-su-dung-update.component';
import { DichVuSuDungDeleteDialogComponent } from './delete/dich-vu-su-dung-delete-dialog.component';
import { DichVuSuDungRoutingModule } from './route/dich-vu-su-dung-routing.module';

@NgModule({
  imports: [SharedModule, DichVuSuDungRoutingModule],
  declarations: [DichVuSuDungComponent, DichVuSuDungDetailComponent, DichVuSuDungUpdateComponent, DichVuSuDungDeleteDialogComponent],
  entryComponents: [DichVuSuDungDeleteDialogComponent],
})
export class DichVuSuDungModule {}
