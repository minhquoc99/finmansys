import { NgModule } from '@angular/core';

import { SharedModule } from 'app/shared/shared.module';
import { CongThucComponent } from './list/cong-thuc.component';
import { CongThucDetailComponent } from './detail/cong-thuc-detail.component';
import { CongThucUpdateComponent } from './update/cong-thuc-update.component';
import { CongThucDeleteDialogComponent } from './delete/cong-thuc-delete-dialog.component';
import { CongThucRoutingModule } from './route/cong-thuc-routing.module';

@NgModule({
  imports: [SharedModule, CongThucRoutingModule],
  declarations: [CongThucComponent, CongThucDetailComponent, CongThucUpdateComponent, CongThucDeleteDialogComponent],
  entryComponents: [CongThucDeleteDialogComponent],
})
export class CongThucModule {}
