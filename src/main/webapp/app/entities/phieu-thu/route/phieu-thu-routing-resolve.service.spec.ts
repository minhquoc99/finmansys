jest.mock('@angular/router');

import { TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ActivatedRouteSnapshot, Router } from '@angular/router';
import { of } from 'rxjs';

import { IPhieuThu, PhieuThu } from '../phieu-thu.model';
import { PhieuThuService } from '../service/phieu-thu.service';

import { PhieuThuRoutingResolveService } from './phieu-thu-routing-resolve.service';

describe('Service Tests', () => {
  describe('PhieuThu routing resolve service', () => {
    let mockRouter: Router;
    let mockActivatedRouteSnapshot: ActivatedRouteSnapshot;
    let routingResolveService: PhieuThuRoutingResolveService;
    let service: PhieuThuService;
    let resultPhieuThu: IPhieuThu | undefined;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule],
        providers: [Router, ActivatedRouteSnapshot],
      });
      mockRouter = TestBed.inject(Router);
      mockActivatedRouteSnapshot = TestBed.inject(ActivatedRouteSnapshot);
      routingResolveService = TestBed.inject(PhieuThuRoutingResolveService);
      service = TestBed.inject(PhieuThuService);
      resultPhieuThu = undefined;
    });

    describe('resolve', () => {
      it('should return IPhieuThu returned by find', () => {
        // GIVEN
        service.find = jest.fn(id => of(new HttpResponse({ body: { id } })));
        mockActivatedRouteSnapshot.params = { id: 123 };

        // WHEN
        routingResolveService.resolve(mockActivatedRouteSnapshot).subscribe(result => {
          resultPhieuThu = result;
        });

        // THEN
        expect(service.find).toBeCalledWith(123);
        expect(resultPhieuThu).toEqual({ id: 123 });
      });

      it('should return new IPhieuThu if id is not provided', () => {
        // GIVEN
        service.find = jest.fn();
        mockActivatedRouteSnapshot.params = {};

        // WHEN
        routingResolveService.resolve(mockActivatedRouteSnapshot).subscribe(result => {
          resultPhieuThu = result;
        });

        // THEN
        expect(service.find).not.toBeCalled();
        expect(resultPhieuThu).toEqual(new PhieuThu());
      });

      it('should route to 404 page if data not found in server', () => {
        // GIVEN
        spyOn(service, 'find').and.returnValue(of(new HttpResponse({ body: null })));
        mockActivatedRouteSnapshot.params = { id: 123 };

        // WHEN
        routingResolveService.resolve(mockActivatedRouteSnapshot).subscribe(result => {
          resultPhieuThu = result;
        });

        // THEN
        expect(service.find).toBeCalledWith(123);
        expect(resultPhieuThu).toEqual(undefined);
        expect(mockRouter.navigate).toHaveBeenCalledWith(['404']);
      });
    });
  });
});
