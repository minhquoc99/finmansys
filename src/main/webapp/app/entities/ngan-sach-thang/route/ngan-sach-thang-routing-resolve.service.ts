import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { mergeMap } from 'rxjs/operators';

import { INganSachThang, NganSachThang } from '../ngan-sach-thang.model';
import { NganSachThangService } from '../service/ngan-sach-thang.service';

@Injectable({ providedIn: 'root' })
export class NganSachThangRoutingResolveService implements Resolve<INganSachThang> {
  constructor(protected service: NganSachThangService, protected router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<INganSachThang> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        mergeMap((nganSachThang: HttpResponse<NganSachThang>) => {
          if (nganSachThang.body) {
            return of(nganSachThang.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new NganSachThang());
  }
}
