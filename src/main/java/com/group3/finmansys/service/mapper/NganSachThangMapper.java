package com.group3.finmansys.service.mapper;

import com.group3.finmansys.domain.*;
import com.group3.finmansys.service.dto.NganSachThangDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link NganSachThang} and its DTO {@link NganSachThangDTO}.
 */
@Mapper(componentModel = "spring", uses = { NganSachNamMapper.class, KhoanMucChiPhiMapper.class })
public interface NganSachThangMapper extends EntityMapper<NganSachThangDTO, NganSachThang> {
    @Mapping(target = "nganSachNam", source = "nganSachNam", qualifiedByName = "ten")
    @Mapping(target = "khoanMucChiPhi", source = "khoanMucChiPhi", qualifiedByName = "ten")
    NganSachThangDTO toDto(NganSachThang s);
}
