package com.group3.finmansys.service.mapper;

import com.group3.finmansys.domain.*;
import com.group3.finmansys.service.dto.CongThucDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link CongThuc} and its DTO {@link CongThucDTO}.
 */
@Mapper(componentModel = "spring", uses = { ToanTuMapper.class })
public interface CongThucMapper extends EntityMapper<CongThucDTO, CongThuc> {
    @Mapping(target = "capCha", source = "capCha", qualifiedByName = "maSo")
    CongThucDTO toDto(CongThuc s);

    @Named("maSo")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    @Mapping(target = "maSo", source = "maSo")
    @Mapping(target = "tenChiTieu", source = "tenChiTieu")
    CongThucDTO toDtoMaSo(CongThuc congThuc);
}
