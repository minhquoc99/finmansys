package com.group3.finmansys.service.dto;

import static org.assertj.core.api.Assertions.assertThat;

import com.group3.finmansys.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class ChiTietGiamTSCDDTOTest {

    @Test
    void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(ChiTietGiamTSCDDTO.class);
        ChiTietGiamTSCDDTO chiTietGiamTSCDDTO1 = new ChiTietGiamTSCDDTO();
        chiTietGiamTSCDDTO1.setId(1L);
        ChiTietGiamTSCDDTO chiTietGiamTSCDDTO2 = new ChiTietGiamTSCDDTO();
        assertThat(chiTietGiamTSCDDTO1).isNotEqualTo(chiTietGiamTSCDDTO2);
        chiTietGiamTSCDDTO2.setId(chiTietGiamTSCDDTO1.getId());
        assertThat(chiTietGiamTSCDDTO1).isEqualTo(chiTietGiamTSCDDTO2);
        chiTietGiamTSCDDTO2.setId(2L);
        assertThat(chiTietGiamTSCDDTO1).isNotEqualTo(chiTietGiamTSCDDTO2);
        chiTietGiamTSCDDTO1.setId(null);
        assertThat(chiTietGiamTSCDDTO1).isNotEqualTo(chiTietGiamTSCDDTO2);
    }
}
