package com.group3.finmansys.service.criteria;

import java.io.Serializable;
import java.util.Objects;
import tech.jhipster.service.Criteria;
import tech.jhipster.service.filter.BooleanFilter;
import tech.jhipster.service.filter.DoubleFilter;
import tech.jhipster.service.filter.Filter;
import tech.jhipster.service.filter.FloatFilter;
import tech.jhipster.service.filter.InstantFilter;
import tech.jhipster.service.filter.IntegerFilter;
import tech.jhipster.service.filter.LongFilter;
import tech.jhipster.service.filter.StringFilter;

/**
 * Criteria class for the {@link com.group3.finmansys.domain.KhoanPhaiThu} entity. This class is used
 * in {@link com.group3.finmansys.web.rest.KhoanPhaiThuResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /khoan-phai-thus?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class KhoanPhaiThuCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private DoubleFilter soTien;

    private InstantFilter ngayTao;

    private InstantFilter ngayPhaiThu;

    private StringFilter ghiChu;

    private LongFilter khachHangId;

    private LongFilter maKeToanId;

    private LongFilter phieuThuId;

    private LongFilter phongDuocDatId;

    public KhoanPhaiThuCriteria() {}

    public KhoanPhaiThuCriteria(KhoanPhaiThuCriteria other) {
        this.id = other.id == null ? null : other.id.copy();
        this.soTien = other.soTien == null ? null : other.soTien.copy();
        this.ngayTao = other.ngayTao == null ? null : other.ngayTao.copy();
        this.ngayPhaiThu = other.ngayPhaiThu == null ? null : other.ngayPhaiThu.copy();
        this.ghiChu = other.ghiChu == null ? null : other.ghiChu.copy();
        this.khachHangId = other.khachHangId == null ? null : other.khachHangId.copy();
        this.maKeToanId = other.maKeToanId == null ? null : other.maKeToanId.copy();
        this.phieuThuId = other.phieuThuId == null ? null : other.phieuThuId.copy();
        this.phongDuocDatId = other.phongDuocDatId == null ? null : other.phongDuocDatId.copy();
    }

    @Override
    public KhoanPhaiThuCriteria copy() {
        return new KhoanPhaiThuCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public LongFilter id() {
        if (id == null) {
            id = new LongFilter();
        }
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public DoubleFilter getSoTien() {
        return soTien;
    }

    public DoubleFilter soTien() {
        if (soTien == null) {
            soTien = new DoubleFilter();
        }
        return soTien;
    }

    public void setSoTien(DoubleFilter soTien) {
        this.soTien = soTien;
    }

    public InstantFilter getNgayTao() {
        return ngayTao;
    }

    public InstantFilter ngayTao() {
        if (ngayTao == null) {
            ngayTao = new InstantFilter();
        }
        return ngayTao;
    }

    public void setNgayTao(InstantFilter ngayTao) {
        this.ngayTao = ngayTao;
    }

    public InstantFilter getNgayPhaiThu() {
        return ngayPhaiThu;
    }

    public InstantFilter ngayPhaiThu() {
        if (ngayPhaiThu == null) {
            ngayPhaiThu = new InstantFilter();
        }
        return ngayPhaiThu;
    }

    public void setNgayPhaiThu(InstantFilter ngayPhaiThu) {
        this.ngayPhaiThu = ngayPhaiThu;
    }

    public StringFilter getGhiChu() {
        return ghiChu;
    }

    public StringFilter ghiChu() {
        if (ghiChu == null) {
            ghiChu = new StringFilter();
        }
        return ghiChu;
    }

    public void setGhiChu(StringFilter ghiChu) {
        this.ghiChu = ghiChu;
    }

    public LongFilter getKhachHangId() {
        return khachHangId;
    }

    public LongFilter khachHangId() {
        if (khachHangId == null) {
            khachHangId = new LongFilter();
        }
        return khachHangId;
    }

    public void setKhachHangId(LongFilter khachHangId) {
        this.khachHangId = khachHangId;
    }

    public LongFilter getMaKeToanId() {
        return maKeToanId;
    }

    public LongFilter maKeToanId() {
        if (maKeToanId == null) {
            maKeToanId = new LongFilter();
        }
        return maKeToanId;
    }

    public void setMaKeToanId(LongFilter maKeToanId) {
        this.maKeToanId = maKeToanId;
    }

    public LongFilter getPhieuThuId() {
        return phieuThuId;
    }

    public LongFilter phieuThuId() {
        if (phieuThuId == null) {
            phieuThuId = new LongFilter();
        }
        return phieuThuId;
    }

    public void setPhieuThuId(LongFilter phieuThuId) {
        this.phieuThuId = phieuThuId;
    }

    public LongFilter getPhongDuocDatId() {
        return phongDuocDatId;
    }

    public LongFilter phongDuocDatId() {
        if (phongDuocDatId == null) {
            phongDuocDatId = new LongFilter();
        }
        return phongDuocDatId;
    }

    public void setPhongDuocDatId(LongFilter phongDuocDatId) {
        this.phongDuocDatId = phongDuocDatId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final KhoanPhaiThuCriteria that = (KhoanPhaiThuCriteria) o;
        return (
            Objects.equals(id, that.id) &&
            Objects.equals(soTien, that.soTien) &&
            Objects.equals(ngayTao, that.ngayTao) &&
            Objects.equals(ngayPhaiThu, that.ngayPhaiThu) &&
            Objects.equals(ghiChu, that.ghiChu) &&
            Objects.equals(khachHangId, that.khachHangId) &&
            Objects.equals(maKeToanId, that.maKeToanId) &&
            Objects.equals(phieuThuId, that.phieuThuId) &&
            Objects.equals(phongDuocDatId, that.phongDuocDatId)
        );
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, soTien, ngayTao, ngayPhaiThu, ghiChu, khachHangId, maKeToanId, phieuThuId, phongDuocDatId);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "KhoanPhaiThuCriteria{" +
            (id != null ? "id=" + id + ", " : "") +
            (soTien != null ? "soTien=" + soTien + ", " : "") +
            (ngayTao != null ? "ngayTao=" + ngayTao + ", " : "") +
            (ngayPhaiThu != null ? "ngayPhaiThu=" + ngayPhaiThu + ", " : "") +
            (ghiChu != null ? "ghiChu=" + ghiChu + ", " : "") +
            (khachHangId != null ? "khachHangId=" + khachHangId + ", " : "") +
            (maKeToanId != null ? "maKeToanId=" + maKeToanId + ", " : "") +
            (phieuThuId != null ? "phieuThuId=" + phieuThuId + ", " : "") +
            (phongDuocDatId != null ? "phongDuocDatId=" + phongDuocDatId + ", " : "") +
            "}";
    }
}
