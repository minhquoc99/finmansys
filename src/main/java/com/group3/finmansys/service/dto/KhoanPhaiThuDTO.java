package com.group3.finmansys.service.dto;

import com.group3.finmansys.domain.PhieuThu;
import java.io.Serializable;
import java.time.Instant;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import javax.validation.constraints.*;

/**
 * A DTO for the {@link com.group3.finmansys.domain.KhoanPhaiThu} entity.
 */
public class KhoanPhaiThuDTO implements Serializable {

    private Long id;

    @NotNull
    @DecimalMin(value = "1")
    private Double soTien;

    @NotNull
    private Instant ngayTao;

    @NotNull
    private Instant ngayPhaiThu;

    private String ghiChu;

    private KhachHangDTO khachHang;

    private MaKeToanDTO maKeToan;

    private PhongDuocDatDTO phongDuocDat;

    private Set<PhieuThu> phieuThus = new HashSet<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Double getSoTien() {
        return soTien;
    }

    public void setSoTien(Double soTien) {
        this.soTien = soTien;
    }

    public Instant getNgayTao() {
        return ngayTao;
    }

    public void setNgayTao(Instant ngayTao) {
        this.ngayTao = ngayTao;
    }

    public Instant getNgayPhaiThu() {
        return ngayPhaiThu;
    }

    public void setNgayPhaiThu(Instant ngayPhaiThu) {
        this.ngayPhaiThu = ngayPhaiThu;
    }

    public String getGhiChu() {
        return ghiChu;
    }

    public void setGhiChu(String ghiChu) {
        this.ghiChu = ghiChu;
    }

    public KhachHangDTO getKhachHang() {
        return khachHang;
    }

    public void setKhachHang(KhachHangDTO khachHang) {
        this.khachHang = khachHang;
    }

    public MaKeToanDTO getMaKeToan() {
        return maKeToan;
    }

    public void setMaKeToan(MaKeToanDTO maKeToan) {
        this.maKeToan = maKeToan;
    }

    public PhongDuocDatDTO getPhongDuocDat() {
        return phongDuocDat;
    }

    public void setPhongDuocDat(PhongDuocDatDTO phongDuocDat) {
        this.phongDuocDat = phongDuocDat;
    }

    public Set<PhieuThu> getPhieuThus() {
        return phieuThus;
    }

    public void setPhieuThus(Set<PhieuThu> phieuThus) {
        this.phieuThus = phieuThus;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof KhoanPhaiThuDTO)) {
            return false;
        }

        KhoanPhaiThuDTO khoanPhaiThuDTO = (KhoanPhaiThuDTO) o;
        if (this.id == null) {
            return false;
        }
        return Objects.equals(this.id, khoanPhaiThuDTO.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.id);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "KhoanPhaiThuDTO{" +
            "id=" + getId() +
            ", soTien=" + getSoTien() +
            ", ngayTao='" + getNgayTao() + "'" +
            ", ngayPhaiThu='" + getNgayPhaiThu() + "'" +
            ", ghiChu='" + getGhiChu() + "'" +
            ", khachHang=" + getKhachHang() +
            ", maKeToan=" + getMaKeToan() +
            ", phongDuocDat=" + getPhongDuocDat() +
            "}";
    }
}
