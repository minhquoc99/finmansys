import { NgModule } from '@angular/core';

import { SharedModule } from 'app/shared/shared.module';
import { KhoanMucChiPhiComponent } from './list/khoan-muc-chi-phi.component';
import { KhoanMucChiPhiDetailComponent } from './detail/khoan-muc-chi-phi-detail.component';
import { KhoanMucChiPhiUpdateComponent } from './update/khoan-muc-chi-phi-update.component';
import { KhoanMucChiPhiDeleteDialogComponent } from './delete/khoan-muc-chi-phi-delete-dialog.component';
import { KhoanMucChiPhiRoutingModule } from './route/khoan-muc-chi-phi-routing.module';

@NgModule({
  imports: [SharedModule, KhoanMucChiPhiRoutingModule],
  declarations: [
    KhoanMucChiPhiComponent,
    KhoanMucChiPhiDetailComponent,
    KhoanMucChiPhiUpdateComponent,
    KhoanMucChiPhiDeleteDialogComponent,
  ],
  entryComponents: [KhoanMucChiPhiDeleteDialogComponent],
})
export class KhoanMucChiPhiModule {}
