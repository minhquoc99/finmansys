package com.group3.finmansys.service.dto;

import static org.assertj.core.api.Assertions.assertThat;

import com.group3.finmansys.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class KhoanPhaiTraDTOTest {

    @Test
    void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(KhoanPhaiTraDTO.class);
        KhoanPhaiTraDTO khoanPhaiTraDTO1 = new KhoanPhaiTraDTO();
        khoanPhaiTraDTO1.setId(1L);
        KhoanPhaiTraDTO khoanPhaiTraDTO2 = new KhoanPhaiTraDTO();
        assertThat(khoanPhaiTraDTO1).isNotEqualTo(khoanPhaiTraDTO2);
        khoanPhaiTraDTO2.setId(khoanPhaiTraDTO1.getId());
        assertThat(khoanPhaiTraDTO1).isEqualTo(khoanPhaiTraDTO2);
        khoanPhaiTraDTO2.setId(2L);
        assertThat(khoanPhaiTraDTO1).isNotEqualTo(khoanPhaiTraDTO2);
        khoanPhaiTraDTO1.setId(null);
        assertThat(khoanPhaiTraDTO1).isNotEqualTo(khoanPhaiTraDTO2);
    }
}
