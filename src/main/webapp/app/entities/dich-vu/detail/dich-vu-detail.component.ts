import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IDichVu } from '../dich-vu.model';

@Component({
  selector: 'jhi-dich-vu-detail',
  templateUrl: './dich-vu-detail.component.html',
})
export class DichVuDetailComponent implements OnInit {
  dichVu: IDichVu | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ dichVu }) => {
      this.dichVu = dichVu;
    });
  }

  previousState(): void {
    window.history.back();
  }
}
