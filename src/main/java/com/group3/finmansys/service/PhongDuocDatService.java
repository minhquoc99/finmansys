package com.group3.finmansys.service;

import com.group3.finmansys.service.dto.PhongDuocDatDTO;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Service Interface for managing {@link com.group3.finmansys.domain.PhongDuocDat}.
 */
public interface PhongDuocDatService {
    /**
     * Save a phongDuocDat.
     *
     * @param phongDuocDatDTO the entity to save.
     * @return the persisted entity.
     */
    PhongDuocDatDTO save(PhongDuocDatDTO phongDuocDatDTO);

    /**
     * Partially updates a phongDuocDat.
     *
     * @param phongDuocDatDTO the entity to update partially.
     * @return the persisted entity.
     */
    Optional<PhongDuocDatDTO> partialUpdate(PhongDuocDatDTO phongDuocDatDTO);

    /**
     * Get all the phongDuocDats.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<PhongDuocDatDTO> findAll(Pageable pageable);

    /**
     * Get the "id" phongDuocDat.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<PhongDuocDatDTO> findOne(Long id);

    /**
     * Delete the "id" phongDuocDat.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
