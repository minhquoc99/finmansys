package com.group3.finmansys.service;

import com.group3.finmansys.service.dto.KhoanMucChiPhiDTO;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Service Interface for managing {@link com.group3.finmansys.domain.KhoanMucChiPhi}.
 */
public interface KhoanMucChiPhiService {
    /**
     * Save a khoanMucChiPhi.
     *
     * @param khoanMucChiPhiDTO the entity to save.
     * @return the persisted entity.
     */
    KhoanMucChiPhiDTO save(KhoanMucChiPhiDTO khoanMucChiPhiDTO);

    /**
     * Partially updates a khoanMucChiPhi.
     *
     * @param khoanMucChiPhiDTO the entity to update partially.
     * @return the persisted entity.
     */
    Optional<KhoanMucChiPhiDTO> partialUpdate(KhoanMucChiPhiDTO khoanMucChiPhiDTO);

    /**
     * Get all the khoanMucChiPhis.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<KhoanMucChiPhiDTO> findAll(Pageable pageable);

    /**
     * Get the "id" khoanMucChiPhi.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<KhoanMucChiPhiDTO> findOne(Long id);

    /**
     * Delete the "id" khoanMucChiPhi.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
