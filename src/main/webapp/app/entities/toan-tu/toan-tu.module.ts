import { NgModule } from '@angular/core';

import { SharedModule } from 'app/shared/shared.module';
import { ToanTuComponent } from './list/toan-tu.component';
import { ToanTuDetailComponent } from './detail/toan-tu-detail.component';
import { ToanTuUpdateComponent } from './update/toan-tu-update.component';
import { ToanTuDeleteDialogComponent } from './delete/toan-tu-delete-dialog.component';
import { ToanTuRoutingModule } from './route/toan-tu-routing.module';

@NgModule({
  imports: [SharedModule, ToanTuRoutingModule],
  declarations: [ToanTuComponent, ToanTuDetailComponent, ToanTuUpdateComponent, ToanTuDeleteDialogComponent],
  entryComponents: [ToanTuDeleteDialogComponent],
})
export class ToanTuModule {}
