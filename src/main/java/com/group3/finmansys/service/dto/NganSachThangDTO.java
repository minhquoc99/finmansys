package com.group3.finmansys.service.dto;

import java.io.Serializable;
import java.util.Objects;
import javax.validation.constraints.*;

/**
 * A DTO for the {@link com.group3.finmansys.domain.NganSachThang} entity.
 */
public class NganSachThangDTO implements Serializable {

    private Long id;

    @NotNull
    @Min(value = 1)
    @Max(value = 12)
    private Integer thang;

    @NotNull
    @DecimalMin(value = "1")
    private Double soTien;

    private NganSachNamDTO nganSachNam;

    private KhoanMucChiPhiDTO khoanMucChiPhi;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getThang() {
        return thang;
    }

    public void setThang(Integer thang) {
        this.thang = thang;
    }

    public Double getSoTien() {
        return soTien;
    }

    public void setSoTien(Double soTien) {
        this.soTien = soTien;
    }

    public NganSachNamDTO getNganSachNam() {
        return nganSachNam;
    }

    public void setNganSachNam(NganSachNamDTO nganSachNam) {
        this.nganSachNam = nganSachNam;
    }

    public KhoanMucChiPhiDTO getKhoanMucChiPhi() {
        return khoanMucChiPhi;
    }

    public void setKhoanMucChiPhi(KhoanMucChiPhiDTO khoanMucChiPhi) {
        this.khoanMucChiPhi = khoanMucChiPhi;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof NganSachThangDTO)) {
            return false;
        }

        NganSachThangDTO nganSachThangDTO = (NganSachThangDTO) o;
        if (this.id == null) {
            return false;
        }
        return Objects.equals(this.id, nganSachThangDTO.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.id);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "NganSachThangDTO{" +
            "id=" + getId() +
            ", thang=" + getThang() +
            ", soTien=" + getSoTien() +
            ", nganSachNam=" + getNganSachNam() +
            ", khoanMucChiPhi=" + getKhoanMucChiPhi() +
            "}";
    }
}
