package com.group3.finmansys.domain;

import static org.assertj.core.api.Assertions.assertThat;

import com.group3.finmansys.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class ToanTuTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(ToanTu.class);
        ToanTu toanTu1 = new ToanTu();
        toanTu1.setId(1L);
        ToanTu toanTu2 = new ToanTu();
        toanTu2.setId(toanTu1.getId());
        assertThat(toanTu1).isEqualTo(toanTu2);
        toanTu2.setId(2L);
        assertThat(toanTu1).isNotEqualTo(toanTu2);
        toanTu1.setId(null);
        assertThat(toanTu1).isNotEqualTo(toanTu2);
    }
}
