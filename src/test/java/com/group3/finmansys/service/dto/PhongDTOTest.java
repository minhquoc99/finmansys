package com.group3.finmansys.service.dto;

import static org.assertj.core.api.Assertions.assertThat;

import com.group3.finmansys.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class PhongDTOTest {

    @Test
    void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(PhongDTO.class);
        PhongDTO phongDTO1 = new PhongDTO();
        phongDTO1.setId(1L);
        PhongDTO phongDTO2 = new PhongDTO();
        assertThat(phongDTO1).isNotEqualTo(phongDTO2);
        phongDTO2.setId(phongDTO1.getId());
        assertThat(phongDTO1).isEqualTo(phongDTO2);
        phongDTO2.setId(2L);
        assertThat(phongDTO1).isNotEqualTo(phongDTO2);
        phongDTO1.setId(null);
        assertThat(phongDTO1).isNotEqualTo(phongDTO2);
    }
}
