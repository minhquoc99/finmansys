package com.group3.finmansys.service.dto;

import java.io.Serializable;
import java.time.Instant;
import java.util.Objects;
import javax.validation.constraints.*;

/**
 * A DTO for the {@link com.group3.finmansys.domain.PhongDat} entity.
 */
public class PhongDatDTO implements Serializable {

    private Long id;

    @NotNull
    private Instant ngayDat;

    private PhongDTO phong;

    private PhongDuocDatDTO phongDuocDat;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Instant getNgayDat() {
        return ngayDat;
    }

    public void setNgayDat(Instant ngayDat) {
        this.ngayDat = ngayDat;
    }

    public PhongDTO getPhong() {
        return phong;
    }

    public void setPhong(PhongDTO phong) {
        this.phong = phong;
    }

    public PhongDuocDatDTO getPhongDuocDat() {
        return phongDuocDat;
    }

    public void setPhongDuocDat(PhongDuocDatDTO phongDuocDat) {
        this.phongDuocDat = phongDuocDat;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof PhongDatDTO)) {
            return false;
        }

        PhongDatDTO phongDatDTO = (PhongDatDTO) o;
        if (this.id == null) {
            return false;
        }
        return Objects.equals(this.id, phongDatDTO.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.id);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "PhongDatDTO{" +
            "id=" + getId() +
            ", ngayDat='" + getNgayDat() + "'" +
            ", phong=" + getPhong() +
            ", phongDuocDat=" + getPhongDuocDat() +
            "}";
    }
}
