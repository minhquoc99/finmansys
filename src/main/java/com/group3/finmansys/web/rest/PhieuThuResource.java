package com.group3.finmansys.web.rest;

import com.group3.finmansys.repository.PhieuThuRepository;
import com.group3.finmansys.service.PhieuThuQueryService;
import com.group3.finmansys.service.PhieuThuService;
import com.group3.finmansys.service.criteria.PhieuThuCriteria;
import com.group3.finmansys.service.dto.PhieuThuDTO;
import com.group3.finmansys.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.PaginationUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link com.group3.finmansys.domain.PhieuThu}.
 */
@RestController
@RequestMapping("/api")
public class PhieuThuResource {

    private final Logger log = LoggerFactory.getLogger(PhieuThuResource.class);

    private static final String ENTITY_NAME = "phieuThu";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final PhieuThuService phieuThuService;

    private final PhieuThuRepository phieuThuRepository;

    private final PhieuThuQueryService phieuThuQueryService;

    public PhieuThuResource(
        PhieuThuService phieuThuService,
        PhieuThuRepository phieuThuRepository,
        PhieuThuQueryService phieuThuQueryService
    ) {
        this.phieuThuService = phieuThuService;
        this.phieuThuRepository = phieuThuRepository;
        this.phieuThuQueryService = phieuThuQueryService;
    }

    /**
     * {@code POST  /phieu-thus} : Create a new phieuThu.
     *
     * @param phieuThuDTO the phieuThuDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new phieuThuDTO, or with status {@code 400 (Bad Request)} if the phieuThu has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/phieu-thus")
    public ResponseEntity<PhieuThuDTO> createPhieuThu(@Valid @RequestBody PhieuThuDTO phieuThuDTO) throws URISyntaxException {
        log.debug("REST request to save PhieuThu : {}", phieuThuDTO);
        if (phieuThuDTO.getId() != null) {
            throw new BadRequestAlertException("A new phieuThu cannot already have an ID", ENTITY_NAME, "idexists");
        }
        PhieuThuDTO result = phieuThuService.save(phieuThuDTO);
        return ResponseEntity
            .created(new URI("/api/phieu-thus/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /phieu-thus/:id} : Updates an existing phieuThu.
     *
     * @param id the id of the phieuThuDTO to save.
     * @param phieuThuDTO the phieuThuDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated phieuThuDTO,
     * or with status {@code 400 (Bad Request)} if the phieuThuDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the phieuThuDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/phieu-thus/{id}")
    public ResponseEntity<PhieuThuDTO> updatePhieuThu(
        @PathVariable(value = "id", required = false) final Long id,
        @Valid @RequestBody PhieuThuDTO phieuThuDTO
    ) throws URISyntaxException {
        log.debug("REST request to update PhieuThu : {}, {}", id, phieuThuDTO);
        if (phieuThuDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, phieuThuDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!phieuThuRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        PhieuThuDTO result = phieuThuService.save(phieuThuDTO);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, phieuThuDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /phieu-thus/:id} : Partial updates given fields of an existing phieuThu, field will ignore if it is null
     *
     * @param id the id of the phieuThuDTO to save.
     * @param phieuThuDTO the phieuThuDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated phieuThuDTO,
     * or with status {@code 400 (Bad Request)} if the phieuThuDTO is not valid,
     * or with status {@code 404 (Not Found)} if the phieuThuDTO is not found,
     * or with status {@code 500 (Internal Server Error)} if the phieuThuDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/phieu-thus/{id}", consumes = "application/merge-patch+json")
    public ResponseEntity<PhieuThuDTO> partialUpdatePhieuThu(
        @PathVariable(value = "id", required = false) final Long id,
        @NotNull @RequestBody PhieuThuDTO phieuThuDTO
    ) throws URISyntaxException {
        log.debug("REST request to partial update PhieuThu partially : {}, {}", id, phieuThuDTO);
        if (phieuThuDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, phieuThuDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!phieuThuRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<PhieuThuDTO> result = phieuThuService.partialUpdate(phieuThuDTO);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, phieuThuDTO.getId().toString())
        );
    }

    /**
     * {@code GET  /phieu-thus} : get all the phieuThus.
     *
     * @param pageable the pagination information.
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of phieuThus in body.
     */
    @GetMapping("/phieu-thus")
    public ResponseEntity<List<PhieuThuDTO>> getAllPhieuThus(PhieuThuCriteria criteria, Pageable pageable) {
        log.debug("REST request to get PhieuThus by criteria: {}", criteria);
        Page<PhieuThuDTO> page = phieuThuQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /phieu-thus/count} : count all the phieuThus.
     *
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
     */
    @GetMapping("/phieu-thus/count")
    public ResponseEntity<Long> countPhieuThus(PhieuThuCriteria criteria) {
        log.debug("REST request to count PhieuThus by criteria: {}", criteria);
        return ResponseEntity.ok().body(phieuThuQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /phieu-thus/:id} : get the "id" phieuThu.
     *
     * @param id the id of the phieuThuDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the phieuThuDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/phieu-thus/{id}")
    public ResponseEntity<PhieuThuDTO> getPhieuThu(@PathVariable Long id) {
        log.debug("REST request to get PhieuThu : {}", id);
        Optional<PhieuThuDTO> phieuThuDTO = phieuThuService.findOne(id);
        return ResponseUtil.wrapOrNotFound(phieuThuDTO);
    }

    /**
     * {@code DELETE  /phieu-thus/:id} : delete the "id" phieuThu.
     *
     * @param id the id of the phieuThuDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/phieu-thus/{id}")
    public ResponseEntity<Void> deletePhieuThu(@PathVariable Long id) {
        log.debug("REST request to delete PhieuThu : {}", id);
        phieuThuService.delete(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
            .build();
    }
}
