package com.group3.finmansys.service.mapper;

import com.group3.finmansys.domain.*;
import com.group3.finmansys.service.dto.NganSachNamDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link NganSachNam} and its DTO {@link NganSachNamDTO}.
 */
@Mapper(componentModel = "spring", uses = { KhachSanMapper.class, NganSachThangMapper.class })
public interface NganSachNamMapper extends EntityMapper<NganSachNamDTO, NganSachNam> {
    @Mapping(target = "khachSan", source = "khachSan", qualifiedByName = "ten")
    NganSachNamDTO toDto(NganSachNam s);

    @Named("ten")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    @Mapping(target = "ten", source = "ten")
    NganSachNamDTO toDtoTen(NganSachNam nganSachNam);
}
