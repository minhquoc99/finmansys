package com.group3.finmansys.service;

import com.group3.finmansys.service.dto.MaKeToanDTO;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Service Interface for managing {@link com.group3.finmansys.domain.MaKeToan}.
 */
public interface MaKeToanService {
    /**
     * Save a maKeToan.
     *
     * @param maKeToanDTO the entity to save.
     * @return the persisted entity.
     */
    MaKeToanDTO save(MaKeToanDTO maKeToanDTO);

    /**
     * Partially updates a maKeToan.
     *
     * @param maKeToanDTO the entity to update partially.
     * @return the persisted entity.
     */
    Optional<MaKeToanDTO> partialUpdate(MaKeToanDTO maKeToanDTO);

    /**
     * Get all the maKeToans.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<MaKeToanDTO> findAll(Pageable pageable);

    /**
     * Get the "id" maKeToan.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<MaKeToanDTO> findOne(Long id);

    /**
     * Delete the "id" maKeToan.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
