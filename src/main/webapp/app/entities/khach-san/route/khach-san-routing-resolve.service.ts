import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { mergeMap } from 'rxjs/operators';

import { IKhachSan, KhachSan } from '../khach-san.model';
import { KhachSanService } from '../service/khach-san.service';

@Injectable({ providedIn: 'root' })
export class KhachSanRoutingResolveService implements Resolve<IKhachSan> {
  constructor(protected service: KhachSanService, protected router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IKhachSan> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        mergeMap((khachSan: HttpResponse<KhachSan>) => {
          if (khachSan.body) {
            return of(khachSan.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new KhachSan());
  }
}
