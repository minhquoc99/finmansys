import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { UserRouteAccessService } from 'app/core/auth/user-route-access.service';
import { PhieuChiComponent } from '../list/phieu-chi.component';
import { PhieuChiDetailComponent } from '../detail/phieu-chi-detail.component';
import { PhieuChiUpdateComponent } from '../update/phieu-chi-update.component';
import { PhieuChiRoutingResolveService } from './phieu-chi-routing-resolve.service';

const phieuChiRoute: Routes = [
  {
    path: '',
    component: PhieuChiComponent,
    data: {
      defaultSort: 'id,asc',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: PhieuChiDetailComponent,
    resolve: {
      phieuChi: PhieuChiRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: PhieuChiUpdateComponent,
    resolve: {
      phieuChi: PhieuChiRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: PhieuChiUpdateComponent,
    resolve: {
      phieuChi: PhieuChiRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
];

@NgModule({
  imports: [RouterModule.forChild(phieuChiRoute)],
  exports: [RouterModule],
})
export class PhieuChiRoutingModule {}
