package com.group3.finmansys.service.dto;

import static org.assertj.core.api.Assertions.assertThat;

import com.group3.finmansys.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class LoaiQuyDTOTest {

    @Test
    void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(LoaiQuyDTO.class);
        LoaiQuyDTO loaiQuyDTO1 = new LoaiQuyDTO();
        loaiQuyDTO1.setId(1L);
        LoaiQuyDTO loaiQuyDTO2 = new LoaiQuyDTO();
        assertThat(loaiQuyDTO1).isNotEqualTo(loaiQuyDTO2);
        loaiQuyDTO2.setId(loaiQuyDTO1.getId());
        assertThat(loaiQuyDTO1).isEqualTo(loaiQuyDTO2);
        loaiQuyDTO2.setId(2L);
        assertThat(loaiQuyDTO1).isNotEqualTo(loaiQuyDTO2);
        loaiQuyDTO1.setId(null);
        assertThat(loaiQuyDTO1).isNotEqualTo(loaiQuyDTO2);
    }
}
