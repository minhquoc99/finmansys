import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { KhoanMucChiPhiDetailComponent } from './khoan-muc-chi-phi-detail.component';

describe('Component Tests', () => {
  describe('KhoanMucChiPhi Management Detail Component', () => {
    let comp: KhoanMucChiPhiDetailComponent;
    let fixture: ComponentFixture<KhoanMucChiPhiDetailComponent>;

    beforeEach(() => {
      TestBed.configureTestingModule({
        declarations: [KhoanMucChiPhiDetailComponent],
        providers: [
          {
            provide: ActivatedRoute,
            useValue: { data: of({ khoanMucChiPhi: { id: 123 } }) },
          },
        ],
      })
        .overrideTemplate(KhoanMucChiPhiDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(KhoanMucChiPhiDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should load khoanMucChiPhi on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.khoanMucChiPhi).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
