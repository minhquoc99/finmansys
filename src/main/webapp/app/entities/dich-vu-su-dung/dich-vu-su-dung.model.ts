import * as dayjs from 'dayjs';
import { IDichVu } from 'app/entities/dich-vu/dich-vu.model';
import { IPhongDuocDat } from 'app/entities/phong-duoc-dat/phong-duoc-dat.model';

export interface IDichVuSuDung {
  id?: number;
  soLuong?: number;
  gia?: number;
  ngaySuDung?: dayjs.Dayjs;
  dichVu?: IDichVu;
  phongDuocDat?: IPhongDuocDat;
}

export class DichVuSuDung implements IDichVuSuDung {
  constructor(
    public id?: number,
    public soLuong?: number,
    public gia?: number,
    public ngaySuDung?: dayjs.Dayjs,
    public dichVu?: IDichVu,
    public phongDuocDat?: IPhongDuocDat
  ) {}
}

export function getDichVuSuDungIdentifier(dichVuSuDung: IDichVuSuDung): number | undefined {
  return dichVuSuDung.id;
}
