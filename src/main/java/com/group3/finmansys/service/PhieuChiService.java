package com.group3.finmansys.service;

import com.group3.finmansys.service.dto.PhieuChiDTO;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Service Interface for managing {@link com.group3.finmansys.domain.PhieuChi}.
 */
public interface PhieuChiService {
    /**
     * Save a phieuChi.
     *
     * @param phieuChiDTO the entity to save.
     * @return the persisted entity.
     */
    PhieuChiDTO save(PhieuChiDTO phieuChiDTO);

    /**
     * Partially updates a phieuChi.
     *
     * @param phieuChiDTO the entity to update partially.
     * @return the persisted entity.
     */
    Optional<PhieuChiDTO> partialUpdate(PhieuChiDTO phieuChiDTO);

    /**
     * Get all the phieuChis.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<PhieuChiDTO> findAll(Pageable pageable);

    /**
     * Get the "id" phieuChi.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<PhieuChiDTO> findOne(Long id);

    /**
     * Delete the "id" phieuChi.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
