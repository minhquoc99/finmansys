package com.group3.finmansys.service.criteria;

import java.io.Serializable;
import java.util.Objects;
import tech.jhipster.service.Criteria;
import tech.jhipster.service.filter.BooleanFilter;
import tech.jhipster.service.filter.DoubleFilter;
import tech.jhipster.service.filter.Filter;
import tech.jhipster.service.filter.FloatFilter;
import tech.jhipster.service.filter.InstantFilter;
import tech.jhipster.service.filter.IntegerFilter;
import tech.jhipster.service.filter.LongFilter;
import tech.jhipster.service.filter.StringFilter;

/**
 * Criteria class for the {@link com.group3.finmansys.domain.ChiTietTangTSCD} entity. This class is used
 * in {@link com.group3.finmansys.web.rest.ChiTietTangTSCDResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /chi-tiet-tang-tscds?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class ChiTietTangTSCDCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private InstantFilter ngayThuMua;

    private IntegerFilter soLuong;

    private DoubleFilter chiPhiMua;

    private LongFilter taiSanCDId;

    public ChiTietTangTSCDCriteria() {}

    public ChiTietTangTSCDCriteria(ChiTietTangTSCDCriteria other) {
        this.id = other.id == null ? null : other.id.copy();
        this.ngayThuMua = other.ngayThuMua == null ? null : other.ngayThuMua.copy();
        this.soLuong = other.soLuong == null ? null : other.soLuong.copy();
        this.chiPhiMua = other.chiPhiMua == null ? null : other.chiPhiMua.copy();
        this.taiSanCDId = other.taiSanCDId == null ? null : other.taiSanCDId.copy();
    }

    @Override
    public ChiTietTangTSCDCriteria copy() {
        return new ChiTietTangTSCDCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public LongFilter id() {
        if (id == null) {
            id = new LongFilter();
        }
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public InstantFilter getNgayThuMua() {
        return ngayThuMua;
    }

    public InstantFilter ngayThuMua() {
        if (ngayThuMua == null) {
            ngayThuMua = new InstantFilter();
        }
        return ngayThuMua;
    }

    public void setNgayThuMua(InstantFilter ngayThuMua) {
        this.ngayThuMua = ngayThuMua;
    }

    public IntegerFilter getSoLuong() {
        return soLuong;
    }

    public IntegerFilter soLuong() {
        if (soLuong == null) {
            soLuong = new IntegerFilter();
        }
        return soLuong;
    }

    public void setSoLuong(IntegerFilter soLuong) {
        this.soLuong = soLuong;
    }

    public DoubleFilter getChiPhiMua() {
        return chiPhiMua;
    }

    public DoubleFilter chiPhiMua() {
        if (chiPhiMua == null) {
            chiPhiMua = new DoubleFilter();
        }
        return chiPhiMua;
    }

    public void setChiPhiMua(DoubleFilter chiPhiMua) {
        this.chiPhiMua = chiPhiMua;
    }

    public LongFilter getTaiSanCDId() {
        return taiSanCDId;
    }

    public LongFilter taiSanCDId() {
        if (taiSanCDId == null) {
            taiSanCDId = new LongFilter();
        }
        return taiSanCDId;
    }

    public void setTaiSanCDId(LongFilter taiSanCDId) {
        this.taiSanCDId = taiSanCDId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final ChiTietTangTSCDCriteria that = (ChiTietTangTSCDCriteria) o;
        return (
            Objects.equals(id, that.id) &&
            Objects.equals(ngayThuMua, that.ngayThuMua) &&
            Objects.equals(soLuong, that.soLuong) &&
            Objects.equals(chiPhiMua, that.chiPhiMua) &&
            Objects.equals(taiSanCDId, that.taiSanCDId)
        );
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, ngayThuMua, soLuong, chiPhiMua, taiSanCDId);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "ChiTietTangTSCDCriteria{" +
            (id != null ? "id=" + id + ", " : "") +
            (ngayThuMua != null ? "ngayThuMua=" + ngayThuMua + ", " : "") +
            (soLuong != null ? "soLuong=" + soLuong + ", " : "") +
            (chiPhiMua != null ? "chiPhiMua=" + chiPhiMua + ", " : "") +
            (taiSanCDId != null ? "taiSanCDId=" + taiSanCDId + ", " : "") +
            "}";
    }
}
