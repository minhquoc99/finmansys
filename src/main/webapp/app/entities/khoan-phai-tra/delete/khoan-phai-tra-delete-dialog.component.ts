import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import { IKhoanPhaiTra } from '../khoan-phai-tra.model';
import { KhoanPhaiTraService } from '../service/khoan-phai-tra.service';

@Component({
  templateUrl: './khoan-phai-tra-delete-dialog.component.html',
})
export class KhoanPhaiTraDeleteDialogComponent {
  khoanPhaiTra?: IKhoanPhaiTra;

  constructor(protected khoanPhaiTraService: KhoanPhaiTraService, public activeModal: NgbActiveModal) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.khoanPhaiTraService.delete(id).subscribe(() => {
      this.activeModal.close('deleted');
    });
  }
}
