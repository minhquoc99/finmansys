import * as dayjs from 'dayjs';
import { IKhachHang } from 'app/entities/khach-hang/khach-hang.model';
import { IKhoanMucChiPhi } from 'app/entities/khoan-muc-chi-phi/khoan-muc-chi-phi.model';
import { IKhoanPhaiTra } from 'app/entities/khoan-phai-tra/khoan-phai-tra.model';
import { IQuyTien } from 'app/entities/quy-tien/quy-tien.model';

export interface IPhieuChi {
  id?: number;
  ngayChungTu?: dayjs.Dayjs;
  soTien?: number;
  khachHang?: IKhachHang;
  khoanMucChiPhi?: IKhoanMucChiPhi;
  khoanPhaiTra?: IKhoanPhaiTra | null;
  quyTien?: IQuyTien;
}

export class PhieuChi implements IPhieuChi {
  constructor(
    public id?: number,
    public ngayChungTu?: dayjs.Dayjs,
    public soTien?: number,
    public khachHang?: IKhachHang,
    public khoanMucChiPhi?: IKhoanMucChiPhi,
    public khoanPhaiTra?: IKhoanPhaiTra | null,
    public quyTien?: IQuyTien
  ) {}
}

export function getPhieuChiIdentifier(phieuChi: IPhieuChi): number | undefined {
  return phieuChi.id;
}
