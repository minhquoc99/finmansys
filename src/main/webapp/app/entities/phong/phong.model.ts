import { IKhachSan } from 'app/entities/khach-san/khach-san.model';

export interface IPhong {
  id?: number;
  ten?: string;
  loai?: string;
  gia?: number;
  khachSan?: IKhachSan;
}

export class Phong implements IPhong {
  constructor(public id?: number, public ten?: string, public loai?: string, public gia?: number, public khachSan?: IKhachSan) {}
}

export function getPhongIdentifier(phong: IPhong): number | undefined {
  return phong.id;
}
