import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { isPresent } from 'app/core/util/operators';
import { ApplicationConfigService } from 'app/core/config/application-config.service';
import { createRequestOption } from 'app/core/request/request-util';
import { ICongThuc, getCongThucIdentifier } from '../cong-thuc.model';

export type EntityResponseType = HttpResponse<ICongThuc>;
export type EntityArrayResponseType = HttpResponse<ICongThuc[]>;

@Injectable({ providedIn: 'root' })
export class CongThucService {
  public resourceUrl = this.applicationConfigService.getEndpointFor('api/cong-thucs');

  constructor(protected http: HttpClient, private applicationConfigService: ApplicationConfigService) {}

  create(congThuc: ICongThuc): Observable<EntityResponseType> {
    return this.http.post<ICongThuc>(this.resourceUrl, congThuc, { observe: 'response' });
  }

  update(congThuc: ICongThuc): Observable<EntityResponseType> {
    return this.http.put<ICongThuc>(`${this.resourceUrl}/${getCongThucIdentifier(congThuc) as number}`, congThuc, { observe: 'response' });
  }

  partialUpdate(congThuc: ICongThuc): Observable<EntityResponseType> {
    return this.http.patch<ICongThuc>(`${this.resourceUrl}/${getCongThucIdentifier(congThuc) as number}`, congThuc, {
      observe: 'response',
    });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<ICongThuc>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<ICongThuc[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  getBangCanDoiKeToan(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<ICongThuc[]>(`${this.resourceUrl}/bang-can-doi-ke-toan`, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  addCongThucToCollectionIfMissing(congThucCollection: ICongThuc[], ...congThucsToCheck: (ICongThuc | null | undefined)[]): ICongThuc[] {
    const congThucs: ICongThuc[] = congThucsToCheck.filter(isPresent);
    if (congThucs.length > 0) {
      const congThucCollectionIdentifiers = congThucCollection.map(congThucItem => getCongThucIdentifier(congThucItem)!);
      const congThucsToAdd = congThucs.filter(congThucItem => {
        const congThucIdentifier = getCongThucIdentifier(congThucItem);
        if (congThucIdentifier == null || congThucCollectionIdentifiers.includes(congThucIdentifier)) {
          return false;
        }
        congThucCollectionIdentifiers.push(congThucIdentifier);
        return true;
      });
      return [...congThucsToAdd, ...congThucCollection];
    }
    return congThucCollection;
  }
}
