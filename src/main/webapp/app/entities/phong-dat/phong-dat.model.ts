import * as dayjs from 'dayjs';
import { IPhong } from 'app/entities/phong/phong.model';
import { IPhongDuocDat } from 'app/entities/phong-duoc-dat/phong-duoc-dat.model';

export interface IPhongDat {
  id?: number;
  ngayDat?: dayjs.Dayjs;
  phong?: IPhong;
  phongDuocDat?: IPhongDuocDat;
}

export class PhongDat implements IPhongDat {
  constructor(public id?: number, public ngayDat?: dayjs.Dayjs, public phong?: IPhong, public phongDuocDat?: IPhongDuocDat) {}
}

export function getPhongDatIdentifier(phongDat: IPhongDat): number | undefined {
  return phongDat.id;
}
