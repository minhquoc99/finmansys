package com.group3.finmansys.domain;

import static org.assertj.core.api.Assertions.assertThat;

import com.group3.finmansys.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class KhachSanTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(KhachSan.class);
        KhachSan khachSan1 = new KhachSan();
        khachSan1.setId(1L);
        KhachSan khachSan2 = new KhachSan();
        khachSan2.setId(khachSan1.getId());
        assertThat(khachSan1).isEqualTo(khachSan2);
        khachSan2.setId(2L);
        assertThat(khachSan1).isNotEqualTo(khachSan2);
        khachSan1.setId(null);
        assertThat(khachSan1).isNotEqualTo(khachSan2);
    }
}
