import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { UserRouteAccessService } from 'app/core/auth/user-route-access.service';
import { NganSachThangComponent } from '../list/ngan-sach-thang.component';
import { NganSachThangDetailComponent } from '../detail/ngan-sach-thang-detail.component';
import { NganSachThangUpdateComponent } from '../update/ngan-sach-thang-update.component';
import { NganSachThangRoutingResolveService } from './ngan-sach-thang-routing-resolve.service';

const nganSachThangRoute: Routes = [
  {
    path: '',
    component: NganSachThangComponent,
    data: {
      defaultSort: 'id,asc',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: NganSachThangDetailComponent,
    resolve: {
      nganSachThang: NganSachThangRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: NganSachThangUpdateComponent,
    resolve: {
      nganSachThang: NganSachThangRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: NganSachThangUpdateComponent,
    resolve: {
      nganSachThang: NganSachThangRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
];

@NgModule({
  imports: [RouterModule.forChild(nganSachThangRoute)],
  exports: [RouterModule],
})
export class NganSachThangRoutingModule {}
