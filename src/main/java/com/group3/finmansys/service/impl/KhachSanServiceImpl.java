package com.group3.finmansys.service.impl;

import com.group3.finmansys.domain.KhachSan;
import com.group3.finmansys.repository.KhachSanRepository;
import com.group3.finmansys.service.KhachSanService;
import com.group3.finmansys.service.dto.KhachSanDTO;
import com.group3.finmansys.service.mapper.KhachSanMapper;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link KhachSan}.
 */
@Service
@Transactional
public class KhachSanServiceImpl implements KhachSanService {

    private final Logger log = LoggerFactory.getLogger(KhachSanServiceImpl.class);

    private final KhachSanRepository khachSanRepository;

    private final KhachSanMapper khachSanMapper;

    public KhachSanServiceImpl(KhachSanRepository khachSanRepository, KhachSanMapper khachSanMapper) {
        this.khachSanRepository = khachSanRepository;
        this.khachSanMapper = khachSanMapper;
    }

    @Override
    public KhachSanDTO save(KhachSanDTO khachSanDTO) {
        log.debug("Request to save KhachSan : {}", khachSanDTO);
        KhachSan khachSan = khachSanMapper.toEntity(khachSanDTO);
        khachSan = khachSanRepository.save(khachSan);
        return khachSanMapper.toDto(khachSan);
    }

    @Override
    public Optional<KhachSanDTO> partialUpdate(KhachSanDTO khachSanDTO) {
        log.debug("Request to partially update KhachSan : {}", khachSanDTO);

        return khachSanRepository
            .findById(khachSanDTO.getId())
            .map(
                existingKhachSan -> {
                    khachSanMapper.partialUpdate(existingKhachSan, khachSanDTO);
                    return existingKhachSan;
                }
            )
            .map(khachSanRepository::save)
            .map(khachSanMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<KhachSanDTO> findAll(Pageable pageable) {
        log.debug("Request to get all KhachSans");
        return khachSanRepository.findAll(pageable).map(khachSanMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<KhachSanDTO> findOne(Long id) {
        log.debug("Request to get KhachSan : {}", id);
        return khachSanRepository.findById(id).map(khachSanMapper::toDto);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete KhachSan : {}", id);
        khachSanRepository.deleteById(id);
    }
}
