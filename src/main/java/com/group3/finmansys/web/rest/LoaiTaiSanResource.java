package com.group3.finmansys.web.rest;

import com.group3.finmansys.repository.LoaiTaiSanRepository;
import com.group3.finmansys.service.LoaiTaiSanQueryService;
import com.group3.finmansys.service.LoaiTaiSanService;
import com.group3.finmansys.service.criteria.LoaiTaiSanCriteria;
import com.group3.finmansys.service.dto.LoaiTaiSanDTO;
import com.group3.finmansys.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.PaginationUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link com.group3.finmansys.domain.LoaiTaiSan}.
 */
@RestController
@RequestMapping("/api")
public class LoaiTaiSanResource {

    private final Logger log = LoggerFactory.getLogger(LoaiTaiSanResource.class);

    private static final String ENTITY_NAME = "loaiTaiSan";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final LoaiTaiSanService loaiTaiSanService;

    private final LoaiTaiSanRepository loaiTaiSanRepository;

    private final LoaiTaiSanQueryService loaiTaiSanQueryService;

    public LoaiTaiSanResource(
        LoaiTaiSanService loaiTaiSanService,
        LoaiTaiSanRepository loaiTaiSanRepository,
        LoaiTaiSanQueryService loaiTaiSanQueryService
    ) {
        this.loaiTaiSanService = loaiTaiSanService;
        this.loaiTaiSanRepository = loaiTaiSanRepository;
        this.loaiTaiSanQueryService = loaiTaiSanQueryService;
    }

    /**
     * {@code POST  /loai-tai-sans} : Create a new loaiTaiSan.
     *
     * @param loaiTaiSanDTO the loaiTaiSanDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new loaiTaiSanDTO, or with status {@code 400 (Bad Request)} if the loaiTaiSan has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/loai-tai-sans")
    public ResponseEntity<LoaiTaiSanDTO> createLoaiTaiSan(@Valid @RequestBody LoaiTaiSanDTO loaiTaiSanDTO) throws URISyntaxException {
        log.debug("REST request to save LoaiTaiSan : {}", loaiTaiSanDTO);
        if (loaiTaiSanDTO.getId() != null) {
            throw new BadRequestAlertException("A new loaiTaiSan cannot already have an ID", ENTITY_NAME, "idexists");
        }
        LoaiTaiSanDTO result = loaiTaiSanService.save(loaiTaiSanDTO);
        return ResponseEntity
            .created(new URI("/api/loai-tai-sans/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /loai-tai-sans/:id} : Updates an existing loaiTaiSan.
     *
     * @param id the id of the loaiTaiSanDTO to save.
     * @param loaiTaiSanDTO the loaiTaiSanDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated loaiTaiSanDTO,
     * or with status {@code 400 (Bad Request)} if the loaiTaiSanDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the loaiTaiSanDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/loai-tai-sans/{id}")
    public ResponseEntity<LoaiTaiSanDTO> updateLoaiTaiSan(
        @PathVariable(value = "id", required = false) final Long id,
        @Valid @RequestBody LoaiTaiSanDTO loaiTaiSanDTO
    ) throws URISyntaxException {
        log.debug("REST request to update LoaiTaiSan : {}, {}", id, loaiTaiSanDTO);
        if (loaiTaiSanDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, loaiTaiSanDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!loaiTaiSanRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        LoaiTaiSanDTO result = loaiTaiSanService.save(loaiTaiSanDTO);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, loaiTaiSanDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /loai-tai-sans/:id} : Partial updates given fields of an existing loaiTaiSan, field will ignore if it is null
     *
     * @param id the id of the loaiTaiSanDTO to save.
     * @param loaiTaiSanDTO the loaiTaiSanDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated loaiTaiSanDTO,
     * or with status {@code 400 (Bad Request)} if the loaiTaiSanDTO is not valid,
     * or with status {@code 404 (Not Found)} if the loaiTaiSanDTO is not found,
     * or with status {@code 500 (Internal Server Error)} if the loaiTaiSanDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/loai-tai-sans/{id}", consumes = "application/merge-patch+json")
    public ResponseEntity<LoaiTaiSanDTO> partialUpdateLoaiTaiSan(
        @PathVariable(value = "id", required = false) final Long id,
        @NotNull @RequestBody LoaiTaiSanDTO loaiTaiSanDTO
    ) throws URISyntaxException {
        log.debug("REST request to partial update LoaiTaiSan partially : {}, {}", id, loaiTaiSanDTO);
        if (loaiTaiSanDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, loaiTaiSanDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!loaiTaiSanRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<LoaiTaiSanDTO> result = loaiTaiSanService.partialUpdate(loaiTaiSanDTO);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, loaiTaiSanDTO.getId().toString())
        );
    }

    /**
     * {@code GET  /loai-tai-sans} : get all the loaiTaiSans.
     *
     * @param pageable the pagination information.
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of loaiTaiSans in body.
     */
    @GetMapping("/loai-tai-sans")
    public ResponseEntity<List<LoaiTaiSanDTO>> getAllLoaiTaiSans(LoaiTaiSanCriteria criteria, Pageable pageable) {
        log.debug("REST request to get LoaiTaiSans by criteria: {}", criteria);
        Page<LoaiTaiSanDTO> page = loaiTaiSanQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /loai-tai-sans/count} : count all the loaiTaiSans.
     *
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
     */
    @GetMapping("/loai-tai-sans/count")
    public ResponseEntity<Long> countLoaiTaiSans(LoaiTaiSanCriteria criteria) {
        log.debug("REST request to count LoaiTaiSans by criteria: {}", criteria);
        return ResponseEntity.ok().body(loaiTaiSanQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /loai-tai-sans/:id} : get the "id" loaiTaiSan.
     *
     * @param id the id of the loaiTaiSanDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the loaiTaiSanDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/loai-tai-sans/{id}")
    public ResponseEntity<LoaiTaiSanDTO> getLoaiTaiSan(@PathVariable Long id) {
        log.debug("REST request to get LoaiTaiSan : {}", id);
        Optional<LoaiTaiSanDTO> loaiTaiSanDTO = loaiTaiSanService.findOne(id);
        return ResponseUtil.wrapOrNotFound(loaiTaiSanDTO);
    }

    /**
     * {@code DELETE  /loai-tai-sans/:id} : delete the "id" loaiTaiSan.
     *
     * @param id the id of the loaiTaiSanDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/loai-tai-sans/{id}")
    public ResponseEntity<Void> deleteLoaiTaiSan(@PathVariable Long id) {
        log.debug("REST request to delete LoaiTaiSan : {}", id);
        loaiTaiSanService.delete(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
            .build();
    }
}
