import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { UserRouteAccessService } from 'app/core/auth/user-route-access.service';
import { LoaiQuyComponent } from '../list/loai-quy.component';
import { LoaiQuyDetailComponent } from '../detail/loai-quy-detail.component';
import { LoaiQuyUpdateComponent } from '../update/loai-quy-update.component';
import { LoaiQuyRoutingResolveService } from './loai-quy-routing-resolve.service';

const loaiQuyRoute: Routes = [
  {
    path: '',
    component: LoaiQuyComponent,
    data: {
      defaultSort: 'id,asc',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: LoaiQuyDetailComponent,
    resolve: {
      loaiQuy: LoaiQuyRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: LoaiQuyUpdateComponent,
    resolve: {
      loaiQuy: LoaiQuyRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: LoaiQuyUpdateComponent,
    resolve: {
      loaiQuy: LoaiQuyRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
];

@NgModule({
  imports: [RouterModule.forChild(loaiQuyRoute)],
  exports: [RouterModule],
})
export class LoaiQuyRoutingModule {}
