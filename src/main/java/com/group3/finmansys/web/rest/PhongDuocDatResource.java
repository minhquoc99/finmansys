package com.group3.finmansys.web.rest;

import com.group3.finmansys.repository.PhongDuocDatRepository;
import com.group3.finmansys.service.PhongDuocDatQueryService;
import com.group3.finmansys.service.PhongDuocDatService;
import com.group3.finmansys.service.criteria.PhongDuocDatCriteria;
import com.group3.finmansys.service.dto.PhongDuocDatDTO;
import com.group3.finmansys.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.PaginationUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link com.group3.finmansys.domain.PhongDuocDat}.
 */
@RestController
@RequestMapping("/api")
public class PhongDuocDatResource {

    private final Logger log = LoggerFactory.getLogger(PhongDuocDatResource.class);

    private static final String ENTITY_NAME = "phongDuocDat";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final PhongDuocDatService phongDuocDatService;

    private final PhongDuocDatRepository phongDuocDatRepository;

    private final PhongDuocDatQueryService phongDuocDatQueryService;

    public PhongDuocDatResource(
        PhongDuocDatService phongDuocDatService,
        PhongDuocDatRepository phongDuocDatRepository,
        PhongDuocDatQueryService phongDuocDatQueryService
    ) {
        this.phongDuocDatService = phongDuocDatService;
        this.phongDuocDatRepository = phongDuocDatRepository;
        this.phongDuocDatQueryService = phongDuocDatQueryService;
    }

    /**
     * {@code POST  /phong-duoc-dats} : Create a new phongDuocDat.
     *
     * @param phongDuocDatDTO the phongDuocDatDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new phongDuocDatDTO, or with status {@code 400 (Bad Request)} if the phongDuocDat has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/phong-duoc-dats")
    public ResponseEntity<PhongDuocDatDTO> createPhongDuocDat(@Valid @RequestBody PhongDuocDatDTO phongDuocDatDTO)
        throws URISyntaxException {
        log.debug("REST request to save PhongDuocDat : {}", phongDuocDatDTO);
        if (phongDuocDatDTO.getId() != null) {
            throw new BadRequestAlertException("A new phongDuocDat cannot already have an ID", ENTITY_NAME, "idexists");
        }
        PhongDuocDatDTO result = phongDuocDatService.save(phongDuocDatDTO);
        return ResponseEntity
            .created(new URI("/api/phong-duoc-dats/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /phong-duoc-dats/:id} : Updates an existing phongDuocDat.
     *
     * @param id the id of the phongDuocDatDTO to save.
     * @param phongDuocDatDTO the phongDuocDatDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated phongDuocDatDTO,
     * or with status {@code 400 (Bad Request)} if the phongDuocDatDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the phongDuocDatDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/phong-duoc-dats/{id}")
    public ResponseEntity<PhongDuocDatDTO> updatePhongDuocDat(
        @PathVariable(value = "id", required = false) final Long id,
        @Valid @RequestBody PhongDuocDatDTO phongDuocDatDTO
    ) throws URISyntaxException {
        log.debug("REST request to update PhongDuocDat : {}, {}", id, phongDuocDatDTO);
        if (phongDuocDatDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, phongDuocDatDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!phongDuocDatRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        PhongDuocDatDTO result = phongDuocDatService.save(phongDuocDatDTO);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, phongDuocDatDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /phong-duoc-dats/:id} : Partial updates given fields of an existing phongDuocDat, field will ignore if it is null
     *
     * @param id the id of the phongDuocDatDTO to save.
     * @param phongDuocDatDTO the phongDuocDatDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated phongDuocDatDTO,
     * or with status {@code 400 (Bad Request)} if the phongDuocDatDTO is not valid,
     * or with status {@code 404 (Not Found)} if the phongDuocDatDTO is not found,
     * or with status {@code 500 (Internal Server Error)} if the phongDuocDatDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/phong-duoc-dats/{id}", consumes = "application/merge-patch+json")
    public ResponseEntity<PhongDuocDatDTO> partialUpdatePhongDuocDat(
        @PathVariable(value = "id", required = false) final Long id,
        @NotNull @RequestBody PhongDuocDatDTO phongDuocDatDTO
    ) throws URISyntaxException {
        log.debug("REST request to partial update PhongDuocDat partially : {}, {}", id, phongDuocDatDTO);
        if (phongDuocDatDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, phongDuocDatDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!phongDuocDatRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<PhongDuocDatDTO> result = phongDuocDatService.partialUpdate(phongDuocDatDTO);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, phongDuocDatDTO.getId().toString())
        );
    }

    /**
     * {@code GET  /phong-duoc-dats} : get all the phongDuocDats.
     *
     * @param pageable the pagination information.
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of phongDuocDats in body.
     */
    @GetMapping("/phong-duoc-dats")
    public ResponseEntity<List<PhongDuocDatDTO>> getAllPhongDuocDats(PhongDuocDatCriteria criteria, Pageable pageable) {
        log.debug("REST request to get PhongDuocDats by criteria: {}", criteria);
        Page<PhongDuocDatDTO> page = phongDuocDatQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /phong-duoc-dats/count} : count all the phongDuocDats.
     *
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
     */
    @GetMapping("/phong-duoc-dats/count")
    public ResponseEntity<Long> countPhongDuocDats(PhongDuocDatCriteria criteria) {
        log.debug("REST request to count PhongDuocDats by criteria: {}", criteria);
        return ResponseEntity.ok().body(phongDuocDatQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /phong-duoc-dats/:id} : get the "id" phongDuocDat.
     *
     * @param id the id of the phongDuocDatDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the phongDuocDatDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/phong-duoc-dats/{id}")
    public ResponseEntity<PhongDuocDatDTO> getPhongDuocDat(@PathVariable Long id) {
        log.debug("REST request to get PhongDuocDat : {}", id);
        Optional<PhongDuocDatDTO> phongDuocDatDTO = phongDuocDatService.findOne(id);
        return ResponseUtil.wrapOrNotFound(phongDuocDatDTO);
    }

    /**
     * {@code DELETE  /phong-duoc-dats/:id} : delete the "id" phongDuocDat.
     *
     * @param id the id of the phongDuocDatDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/phong-duoc-dats/{id}")
    public ResponseEntity<Void> deletePhongDuocDat(@PathVariable Long id) {
        log.debug("REST request to delete PhongDuocDat : {}", id);
        phongDuocDatService.delete(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
            .build();
    }
}
