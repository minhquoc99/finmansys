import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { UserRouteAccessService } from 'app/core/auth/user-route-access.service';
import { NganSachNamComponent } from '../list/ngan-sach-nam.component';
import { NganSachNamDetailComponent } from '../detail/ngan-sach-nam-detail.component';
import { NganSachNamUpdateComponent } from '../update/ngan-sach-nam-update.component';
import { NganSachNamRoutingResolveService } from './ngan-sach-nam-routing-resolve.service';
import { HoachToanComponent } from 'app/entities/ngan-sach-nam/hoach-toan/hoach-toan.component';

const nganSachNamRoute: Routes = [
  {
    path: '',
    component: NganSachNamComponent,
    data: {
      defaultSort: 'id,asc',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: NganSachNamDetailComponent,
    resolve: {
      nganSachNam: NganSachNamRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: NganSachNamUpdateComponent,
    resolve: {
      nganSachNam: NganSachNamRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: NganSachNamUpdateComponent,
    resolve: {
      nganSachNam: NganSachNamRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'hach-toan',
    component: HoachToanComponent,
    data: {
      defaultSort: 'id,asc',
    },
    canActivate: [UserRouteAccessService],
  },
];

@NgModule({
  imports: [RouterModule.forChild(nganSachNamRoute)],
  exports: [RouterModule],
})
export class NganSachNamRoutingModule {}
