import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { UserRouteAccessService } from 'app/core/auth/user-route-access.service';
import { DichVuSuDungComponent } from '../list/dich-vu-su-dung.component';
import { DichVuSuDungDetailComponent } from '../detail/dich-vu-su-dung-detail.component';
import { DichVuSuDungUpdateComponent } from '../update/dich-vu-su-dung-update.component';
import { DichVuSuDungRoutingResolveService } from './dich-vu-su-dung-routing-resolve.service';

const dichVuSuDungRoute: Routes = [
  {
    path: '',
    component: DichVuSuDungComponent,
    data: {
      defaultSort: 'id,asc',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: DichVuSuDungDetailComponent,
    resolve: {
      dichVuSuDung: DichVuSuDungRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: DichVuSuDungUpdateComponent,
    resolve: {
      dichVuSuDung: DichVuSuDungRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: DichVuSuDungUpdateComponent,
    resolve: {
      dichVuSuDung: DichVuSuDungRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
];

@NgModule({
  imports: [RouterModule.forChild(dichVuSuDungRoute)],
  exports: [RouterModule],
})
export class DichVuSuDungRoutingModule {}
