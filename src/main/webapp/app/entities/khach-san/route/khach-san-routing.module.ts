import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { UserRouteAccessService } from 'app/core/auth/user-route-access.service';
import { KhachSanComponent } from '../list/khach-san.component';
import { KhachSanDetailComponent } from '../detail/khach-san-detail.component';
import { KhachSanUpdateComponent } from '../update/khach-san-update.component';
import { KhachSanRoutingResolveService } from './khach-san-routing-resolve.service';

const khachSanRoute: Routes = [
  {
    path: '',
    component: KhachSanComponent,
    data: {
      defaultSort: 'id,asc',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: KhachSanDetailComponent,
    resolve: {
      khachSan: KhachSanRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: KhachSanUpdateComponent,
    resolve: {
      khachSan: KhachSanRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: KhachSanUpdateComponent,
    resolve: {
      khachSan: KhachSanRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
];

@NgModule({
  imports: [RouterModule.forChild(khachSanRoute)],
  exports: [RouterModule],
})
export class KhachSanRoutingModule {}
