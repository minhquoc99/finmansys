import { NgModule } from '@angular/core';

import { SharedModule } from 'app/shared/shared.module';
import { LoaiQuyComponent } from './list/loai-quy.component';
import { LoaiQuyDetailComponent } from './detail/loai-quy-detail.component';
import { LoaiQuyUpdateComponent } from './update/loai-quy-update.component';
import { LoaiQuyDeleteDialogComponent } from './delete/loai-quy-delete-dialog.component';
import { LoaiQuyRoutingModule } from './route/loai-quy-routing.module';

@NgModule({
  imports: [SharedModule, LoaiQuyRoutingModule],
  declarations: [LoaiQuyComponent, LoaiQuyDetailComponent, LoaiQuyUpdateComponent, LoaiQuyDeleteDialogComponent],
  entryComponents: [LoaiQuyDeleteDialogComponent],
})
export class LoaiQuyModule {}
