import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import * as dayjs from 'dayjs';

import { DATE_TIME_FORMAT } from 'app/config/input.constants';
import { IPhieuChi, PhieuChi } from '../phieu-chi.model';

import { PhieuChiService } from './phieu-chi.service';

describe('Service Tests', () => {
  describe('PhieuChi Service', () => {
    let service: PhieuChiService;
    let httpMock: HttpTestingController;
    let elemDefault: IPhieuChi;
    let expectedResult: IPhieuChi | IPhieuChi[] | boolean | null;
    let currentDate: dayjs.Dayjs;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule],
      });
      expectedResult = null;
      service = TestBed.inject(PhieuChiService);
      httpMock = TestBed.inject(HttpTestingController);
      currentDate = dayjs();

      elemDefault = {
        id: 0,
        ngayChungTu: currentDate,
        soTien: 0,
      };
    });

    describe('Service methods', () => {
      it('should find an element', () => {
        const returnedFromService = Object.assign(
          {
            ngayChungTu: currentDate.format(DATE_TIME_FORMAT),
          },
          elemDefault
        );

        service.find(123).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(elemDefault);
      });

      it('should create a PhieuChi', () => {
        const returnedFromService = Object.assign(
          {
            id: 0,
            ngayChungTu: currentDate.format(DATE_TIME_FORMAT),
          },
          elemDefault
        );

        const expected = Object.assign(
          {
            ngayChungTu: currentDate,
          },
          returnedFromService
        );

        service.create(new PhieuChi()).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'POST' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should update a PhieuChi', () => {
        const returnedFromService = Object.assign(
          {
            id: 1,
            ngayChungTu: currentDate.format(DATE_TIME_FORMAT),
            soTien: 1,
          },
          elemDefault
        );

        const expected = Object.assign(
          {
            ngayChungTu: currentDate,
          },
          returnedFromService
        );

        service.update(expected).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'PUT' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should partial update a PhieuChi', () => {
        const patchObject = Object.assign(
          {
            ngayChungTu: currentDate.format(DATE_TIME_FORMAT),
            soTien: 1,
          },
          new PhieuChi()
        );

        const returnedFromService = Object.assign(patchObject, elemDefault);

        const expected = Object.assign(
          {
            ngayChungTu: currentDate,
          },
          returnedFromService
        );

        service.partialUpdate(patchObject).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'PATCH' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should return a list of PhieuChi', () => {
        const returnedFromService = Object.assign(
          {
            id: 1,
            ngayChungTu: currentDate.format(DATE_TIME_FORMAT),
            soTien: 1,
          },
          elemDefault
        );

        const expected = Object.assign(
          {
            ngayChungTu: currentDate,
          },
          returnedFromService
        );

        service.query().subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush([returnedFromService]);
        httpMock.verify();
        expect(expectedResult).toContainEqual(expected);
      });

      it('should delete a PhieuChi', () => {
        service.delete(123).subscribe(resp => (expectedResult = resp.ok));

        const req = httpMock.expectOne({ method: 'DELETE' });
        req.flush({ status: 200 });
        expect(expectedResult);
      });

      describe('addPhieuChiToCollectionIfMissing', () => {
        it('should add a PhieuChi to an empty array', () => {
          const phieuChi: IPhieuChi = { id: 123 };
          expectedResult = service.addPhieuChiToCollectionIfMissing([], phieuChi);
          expect(expectedResult).toHaveLength(1);
          expect(expectedResult).toContain(phieuChi);
        });

        it('should not add a PhieuChi to an array that contains it', () => {
          const phieuChi: IPhieuChi = { id: 123 };
          const phieuChiCollection: IPhieuChi[] = [
            {
              ...phieuChi,
            },
            { id: 456 },
          ];
          expectedResult = service.addPhieuChiToCollectionIfMissing(phieuChiCollection, phieuChi);
          expect(expectedResult).toHaveLength(2);
        });

        it("should add a PhieuChi to an array that doesn't contain it", () => {
          const phieuChi: IPhieuChi = { id: 123 };
          const phieuChiCollection: IPhieuChi[] = [{ id: 456 }];
          expectedResult = service.addPhieuChiToCollectionIfMissing(phieuChiCollection, phieuChi);
          expect(expectedResult).toHaveLength(2);
          expect(expectedResult).toContain(phieuChi);
        });

        it('should add only unique PhieuChi to an array', () => {
          const phieuChiArray: IPhieuChi[] = [{ id: 123 }, { id: 456 }, { id: 9835 }];
          const phieuChiCollection: IPhieuChi[] = [{ id: 123 }];
          expectedResult = service.addPhieuChiToCollectionIfMissing(phieuChiCollection, ...phieuChiArray);
          expect(expectedResult).toHaveLength(3);
        });

        it('should accept varargs', () => {
          const phieuChi: IPhieuChi = { id: 123 };
          const phieuChi2: IPhieuChi = { id: 456 };
          expectedResult = service.addPhieuChiToCollectionIfMissing([], phieuChi, phieuChi2);
          expect(expectedResult).toHaveLength(2);
          expect(expectedResult).toContain(phieuChi);
          expect(expectedResult).toContain(phieuChi2);
        });

        it('should accept null and undefined values', () => {
          const phieuChi: IPhieuChi = { id: 123 };
          expectedResult = service.addPhieuChiToCollectionIfMissing([], null, phieuChi, undefined);
          expect(expectedResult).toHaveLength(1);
          expect(expectedResult).toContain(phieuChi);
        });
      });
    });

    afterEach(() => {
      httpMock.verify();
    });
  });
});
