package com.group3.finmansys.repository;

import com.group3.finmansys.domain.MaKeToan;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data SQL repository for the MaKeToan entity.
 */
@SuppressWarnings("unused")
@Repository
public interface MaKeToanRepository extends JpaRepository<MaKeToan, Long>, JpaSpecificationExecutor<MaKeToan> {}
