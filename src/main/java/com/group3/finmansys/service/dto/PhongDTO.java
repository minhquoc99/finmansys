package com.group3.finmansys.service.dto;

import java.io.Serializable;
import java.util.Objects;
import javax.validation.constraints.*;

/**
 * A DTO for the {@link com.group3.finmansys.domain.Phong} entity.
 */
public class PhongDTO implements Serializable {

    private Long id;

    @NotNull
    private String ten;

    @NotNull
    private String loai;

    @NotNull
    @DecimalMin(value = "1")
    private Double gia;

    private KhachSanDTO khachSan;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTen() {
        return ten;
    }

    public void setTen(String ten) {
        this.ten = ten;
    }

    public String getLoai() {
        return loai;
    }

    public void setLoai(String loai) {
        this.loai = loai;
    }

    public Double getGia() {
        return gia;
    }

    public void setGia(Double gia) {
        this.gia = gia;
    }

    public KhachSanDTO getKhachSan() {
        return khachSan;
    }

    public void setKhachSan(KhachSanDTO khachSan) {
        this.khachSan = khachSan;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof PhongDTO)) {
            return false;
        }

        PhongDTO phongDTO = (PhongDTO) o;
        if (this.id == null) {
            return false;
        }
        return Objects.equals(this.id, phongDTO.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.id);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "PhongDTO{" +
            "id=" + getId() +
            ", ten='" + getTen() + "'" +
            ", loai='" + getLoai() + "'" +
            ", gia=" + getGia() +
            ", khachSan=" + getKhachSan() +
            "}";
    }
}
