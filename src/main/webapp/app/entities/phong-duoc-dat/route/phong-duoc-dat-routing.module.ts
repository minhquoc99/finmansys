import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { UserRouteAccessService } from 'app/core/auth/user-route-access.service';
import { PhongDuocDatComponent } from '../list/phong-duoc-dat.component';
import { PhongDuocDatDetailComponent } from '../detail/phong-duoc-dat-detail.component';
import { PhongDuocDatUpdateComponent } from '../update/phong-duoc-dat-update.component';
import { PhongDuocDatRoutingResolveService } from './phong-duoc-dat-routing-resolve.service';

const phongDuocDatRoute: Routes = [
  {
    path: '',
    component: PhongDuocDatComponent,
    data: {
      defaultSort: 'id,asc',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: PhongDuocDatDetailComponent,
    resolve: {
      phongDuocDat: PhongDuocDatRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: PhongDuocDatUpdateComponent,
    resolve: {
      phongDuocDat: PhongDuocDatRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: PhongDuocDatUpdateComponent,
    resolve: {
      phongDuocDat: PhongDuocDatRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
];

@NgModule({
  imports: [RouterModule.forChild(phongDuocDatRoute)],
  exports: [RouterModule],
})
export class PhongDuocDatRoutingModule {}
