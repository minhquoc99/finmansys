package com.group3.finmansys.domain;

import static org.assertj.core.api.Assertions.assertThat;

import com.group3.finmansys.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class CongThucTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(CongThuc.class);
        CongThuc congThuc1 = new CongThuc();
        congThuc1.setId(1L);
        CongThuc congThuc2 = new CongThuc();
        congThuc2.setId(congThuc1.getId());
        assertThat(congThuc1).isEqualTo(congThuc2);
        congThuc2.setId(2L);
        assertThat(congThuc1).isNotEqualTo(congThuc2);
        congThuc1.setId(null);
        assertThat(congThuc1).isNotEqualTo(congThuc2);
    }
}
