package com.group3.finmansys.repository;

import com.group3.finmansys.domain.KhachSan;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data SQL repository for the KhachSan entity.
 */
@SuppressWarnings("unused")
@Repository
public interface KhachSanRepository extends JpaRepository<KhachSan, Long>, JpaSpecificationExecutor<KhachSan> {}
