import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { ITaiSanCD, TaiSanCD } from '../tai-san-cd.model';

import { TaiSanCDService } from './tai-san-cd.service';

describe('Service Tests', () => {
  describe('TaiSanCD Service', () => {
    let service: TaiSanCDService;
    let httpMock: HttpTestingController;
    let elemDefault: ITaiSanCD;
    let expectedResult: ITaiSanCD | ITaiSanCD[] | boolean | null;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule],
      });
      expectedResult = null;
      service = TestBed.inject(TaiSanCDService);
      httpMock = TestBed.inject(HttpTestingController);

      elemDefault = {
        id: 0,
        ten: 'AAAAAAA',
        donViTuoiTho: 'AAAAAAA',
        thoiGianSuDung: 0,
        hinhAnhContentType: 'image/png',
        hinhAnh: 'AAAAAAA',
      };
    });

    describe('Service methods', () => {
      it('should find an element', () => {
        const returnedFromService = Object.assign({}, elemDefault);

        service.find(123).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(elemDefault);
      });

      it('should create a TaiSanCD', () => {
        const returnedFromService = Object.assign(
          {
            id: 0,
          },
          elemDefault
        );

        const expected = Object.assign({}, returnedFromService);

        service.create(new TaiSanCD()).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'POST' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should update a TaiSanCD', () => {
        const returnedFromService = Object.assign(
          {
            id: 1,
            ten: 'BBBBBB',
            donViTuoiTho: 'BBBBBB',
            thoiGianSuDung: 1,
            hinhAnh: 'BBBBBB',
          },
          elemDefault
        );

        const expected = Object.assign({}, returnedFromService);

        service.update(expected).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'PUT' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should partial update a TaiSanCD', () => {
        const patchObject = Object.assign(
          {
            ten: 'BBBBBB',
            thoiGianSuDung: 1,
            hinhAnh: 'BBBBBB',
          },
          new TaiSanCD()
        );

        const returnedFromService = Object.assign(patchObject, elemDefault);

        const expected = Object.assign({}, returnedFromService);

        service.partialUpdate(patchObject).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'PATCH' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should return a list of TaiSanCD', () => {
        const returnedFromService = Object.assign(
          {
            id: 1,
            ten: 'BBBBBB',
            donViTuoiTho: 'BBBBBB',
            thoiGianSuDung: 1,
            hinhAnh: 'BBBBBB',
          },
          elemDefault
        );

        const expected = Object.assign({}, returnedFromService);

        service.query().subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush([returnedFromService]);
        httpMock.verify();
        expect(expectedResult).toContainEqual(expected);
      });

      it('should delete a TaiSanCD', () => {
        service.delete(123).subscribe(resp => (expectedResult = resp.ok));

        const req = httpMock.expectOne({ method: 'DELETE' });
        req.flush({ status: 200 });
        expect(expectedResult);
      });

      describe('addTaiSanCDToCollectionIfMissing', () => {
        it('should add a TaiSanCD to an empty array', () => {
          const taiSanCD: ITaiSanCD = { id: 123 };
          expectedResult = service.addTaiSanCDToCollectionIfMissing([], taiSanCD);
          expect(expectedResult).toHaveLength(1);
          expect(expectedResult).toContain(taiSanCD);
        });

        it('should not add a TaiSanCD to an array that contains it', () => {
          const taiSanCD: ITaiSanCD = { id: 123 };
          const taiSanCDCollection: ITaiSanCD[] = [
            {
              ...taiSanCD,
            },
            { id: 456 },
          ];
          expectedResult = service.addTaiSanCDToCollectionIfMissing(taiSanCDCollection, taiSanCD);
          expect(expectedResult).toHaveLength(2);
        });

        it("should add a TaiSanCD to an array that doesn't contain it", () => {
          const taiSanCD: ITaiSanCD = { id: 123 };
          const taiSanCDCollection: ITaiSanCD[] = [{ id: 456 }];
          expectedResult = service.addTaiSanCDToCollectionIfMissing(taiSanCDCollection, taiSanCD);
          expect(expectedResult).toHaveLength(2);
          expect(expectedResult).toContain(taiSanCD);
        });

        it('should add only unique TaiSanCD to an array', () => {
          const taiSanCDArray: ITaiSanCD[] = [{ id: 123 }, { id: 456 }, { id: 72666 }];
          const taiSanCDCollection: ITaiSanCD[] = [{ id: 123 }];
          expectedResult = service.addTaiSanCDToCollectionIfMissing(taiSanCDCollection, ...taiSanCDArray);
          expect(expectedResult).toHaveLength(3);
        });

        it('should accept varargs', () => {
          const taiSanCD: ITaiSanCD = { id: 123 };
          const taiSanCD2: ITaiSanCD = { id: 456 };
          expectedResult = service.addTaiSanCDToCollectionIfMissing([], taiSanCD, taiSanCD2);
          expect(expectedResult).toHaveLength(2);
          expect(expectedResult).toContain(taiSanCD);
          expect(expectedResult).toContain(taiSanCD2);
        });

        it('should accept null and undefined values', () => {
          const taiSanCD: ITaiSanCD = { id: 123 };
          expectedResult = service.addTaiSanCDToCollectionIfMissing([], null, taiSanCD, undefined);
          expect(expectedResult).toHaveLength(1);
          expect(expectedResult).toContain(taiSanCD);
        });
      });
    });

    afterEach(() => {
      httpMock.verify();
    });
  });
});
