import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { mergeMap } from 'rxjs/operators';

import { IPhongDat, PhongDat } from '../phong-dat.model';
import { PhongDatService } from '../service/phong-dat.service';

@Injectable({ providedIn: 'root' })
export class PhongDatRoutingResolveService implements Resolve<IPhongDat> {
  constructor(protected service: PhongDatService, protected router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IPhongDat> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        mergeMap((phongDat: HttpResponse<PhongDat>) => {
          if (phongDat.body) {
            return of(phongDat.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new PhongDat());
  }
}
