package com.group3.finmansys.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.group3.finmansys.IntegrationTest;
import com.group3.finmansys.domain.CongThuc;
import com.group3.finmansys.domain.CongThuc;
import com.group3.finmansys.domain.ToanTu;
import com.group3.finmansys.repository.CongThucRepository;
import com.group3.finmansys.service.criteria.CongThucCriteria;
import com.group3.finmansys.service.dto.CongThucDTO;
import com.group3.finmansys.service.mapper.CongThucMapper;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link CongThucResource} REST controller.
 */
@IntegrationTest
@ExtendWith(MockitoExtension.class)
@AutoConfigureMockMvc
@WithMockUser
class CongThucResourceIT {

    private static final Integer DEFAULT_MA_SO = 1;
    private static final Integer UPDATED_MA_SO = 2;
    private static final Integer SMALLER_MA_SO = 1 - 1;

    private static final String DEFAULT_TEN_CHI_TIEU = "AAAAAAAAAA";
    private static final String UPDATED_TEN_CHI_TIEU = "BBBBBBBBBB";

    private static final Integer DEFAULT_CAP = 1;
    private static final Integer UPDATED_CAP = 2;
    private static final Integer SMALLER_CAP = 1 - 1;

    private static final Double DEFAULT_SO_DAU_KY = 1D;
    private static final Double UPDATED_SO_DAU_KY = 2D;
    private static final Double SMALLER_SO_DAU_KY = 1D - 1D;

    private static final Double DEFAULT_SO_CUOI_KY = 1D;
    private static final Double UPDATED_SO_CUOI_KY = 2D;
    private static final Double SMALLER_SO_CUOI_KY = 1D - 1D;

    private static final String ENTITY_API_URL = "/api/cong-thucs";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private CongThucRepository congThucRepository;

    @Autowired
    private CongThucMapper congThucMapper;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restCongThucMockMvc;

    private CongThuc congThuc;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static CongThuc createEntity(EntityManager em) {
        CongThuc congThuc = new CongThuc()
            .maSo(DEFAULT_MA_SO)
            .tenChiTieu(DEFAULT_TEN_CHI_TIEU)
            .cap(DEFAULT_CAP)
            .soDauKy(DEFAULT_SO_DAU_KY)
            .soCuoiKy(DEFAULT_SO_CUOI_KY);
        return congThuc;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static CongThuc createUpdatedEntity(EntityManager em) {
        CongThuc congThuc = new CongThuc()
            .maSo(UPDATED_MA_SO)
            .tenChiTieu(UPDATED_TEN_CHI_TIEU)
            .cap(UPDATED_CAP)
            .soDauKy(UPDATED_SO_DAU_KY)
            .soCuoiKy(UPDATED_SO_CUOI_KY);
        return congThuc;
    }

    @BeforeEach
    public void initTest() {
        congThuc = createEntity(em);
    }

    @Test
    @Transactional
    void createCongThuc() throws Exception {
        int databaseSizeBeforeCreate = congThucRepository.findAll().size();
        // Create the CongThuc
        CongThucDTO congThucDTO = congThucMapper.toDto(congThuc);
        restCongThucMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(congThucDTO)))
            .andExpect(status().isCreated());

        // Validate the CongThuc in the database
        List<CongThuc> congThucList = congThucRepository.findAll();
        assertThat(congThucList).hasSize(databaseSizeBeforeCreate + 1);
        CongThuc testCongThuc = congThucList.get(congThucList.size() - 1);
        assertThat(testCongThuc.getMaSo()).isEqualTo(DEFAULT_MA_SO);
        assertThat(testCongThuc.getTenChiTieu()).isEqualTo(DEFAULT_TEN_CHI_TIEU);
        assertThat(testCongThuc.getCap()).isEqualTo(DEFAULT_CAP);
        assertThat(testCongThuc.getSoDauKy()).isEqualTo(DEFAULT_SO_DAU_KY);
        assertThat(testCongThuc.getSoCuoiKy()).isEqualTo(DEFAULT_SO_CUOI_KY);
    }

    @Test
    @Transactional
    void createCongThucWithExistingId() throws Exception {
        // Create the CongThuc with an existing ID
        congThuc.setId(1L);
        CongThucDTO congThucDTO = congThucMapper.toDto(congThuc);

        int databaseSizeBeforeCreate = congThucRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restCongThucMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(congThucDTO)))
            .andExpect(status().isBadRequest());

        // Validate the CongThuc in the database
        List<CongThuc> congThucList = congThucRepository.findAll();
        assertThat(congThucList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void checkMaSoIsRequired() throws Exception {
        int databaseSizeBeforeTest = congThucRepository.findAll().size();
        // set the field null
        congThuc.setMaSo(null);

        // Create the CongThuc, which fails.
        CongThucDTO congThucDTO = congThucMapper.toDto(congThuc);

        restCongThucMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(congThucDTO)))
            .andExpect(status().isBadRequest());

        List<CongThuc> congThucList = congThucRepository.findAll();
        assertThat(congThucList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkTenChiTieuIsRequired() throws Exception {
        int databaseSizeBeforeTest = congThucRepository.findAll().size();
        // set the field null
        congThuc.setTenChiTieu(null);

        // Create the CongThuc, which fails.
        CongThucDTO congThucDTO = congThucMapper.toDto(congThuc);

        restCongThucMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(congThucDTO)))
            .andExpect(status().isBadRequest());

        List<CongThuc> congThucList = congThucRepository.findAll();
        assertThat(congThucList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkCapIsRequired() throws Exception {
        int databaseSizeBeforeTest = congThucRepository.findAll().size();
        // set the field null
        congThuc.setCap(null);

        // Create the CongThuc, which fails.
        CongThucDTO congThucDTO = congThucMapper.toDto(congThuc);

        restCongThucMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(congThucDTO)))
            .andExpect(status().isBadRequest());

        List<CongThuc> congThucList = congThucRepository.findAll();
        assertThat(congThucList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void getAllCongThucs() throws Exception {
        // Initialize the database
        congThucRepository.saveAndFlush(congThuc);

        // Get all the congThucList
        restCongThucMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(congThuc.getId().intValue())))
            .andExpect(jsonPath("$.[*].maSo").value(hasItem(DEFAULT_MA_SO)))
            .andExpect(jsonPath("$.[*].tenChiTieu").value(hasItem(DEFAULT_TEN_CHI_TIEU)))
            .andExpect(jsonPath("$.[*].cap").value(hasItem(DEFAULT_CAP)))
            .andExpect(jsonPath("$.[*].soDauKy").value(hasItem(DEFAULT_SO_DAU_KY.doubleValue())))
            .andExpect(jsonPath("$.[*].soCuoiKy").value(hasItem(DEFAULT_SO_CUOI_KY.doubleValue())));
    }

    @Test
    @Transactional
    void getCongThuc() throws Exception {
        // Initialize the database
        congThucRepository.saveAndFlush(congThuc);

        // Get the congThuc
        restCongThucMockMvc
            .perform(get(ENTITY_API_URL_ID, congThuc.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(congThuc.getId().intValue()))
            .andExpect(jsonPath("$.maSo").value(DEFAULT_MA_SO))
            .andExpect(jsonPath("$.tenChiTieu").value(DEFAULT_TEN_CHI_TIEU))
            .andExpect(jsonPath("$.cap").value(DEFAULT_CAP))
            .andExpect(jsonPath("$.soDauKy").value(DEFAULT_SO_DAU_KY.doubleValue()))
            .andExpect(jsonPath("$.soCuoiKy").value(DEFAULT_SO_CUOI_KY.doubleValue()));
    }

    @Test
    @Transactional
    void getCongThucsByIdFiltering() throws Exception {
        // Initialize the database
        congThucRepository.saveAndFlush(congThuc);

        Long id = congThuc.getId();

        defaultCongThucShouldBeFound("id.equals=" + id);
        defaultCongThucShouldNotBeFound("id.notEquals=" + id);

        defaultCongThucShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultCongThucShouldNotBeFound("id.greaterThan=" + id);

        defaultCongThucShouldBeFound("id.lessThanOrEqual=" + id);
        defaultCongThucShouldNotBeFound("id.lessThan=" + id);
    }

    @Test
    @Transactional
    void getAllCongThucsByMaSoIsEqualToSomething() throws Exception {
        // Initialize the database
        congThucRepository.saveAndFlush(congThuc);

        // Get all the congThucList where maSo equals to DEFAULT_MA_SO
        defaultCongThucShouldBeFound("maSo.equals=" + DEFAULT_MA_SO);

        // Get all the congThucList where maSo equals to UPDATED_MA_SO
        defaultCongThucShouldNotBeFound("maSo.equals=" + UPDATED_MA_SO);
    }

    @Test
    @Transactional
    void getAllCongThucsByMaSoIsNotEqualToSomething() throws Exception {
        // Initialize the database
        congThucRepository.saveAndFlush(congThuc);

        // Get all the congThucList where maSo not equals to DEFAULT_MA_SO
        defaultCongThucShouldNotBeFound("maSo.notEquals=" + DEFAULT_MA_SO);

        // Get all the congThucList where maSo not equals to UPDATED_MA_SO
        defaultCongThucShouldBeFound("maSo.notEquals=" + UPDATED_MA_SO);
    }

    @Test
    @Transactional
    void getAllCongThucsByMaSoIsInShouldWork() throws Exception {
        // Initialize the database
        congThucRepository.saveAndFlush(congThuc);

        // Get all the congThucList where maSo in DEFAULT_MA_SO or UPDATED_MA_SO
        defaultCongThucShouldBeFound("maSo.in=" + DEFAULT_MA_SO + "," + UPDATED_MA_SO);

        // Get all the congThucList where maSo equals to UPDATED_MA_SO
        defaultCongThucShouldNotBeFound("maSo.in=" + UPDATED_MA_SO);
    }

    @Test
    @Transactional
    void getAllCongThucsByMaSoIsNullOrNotNull() throws Exception {
        // Initialize the database
        congThucRepository.saveAndFlush(congThuc);

        // Get all the congThucList where maSo is not null
        defaultCongThucShouldBeFound("maSo.specified=true");

        // Get all the congThucList where maSo is null
        defaultCongThucShouldNotBeFound("maSo.specified=false");
    }

    @Test
    @Transactional
    void getAllCongThucsByMaSoIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        congThucRepository.saveAndFlush(congThuc);

        // Get all the congThucList where maSo is greater than or equal to DEFAULT_MA_SO
        defaultCongThucShouldBeFound("maSo.greaterThanOrEqual=" + DEFAULT_MA_SO);

        // Get all the congThucList where maSo is greater than or equal to UPDATED_MA_SO
        defaultCongThucShouldNotBeFound("maSo.greaterThanOrEqual=" + UPDATED_MA_SO);
    }

    @Test
    @Transactional
    void getAllCongThucsByMaSoIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        congThucRepository.saveAndFlush(congThuc);

        // Get all the congThucList where maSo is less than or equal to DEFAULT_MA_SO
        defaultCongThucShouldBeFound("maSo.lessThanOrEqual=" + DEFAULT_MA_SO);

        // Get all the congThucList where maSo is less than or equal to SMALLER_MA_SO
        defaultCongThucShouldNotBeFound("maSo.lessThanOrEqual=" + SMALLER_MA_SO);
    }

    @Test
    @Transactional
    void getAllCongThucsByMaSoIsLessThanSomething() throws Exception {
        // Initialize the database
        congThucRepository.saveAndFlush(congThuc);

        // Get all the congThucList where maSo is less than DEFAULT_MA_SO
        defaultCongThucShouldNotBeFound("maSo.lessThan=" + DEFAULT_MA_SO);

        // Get all the congThucList where maSo is less than UPDATED_MA_SO
        defaultCongThucShouldBeFound("maSo.lessThan=" + UPDATED_MA_SO);
    }

    @Test
    @Transactional
    void getAllCongThucsByMaSoIsGreaterThanSomething() throws Exception {
        // Initialize the database
        congThucRepository.saveAndFlush(congThuc);

        // Get all the congThucList where maSo is greater than DEFAULT_MA_SO
        defaultCongThucShouldNotBeFound("maSo.greaterThan=" + DEFAULT_MA_SO);

        // Get all the congThucList where maSo is greater than SMALLER_MA_SO
        defaultCongThucShouldBeFound("maSo.greaterThan=" + SMALLER_MA_SO);
    }

    @Test
    @Transactional
    void getAllCongThucsByTenChiTieuIsEqualToSomething() throws Exception {
        // Initialize the database
        congThucRepository.saveAndFlush(congThuc);

        // Get all the congThucList where tenChiTieu equals to DEFAULT_TEN_CHI_TIEU
        defaultCongThucShouldBeFound("tenChiTieu.equals=" + DEFAULT_TEN_CHI_TIEU);

        // Get all the congThucList where tenChiTieu equals to UPDATED_TEN_CHI_TIEU
        defaultCongThucShouldNotBeFound("tenChiTieu.equals=" + UPDATED_TEN_CHI_TIEU);
    }

    @Test
    @Transactional
    void getAllCongThucsByTenChiTieuIsNotEqualToSomething() throws Exception {
        // Initialize the database
        congThucRepository.saveAndFlush(congThuc);

        // Get all the congThucList where tenChiTieu not equals to DEFAULT_TEN_CHI_TIEU
        defaultCongThucShouldNotBeFound("tenChiTieu.notEquals=" + DEFAULT_TEN_CHI_TIEU);

        // Get all the congThucList where tenChiTieu not equals to UPDATED_TEN_CHI_TIEU
        defaultCongThucShouldBeFound("tenChiTieu.notEquals=" + UPDATED_TEN_CHI_TIEU);
    }

    @Test
    @Transactional
    void getAllCongThucsByTenChiTieuIsInShouldWork() throws Exception {
        // Initialize the database
        congThucRepository.saveAndFlush(congThuc);

        // Get all the congThucList where tenChiTieu in DEFAULT_TEN_CHI_TIEU or UPDATED_TEN_CHI_TIEU
        defaultCongThucShouldBeFound("tenChiTieu.in=" + DEFAULT_TEN_CHI_TIEU + "," + UPDATED_TEN_CHI_TIEU);

        // Get all the congThucList where tenChiTieu equals to UPDATED_TEN_CHI_TIEU
        defaultCongThucShouldNotBeFound("tenChiTieu.in=" + UPDATED_TEN_CHI_TIEU);
    }

    @Test
    @Transactional
    void getAllCongThucsByTenChiTieuIsNullOrNotNull() throws Exception {
        // Initialize the database
        congThucRepository.saveAndFlush(congThuc);

        // Get all the congThucList where tenChiTieu is not null
        defaultCongThucShouldBeFound("tenChiTieu.specified=true");

        // Get all the congThucList where tenChiTieu is null
        defaultCongThucShouldNotBeFound("tenChiTieu.specified=false");
    }

    @Test
    @Transactional
    void getAllCongThucsByTenChiTieuContainsSomething() throws Exception {
        // Initialize the database
        congThucRepository.saveAndFlush(congThuc);

        // Get all the congThucList where tenChiTieu contains DEFAULT_TEN_CHI_TIEU
        defaultCongThucShouldBeFound("tenChiTieu.contains=" + DEFAULT_TEN_CHI_TIEU);

        // Get all the congThucList where tenChiTieu contains UPDATED_TEN_CHI_TIEU
        defaultCongThucShouldNotBeFound("tenChiTieu.contains=" + UPDATED_TEN_CHI_TIEU);
    }

    @Test
    @Transactional
    void getAllCongThucsByTenChiTieuNotContainsSomething() throws Exception {
        // Initialize the database
        congThucRepository.saveAndFlush(congThuc);

        // Get all the congThucList where tenChiTieu does not contain DEFAULT_TEN_CHI_TIEU
        defaultCongThucShouldNotBeFound("tenChiTieu.doesNotContain=" + DEFAULT_TEN_CHI_TIEU);

        // Get all the congThucList where tenChiTieu does not contain UPDATED_TEN_CHI_TIEU
        defaultCongThucShouldBeFound("tenChiTieu.doesNotContain=" + UPDATED_TEN_CHI_TIEU);
    }

    @Test
    @Transactional
    void getAllCongThucsByCapIsEqualToSomething() throws Exception {
        // Initialize the database
        congThucRepository.saveAndFlush(congThuc);

        // Get all the congThucList where cap equals to DEFAULT_CAP
        defaultCongThucShouldBeFound("cap.equals=" + DEFAULT_CAP);

        // Get all the congThucList where cap equals to UPDATED_CAP
        defaultCongThucShouldNotBeFound("cap.equals=" + UPDATED_CAP);
    }

    @Test
    @Transactional
    void getAllCongThucsByCapIsNotEqualToSomething() throws Exception {
        // Initialize the database
        congThucRepository.saveAndFlush(congThuc);

        // Get all the congThucList where cap not equals to DEFAULT_CAP
        defaultCongThucShouldNotBeFound("cap.notEquals=" + DEFAULT_CAP);

        // Get all the congThucList where cap not equals to UPDATED_CAP
        defaultCongThucShouldBeFound("cap.notEquals=" + UPDATED_CAP);
    }

    @Test
    @Transactional
    void getAllCongThucsByCapIsInShouldWork() throws Exception {
        // Initialize the database
        congThucRepository.saveAndFlush(congThuc);

        // Get all the congThucList where cap in DEFAULT_CAP or UPDATED_CAP
        defaultCongThucShouldBeFound("cap.in=" + DEFAULT_CAP + "," + UPDATED_CAP);

        // Get all the congThucList where cap equals to UPDATED_CAP
        defaultCongThucShouldNotBeFound("cap.in=" + UPDATED_CAP);
    }

    @Test
    @Transactional
    void getAllCongThucsByCapIsNullOrNotNull() throws Exception {
        // Initialize the database
        congThucRepository.saveAndFlush(congThuc);

        // Get all the congThucList where cap is not null
        defaultCongThucShouldBeFound("cap.specified=true");

        // Get all the congThucList where cap is null
        defaultCongThucShouldNotBeFound("cap.specified=false");
    }

    @Test
    @Transactional
    void getAllCongThucsByCapIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        congThucRepository.saveAndFlush(congThuc);

        // Get all the congThucList where cap is greater than or equal to DEFAULT_CAP
        defaultCongThucShouldBeFound("cap.greaterThanOrEqual=" + DEFAULT_CAP);

        // Get all the congThucList where cap is greater than or equal to UPDATED_CAP
        defaultCongThucShouldNotBeFound("cap.greaterThanOrEqual=" + UPDATED_CAP);
    }

    @Test
    @Transactional
    void getAllCongThucsByCapIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        congThucRepository.saveAndFlush(congThuc);

        // Get all the congThucList where cap is less than or equal to DEFAULT_CAP
        defaultCongThucShouldBeFound("cap.lessThanOrEqual=" + DEFAULT_CAP);

        // Get all the congThucList where cap is less than or equal to SMALLER_CAP
        defaultCongThucShouldNotBeFound("cap.lessThanOrEqual=" + SMALLER_CAP);
    }

    @Test
    @Transactional
    void getAllCongThucsByCapIsLessThanSomething() throws Exception {
        // Initialize the database
        congThucRepository.saveAndFlush(congThuc);

        // Get all the congThucList where cap is less than DEFAULT_CAP
        defaultCongThucShouldNotBeFound("cap.lessThan=" + DEFAULT_CAP);

        // Get all the congThucList where cap is less than UPDATED_CAP
        defaultCongThucShouldBeFound("cap.lessThan=" + UPDATED_CAP);
    }

    @Test
    @Transactional
    void getAllCongThucsByCapIsGreaterThanSomething() throws Exception {
        // Initialize the database
        congThucRepository.saveAndFlush(congThuc);

        // Get all the congThucList where cap is greater than DEFAULT_CAP
        defaultCongThucShouldNotBeFound("cap.greaterThan=" + DEFAULT_CAP);

        // Get all the congThucList where cap is greater than SMALLER_CAP
        defaultCongThucShouldBeFound("cap.greaterThan=" + SMALLER_CAP);
    }

    @Test
    @Transactional
    void getAllCongThucsBySoDauKyIsEqualToSomething() throws Exception {
        // Initialize the database
        congThucRepository.saveAndFlush(congThuc);

        // Get all the congThucList where soDauKy equals to DEFAULT_SO_DAU_KY
        defaultCongThucShouldBeFound("soDauKy.equals=" + DEFAULT_SO_DAU_KY);

        // Get all the congThucList where soDauKy equals to UPDATED_SO_DAU_KY
        defaultCongThucShouldNotBeFound("soDauKy.equals=" + UPDATED_SO_DAU_KY);
    }

    @Test
    @Transactional
    void getAllCongThucsBySoDauKyIsNotEqualToSomething() throws Exception {
        // Initialize the database
        congThucRepository.saveAndFlush(congThuc);

        // Get all the congThucList where soDauKy not equals to DEFAULT_SO_DAU_KY
        defaultCongThucShouldNotBeFound("soDauKy.notEquals=" + DEFAULT_SO_DAU_KY);

        // Get all the congThucList where soDauKy not equals to UPDATED_SO_DAU_KY
        defaultCongThucShouldBeFound("soDauKy.notEquals=" + UPDATED_SO_DAU_KY);
    }

    @Test
    @Transactional
    void getAllCongThucsBySoDauKyIsInShouldWork() throws Exception {
        // Initialize the database
        congThucRepository.saveAndFlush(congThuc);

        // Get all the congThucList where soDauKy in DEFAULT_SO_DAU_KY or UPDATED_SO_DAU_KY
        defaultCongThucShouldBeFound("soDauKy.in=" + DEFAULT_SO_DAU_KY + "," + UPDATED_SO_DAU_KY);

        // Get all the congThucList where soDauKy equals to UPDATED_SO_DAU_KY
        defaultCongThucShouldNotBeFound("soDauKy.in=" + UPDATED_SO_DAU_KY);
    }

    @Test
    @Transactional
    void getAllCongThucsBySoDauKyIsNullOrNotNull() throws Exception {
        // Initialize the database
        congThucRepository.saveAndFlush(congThuc);

        // Get all the congThucList where soDauKy is not null
        defaultCongThucShouldBeFound("soDauKy.specified=true");

        // Get all the congThucList where soDauKy is null
        defaultCongThucShouldNotBeFound("soDauKy.specified=false");
    }

    @Test
    @Transactional
    void getAllCongThucsBySoDauKyIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        congThucRepository.saveAndFlush(congThuc);

        // Get all the congThucList where soDauKy is greater than or equal to DEFAULT_SO_DAU_KY
        defaultCongThucShouldBeFound("soDauKy.greaterThanOrEqual=" + DEFAULT_SO_DAU_KY);

        // Get all the congThucList where soDauKy is greater than or equal to UPDATED_SO_DAU_KY
        defaultCongThucShouldNotBeFound("soDauKy.greaterThanOrEqual=" + UPDATED_SO_DAU_KY);
    }

    @Test
    @Transactional
    void getAllCongThucsBySoDauKyIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        congThucRepository.saveAndFlush(congThuc);

        // Get all the congThucList where soDauKy is less than or equal to DEFAULT_SO_DAU_KY
        defaultCongThucShouldBeFound("soDauKy.lessThanOrEqual=" + DEFAULT_SO_DAU_KY);

        // Get all the congThucList where soDauKy is less than or equal to SMALLER_SO_DAU_KY
        defaultCongThucShouldNotBeFound("soDauKy.lessThanOrEqual=" + SMALLER_SO_DAU_KY);
    }

    @Test
    @Transactional
    void getAllCongThucsBySoDauKyIsLessThanSomething() throws Exception {
        // Initialize the database
        congThucRepository.saveAndFlush(congThuc);

        // Get all the congThucList where soDauKy is less than DEFAULT_SO_DAU_KY
        defaultCongThucShouldNotBeFound("soDauKy.lessThan=" + DEFAULT_SO_DAU_KY);

        // Get all the congThucList where soDauKy is less than UPDATED_SO_DAU_KY
        defaultCongThucShouldBeFound("soDauKy.lessThan=" + UPDATED_SO_DAU_KY);
    }

    @Test
    @Transactional
    void getAllCongThucsBySoDauKyIsGreaterThanSomething() throws Exception {
        // Initialize the database
        congThucRepository.saveAndFlush(congThuc);

        // Get all the congThucList where soDauKy is greater than DEFAULT_SO_DAU_KY
        defaultCongThucShouldNotBeFound("soDauKy.greaterThan=" + DEFAULT_SO_DAU_KY);

        // Get all the congThucList where soDauKy is greater than SMALLER_SO_DAU_KY
        defaultCongThucShouldBeFound("soDauKy.greaterThan=" + SMALLER_SO_DAU_KY);
    }

    @Test
    @Transactional
    void getAllCongThucsBySoCuoiKyIsEqualToSomething() throws Exception {
        // Initialize the database
        congThucRepository.saveAndFlush(congThuc);

        // Get all the congThucList where soCuoiKy equals to DEFAULT_SO_CUOI_KY
        defaultCongThucShouldBeFound("soCuoiKy.equals=" + DEFAULT_SO_CUOI_KY);

        // Get all the congThucList where soCuoiKy equals to UPDATED_SO_CUOI_KY
        defaultCongThucShouldNotBeFound("soCuoiKy.equals=" + UPDATED_SO_CUOI_KY);
    }

    @Test
    @Transactional
    void getAllCongThucsBySoCuoiKyIsNotEqualToSomething() throws Exception {
        // Initialize the database
        congThucRepository.saveAndFlush(congThuc);

        // Get all the congThucList where soCuoiKy not equals to DEFAULT_SO_CUOI_KY
        defaultCongThucShouldNotBeFound("soCuoiKy.notEquals=" + DEFAULT_SO_CUOI_KY);

        // Get all the congThucList where soCuoiKy not equals to UPDATED_SO_CUOI_KY
        defaultCongThucShouldBeFound("soCuoiKy.notEquals=" + UPDATED_SO_CUOI_KY);
    }

    @Test
    @Transactional
    void getAllCongThucsBySoCuoiKyIsInShouldWork() throws Exception {
        // Initialize the database
        congThucRepository.saveAndFlush(congThuc);

        // Get all the congThucList where soCuoiKy in DEFAULT_SO_CUOI_KY or UPDATED_SO_CUOI_KY
        defaultCongThucShouldBeFound("soCuoiKy.in=" + DEFAULT_SO_CUOI_KY + "," + UPDATED_SO_CUOI_KY);

        // Get all the congThucList where soCuoiKy equals to UPDATED_SO_CUOI_KY
        defaultCongThucShouldNotBeFound("soCuoiKy.in=" + UPDATED_SO_CUOI_KY);
    }

    @Test
    @Transactional
    void getAllCongThucsBySoCuoiKyIsNullOrNotNull() throws Exception {
        // Initialize the database
        congThucRepository.saveAndFlush(congThuc);

        // Get all the congThucList where soCuoiKy is not null
        defaultCongThucShouldBeFound("soCuoiKy.specified=true");

        // Get all the congThucList where soCuoiKy is null
        defaultCongThucShouldNotBeFound("soCuoiKy.specified=false");
    }

    @Test
    @Transactional
    void getAllCongThucsBySoCuoiKyIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        congThucRepository.saveAndFlush(congThuc);

        // Get all the congThucList where soCuoiKy is greater than or equal to DEFAULT_SO_CUOI_KY
        defaultCongThucShouldBeFound("soCuoiKy.greaterThanOrEqual=" + DEFAULT_SO_CUOI_KY);

        // Get all the congThucList where soCuoiKy is greater than or equal to UPDATED_SO_CUOI_KY
        defaultCongThucShouldNotBeFound("soCuoiKy.greaterThanOrEqual=" + UPDATED_SO_CUOI_KY);
    }

    @Test
    @Transactional
    void getAllCongThucsBySoCuoiKyIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        congThucRepository.saveAndFlush(congThuc);

        // Get all the congThucList where soCuoiKy is less than or equal to DEFAULT_SO_CUOI_KY
        defaultCongThucShouldBeFound("soCuoiKy.lessThanOrEqual=" + DEFAULT_SO_CUOI_KY);

        // Get all the congThucList where soCuoiKy is less than or equal to SMALLER_SO_CUOI_KY
        defaultCongThucShouldNotBeFound("soCuoiKy.lessThanOrEqual=" + SMALLER_SO_CUOI_KY);
    }

    @Test
    @Transactional
    void getAllCongThucsBySoCuoiKyIsLessThanSomething() throws Exception {
        // Initialize the database
        congThucRepository.saveAndFlush(congThuc);

        // Get all the congThucList where soCuoiKy is less than DEFAULT_SO_CUOI_KY
        defaultCongThucShouldNotBeFound("soCuoiKy.lessThan=" + DEFAULT_SO_CUOI_KY);

        // Get all the congThucList where soCuoiKy is less than UPDATED_SO_CUOI_KY
        defaultCongThucShouldBeFound("soCuoiKy.lessThan=" + UPDATED_SO_CUOI_KY);
    }

    @Test
    @Transactional
    void getAllCongThucsBySoCuoiKyIsGreaterThanSomething() throws Exception {
        // Initialize the database
        congThucRepository.saveAndFlush(congThuc);

        // Get all the congThucList where soCuoiKy is greater than DEFAULT_SO_CUOI_KY
        defaultCongThucShouldNotBeFound("soCuoiKy.greaterThan=" + DEFAULT_SO_CUOI_KY);

        // Get all the congThucList where soCuoiKy is greater than SMALLER_SO_CUOI_KY
        defaultCongThucShouldBeFound("soCuoiKy.greaterThan=" + SMALLER_SO_CUOI_KY);
    }

    @Test
    @Transactional
    void getAllCongThucsByToanTuIsEqualToSomething() throws Exception {
        // Initialize the database
        congThucRepository.saveAndFlush(congThuc);
        ToanTu toanTu = ToanTuResourceIT.createEntity(em);
        em.persist(toanTu);
        em.flush();
        congThuc.addToanTu(toanTu);
        congThucRepository.saveAndFlush(congThuc);
        Long toanTuId = toanTu.getId();

        // Get all the congThucList where toanTu equals to toanTuId
        defaultCongThucShouldBeFound("toanTuId.equals=" + toanTuId);

        // Get all the congThucList where toanTu equals to (toanTuId + 1)
        defaultCongThucShouldNotBeFound("toanTuId.equals=" + (toanTuId + 1));
    }

    @Test
    @Transactional
    void getAllCongThucsByCapChaIsEqualToSomething() throws Exception {
        // Initialize the database
        congThucRepository.saveAndFlush(congThuc);
        CongThuc capCha = CongThucResourceIT.createEntity(em);
        em.persist(capCha);
        em.flush();
        congThuc.setCapCha(capCha);
        congThucRepository.saveAndFlush(congThuc);
        Long capChaId = capCha.getId();

        // Get all the congThucList where capCha equals to capChaId
        defaultCongThucShouldBeFound("capChaId.equals=" + capChaId);

        // Get all the congThucList where capCha equals to (capChaId + 1)
        defaultCongThucShouldNotBeFound("capChaId.equals=" + (capChaId + 1));
    }

    @Test
    @Transactional
    void getAllCongThucsByCapConIsEqualToSomething() throws Exception {
        // Initialize the database
        congThucRepository.saveAndFlush(congThuc);
        CongThuc capCon = CongThucResourceIT.createEntity(em);
        em.persist(capCon);
        em.flush();
        congThuc.addCapCon(capCon);
        congThucRepository.saveAndFlush(congThuc);
        Long capConId = capCon.getId();

        // Get all the congThucList where capCon equals to capConId
        defaultCongThucShouldBeFound("capConId.equals=" + capConId);

        // Get all the congThucList where capCon equals to (capConId + 1)
        defaultCongThucShouldNotBeFound("capConId.equals=" + (capConId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultCongThucShouldBeFound(String filter) throws Exception {
        restCongThucMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(congThuc.getId().intValue())))
            .andExpect(jsonPath("$.[*].maSo").value(hasItem(DEFAULT_MA_SO)))
            .andExpect(jsonPath("$.[*].tenChiTieu").value(hasItem(DEFAULT_TEN_CHI_TIEU)))
            .andExpect(jsonPath("$.[*].cap").value(hasItem(DEFAULT_CAP)))
            .andExpect(jsonPath("$.[*].soDauKy").value(hasItem(DEFAULT_SO_DAU_KY.doubleValue())))
            .andExpect(jsonPath("$.[*].soCuoiKy").value(hasItem(DEFAULT_SO_CUOI_KY.doubleValue())));

        // Check, that the count call also returns 1
        restCongThucMockMvc
            .perform(get(ENTITY_API_URL + "/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultCongThucShouldNotBeFound(String filter) throws Exception {
        restCongThucMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restCongThucMockMvc
            .perform(get(ENTITY_API_URL + "/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }

    @Test
    @Transactional
    void getNonExistingCongThuc() throws Exception {
        // Get the congThuc
        restCongThucMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putNewCongThuc() throws Exception {
        // Initialize the database
        congThucRepository.saveAndFlush(congThuc);

        int databaseSizeBeforeUpdate = congThucRepository.findAll().size();

        // Update the congThuc
        CongThuc updatedCongThuc = congThucRepository.findById(congThuc.getId()).get();
        // Disconnect from session so that the updates on updatedCongThuc are not directly saved in db
        em.detach(updatedCongThuc);
        updatedCongThuc
            .maSo(UPDATED_MA_SO)
            .tenChiTieu(UPDATED_TEN_CHI_TIEU)
            .cap(UPDATED_CAP)
            .soDauKy(UPDATED_SO_DAU_KY)
            .soCuoiKy(UPDATED_SO_CUOI_KY);
        CongThucDTO congThucDTO = congThucMapper.toDto(updatedCongThuc);

        restCongThucMockMvc
            .perform(
                put(ENTITY_API_URL_ID, congThucDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(congThucDTO))
            )
            .andExpect(status().isOk());

        // Validate the CongThuc in the database
        List<CongThuc> congThucList = congThucRepository.findAll();
        assertThat(congThucList).hasSize(databaseSizeBeforeUpdate);
        CongThuc testCongThuc = congThucList.get(congThucList.size() - 1);
        assertThat(testCongThuc.getMaSo()).isEqualTo(UPDATED_MA_SO);
        assertThat(testCongThuc.getTenChiTieu()).isEqualTo(UPDATED_TEN_CHI_TIEU);
        assertThat(testCongThuc.getCap()).isEqualTo(UPDATED_CAP);
        assertThat(testCongThuc.getSoDauKy()).isEqualTo(UPDATED_SO_DAU_KY);
        assertThat(testCongThuc.getSoCuoiKy()).isEqualTo(UPDATED_SO_CUOI_KY);
    }

    @Test
    @Transactional
    void putNonExistingCongThuc() throws Exception {
        int databaseSizeBeforeUpdate = congThucRepository.findAll().size();
        congThuc.setId(count.incrementAndGet());

        // Create the CongThuc
        CongThucDTO congThucDTO = congThucMapper.toDto(congThuc);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restCongThucMockMvc
            .perform(
                put(ENTITY_API_URL_ID, congThucDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(congThucDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the CongThuc in the database
        List<CongThuc> congThucList = congThucRepository.findAll();
        assertThat(congThucList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchCongThuc() throws Exception {
        int databaseSizeBeforeUpdate = congThucRepository.findAll().size();
        congThuc.setId(count.incrementAndGet());

        // Create the CongThuc
        CongThucDTO congThucDTO = congThucMapper.toDto(congThuc);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restCongThucMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(congThucDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the CongThuc in the database
        List<CongThuc> congThucList = congThucRepository.findAll();
        assertThat(congThucList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamCongThuc() throws Exception {
        int databaseSizeBeforeUpdate = congThucRepository.findAll().size();
        congThuc.setId(count.incrementAndGet());

        // Create the CongThuc
        CongThucDTO congThucDTO = congThucMapper.toDto(congThuc);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restCongThucMockMvc
            .perform(put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(congThucDTO)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the CongThuc in the database
        List<CongThuc> congThucList = congThucRepository.findAll();
        assertThat(congThucList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateCongThucWithPatch() throws Exception {
        // Initialize the database
        congThucRepository.saveAndFlush(congThuc);

        int databaseSizeBeforeUpdate = congThucRepository.findAll().size();

        // Update the congThuc using partial update
        CongThuc partialUpdatedCongThuc = new CongThuc();
        partialUpdatedCongThuc.setId(congThuc.getId());

        partialUpdatedCongThuc.maSo(UPDATED_MA_SO).soDauKy(UPDATED_SO_DAU_KY).soCuoiKy(UPDATED_SO_CUOI_KY);

        restCongThucMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedCongThuc.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedCongThuc))
            )
            .andExpect(status().isOk());

        // Validate the CongThuc in the database
        List<CongThuc> congThucList = congThucRepository.findAll();
        assertThat(congThucList).hasSize(databaseSizeBeforeUpdate);
        CongThuc testCongThuc = congThucList.get(congThucList.size() - 1);
        assertThat(testCongThuc.getMaSo()).isEqualTo(UPDATED_MA_SO);
        assertThat(testCongThuc.getTenChiTieu()).isEqualTo(DEFAULT_TEN_CHI_TIEU);
        assertThat(testCongThuc.getCap()).isEqualTo(DEFAULT_CAP);
        assertThat(testCongThuc.getSoDauKy()).isEqualTo(UPDATED_SO_DAU_KY);
        assertThat(testCongThuc.getSoCuoiKy()).isEqualTo(UPDATED_SO_CUOI_KY);
    }

    @Test
    @Transactional
    void fullUpdateCongThucWithPatch() throws Exception {
        // Initialize the database
        congThucRepository.saveAndFlush(congThuc);

        int databaseSizeBeforeUpdate = congThucRepository.findAll().size();

        // Update the congThuc using partial update
        CongThuc partialUpdatedCongThuc = new CongThuc();
        partialUpdatedCongThuc.setId(congThuc.getId());

        partialUpdatedCongThuc
            .maSo(UPDATED_MA_SO)
            .tenChiTieu(UPDATED_TEN_CHI_TIEU)
            .cap(UPDATED_CAP)
            .soDauKy(UPDATED_SO_DAU_KY)
            .soCuoiKy(UPDATED_SO_CUOI_KY);

        restCongThucMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedCongThuc.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedCongThuc))
            )
            .andExpect(status().isOk());

        // Validate the CongThuc in the database
        List<CongThuc> congThucList = congThucRepository.findAll();
        assertThat(congThucList).hasSize(databaseSizeBeforeUpdate);
        CongThuc testCongThuc = congThucList.get(congThucList.size() - 1);
        assertThat(testCongThuc.getMaSo()).isEqualTo(UPDATED_MA_SO);
        assertThat(testCongThuc.getTenChiTieu()).isEqualTo(UPDATED_TEN_CHI_TIEU);
        assertThat(testCongThuc.getCap()).isEqualTo(UPDATED_CAP);
        assertThat(testCongThuc.getSoDauKy()).isEqualTo(UPDATED_SO_DAU_KY);
        assertThat(testCongThuc.getSoCuoiKy()).isEqualTo(UPDATED_SO_CUOI_KY);
    }

    @Test
    @Transactional
    void patchNonExistingCongThuc() throws Exception {
        int databaseSizeBeforeUpdate = congThucRepository.findAll().size();
        congThuc.setId(count.incrementAndGet());

        // Create the CongThuc
        CongThucDTO congThucDTO = congThucMapper.toDto(congThuc);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restCongThucMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, congThucDTO.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(congThucDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the CongThuc in the database
        List<CongThuc> congThucList = congThucRepository.findAll();
        assertThat(congThucList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchCongThuc() throws Exception {
        int databaseSizeBeforeUpdate = congThucRepository.findAll().size();
        congThuc.setId(count.incrementAndGet());

        // Create the CongThuc
        CongThucDTO congThucDTO = congThucMapper.toDto(congThuc);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restCongThucMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(congThucDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the CongThuc in the database
        List<CongThuc> congThucList = congThucRepository.findAll();
        assertThat(congThucList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamCongThuc() throws Exception {
        int databaseSizeBeforeUpdate = congThucRepository.findAll().size();
        congThuc.setId(count.incrementAndGet());

        // Create the CongThuc
        CongThucDTO congThucDTO = congThucMapper.toDto(congThuc);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restCongThucMockMvc
            .perform(
                patch(ENTITY_API_URL).contentType("application/merge-patch+json").content(TestUtil.convertObjectToJsonBytes(congThucDTO))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the CongThuc in the database
        List<CongThuc> congThucList = congThucRepository.findAll();
        assertThat(congThucList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteCongThuc() throws Exception {
        // Initialize the database
        congThucRepository.saveAndFlush(congThuc);

        int databaseSizeBeforeDelete = congThucRepository.findAll().size();

        // Delete the congThuc
        restCongThucMockMvc
            .perform(delete(ENTITY_API_URL_ID, congThuc.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<CongThuc> congThucList = congThucRepository.findAll();
        assertThat(congThucList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
