import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { PhongDatDetailComponent } from './phong-dat-detail.component';

describe('Component Tests', () => {
  describe('PhongDat Management Detail Component', () => {
    let comp: PhongDatDetailComponent;
    let fixture: ComponentFixture<PhongDatDetailComponent>;

    beforeEach(() => {
      TestBed.configureTestingModule({
        declarations: [PhongDatDetailComponent],
        providers: [
          {
            provide: ActivatedRoute,
            useValue: { data: of({ phongDat: { id: 123 } }) },
          },
        ],
      })
        .overrideTemplate(PhongDatDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(PhongDatDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should load phongDat on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.phongDat).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
