import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { mergeMap } from 'rxjs/operators';

import { IKhoanMucChiPhi, KhoanMucChiPhi } from '../khoan-muc-chi-phi.model';
import { KhoanMucChiPhiService } from '../service/khoan-muc-chi-phi.service';

@Injectable({ providedIn: 'root' })
export class KhoanMucChiPhiRoutingResolveService implements Resolve<IKhoanMucChiPhi> {
  constructor(protected service: KhoanMucChiPhiService, protected router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IKhoanMucChiPhi> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        mergeMap((khoanMucChiPhi: HttpResponse<KhoanMucChiPhi>) => {
          if (khoanMucChiPhi.body) {
            return of(khoanMucChiPhi.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new KhoanMucChiPhi());
  }
}
