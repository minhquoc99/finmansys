package com.group3.finmansys.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.group3.finmansys.IntegrationTest;
import com.group3.finmansys.domain.DichVu;
import com.group3.finmansys.domain.DichVuSuDung;
import com.group3.finmansys.domain.PhongDuocDat;
import com.group3.finmansys.repository.DichVuSuDungRepository;
import com.group3.finmansys.service.criteria.DichVuSuDungCriteria;
import com.group3.finmansys.service.dto.DichVuSuDungDTO;
import com.group3.finmansys.service.mapper.DichVuSuDungMapper;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link DichVuSuDungResource} REST controller.
 */
@IntegrationTest
@ExtendWith(MockitoExtension.class)
@AutoConfigureMockMvc
@WithMockUser
class DichVuSuDungResourceIT {

    private static final Integer DEFAULT_SO_LUONG = 1;
    private static final Integer UPDATED_SO_LUONG = 2;
    private static final Integer SMALLER_SO_LUONG = 1 - 1;

    private static final Double DEFAULT_GIA = 1D;
    private static final Double UPDATED_GIA = 2D;
    private static final Double SMALLER_GIA = 1D - 1D;

    private static final Instant DEFAULT_NGAY_SU_DUNG = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_NGAY_SU_DUNG = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final String ENTITY_API_URL = "/api/dich-vu-su-dungs";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private DichVuSuDungRepository dichVuSuDungRepository;

    @Autowired
    private DichVuSuDungMapper dichVuSuDungMapper;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restDichVuSuDungMockMvc;

    private DichVuSuDung dichVuSuDung;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static DichVuSuDung createEntity(EntityManager em) {
        DichVuSuDung dichVuSuDung = new DichVuSuDung().soLuong(DEFAULT_SO_LUONG).gia(DEFAULT_GIA).ngaySuDung(DEFAULT_NGAY_SU_DUNG);
        // Add required entity
        DichVu dichVu;
        if (TestUtil.findAll(em, DichVu.class).isEmpty()) {
            dichVu = DichVuResourceIT.createEntity(em);
            em.persist(dichVu);
            em.flush();
        } else {
            dichVu = TestUtil.findAll(em, DichVu.class).get(0);
        }
        dichVuSuDung.setDichVu(dichVu);
        // Add required entity
        PhongDuocDat phongDuocDat;
        if (TestUtil.findAll(em, PhongDuocDat.class).isEmpty()) {
            phongDuocDat = PhongDuocDatResourceIT.createEntity(em);
            em.persist(phongDuocDat);
            em.flush();
        } else {
            phongDuocDat = TestUtil.findAll(em, PhongDuocDat.class).get(0);
        }
        dichVuSuDung.setPhongDuocDat(phongDuocDat);
        return dichVuSuDung;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static DichVuSuDung createUpdatedEntity(EntityManager em) {
        DichVuSuDung dichVuSuDung = new DichVuSuDung().soLuong(UPDATED_SO_LUONG).gia(UPDATED_GIA).ngaySuDung(UPDATED_NGAY_SU_DUNG);
        // Add required entity
        DichVu dichVu;
        if (TestUtil.findAll(em, DichVu.class).isEmpty()) {
            dichVu = DichVuResourceIT.createUpdatedEntity(em);
            em.persist(dichVu);
            em.flush();
        } else {
            dichVu = TestUtil.findAll(em, DichVu.class).get(0);
        }
        dichVuSuDung.setDichVu(dichVu);
        // Add required entity
        PhongDuocDat phongDuocDat;
        if (TestUtil.findAll(em, PhongDuocDat.class).isEmpty()) {
            phongDuocDat = PhongDuocDatResourceIT.createUpdatedEntity(em);
            em.persist(phongDuocDat);
            em.flush();
        } else {
            phongDuocDat = TestUtil.findAll(em, PhongDuocDat.class).get(0);
        }
        dichVuSuDung.setPhongDuocDat(phongDuocDat);
        return dichVuSuDung;
    }

    @BeforeEach
    public void initTest() {
        dichVuSuDung = createEntity(em);
    }

    @Test
    @Transactional
    void createDichVuSuDung() throws Exception {
        int databaseSizeBeforeCreate = dichVuSuDungRepository.findAll().size();
        // Create the DichVuSuDung
        DichVuSuDungDTO dichVuSuDungDTO = dichVuSuDungMapper.toDto(dichVuSuDung);
        restDichVuSuDungMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(dichVuSuDungDTO))
            )
            .andExpect(status().isCreated());

        // Validate the DichVuSuDung in the database
        List<DichVuSuDung> dichVuSuDungList = dichVuSuDungRepository.findAll();
        assertThat(dichVuSuDungList).hasSize(databaseSizeBeforeCreate + 1);
        DichVuSuDung testDichVuSuDung = dichVuSuDungList.get(dichVuSuDungList.size() - 1);
        assertThat(testDichVuSuDung.getSoLuong()).isEqualTo(DEFAULT_SO_LUONG);
        assertThat(testDichVuSuDung.getGia()).isEqualTo(DEFAULT_GIA);
        assertThat(testDichVuSuDung.getNgaySuDung()).isEqualTo(DEFAULT_NGAY_SU_DUNG);
    }

    @Test
    @Transactional
    void createDichVuSuDungWithExistingId() throws Exception {
        // Create the DichVuSuDung with an existing ID
        dichVuSuDung.setId(1L);
        DichVuSuDungDTO dichVuSuDungDTO = dichVuSuDungMapper.toDto(dichVuSuDung);

        int databaseSizeBeforeCreate = dichVuSuDungRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restDichVuSuDungMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(dichVuSuDungDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the DichVuSuDung in the database
        List<DichVuSuDung> dichVuSuDungList = dichVuSuDungRepository.findAll();
        assertThat(dichVuSuDungList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void checkSoLuongIsRequired() throws Exception {
        int databaseSizeBeforeTest = dichVuSuDungRepository.findAll().size();
        // set the field null
        dichVuSuDung.setSoLuong(null);

        // Create the DichVuSuDung, which fails.
        DichVuSuDungDTO dichVuSuDungDTO = dichVuSuDungMapper.toDto(dichVuSuDung);

        restDichVuSuDungMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(dichVuSuDungDTO))
            )
            .andExpect(status().isBadRequest());

        List<DichVuSuDung> dichVuSuDungList = dichVuSuDungRepository.findAll();
        assertThat(dichVuSuDungList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkGiaIsRequired() throws Exception {
        int databaseSizeBeforeTest = dichVuSuDungRepository.findAll().size();
        // set the field null
        dichVuSuDung.setGia(null);

        // Create the DichVuSuDung, which fails.
        DichVuSuDungDTO dichVuSuDungDTO = dichVuSuDungMapper.toDto(dichVuSuDung);

        restDichVuSuDungMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(dichVuSuDungDTO))
            )
            .andExpect(status().isBadRequest());

        List<DichVuSuDung> dichVuSuDungList = dichVuSuDungRepository.findAll();
        assertThat(dichVuSuDungList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkNgaySuDungIsRequired() throws Exception {
        int databaseSizeBeforeTest = dichVuSuDungRepository.findAll().size();
        // set the field null
        dichVuSuDung.setNgaySuDung(null);

        // Create the DichVuSuDung, which fails.
        DichVuSuDungDTO dichVuSuDungDTO = dichVuSuDungMapper.toDto(dichVuSuDung);

        restDichVuSuDungMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(dichVuSuDungDTO))
            )
            .andExpect(status().isBadRequest());

        List<DichVuSuDung> dichVuSuDungList = dichVuSuDungRepository.findAll();
        assertThat(dichVuSuDungList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void getAllDichVuSuDungs() throws Exception {
        // Initialize the database
        dichVuSuDungRepository.saveAndFlush(dichVuSuDung);

        // Get all the dichVuSuDungList
        restDichVuSuDungMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(dichVuSuDung.getId().intValue())))
            .andExpect(jsonPath("$.[*].soLuong").value(hasItem(DEFAULT_SO_LUONG)))
            .andExpect(jsonPath("$.[*].gia").value(hasItem(DEFAULT_GIA.doubleValue())))
            .andExpect(jsonPath("$.[*].ngaySuDung").value(hasItem(DEFAULT_NGAY_SU_DUNG.toString())));
    }

    @Test
    @Transactional
    void getDichVuSuDung() throws Exception {
        // Initialize the database
        dichVuSuDungRepository.saveAndFlush(dichVuSuDung);

        // Get the dichVuSuDung
        restDichVuSuDungMockMvc
            .perform(get(ENTITY_API_URL_ID, dichVuSuDung.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(dichVuSuDung.getId().intValue()))
            .andExpect(jsonPath("$.soLuong").value(DEFAULT_SO_LUONG))
            .andExpect(jsonPath("$.gia").value(DEFAULT_GIA.doubleValue()))
            .andExpect(jsonPath("$.ngaySuDung").value(DEFAULT_NGAY_SU_DUNG.toString()));
    }

    @Test
    @Transactional
    void getDichVuSuDungsByIdFiltering() throws Exception {
        // Initialize the database
        dichVuSuDungRepository.saveAndFlush(dichVuSuDung);

        Long id = dichVuSuDung.getId();

        defaultDichVuSuDungShouldBeFound("id.equals=" + id);
        defaultDichVuSuDungShouldNotBeFound("id.notEquals=" + id);

        defaultDichVuSuDungShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultDichVuSuDungShouldNotBeFound("id.greaterThan=" + id);

        defaultDichVuSuDungShouldBeFound("id.lessThanOrEqual=" + id);
        defaultDichVuSuDungShouldNotBeFound("id.lessThan=" + id);
    }

    @Test
    @Transactional
    void getAllDichVuSuDungsBySoLuongIsEqualToSomething() throws Exception {
        // Initialize the database
        dichVuSuDungRepository.saveAndFlush(dichVuSuDung);

        // Get all the dichVuSuDungList where soLuong equals to DEFAULT_SO_LUONG
        defaultDichVuSuDungShouldBeFound("soLuong.equals=" + DEFAULT_SO_LUONG);

        // Get all the dichVuSuDungList where soLuong equals to UPDATED_SO_LUONG
        defaultDichVuSuDungShouldNotBeFound("soLuong.equals=" + UPDATED_SO_LUONG);
    }

    @Test
    @Transactional
    void getAllDichVuSuDungsBySoLuongIsNotEqualToSomething() throws Exception {
        // Initialize the database
        dichVuSuDungRepository.saveAndFlush(dichVuSuDung);

        // Get all the dichVuSuDungList where soLuong not equals to DEFAULT_SO_LUONG
        defaultDichVuSuDungShouldNotBeFound("soLuong.notEquals=" + DEFAULT_SO_LUONG);

        // Get all the dichVuSuDungList where soLuong not equals to UPDATED_SO_LUONG
        defaultDichVuSuDungShouldBeFound("soLuong.notEquals=" + UPDATED_SO_LUONG);
    }

    @Test
    @Transactional
    void getAllDichVuSuDungsBySoLuongIsInShouldWork() throws Exception {
        // Initialize the database
        dichVuSuDungRepository.saveAndFlush(dichVuSuDung);

        // Get all the dichVuSuDungList where soLuong in DEFAULT_SO_LUONG or UPDATED_SO_LUONG
        defaultDichVuSuDungShouldBeFound("soLuong.in=" + DEFAULT_SO_LUONG + "," + UPDATED_SO_LUONG);

        // Get all the dichVuSuDungList where soLuong equals to UPDATED_SO_LUONG
        defaultDichVuSuDungShouldNotBeFound("soLuong.in=" + UPDATED_SO_LUONG);
    }

    @Test
    @Transactional
    void getAllDichVuSuDungsBySoLuongIsNullOrNotNull() throws Exception {
        // Initialize the database
        dichVuSuDungRepository.saveAndFlush(dichVuSuDung);

        // Get all the dichVuSuDungList where soLuong is not null
        defaultDichVuSuDungShouldBeFound("soLuong.specified=true");

        // Get all the dichVuSuDungList where soLuong is null
        defaultDichVuSuDungShouldNotBeFound("soLuong.specified=false");
    }

    @Test
    @Transactional
    void getAllDichVuSuDungsBySoLuongIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        dichVuSuDungRepository.saveAndFlush(dichVuSuDung);

        // Get all the dichVuSuDungList where soLuong is greater than or equal to DEFAULT_SO_LUONG
        defaultDichVuSuDungShouldBeFound("soLuong.greaterThanOrEqual=" + DEFAULT_SO_LUONG);

        // Get all the dichVuSuDungList where soLuong is greater than or equal to UPDATED_SO_LUONG
        defaultDichVuSuDungShouldNotBeFound("soLuong.greaterThanOrEqual=" + UPDATED_SO_LUONG);
    }

    @Test
    @Transactional
    void getAllDichVuSuDungsBySoLuongIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        dichVuSuDungRepository.saveAndFlush(dichVuSuDung);

        // Get all the dichVuSuDungList where soLuong is less than or equal to DEFAULT_SO_LUONG
        defaultDichVuSuDungShouldBeFound("soLuong.lessThanOrEqual=" + DEFAULT_SO_LUONG);

        // Get all the dichVuSuDungList where soLuong is less than or equal to SMALLER_SO_LUONG
        defaultDichVuSuDungShouldNotBeFound("soLuong.lessThanOrEqual=" + SMALLER_SO_LUONG);
    }

    @Test
    @Transactional
    void getAllDichVuSuDungsBySoLuongIsLessThanSomething() throws Exception {
        // Initialize the database
        dichVuSuDungRepository.saveAndFlush(dichVuSuDung);

        // Get all the dichVuSuDungList where soLuong is less than DEFAULT_SO_LUONG
        defaultDichVuSuDungShouldNotBeFound("soLuong.lessThan=" + DEFAULT_SO_LUONG);

        // Get all the dichVuSuDungList where soLuong is less than UPDATED_SO_LUONG
        defaultDichVuSuDungShouldBeFound("soLuong.lessThan=" + UPDATED_SO_LUONG);
    }

    @Test
    @Transactional
    void getAllDichVuSuDungsBySoLuongIsGreaterThanSomething() throws Exception {
        // Initialize the database
        dichVuSuDungRepository.saveAndFlush(dichVuSuDung);

        // Get all the dichVuSuDungList where soLuong is greater than DEFAULT_SO_LUONG
        defaultDichVuSuDungShouldNotBeFound("soLuong.greaterThan=" + DEFAULT_SO_LUONG);

        // Get all the dichVuSuDungList where soLuong is greater than SMALLER_SO_LUONG
        defaultDichVuSuDungShouldBeFound("soLuong.greaterThan=" + SMALLER_SO_LUONG);
    }

    @Test
    @Transactional
    void getAllDichVuSuDungsByGiaIsEqualToSomething() throws Exception {
        // Initialize the database
        dichVuSuDungRepository.saveAndFlush(dichVuSuDung);

        // Get all the dichVuSuDungList where gia equals to DEFAULT_GIA
        defaultDichVuSuDungShouldBeFound("gia.equals=" + DEFAULT_GIA);

        // Get all the dichVuSuDungList where gia equals to UPDATED_GIA
        defaultDichVuSuDungShouldNotBeFound("gia.equals=" + UPDATED_GIA);
    }

    @Test
    @Transactional
    void getAllDichVuSuDungsByGiaIsNotEqualToSomething() throws Exception {
        // Initialize the database
        dichVuSuDungRepository.saveAndFlush(dichVuSuDung);

        // Get all the dichVuSuDungList where gia not equals to DEFAULT_GIA
        defaultDichVuSuDungShouldNotBeFound("gia.notEquals=" + DEFAULT_GIA);

        // Get all the dichVuSuDungList where gia not equals to UPDATED_GIA
        defaultDichVuSuDungShouldBeFound("gia.notEquals=" + UPDATED_GIA);
    }

    @Test
    @Transactional
    void getAllDichVuSuDungsByGiaIsInShouldWork() throws Exception {
        // Initialize the database
        dichVuSuDungRepository.saveAndFlush(dichVuSuDung);

        // Get all the dichVuSuDungList where gia in DEFAULT_GIA or UPDATED_GIA
        defaultDichVuSuDungShouldBeFound("gia.in=" + DEFAULT_GIA + "," + UPDATED_GIA);

        // Get all the dichVuSuDungList where gia equals to UPDATED_GIA
        defaultDichVuSuDungShouldNotBeFound("gia.in=" + UPDATED_GIA);
    }

    @Test
    @Transactional
    void getAllDichVuSuDungsByGiaIsNullOrNotNull() throws Exception {
        // Initialize the database
        dichVuSuDungRepository.saveAndFlush(dichVuSuDung);

        // Get all the dichVuSuDungList where gia is not null
        defaultDichVuSuDungShouldBeFound("gia.specified=true");

        // Get all the dichVuSuDungList where gia is null
        defaultDichVuSuDungShouldNotBeFound("gia.specified=false");
    }

    @Test
    @Transactional
    void getAllDichVuSuDungsByGiaIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        dichVuSuDungRepository.saveAndFlush(dichVuSuDung);

        // Get all the dichVuSuDungList where gia is greater than or equal to DEFAULT_GIA
        defaultDichVuSuDungShouldBeFound("gia.greaterThanOrEqual=" + DEFAULT_GIA);

        // Get all the dichVuSuDungList where gia is greater than or equal to UPDATED_GIA
        defaultDichVuSuDungShouldNotBeFound("gia.greaterThanOrEqual=" + UPDATED_GIA);
    }

    @Test
    @Transactional
    void getAllDichVuSuDungsByGiaIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        dichVuSuDungRepository.saveAndFlush(dichVuSuDung);

        // Get all the dichVuSuDungList where gia is less than or equal to DEFAULT_GIA
        defaultDichVuSuDungShouldBeFound("gia.lessThanOrEqual=" + DEFAULT_GIA);

        // Get all the dichVuSuDungList where gia is less than or equal to SMALLER_GIA
        defaultDichVuSuDungShouldNotBeFound("gia.lessThanOrEqual=" + SMALLER_GIA);
    }

    @Test
    @Transactional
    void getAllDichVuSuDungsByGiaIsLessThanSomething() throws Exception {
        // Initialize the database
        dichVuSuDungRepository.saveAndFlush(dichVuSuDung);

        // Get all the dichVuSuDungList where gia is less than DEFAULT_GIA
        defaultDichVuSuDungShouldNotBeFound("gia.lessThan=" + DEFAULT_GIA);

        // Get all the dichVuSuDungList where gia is less than UPDATED_GIA
        defaultDichVuSuDungShouldBeFound("gia.lessThan=" + UPDATED_GIA);
    }

    @Test
    @Transactional
    void getAllDichVuSuDungsByGiaIsGreaterThanSomething() throws Exception {
        // Initialize the database
        dichVuSuDungRepository.saveAndFlush(dichVuSuDung);

        // Get all the dichVuSuDungList where gia is greater than DEFAULT_GIA
        defaultDichVuSuDungShouldNotBeFound("gia.greaterThan=" + DEFAULT_GIA);

        // Get all the dichVuSuDungList where gia is greater than SMALLER_GIA
        defaultDichVuSuDungShouldBeFound("gia.greaterThan=" + SMALLER_GIA);
    }

    @Test
    @Transactional
    void getAllDichVuSuDungsByNgaySuDungIsEqualToSomething() throws Exception {
        // Initialize the database
        dichVuSuDungRepository.saveAndFlush(dichVuSuDung);

        // Get all the dichVuSuDungList where ngaySuDung equals to DEFAULT_NGAY_SU_DUNG
        defaultDichVuSuDungShouldBeFound("ngaySuDung.equals=" + DEFAULT_NGAY_SU_DUNG);

        // Get all the dichVuSuDungList where ngaySuDung equals to UPDATED_NGAY_SU_DUNG
        defaultDichVuSuDungShouldNotBeFound("ngaySuDung.equals=" + UPDATED_NGAY_SU_DUNG);
    }

    @Test
    @Transactional
    void getAllDichVuSuDungsByNgaySuDungIsNotEqualToSomething() throws Exception {
        // Initialize the database
        dichVuSuDungRepository.saveAndFlush(dichVuSuDung);

        // Get all the dichVuSuDungList where ngaySuDung not equals to DEFAULT_NGAY_SU_DUNG
        defaultDichVuSuDungShouldNotBeFound("ngaySuDung.notEquals=" + DEFAULT_NGAY_SU_DUNG);

        // Get all the dichVuSuDungList where ngaySuDung not equals to UPDATED_NGAY_SU_DUNG
        defaultDichVuSuDungShouldBeFound("ngaySuDung.notEquals=" + UPDATED_NGAY_SU_DUNG);
    }

    @Test
    @Transactional
    void getAllDichVuSuDungsByNgaySuDungIsInShouldWork() throws Exception {
        // Initialize the database
        dichVuSuDungRepository.saveAndFlush(dichVuSuDung);

        // Get all the dichVuSuDungList where ngaySuDung in DEFAULT_NGAY_SU_DUNG or UPDATED_NGAY_SU_DUNG
        defaultDichVuSuDungShouldBeFound("ngaySuDung.in=" + DEFAULT_NGAY_SU_DUNG + "," + UPDATED_NGAY_SU_DUNG);

        // Get all the dichVuSuDungList where ngaySuDung equals to UPDATED_NGAY_SU_DUNG
        defaultDichVuSuDungShouldNotBeFound("ngaySuDung.in=" + UPDATED_NGAY_SU_DUNG);
    }

    @Test
    @Transactional
    void getAllDichVuSuDungsByNgaySuDungIsNullOrNotNull() throws Exception {
        // Initialize the database
        dichVuSuDungRepository.saveAndFlush(dichVuSuDung);

        // Get all the dichVuSuDungList where ngaySuDung is not null
        defaultDichVuSuDungShouldBeFound("ngaySuDung.specified=true");

        // Get all the dichVuSuDungList where ngaySuDung is null
        defaultDichVuSuDungShouldNotBeFound("ngaySuDung.specified=false");
    }

    @Test
    @Transactional
    void getAllDichVuSuDungsByDichVuIsEqualToSomething() throws Exception {
        // Initialize the database
        dichVuSuDungRepository.saveAndFlush(dichVuSuDung);
        DichVu dichVu = DichVuResourceIT.createEntity(em);
        em.persist(dichVu);
        em.flush();
        dichVuSuDung.setDichVu(dichVu);
        dichVuSuDungRepository.saveAndFlush(dichVuSuDung);
        Long dichVuId = dichVu.getId();

        // Get all the dichVuSuDungList where dichVu equals to dichVuId
        defaultDichVuSuDungShouldBeFound("dichVuId.equals=" + dichVuId);

        // Get all the dichVuSuDungList where dichVu equals to (dichVuId + 1)
        defaultDichVuSuDungShouldNotBeFound("dichVuId.equals=" + (dichVuId + 1));
    }

    @Test
    @Transactional
    void getAllDichVuSuDungsByPhongDuocDatIsEqualToSomething() throws Exception {
        // Initialize the database
        dichVuSuDungRepository.saveAndFlush(dichVuSuDung);
        PhongDuocDat phongDuocDat = PhongDuocDatResourceIT.createEntity(em);
        em.persist(phongDuocDat);
        em.flush();
        dichVuSuDung.setPhongDuocDat(phongDuocDat);
        dichVuSuDungRepository.saveAndFlush(dichVuSuDung);
        Long phongDuocDatId = phongDuocDat.getId();

        // Get all the dichVuSuDungList where phongDuocDat equals to phongDuocDatId
        defaultDichVuSuDungShouldBeFound("phongDuocDatId.equals=" + phongDuocDatId);

        // Get all the dichVuSuDungList where phongDuocDat equals to (phongDuocDatId + 1)
        defaultDichVuSuDungShouldNotBeFound("phongDuocDatId.equals=" + (phongDuocDatId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultDichVuSuDungShouldBeFound(String filter) throws Exception {
        restDichVuSuDungMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(dichVuSuDung.getId().intValue())))
            .andExpect(jsonPath("$.[*].soLuong").value(hasItem(DEFAULT_SO_LUONG)))
            .andExpect(jsonPath("$.[*].gia").value(hasItem(DEFAULT_GIA.doubleValue())))
            .andExpect(jsonPath("$.[*].ngaySuDung").value(hasItem(DEFAULT_NGAY_SU_DUNG.toString())));

        // Check, that the count call also returns 1
        restDichVuSuDungMockMvc
            .perform(get(ENTITY_API_URL + "/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultDichVuSuDungShouldNotBeFound(String filter) throws Exception {
        restDichVuSuDungMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restDichVuSuDungMockMvc
            .perform(get(ENTITY_API_URL + "/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }

    @Test
    @Transactional
    void getNonExistingDichVuSuDung() throws Exception {
        // Get the dichVuSuDung
        restDichVuSuDungMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putNewDichVuSuDung() throws Exception {
        // Initialize the database
        dichVuSuDungRepository.saveAndFlush(dichVuSuDung);

        int databaseSizeBeforeUpdate = dichVuSuDungRepository.findAll().size();

        // Update the dichVuSuDung
        DichVuSuDung updatedDichVuSuDung = dichVuSuDungRepository.findById(dichVuSuDung.getId()).get();
        // Disconnect from session so that the updates on updatedDichVuSuDung are not directly saved in db
        em.detach(updatedDichVuSuDung);
        updatedDichVuSuDung.soLuong(UPDATED_SO_LUONG).gia(UPDATED_GIA).ngaySuDung(UPDATED_NGAY_SU_DUNG);
        DichVuSuDungDTO dichVuSuDungDTO = dichVuSuDungMapper.toDto(updatedDichVuSuDung);

        restDichVuSuDungMockMvc
            .perform(
                put(ENTITY_API_URL_ID, dichVuSuDungDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(dichVuSuDungDTO))
            )
            .andExpect(status().isOk());

        // Validate the DichVuSuDung in the database
        List<DichVuSuDung> dichVuSuDungList = dichVuSuDungRepository.findAll();
        assertThat(dichVuSuDungList).hasSize(databaseSizeBeforeUpdate);
        DichVuSuDung testDichVuSuDung = dichVuSuDungList.get(dichVuSuDungList.size() - 1);
        assertThat(testDichVuSuDung.getSoLuong()).isEqualTo(UPDATED_SO_LUONG);
        assertThat(testDichVuSuDung.getGia()).isEqualTo(UPDATED_GIA);
        assertThat(testDichVuSuDung.getNgaySuDung()).isEqualTo(UPDATED_NGAY_SU_DUNG);
    }

    @Test
    @Transactional
    void putNonExistingDichVuSuDung() throws Exception {
        int databaseSizeBeforeUpdate = dichVuSuDungRepository.findAll().size();
        dichVuSuDung.setId(count.incrementAndGet());

        // Create the DichVuSuDung
        DichVuSuDungDTO dichVuSuDungDTO = dichVuSuDungMapper.toDto(dichVuSuDung);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restDichVuSuDungMockMvc
            .perform(
                put(ENTITY_API_URL_ID, dichVuSuDungDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(dichVuSuDungDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the DichVuSuDung in the database
        List<DichVuSuDung> dichVuSuDungList = dichVuSuDungRepository.findAll();
        assertThat(dichVuSuDungList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchDichVuSuDung() throws Exception {
        int databaseSizeBeforeUpdate = dichVuSuDungRepository.findAll().size();
        dichVuSuDung.setId(count.incrementAndGet());

        // Create the DichVuSuDung
        DichVuSuDungDTO dichVuSuDungDTO = dichVuSuDungMapper.toDto(dichVuSuDung);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restDichVuSuDungMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(dichVuSuDungDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the DichVuSuDung in the database
        List<DichVuSuDung> dichVuSuDungList = dichVuSuDungRepository.findAll();
        assertThat(dichVuSuDungList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamDichVuSuDung() throws Exception {
        int databaseSizeBeforeUpdate = dichVuSuDungRepository.findAll().size();
        dichVuSuDung.setId(count.incrementAndGet());

        // Create the DichVuSuDung
        DichVuSuDungDTO dichVuSuDungDTO = dichVuSuDungMapper.toDto(dichVuSuDung);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restDichVuSuDungMockMvc
            .perform(
                put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(dichVuSuDungDTO))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the DichVuSuDung in the database
        List<DichVuSuDung> dichVuSuDungList = dichVuSuDungRepository.findAll();
        assertThat(dichVuSuDungList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateDichVuSuDungWithPatch() throws Exception {
        // Initialize the database
        dichVuSuDungRepository.saveAndFlush(dichVuSuDung);

        int databaseSizeBeforeUpdate = dichVuSuDungRepository.findAll().size();

        // Update the dichVuSuDung using partial update
        DichVuSuDung partialUpdatedDichVuSuDung = new DichVuSuDung();
        partialUpdatedDichVuSuDung.setId(dichVuSuDung.getId());

        partialUpdatedDichVuSuDung.gia(UPDATED_GIA).ngaySuDung(UPDATED_NGAY_SU_DUNG);

        restDichVuSuDungMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedDichVuSuDung.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedDichVuSuDung))
            )
            .andExpect(status().isOk());

        // Validate the DichVuSuDung in the database
        List<DichVuSuDung> dichVuSuDungList = dichVuSuDungRepository.findAll();
        assertThat(dichVuSuDungList).hasSize(databaseSizeBeforeUpdate);
        DichVuSuDung testDichVuSuDung = dichVuSuDungList.get(dichVuSuDungList.size() - 1);
        assertThat(testDichVuSuDung.getSoLuong()).isEqualTo(DEFAULT_SO_LUONG);
        assertThat(testDichVuSuDung.getGia()).isEqualTo(UPDATED_GIA);
        assertThat(testDichVuSuDung.getNgaySuDung()).isEqualTo(UPDATED_NGAY_SU_DUNG);
    }

    @Test
    @Transactional
    void fullUpdateDichVuSuDungWithPatch() throws Exception {
        // Initialize the database
        dichVuSuDungRepository.saveAndFlush(dichVuSuDung);

        int databaseSizeBeforeUpdate = dichVuSuDungRepository.findAll().size();

        // Update the dichVuSuDung using partial update
        DichVuSuDung partialUpdatedDichVuSuDung = new DichVuSuDung();
        partialUpdatedDichVuSuDung.setId(dichVuSuDung.getId());

        partialUpdatedDichVuSuDung.soLuong(UPDATED_SO_LUONG).gia(UPDATED_GIA).ngaySuDung(UPDATED_NGAY_SU_DUNG);

        restDichVuSuDungMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedDichVuSuDung.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedDichVuSuDung))
            )
            .andExpect(status().isOk());

        // Validate the DichVuSuDung in the database
        List<DichVuSuDung> dichVuSuDungList = dichVuSuDungRepository.findAll();
        assertThat(dichVuSuDungList).hasSize(databaseSizeBeforeUpdate);
        DichVuSuDung testDichVuSuDung = dichVuSuDungList.get(dichVuSuDungList.size() - 1);
        assertThat(testDichVuSuDung.getSoLuong()).isEqualTo(UPDATED_SO_LUONG);
        assertThat(testDichVuSuDung.getGia()).isEqualTo(UPDATED_GIA);
        assertThat(testDichVuSuDung.getNgaySuDung()).isEqualTo(UPDATED_NGAY_SU_DUNG);
    }

    @Test
    @Transactional
    void patchNonExistingDichVuSuDung() throws Exception {
        int databaseSizeBeforeUpdate = dichVuSuDungRepository.findAll().size();
        dichVuSuDung.setId(count.incrementAndGet());

        // Create the DichVuSuDung
        DichVuSuDungDTO dichVuSuDungDTO = dichVuSuDungMapper.toDto(dichVuSuDung);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restDichVuSuDungMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, dichVuSuDungDTO.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(dichVuSuDungDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the DichVuSuDung in the database
        List<DichVuSuDung> dichVuSuDungList = dichVuSuDungRepository.findAll();
        assertThat(dichVuSuDungList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchDichVuSuDung() throws Exception {
        int databaseSizeBeforeUpdate = dichVuSuDungRepository.findAll().size();
        dichVuSuDung.setId(count.incrementAndGet());

        // Create the DichVuSuDung
        DichVuSuDungDTO dichVuSuDungDTO = dichVuSuDungMapper.toDto(dichVuSuDung);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restDichVuSuDungMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(dichVuSuDungDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the DichVuSuDung in the database
        List<DichVuSuDung> dichVuSuDungList = dichVuSuDungRepository.findAll();
        assertThat(dichVuSuDungList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamDichVuSuDung() throws Exception {
        int databaseSizeBeforeUpdate = dichVuSuDungRepository.findAll().size();
        dichVuSuDung.setId(count.incrementAndGet());

        // Create the DichVuSuDung
        DichVuSuDungDTO dichVuSuDungDTO = dichVuSuDungMapper.toDto(dichVuSuDung);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restDichVuSuDungMockMvc
            .perform(
                patch(ENTITY_API_URL)
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(dichVuSuDungDTO))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the DichVuSuDung in the database
        List<DichVuSuDung> dichVuSuDungList = dichVuSuDungRepository.findAll();
        assertThat(dichVuSuDungList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteDichVuSuDung() throws Exception {
        // Initialize the database
        dichVuSuDungRepository.saveAndFlush(dichVuSuDung);

        int databaseSizeBeforeDelete = dichVuSuDungRepository.findAll().size();

        // Delete the dichVuSuDung
        restDichVuSuDungMockMvc
            .perform(delete(ENTITY_API_URL_ID, dichVuSuDung.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<DichVuSuDung> dichVuSuDungList = dichVuSuDungRepository.findAll();
        assertThat(dichVuSuDungList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
