jest.mock('@angular/router');

import { TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ActivatedRouteSnapshot, Router } from '@angular/router';
import { of } from 'rxjs';

import { INganSachNam, NganSachNam } from '../ngan-sach-nam.model';
import { NganSachNamService } from '../service/ngan-sach-nam.service';

import { NganSachNamRoutingResolveService } from './ngan-sach-nam-routing-resolve.service';

describe('Service Tests', () => {
  describe('NganSachNam routing resolve service', () => {
    let mockRouter: Router;
    let mockActivatedRouteSnapshot: ActivatedRouteSnapshot;
    let routingResolveService: NganSachNamRoutingResolveService;
    let service: NganSachNamService;
    let resultNganSachNam: INganSachNam | undefined;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule],
        providers: [Router, ActivatedRouteSnapshot],
      });
      mockRouter = TestBed.inject(Router);
      mockActivatedRouteSnapshot = TestBed.inject(ActivatedRouteSnapshot);
      routingResolveService = TestBed.inject(NganSachNamRoutingResolveService);
      service = TestBed.inject(NganSachNamService);
      resultNganSachNam = undefined;
    });

    describe('resolve', () => {
      it('should return INganSachNam returned by find', () => {
        // GIVEN
        service.find = jest.fn(id => of(new HttpResponse({ body: { id } })));
        mockActivatedRouteSnapshot.params = { id: 123 };

        // WHEN
        routingResolveService.resolve(mockActivatedRouteSnapshot).subscribe(result => {
          resultNganSachNam = result;
        });

        // THEN
        expect(service.find).toBeCalledWith(123);
        expect(resultNganSachNam).toEqual({ id: 123 });
      });

      it('should return new INganSachNam if id is not provided', () => {
        // GIVEN
        service.find = jest.fn();
        mockActivatedRouteSnapshot.params = {};

        // WHEN
        routingResolveService.resolve(mockActivatedRouteSnapshot).subscribe(result => {
          resultNganSachNam = result;
        });

        // THEN
        expect(service.find).not.toBeCalled();
        expect(resultNganSachNam).toEqual(new NganSachNam());
      });

      it('should route to 404 page if data not found in server', () => {
        // GIVEN
        spyOn(service, 'find').and.returnValue(of(new HttpResponse({ body: null })));
        mockActivatedRouteSnapshot.params = { id: 123 };

        // WHEN
        routingResolveService.resolve(mockActivatedRouteSnapshot).subscribe(result => {
          resultNganSachNam = result;
        });

        // THEN
        expect(service.find).toBeCalledWith(123);
        expect(resultNganSachNam).toEqual(undefined);
        expect(mockRouter.navigate).toHaveBeenCalledWith(['404']);
      });
    });
  });
});
