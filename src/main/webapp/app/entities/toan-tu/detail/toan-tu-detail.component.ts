import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IToanTu } from '../toan-tu.model';

@Component({
  selector: 'jhi-toan-tu-detail',
  templateUrl: './toan-tu-detail.component.html',
})
export class ToanTuDetailComponent implements OnInit {
  toanTu: IToanTu | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ toanTu }) => {
      this.toanTu = toanTu;
    });
  }

  previousState(): void {
    window.history.back();
  }
}
