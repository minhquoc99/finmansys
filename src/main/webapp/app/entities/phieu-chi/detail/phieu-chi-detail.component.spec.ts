import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { PhieuChiDetailComponent } from './phieu-chi-detail.component';

describe('Component Tests', () => {
  describe('PhieuChi Management Detail Component', () => {
    let comp: PhieuChiDetailComponent;
    let fixture: ComponentFixture<PhieuChiDetailComponent>;

    beforeEach(() => {
      TestBed.configureTestingModule({
        declarations: [PhieuChiDetailComponent],
        providers: [
          {
            provide: ActivatedRoute,
            useValue: { data: of({ phieuChi: { id: 123 } }) },
          },
        ],
      })
        .overrideTemplate(PhieuChiDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(PhieuChiDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should load phieuChi on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.phieuChi).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
