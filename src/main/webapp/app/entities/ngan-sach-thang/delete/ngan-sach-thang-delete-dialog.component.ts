import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import { INganSachThang } from '../ngan-sach-thang.model';
import { NganSachThangService } from '../service/ngan-sach-thang.service';

@Component({
  templateUrl: './ngan-sach-thang-delete-dialog.component.html',
})
export class NganSachThangDeleteDialogComponent {
  nganSachThang?: INganSachThang;

  constructor(protected nganSachThangService: NganSachThangService, public activeModal: NgbActiveModal) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.nganSachThangService.delete(id).subscribe(() => {
      this.activeModal.close('deleted');
    });
  }
}
