package com.group3.finmansys.service.mapper;

import com.group3.finmansys.domain.*;
import com.group3.finmansys.service.dto.QuyTienDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link QuyTien} and its DTO {@link QuyTienDTO}.
 */
@Mapper(
    componentModel = "spring",
    uses = { LoaiQuyMapper.class, MaKeToanMapper.class, KhachSanMapper.class, PhieuThuMapper.class, PhieuChiMapper.class }
)
public interface QuyTienMapper extends EntityMapper<QuyTienDTO, QuyTien> {
    @Mapping(target = "loaiQuy", source = "loaiQuy", qualifiedByName = "ten")
    @Mapping(target = "maKeToan", source = "maKeToan")
    @Mapping(target = "khachSan", source = "khachSan", qualifiedByName = "ten")
    QuyTienDTO toDto(QuyTien s);

    @Named("ten")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    @Mapping(target = "ten", source = "ten")
    QuyTienDTO toDtoTen(QuyTien quyTien);
}
