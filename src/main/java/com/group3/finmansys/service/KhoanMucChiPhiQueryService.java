package com.group3.finmansys.service;

import com.group3.finmansys.domain.*; // for static metamodels
import com.group3.finmansys.domain.KhoanMucChiPhi;
import com.group3.finmansys.repository.KhoanMucChiPhiRepository;
import com.group3.finmansys.service.criteria.KhoanMucChiPhiCriteria;
import com.group3.finmansys.service.dto.KhoanMucChiPhiDTO;
import com.group3.finmansys.service.mapper.KhoanMucChiPhiMapper;
import java.util.List;
import javax.persistence.criteria.JoinType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tech.jhipster.service.QueryService;

/**
 * Service for executing complex queries for {@link KhoanMucChiPhi} entities in the database.
 * The main input is a {@link KhoanMucChiPhiCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link KhoanMucChiPhiDTO} or a {@link Page} of {@link KhoanMucChiPhiDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class KhoanMucChiPhiQueryService extends QueryService<KhoanMucChiPhi> {

    private final Logger log = LoggerFactory.getLogger(KhoanMucChiPhiQueryService.class);

    private final KhoanMucChiPhiRepository khoanMucChiPhiRepository;

    private final KhoanMucChiPhiMapper khoanMucChiPhiMapper;

    public KhoanMucChiPhiQueryService(KhoanMucChiPhiRepository khoanMucChiPhiRepository, KhoanMucChiPhiMapper khoanMucChiPhiMapper) {
        this.khoanMucChiPhiRepository = khoanMucChiPhiRepository;
        this.khoanMucChiPhiMapper = khoanMucChiPhiMapper;
    }

    /**
     * Return a {@link List} of {@link KhoanMucChiPhiDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<KhoanMucChiPhiDTO> findByCriteria(KhoanMucChiPhiCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<KhoanMucChiPhi> specification = createSpecification(criteria);
        return khoanMucChiPhiMapper.toDto(khoanMucChiPhiRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link KhoanMucChiPhiDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<KhoanMucChiPhiDTO> findByCriteria(KhoanMucChiPhiCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<KhoanMucChiPhi> specification = createSpecification(criteria);
        return khoanMucChiPhiRepository.findAll(specification, page).map(khoanMucChiPhiMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(KhoanMucChiPhiCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<KhoanMucChiPhi> specification = createSpecification(criteria);
        return khoanMucChiPhiRepository.count(specification);
    }

    /**
     * Function to convert {@link KhoanMucChiPhiCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<KhoanMucChiPhi> createSpecification(KhoanMucChiPhiCriteria criteria) {
        Specification<KhoanMucChiPhi> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getId(), KhoanMucChiPhi_.id));
            }
            if (criteria.getTen() != null) {
                specification = specification.and(buildStringSpecification(criteria.getTen(), KhoanMucChiPhi_.ten));
            }
            if (criteria.getMoTa() != null) {
                specification = specification.and(buildStringSpecification(criteria.getMoTa(), KhoanMucChiPhi_.moTa));
            }
        }
        return specification;
    }
}
