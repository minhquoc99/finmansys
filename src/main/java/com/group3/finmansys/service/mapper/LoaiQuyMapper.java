package com.group3.finmansys.service.mapper;

import com.group3.finmansys.domain.*;
import com.group3.finmansys.service.dto.LoaiQuyDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link LoaiQuy} and its DTO {@link LoaiQuyDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface LoaiQuyMapper extends EntityMapper<LoaiQuyDTO, LoaiQuy> {
    @Named("ten")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    @Mapping(target = "ten", source = "ten")
    LoaiQuyDTO toDtoTen(LoaiQuy loaiQuy);
}
