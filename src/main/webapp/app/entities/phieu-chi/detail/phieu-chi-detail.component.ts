import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IPhieuChi } from '../phieu-chi.model';

@Component({
  selector: 'jhi-phieu-chi-detail',
  templateUrl: './phieu-chi-detail.component.html',
})
export class PhieuChiDetailComponent implements OnInit {
  phieuChi: IPhieuChi | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ phieuChi }) => {
      this.phieuChi = phieuChi;
    });
  }

  previousState(): void {
    window.history.back();
  }
}
