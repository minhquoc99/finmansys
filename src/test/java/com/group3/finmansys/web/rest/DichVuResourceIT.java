package com.group3.finmansys.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.group3.finmansys.IntegrationTest;
import com.group3.finmansys.domain.DichVu;
import com.group3.finmansys.repository.DichVuRepository;
import com.group3.finmansys.service.criteria.DichVuCriteria;
import com.group3.finmansys.service.dto.DichVuDTO;
import com.group3.finmansys.service.mapper.DichVuMapper;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link DichVuResource} REST controller.
 */
@IntegrationTest
@ExtendWith(MockitoExtension.class)
@AutoConfigureMockMvc
@WithMockUser
class DichVuResourceIT {

    private static final String DEFAULT_TEN = "AAAAAAAAAA";
    private static final String UPDATED_TEN = "BBBBBBBBBB";

    private static final Double DEFAULT_GIA = 1D;
    private static final Double UPDATED_GIA = 2D;
    private static final Double SMALLER_GIA = 1D - 1D;

    private static final String DEFAULT_MO_TA = "AAAAAAAAAA";
    private static final String UPDATED_MO_TA = "BBBBBBBBBB";

    private static final String ENTITY_API_URL = "/api/dich-vus";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private DichVuRepository dichVuRepository;

    @Autowired
    private DichVuMapper dichVuMapper;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restDichVuMockMvc;

    private DichVu dichVu;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static DichVu createEntity(EntityManager em) {
        DichVu dichVu = new DichVu().ten(DEFAULT_TEN).gia(DEFAULT_GIA).moTa(DEFAULT_MO_TA);
        return dichVu;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static DichVu createUpdatedEntity(EntityManager em) {
        DichVu dichVu = new DichVu().ten(UPDATED_TEN).gia(UPDATED_GIA).moTa(UPDATED_MO_TA);
        return dichVu;
    }

    @BeforeEach
    public void initTest() {
        dichVu = createEntity(em);
    }

    @Test
    @Transactional
    void createDichVu() throws Exception {
        int databaseSizeBeforeCreate = dichVuRepository.findAll().size();
        // Create the DichVu
        DichVuDTO dichVuDTO = dichVuMapper.toDto(dichVu);
        restDichVuMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(dichVuDTO)))
            .andExpect(status().isCreated());

        // Validate the DichVu in the database
        List<DichVu> dichVuList = dichVuRepository.findAll();
        assertThat(dichVuList).hasSize(databaseSizeBeforeCreate + 1);
        DichVu testDichVu = dichVuList.get(dichVuList.size() - 1);
        assertThat(testDichVu.getTen()).isEqualTo(DEFAULT_TEN);
        assertThat(testDichVu.getGia()).isEqualTo(DEFAULT_GIA);
        assertThat(testDichVu.getMoTa()).isEqualTo(DEFAULT_MO_TA);
    }

    @Test
    @Transactional
    void createDichVuWithExistingId() throws Exception {
        // Create the DichVu with an existing ID
        dichVu.setId(1L);
        DichVuDTO dichVuDTO = dichVuMapper.toDto(dichVu);

        int databaseSizeBeforeCreate = dichVuRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restDichVuMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(dichVuDTO)))
            .andExpect(status().isBadRequest());

        // Validate the DichVu in the database
        List<DichVu> dichVuList = dichVuRepository.findAll();
        assertThat(dichVuList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void checkTenIsRequired() throws Exception {
        int databaseSizeBeforeTest = dichVuRepository.findAll().size();
        // set the field null
        dichVu.setTen(null);

        // Create the DichVu, which fails.
        DichVuDTO dichVuDTO = dichVuMapper.toDto(dichVu);

        restDichVuMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(dichVuDTO)))
            .andExpect(status().isBadRequest());

        List<DichVu> dichVuList = dichVuRepository.findAll();
        assertThat(dichVuList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkGiaIsRequired() throws Exception {
        int databaseSizeBeforeTest = dichVuRepository.findAll().size();
        // set the field null
        dichVu.setGia(null);

        // Create the DichVu, which fails.
        DichVuDTO dichVuDTO = dichVuMapper.toDto(dichVu);

        restDichVuMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(dichVuDTO)))
            .andExpect(status().isBadRequest());

        List<DichVu> dichVuList = dichVuRepository.findAll();
        assertThat(dichVuList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void getAllDichVus() throws Exception {
        // Initialize the database
        dichVuRepository.saveAndFlush(dichVu);

        // Get all the dichVuList
        restDichVuMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(dichVu.getId().intValue())))
            .andExpect(jsonPath("$.[*].ten").value(hasItem(DEFAULT_TEN)))
            .andExpect(jsonPath("$.[*].gia").value(hasItem(DEFAULT_GIA.doubleValue())))
            .andExpect(jsonPath("$.[*].moTa").value(hasItem(DEFAULT_MO_TA)));
    }

    @Test
    @Transactional
    void getDichVu() throws Exception {
        // Initialize the database
        dichVuRepository.saveAndFlush(dichVu);

        // Get the dichVu
        restDichVuMockMvc
            .perform(get(ENTITY_API_URL_ID, dichVu.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(dichVu.getId().intValue()))
            .andExpect(jsonPath("$.ten").value(DEFAULT_TEN))
            .andExpect(jsonPath("$.gia").value(DEFAULT_GIA.doubleValue()))
            .andExpect(jsonPath("$.moTa").value(DEFAULT_MO_TA));
    }

    @Test
    @Transactional
    void getDichVusByIdFiltering() throws Exception {
        // Initialize the database
        dichVuRepository.saveAndFlush(dichVu);

        Long id = dichVu.getId();

        defaultDichVuShouldBeFound("id.equals=" + id);
        defaultDichVuShouldNotBeFound("id.notEquals=" + id);

        defaultDichVuShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultDichVuShouldNotBeFound("id.greaterThan=" + id);

        defaultDichVuShouldBeFound("id.lessThanOrEqual=" + id);
        defaultDichVuShouldNotBeFound("id.lessThan=" + id);
    }

    @Test
    @Transactional
    void getAllDichVusByTenIsEqualToSomething() throws Exception {
        // Initialize the database
        dichVuRepository.saveAndFlush(dichVu);

        // Get all the dichVuList where ten equals to DEFAULT_TEN
        defaultDichVuShouldBeFound("ten.equals=" + DEFAULT_TEN);

        // Get all the dichVuList where ten equals to UPDATED_TEN
        defaultDichVuShouldNotBeFound("ten.equals=" + UPDATED_TEN);
    }

    @Test
    @Transactional
    void getAllDichVusByTenIsNotEqualToSomething() throws Exception {
        // Initialize the database
        dichVuRepository.saveAndFlush(dichVu);

        // Get all the dichVuList where ten not equals to DEFAULT_TEN
        defaultDichVuShouldNotBeFound("ten.notEquals=" + DEFAULT_TEN);

        // Get all the dichVuList where ten not equals to UPDATED_TEN
        defaultDichVuShouldBeFound("ten.notEquals=" + UPDATED_TEN);
    }

    @Test
    @Transactional
    void getAllDichVusByTenIsInShouldWork() throws Exception {
        // Initialize the database
        dichVuRepository.saveAndFlush(dichVu);

        // Get all the dichVuList where ten in DEFAULT_TEN or UPDATED_TEN
        defaultDichVuShouldBeFound("ten.in=" + DEFAULT_TEN + "," + UPDATED_TEN);

        // Get all the dichVuList where ten equals to UPDATED_TEN
        defaultDichVuShouldNotBeFound("ten.in=" + UPDATED_TEN);
    }

    @Test
    @Transactional
    void getAllDichVusByTenIsNullOrNotNull() throws Exception {
        // Initialize the database
        dichVuRepository.saveAndFlush(dichVu);

        // Get all the dichVuList where ten is not null
        defaultDichVuShouldBeFound("ten.specified=true");

        // Get all the dichVuList where ten is null
        defaultDichVuShouldNotBeFound("ten.specified=false");
    }

    @Test
    @Transactional
    void getAllDichVusByTenContainsSomething() throws Exception {
        // Initialize the database
        dichVuRepository.saveAndFlush(dichVu);

        // Get all the dichVuList where ten contains DEFAULT_TEN
        defaultDichVuShouldBeFound("ten.contains=" + DEFAULT_TEN);

        // Get all the dichVuList where ten contains UPDATED_TEN
        defaultDichVuShouldNotBeFound("ten.contains=" + UPDATED_TEN);
    }

    @Test
    @Transactional
    void getAllDichVusByTenNotContainsSomething() throws Exception {
        // Initialize the database
        dichVuRepository.saveAndFlush(dichVu);

        // Get all the dichVuList where ten does not contain DEFAULT_TEN
        defaultDichVuShouldNotBeFound("ten.doesNotContain=" + DEFAULT_TEN);

        // Get all the dichVuList where ten does not contain UPDATED_TEN
        defaultDichVuShouldBeFound("ten.doesNotContain=" + UPDATED_TEN);
    }

    @Test
    @Transactional
    void getAllDichVusByGiaIsEqualToSomething() throws Exception {
        // Initialize the database
        dichVuRepository.saveAndFlush(dichVu);

        // Get all the dichVuList where gia equals to DEFAULT_GIA
        defaultDichVuShouldBeFound("gia.equals=" + DEFAULT_GIA);

        // Get all the dichVuList where gia equals to UPDATED_GIA
        defaultDichVuShouldNotBeFound("gia.equals=" + UPDATED_GIA);
    }

    @Test
    @Transactional
    void getAllDichVusByGiaIsNotEqualToSomething() throws Exception {
        // Initialize the database
        dichVuRepository.saveAndFlush(dichVu);

        // Get all the dichVuList where gia not equals to DEFAULT_GIA
        defaultDichVuShouldNotBeFound("gia.notEquals=" + DEFAULT_GIA);

        // Get all the dichVuList where gia not equals to UPDATED_GIA
        defaultDichVuShouldBeFound("gia.notEquals=" + UPDATED_GIA);
    }

    @Test
    @Transactional
    void getAllDichVusByGiaIsInShouldWork() throws Exception {
        // Initialize the database
        dichVuRepository.saveAndFlush(dichVu);

        // Get all the dichVuList where gia in DEFAULT_GIA or UPDATED_GIA
        defaultDichVuShouldBeFound("gia.in=" + DEFAULT_GIA + "," + UPDATED_GIA);

        // Get all the dichVuList where gia equals to UPDATED_GIA
        defaultDichVuShouldNotBeFound("gia.in=" + UPDATED_GIA);
    }

    @Test
    @Transactional
    void getAllDichVusByGiaIsNullOrNotNull() throws Exception {
        // Initialize the database
        dichVuRepository.saveAndFlush(dichVu);

        // Get all the dichVuList where gia is not null
        defaultDichVuShouldBeFound("gia.specified=true");

        // Get all the dichVuList where gia is null
        defaultDichVuShouldNotBeFound("gia.specified=false");
    }

    @Test
    @Transactional
    void getAllDichVusByGiaIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        dichVuRepository.saveAndFlush(dichVu);

        // Get all the dichVuList where gia is greater than or equal to DEFAULT_GIA
        defaultDichVuShouldBeFound("gia.greaterThanOrEqual=" + DEFAULT_GIA);

        // Get all the dichVuList where gia is greater than or equal to UPDATED_GIA
        defaultDichVuShouldNotBeFound("gia.greaterThanOrEqual=" + UPDATED_GIA);
    }

    @Test
    @Transactional
    void getAllDichVusByGiaIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        dichVuRepository.saveAndFlush(dichVu);

        // Get all the dichVuList where gia is less than or equal to DEFAULT_GIA
        defaultDichVuShouldBeFound("gia.lessThanOrEqual=" + DEFAULT_GIA);

        // Get all the dichVuList where gia is less than or equal to SMALLER_GIA
        defaultDichVuShouldNotBeFound("gia.lessThanOrEqual=" + SMALLER_GIA);
    }

    @Test
    @Transactional
    void getAllDichVusByGiaIsLessThanSomething() throws Exception {
        // Initialize the database
        dichVuRepository.saveAndFlush(dichVu);

        // Get all the dichVuList where gia is less than DEFAULT_GIA
        defaultDichVuShouldNotBeFound("gia.lessThan=" + DEFAULT_GIA);

        // Get all the dichVuList where gia is less than UPDATED_GIA
        defaultDichVuShouldBeFound("gia.lessThan=" + UPDATED_GIA);
    }

    @Test
    @Transactional
    void getAllDichVusByGiaIsGreaterThanSomething() throws Exception {
        // Initialize the database
        dichVuRepository.saveAndFlush(dichVu);

        // Get all the dichVuList where gia is greater than DEFAULT_GIA
        defaultDichVuShouldNotBeFound("gia.greaterThan=" + DEFAULT_GIA);

        // Get all the dichVuList where gia is greater than SMALLER_GIA
        defaultDichVuShouldBeFound("gia.greaterThan=" + SMALLER_GIA);
    }

    @Test
    @Transactional
    void getAllDichVusByMoTaIsEqualToSomething() throws Exception {
        // Initialize the database
        dichVuRepository.saveAndFlush(dichVu);

        // Get all the dichVuList where moTa equals to DEFAULT_MO_TA
        defaultDichVuShouldBeFound("moTa.equals=" + DEFAULT_MO_TA);

        // Get all the dichVuList where moTa equals to UPDATED_MO_TA
        defaultDichVuShouldNotBeFound("moTa.equals=" + UPDATED_MO_TA);
    }

    @Test
    @Transactional
    void getAllDichVusByMoTaIsNotEqualToSomething() throws Exception {
        // Initialize the database
        dichVuRepository.saveAndFlush(dichVu);

        // Get all the dichVuList where moTa not equals to DEFAULT_MO_TA
        defaultDichVuShouldNotBeFound("moTa.notEquals=" + DEFAULT_MO_TA);

        // Get all the dichVuList where moTa not equals to UPDATED_MO_TA
        defaultDichVuShouldBeFound("moTa.notEquals=" + UPDATED_MO_TA);
    }

    @Test
    @Transactional
    void getAllDichVusByMoTaIsInShouldWork() throws Exception {
        // Initialize the database
        dichVuRepository.saveAndFlush(dichVu);

        // Get all the dichVuList where moTa in DEFAULT_MO_TA or UPDATED_MO_TA
        defaultDichVuShouldBeFound("moTa.in=" + DEFAULT_MO_TA + "," + UPDATED_MO_TA);

        // Get all the dichVuList where moTa equals to UPDATED_MO_TA
        defaultDichVuShouldNotBeFound("moTa.in=" + UPDATED_MO_TA);
    }

    @Test
    @Transactional
    void getAllDichVusByMoTaIsNullOrNotNull() throws Exception {
        // Initialize the database
        dichVuRepository.saveAndFlush(dichVu);

        // Get all the dichVuList where moTa is not null
        defaultDichVuShouldBeFound("moTa.specified=true");

        // Get all the dichVuList where moTa is null
        defaultDichVuShouldNotBeFound("moTa.specified=false");
    }

    @Test
    @Transactional
    void getAllDichVusByMoTaContainsSomething() throws Exception {
        // Initialize the database
        dichVuRepository.saveAndFlush(dichVu);

        // Get all the dichVuList where moTa contains DEFAULT_MO_TA
        defaultDichVuShouldBeFound("moTa.contains=" + DEFAULT_MO_TA);

        // Get all the dichVuList where moTa contains UPDATED_MO_TA
        defaultDichVuShouldNotBeFound("moTa.contains=" + UPDATED_MO_TA);
    }

    @Test
    @Transactional
    void getAllDichVusByMoTaNotContainsSomething() throws Exception {
        // Initialize the database
        dichVuRepository.saveAndFlush(dichVu);

        // Get all the dichVuList where moTa does not contain DEFAULT_MO_TA
        defaultDichVuShouldNotBeFound("moTa.doesNotContain=" + DEFAULT_MO_TA);

        // Get all the dichVuList where moTa does not contain UPDATED_MO_TA
        defaultDichVuShouldBeFound("moTa.doesNotContain=" + UPDATED_MO_TA);
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultDichVuShouldBeFound(String filter) throws Exception {
        restDichVuMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(dichVu.getId().intValue())))
            .andExpect(jsonPath("$.[*].ten").value(hasItem(DEFAULT_TEN)))
            .andExpect(jsonPath("$.[*].gia").value(hasItem(DEFAULT_GIA.doubleValue())))
            .andExpect(jsonPath("$.[*].moTa").value(hasItem(DEFAULT_MO_TA)));

        // Check, that the count call also returns 1
        restDichVuMockMvc
            .perform(get(ENTITY_API_URL + "/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultDichVuShouldNotBeFound(String filter) throws Exception {
        restDichVuMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restDichVuMockMvc
            .perform(get(ENTITY_API_URL + "/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }

    @Test
    @Transactional
    void getNonExistingDichVu() throws Exception {
        // Get the dichVu
        restDichVuMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putNewDichVu() throws Exception {
        // Initialize the database
        dichVuRepository.saveAndFlush(dichVu);

        int databaseSizeBeforeUpdate = dichVuRepository.findAll().size();

        // Update the dichVu
        DichVu updatedDichVu = dichVuRepository.findById(dichVu.getId()).get();
        // Disconnect from session so that the updates on updatedDichVu are not directly saved in db
        em.detach(updatedDichVu);
        updatedDichVu.ten(UPDATED_TEN).gia(UPDATED_GIA).moTa(UPDATED_MO_TA);
        DichVuDTO dichVuDTO = dichVuMapper.toDto(updatedDichVu);

        restDichVuMockMvc
            .perform(
                put(ENTITY_API_URL_ID, dichVuDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(dichVuDTO))
            )
            .andExpect(status().isOk());

        // Validate the DichVu in the database
        List<DichVu> dichVuList = dichVuRepository.findAll();
        assertThat(dichVuList).hasSize(databaseSizeBeforeUpdate);
        DichVu testDichVu = dichVuList.get(dichVuList.size() - 1);
        assertThat(testDichVu.getTen()).isEqualTo(UPDATED_TEN);
        assertThat(testDichVu.getGia()).isEqualTo(UPDATED_GIA);
        assertThat(testDichVu.getMoTa()).isEqualTo(UPDATED_MO_TA);
    }

    @Test
    @Transactional
    void putNonExistingDichVu() throws Exception {
        int databaseSizeBeforeUpdate = dichVuRepository.findAll().size();
        dichVu.setId(count.incrementAndGet());

        // Create the DichVu
        DichVuDTO dichVuDTO = dichVuMapper.toDto(dichVu);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restDichVuMockMvc
            .perform(
                put(ENTITY_API_URL_ID, dichVuDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(dichVuDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the DichVu in the database
        List<DichVu> dichVuList = dichVuRepository.findAll();
        assertThat(dichVuList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchDichVu() throws Exception {
        int databaseSizeBeforeUpdate = dichVuRepository.findAll().size();
        dichVu.setId(count.incrementAndGet());

        // Create the DichVu
        DichVuDTO dichVuDTO = dichVuMapper.toDto(dichVu);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restDichVuMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(dichVuDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the DichVu in the database
        List<DichVu> dichVuList = dichVuRepository.findAll();
        assertThat(dichVuList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamDichVu() throws Exception {
        int databaseSizeBeforeUpdate = dichVuRepository.findAll().size();
        dichVu.setId(count.incrementAndGet());

        // Create the DichVu
        DichVuDTO dichVuDTO = dichVuMapper.toDto(dichVu);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restDichVuMockMvc
            .perform(put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(dichVuDTO)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the DichVu in the database
        List<DichVu> dichVuList = dichVuRepository.findAll();
        assertThat(dichVuList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateDichVuWithPatch() throws Exception {
        // Initialize the database
        dichVuRepository.saveAndFlush(dichVu);

        int databaseSizeBeforeUpdate = dichVuRepository.findAll().size();

        // Update the dichVu using partial update
        DichVu partialUpdatedDichVu = new DichVu();
        partialUpdatedDichVu.setId(dichVu.getId());

        restDichVuMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedDichVu.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedDichVu))
            )
            .andExpect(status().isOk());

        // Validate the DichVu in the database
        List<DichVu> dichVuList = dichVuRepository.findAll();
        assertThat(dichVuList).hasSize(databaseSizeBeforeUpdate);
        DichVu testDichVu = dichVuList.get(dichVuList.size() - 1);
        assertThat(testDichVu.getTen()).isEqualTo(DEFAULT_TEN);
        assertThat(testDichVu.getGia()).isEqualTo(DEFAULT_GIA);
        assertThat(testDichVu.getMoTa()).isEqualTo(DEFAULT_MO_TA);
    }

    @Test
    @Transactional
    void fullUpdateDichVuWithPatch() throws Exception {
        // Initialize the database
        dichVuRepository.saveAndFlush(dichVu);

        int databaseSizeBeforeUpdate = dichVuRepository.findAll().size();

        // Update the dichVu using partial update
        DichVu partialUpdatedDichVu = new DichVu();
        partialUpdatedDichVu.setId(dichVu.getId());

        partialUpdatedDichVu.ten(UPDATED_TEN).gia(UPDATED_GIA).moTa(UPDATED_MO_TA);

        restDichVuMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedDichVu.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedDichVu))
            )
            .andExpect(status().isOk());

        // Validate the DichVu in the database
        List<DichVu> dichVuList = dichVuRepository.findAll();
        assertThat(dichVuList).hasSize(databaseSizeBeforeUpdate);
        DichVu testDichVu = dichVuList.get(dichVuList.size() - 1);
        assertThat(testDichVu.getTen()).isEqualTo(UPDATED_TEN);
        assertThat(testDichVu.getGia()).isEqualTo(UPDATED_GIA);
        assertThat(testDichVu.getMoTa()).isEqualTo(UPDATED_MO_TA);
    }

    @Test
    @Transactional
    void patchNonExistingDichVu() throws Exception {
        int databaseSizeBeforeUpdate = dichVuRepository.findAll().size();
        dichVu.setId(count.incrementAndGet());

        // Create the DichVu
        DichVuDTO dichVuDTO = dichVuMapper.toDto(dichVu);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restDichVuMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, dichVuDTO.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(dichVuDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the DichVu in the database
        List<DichVu> dichVuList = dichVuRepository.findAll();
        assertThat(dichVuList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchDichVu() throws Exception {
        int databaseSizeBeforeUpdate = dichVuRepository.findAll().size();
        dichVu.setId(count.incrementAndGet());

        // Create the DichVu
        DichVuDTO dichVuDTO = dichVuMapper.toDto(dichVu);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restDichVuMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(dichVuDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the DichVu in the database
        List<DichVu> dichVuList = dichVuRepository.findAll();
        assertThat(dichVuList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamDichVu() throws Exception {
        int databaseSizeBeforeUpdate = dichVuRepository.findAll().size();
        dichVu.setId(count.incrementAndGet());

        // Create the DichVu
        DichVuDTO dichVuDTO = dichVuMapper.toDto(dichVu);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restDichVuMockMvc
            .perform(
                patch(ENTITY_API_URL).contentType("application/merge-patch+json").content(TestUtil.convertObjectToJsonBytes(dichVuDTO))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the DichVu in the database
        List<DichVu> dichVuList = dichVuRepository.findAll();
        assertThat(dichVuList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteDichVu() throws Exception {
        // Initialize the database
        dichVuRepository.saveAndFlush(dichVu);

        int databaseSizeBeforeDelete = dichVuRepository.findAll().size();

        // Delete the dichVu
        restDichVuMockMvc
            .perform(delete(ENTITY_API_URL_ID, dichVu.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<DichVu> dichVuList = dichVuRepository.findAll();
        assertThat(dichVuList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
