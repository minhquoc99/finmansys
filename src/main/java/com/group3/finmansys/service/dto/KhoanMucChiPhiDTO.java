package com.group3.finmansys.service.dto;

import java.io.Serializable;
import java.util.Objects;
import javax.validation.constraints.*;

/**
 * A DTO for the {@link com.group3.finmansys.domain.KhoanMucChiPhi} entity.
 */
public class KhoanMucChiPhiDTO implements Serializable {

    private Long id;

    @NotNull
    private String ten;

    private String moTa;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTen() {
        return ten;
    }

    public void setTen(String ten) {
        this.ten = ten;
    }

    public String getMoTa() {
        return moTa;
    }

    public void setMoTa(String moTa) {
        this.moTa = moTa;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof KhoanMucChiPhiDTO)) {
            return false;
        }

        KhoanMucChiPhiDTO khoanMucChiPhiDTO = (KhoanMucChiPhiDTO) o;
        if (this.id == null) {
            return false;
        }
        return Objects.equals(this.id, khoanMucChiPhiDTO.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.id);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "KhoanMucChiPhiDTO{" +
            "id=" + getId() +
            ", ten='" + getTen() + "'" +
            ", moTa='" + getMoTa() + "'" +
            "}";
    }
}
