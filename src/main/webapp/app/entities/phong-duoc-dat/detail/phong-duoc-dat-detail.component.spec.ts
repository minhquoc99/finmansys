import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { PhongDuocDatDetailComponent } from './phong-duoc-dat-detail.component';

describe('Component Tests', () => {
  describe('PhongDuocDat Management Detail Component', () => {
    let comp: PhongDuocDatDetailComponent;
    let fixture: ComponentFixture<PhongDuocDatDetailComponent>;

    beforeEach(() => {
      TestBed.configureTestingModule({
        declarations: [PhongDuocDatDetailComponent],
        providers: [
          {
            provide: ActivatedRoute,
            useValue: { data: of({ phongDuocDat: { id: 123 } }) },
          },
        ],
      })
        .overrideTemplate(PhongDuocDatDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(PhongDuocDatDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should load phongDuocDat on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.phongDuocDat).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
