package com.group3.finmansys.service.dto;

import com.group3.finmansys.domain.PhieuChi;
import com.group3.finmansys.domain.PhieuThu;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import javax.validation.constraints.*;

/**
 * A DTO for the {@link com.group3.finmansys.domain.QuyTien} entity.
 */
public class QuyTienDTO implements Serializable {

    private Long id;

    @NotNull
    @DecimalMin(value = "1")
    private Double soTien;

    private String ghiChu;

    @NotNull
    private String ten;

    private LoaiQuyDTO loaiQuy;

    private MaKeToanDTO maKeToan;

    private KhachSanDTO khachSan;

    private Set<PhieuThu> phieuThus = new HashSet<>();

    private Set<PhieuChi> phieuChis = new HashSet<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Double getSoTien() {
        return soTien;
    }

    public void setSoTien(Double soTien) {
        this.soTien = soTien;
    }

    public String getGhiChu() {
        return ghiChu;
    }

    public void setGhiChu(String ghiChu) {
        this.ghiChu = ghiChu;
    }

    public String getTen() {
        return ten;
    }

    public void setTen(String ten) {
        this.ten = ten;
    }

    public LoaiQuyDTO getLoaiQuy() {
        return loaiQuy;
    }

    public void setLoaiQuy(LoaiQuyDTO loaiQuy) {
        this.loaiQuy = loaiQuy;
    }

    public MaKeToanDTO getMaKeToan() {
        return maKeToan;
    }

    public void setMaKeToan(MaKeToanDTO maKeToan) {
        this.maKeToan = maKeToan;
    }

    public KhachSanDTO getKhachSan() {
        return khachSan;
    }

    public void setKhachSan(KhachSanDTO khachSan) {
        this.khachSan = khachSan;
    }

    public Set<PhieuThu> getPhieuThus() {
        return phieuThus;
    }

    public void setPhieuThus(Set<PhieuThu> phieuThus) {
        this.phieuThus = phieuThus;
    }

    public Set<PhieuChi> getPhieuChis() {
        return phieuChis;
    }

    public void setPhieuChis(Set<PhieuChi> phieuChis) {
        this.phieuChis = phieuChis;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof QuyTienDTO)) {
            return false;
        }

        QuyTienDTO quyTienDTO = (QuyTienDTO) o;
        if (this.id == null) {
            return false;
        }
        return Objects.equals(this.id, quyTienDTO.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.id);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "QuyTienDTO{" +
            "id=" + getId() +
            ", soTien=" + getSoTien() +
            ", ghiChu='" + getGhiChu() + "'" +
            ", ten='" + getTen() + "'" +
            ", loaiQuy=" + getLoaiQuy() +
            ", maKeToan=" + getMaKeToan() +
            ", khachSan=" + getKhachSan() +
            "}";
    }
}
