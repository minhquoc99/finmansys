jest.mock('@angular/router');

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { of, Subject } from 'rxjs';

import { PhongService } from '../service/phong.service';
import { IPhong, Phong } from '../phong.model';
import { IKhachSan } from 'app/entities/khach-san/khach-san.model';
import { KhachSanService } from 'app/entities/khach-san/service/khach-san.service';

import { PhongUpdateComponent } from './phong-update.component';

describe('Component Tests', () => {
  describe('Phong Management Update Component', () => {
    let comp: PhongUpdateComponent;
    let fixture: ComponentFixture<PhongUpdateComponent>;
    let activatedRoute: ActivatedRoute;
    let phongService: PhongService;
    let khachSanService: KhachSanService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule],
        declarations: [PhongUpdateComponent],
        providers: [FormBuilder, ActivatedRoute],
      })
        .overrideTemplate(PhongUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(PhongUpdateComponent);
      activatedRoute = TestBed.inject(ActivatedRoute);
      phongService = TestBed.inject(PhongService);
      khachSanService = TestBed.inject(KhachSanService);

      comp = fixture.componentInstance;
    });

    describe('ngOnInit', () => {
      it('Should call KhachSan query and add missing value', () => {
        const phong: IPhong = { id: 456 };
        const khachSan: IKhachSan = { id: 40945 };
        phong.khachSan = khachSan;

        const khachSanCollection: IKhachSan[] = [{ id: 22075 }];
        spyOn(khachSanService, 'query').and.returnValue(of(new HttpResponse({ body: khachSanCollection })));
        const additionalKhachSans = [khachSan];
        const expectedCollection: IKhachSan[] = [...additionalKhachSans, ...khachSanCollection];
        spyOn(khachSanService, 'addKhachSanToCollectionIfMissing').and.returnValue(expectedCollection);

        activatedRoute.data = of({ phong });
        comp.ngOnInit();

        expect(khachSanService.query).toHaveBeenCalled();
        expect(khachSanService.addKhachSanToCollectionIfMissing).toHaveBeenCalledWith(khachSanCollection, ...additionalKhachSans);
        expect(comp.khachSansSharedCollection).toEqual(expectedCollection);
      });

      it('Should update editForm', () => {
        const phong: IPhong = { id: 456 };
        const khachSan: IKhachSan = { id: 92785 };
        phong.khachSan = khachSan;

        activatedRoute.data = of({ phong });
        comp.ngOnInit();

        expect(comp.editForm.value).toEqual(expect.objectContaining(phong));
        expect(comp.khachSansSharedCollection).toContain(khachSan);
      });
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', () => {
        // GIVEN
        const saveSubject = new Subject();
        const phong = { id: 123 };
        spyOn(phongService, 'update').and.returnValue(saveSubject);
        spyOn(comp, 'previousState');
        activatedRoute.data = of({ phong });
        comp.ngOnInit();

        // WHEN
        comp.save();
        expect(comp.isSaving).toEqual(true);
        saveSubject.next(new HttpResponse({ body: phong }));
        saveSubject.complete();

        // THEN
        expect(comp.previousState).toHaveBeenCalled();
        expect(phongService.update).toHaveBeenCalledWith(phong);
        expect(comp.isSaving).toEqual(false);
      });

      it('Should call create service on save for new entity', () => {
        // GIVEN
        const saveSubject = new Subject();
        const phong = new Phong();
        spyOn(phongService, 'create').and.returnValue(saveSubject);
        spyOn(comp, 'previousState');
        activatedRoute.data = of({ phong });
        comp.ngOnInit();

        // WHEN
        comp.save();
        expect(comp.isSaving).toEqual(true);
        saveSubject.next(new HttpResponse({ body: phong }));
        saveSubject.complete();

        // THEN
        expect(phongService.create).toHaveBeenCalledWith(phong);
        expect(comp.isSaving).toEqual(false);
        expect(comp.previousState).toHaveBeenCalled();
      });

      it('Should set isSaving to false on error', () => {
        // GIVEN
        const saveSubject = new Subject();
        const phong = { id: 123 };
        spyOn(phongService, 'update').and.returnValue(saveSubject);
        spyOn(comp, 'previousState');
        activatedRoute.data = of({ phong });
        comp.ngOnInit();

        // WHEN
        comp.save();
        expect(comp.isSaving).toEqual(true);
        saveSubject.error('This is an error!');

        // THEN
        expect(phongService.update).toHaveBeenCalledWith(phong);
        expect(comp.isSaving).toEqual(false);
        expect(comp.previousState).not.toHaveBeenCalled();
      });
    });

    describe('Tracking relationships identifiers', () => {
      describe('trackKhachSanById', () => {
        it('Should return tracked KhachSan primary key', () => {
          const entity = { id: 123 };
          const trackResult = comp.trackKhachSanById(0, entity);
          expect(trackResult).toEqual(entity.id);
        });
      });
    });
  });
});
