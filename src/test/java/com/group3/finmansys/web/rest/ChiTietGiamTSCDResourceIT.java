package com.group3.finmansys.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.group3.finmansys.IntegrationTest;
import com.group3.finmansys.domain.ChiTietGiamTSCD;
import com.group3.finmansys.domain.TaiSanCD;
import com.group3.finmansys.repository.ChiTietGiamTSCDRepository;
import com.group3.finmansys.service.criteria.ChiTietGiamTSCDCriteria;
import com.group3.finmansys.service.dto.ChiTietGiamTSCDDTO;
import com.group3.finmansys.service.mapper.ChiTietGiamTSCDMapper;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link ChiTietGiamTSCDResource} REST controller.
 */
@IntegrationTest
@ExtendWith(MockitoExtension.class)
@AutoConfigureMockMvc
@WithMockUser
class ChiTietGiamTSCDResourceIT {

    private static final Instant DEFAULT_NGAY_MUA = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_NGAY_MUA = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final Integer DEFAULT_SO_LUONG = 1;
    private static final Integer UPDATED_SO_LUONG = 2;
    private static final Integer SMALLER_SO_LUONG = 1 - 1;

    private static final Double DEFAULT_KHAU_HAO = 0D;
    private static final Double UPDATED_KHAU_HAO = 1D;
    private static final Double SMALLER_KHAU_HAO = 0D - 1D;

    private static final Double DEFAULT_SO_TIEN_THANH_LY = 1D;
    private static final Double UPDATED_SO_TIEN_THANH_LY = 2D;
    private static final Double SMALLER_SO_TIEN_THANH_LY = 1D - 1D;

    private static final Double DEFAULT_CHI_PHI_MUA = 1D;
    private static final Double UPDATED_CHI_PHI_MUA = 2D;
    private static final Double SMALLER_CHI_PHI_MUA = 1D - 1D;

    private static final String ENTITY_API_URL = "/api/chi-tiet-giam-tscds";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private ChiTietGiamTSCDRepository chiTietGiamTSCDRepository;

    @Autowired
    private ChiTietGiamTSCDMapper chiTietGiamTSCDMapper;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restChiTietGiamTSCDMockMvc;

    private ChiTietGiamTSCD chiTietGiamTSCD;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ChiTietGiamTSCD createEntity(EntityManager em) {
        ChiTietGiamTSCD chiTietGiamTSCD = new ChiTietGiamTSCD()
            .ngayMua(DEFAULT_NGAY_MUA)
            .soLuong(DEFAULT_SO_LUONG)
            .khauHao(DEFAULT_KHAU_HAO)
            .soTienThanhLy(DEFAULT_SO_TIEN_THANH_LY)
            .chiPhiMua(DEFAULT_CHI_PHI_MUA);
        // Add required entity
        TaiSanCD taiSanCD;
        if (TestUtil.findAll(em, TaiSanCD.class).isEmpty()) {
            taiSanCD = TaiSanCDResourceIT.createEntity(em);
            em.persist(taiSanCD);
            em.flush();
        } else {
            taiSanCD = TestUtil.findAll(em, TaiSanCD.class).get(0);
        }
        chiTietGiamTSCD.setTaiSanCD(taiSanCD);
        return chiTietGiamTSCD;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ChiTietGiamTSCD createUpdatedEntity(EntityManager em) {
        ChiTietGiamTSCD chiTietGiamTSCD = new ChiTietGiamTSCD()
            .ngayMua(UPDATED_NGAY_MUA)
            .soLuong(UPDATED_SO_LUONG)
            .khauHao(UPDATED_KHAU_HAO)
            .soTienThanhLy(UPDATED_SO_TIEN_THANH_LY)
            .chiPhiMua(UPDATED_CHI_PHI_MUA);
        // Add required entity
        TaiSanCD taiSanCD;
        if (TestUtil.findAll(em, TaiSanCD.class).isEmpty()) {
            taiSanCD = TaiSanCDResourceIT.createUpdatedEntity(em);
            em.persist(taiSanCD);
            em.flush();
        } else {
            taiSanCD = TestUtil.findAll(em, TaiSanCD.class).get(0);
        }
        chiTietGiamTSCD.setTaiSanCD(taiSanCD);
        return chiTietGiamTSCD;
    }

    @BeforeEach
    public void initTest() {
        chiTietGiamTSCD = createEntity(em);
    }

    @Test
    @Transactional
    void createChiTietGiamTSCD() throws Exception {
        int databaseSizeBeforeCreate = chiTietGiamTSCDRepository.findAll().size();
        // Create the ChiTietGiamTSCD
        ChiTietGiamTSCDDTO chiTietGiamTSCDDTO = chiTietGiamTSCDMapper.toDto(chiTietGiamTSCD);
        restChiTietGiamTSCDMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(chiTietGiamTSCDDTO))
            )
            .andExpect(status().isCreated());

        // Validate the ChiTietGiamTSCD in the database
        List<ChiTietGiamTSCD> chiTietGiamTSCDList = chiTietGiamTSCDRepository.findAll();
        assertThat(chiTietGiamTSCDList).hasSize(databaseSizeBeforeCreate + 1);
        ChiTietGiamTSCD testChiTietGiamTSCD = chiTietGiamTSCDList.get(chiTietGiamTSCDList.size() - 1);
        assertThat(testChiTietGiamTSCD.getNgayMua()).isEqualTo(DEFAULT_NGAY_MUA);
        assertThat(testChiTietGiamTSCD.getSoLuong()).isEqualTo(DEFAULT_SO_LUONG);
        assertThat(testChiTietGiamTSCD.getKhauHao()).isEqualTo(DEFAULT_KHAU_HAO);
        assertThat(testChiTietGiamTSCD.getSoTienThanhLy()).isEqualTo(DEFAULT_SO_TIEN_THANH_LY);
        assertThat(testChiTietGiamTSCD.getChiPhiMua()).isEqualTo(DEFAULT_CHI_PHI_MUA);
    }

    @Test
    @Transactional
    void createChiTietGiamTSCDWithExistingId() throws Exception {
        // Create the ChiTietGiamTSCD with an existing ID
        chiTietGiamTSCD.setId(1L);
        ChiTietGiamTSCDDTO chiTietGiamTSCDDTO = chiTietGiamTSCDMapper.toDto(chiTietGiamTSCD);

        int databaseSizeBeforeCreate = chiTietGiamTSCDRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restChiTietGiamTSCDMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(chiTietGiamTSCDDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the ChiTietGiamTSCD in the database
        List<ChiTietGiamTSCD> chiTietGiamTSCDList = chiTietGiamTSCDRepository.findAll();
        assertThat(chiTietGiamTSCDList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void checkNgayMuaIsRequired() throws Exception {
        int databaseSizeBeforeTest = chiTietGiamTSCDRepository.findAll().size();
        // set the field null
        chiTietGiamTSCD.setNgayMua(null);

        // Create the ChiTietGiamTSCD, which fails.
        ChiTietGiamTSCDDTO chiTietGiamTSCDDTO = chiTietGiamTSCDMapper.toDto(chiTietGiamTSCD);

        restChiTietGiamTSCDMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(chiTietGiamTSCDDTO))
            )
            .andExpect(status().isBadRequest());

        List<ChiTietGiamTSCD> chiTietGiamTSCDList = chiTietGiamTSCDRepository.findAll();
        assertThat(chiTietGiamTSCDList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkSoLuongIsRequired() throws Exception {
        int databaseSizeBeforeTest = chiTietGiamTSCDRepository.findAll().size();
        // set the field null
        chiTietGiamTSCD.setSoLuong(null);

        // Create the ChiTietGiamTSCD, which fails.
        ChiTietGiamTSCDDTO chiTietGiamTSCDDTO = chiTietGiamTSCDMapper.toDto(chiTietGiamTSCD);

        restChiTietGiamTSCDMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(chiTietGiamTSCDDTO))
            )
            .andExpect(status().isBadRequest());

        List<ChiTietGiamTSCD> chiTietGiamTSCDList = chiTietGiamTSCDRepository.findAll();
        assertThat(chiTietGiamTSCDList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkSoTienThanhLyIsRequired() throws Exception {
        int databaseSizeBeforeTest = chiTietGiamTSCDRepository.findAll().size();
        // set the field null
        chiTietGiamTSCD.setSoTienThanhLy(null);

        // Create the ChiTietGiamTSCD, which fails.
        ChiTietGiamTSCDDTO chiTietGiamTSCDDTO = chiTietGiamTSCDMapper.toDto(chiTietGiamTSCD);

        restChiTietGiamTSCDMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(chiTietGiamTSCDDTO))
            )
            .andExpect(status().isBadRequest());

        List<ChiTietGiamTSCD> chiTietGiamTSCDList = chiTietGiamTSCDRepository.findAll();
        assertThat(chiTietGiamTSCDList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkChiPhiMuaIsRequired() throws Exception {
        int databaseSizeBeforeTest = chiTietGiamTSCDRepository.findAll().size();
        // set the field null
        chiTietGiamTSCD.setChiPhiMua(null);

        // Create the ChiTietGiamTSCD, which fails.
        ChiTietGiamTSCDDTO chiTietGiamTSCDDTO = chiTietGiamTSCDMapper.toDto(chiTietGiamTSCD);

        restChiTietGiamTSCDMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(chiTietGiamTSCDDTO))
            )
            .andExpect(status().isBadRequest());

        List<ChiTietGiamTSCD> chiTietGiamTSCDList = chiTietGiamTSCDRepository.findAll();
        assertThat(chiTietGiamTSCDList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void getAllChiTietGiamTSCDS() throws Exception {
        // Initialize the database
        chiTietGiamTSCDRepository.saveAndFlush(chiTietGiamTSCD);

        // Get all the chiTietGiamTSCDList
        restChiTietGiamTSCDMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(chiTietGiamTSCD.getId().intValue())))
            .andExpect(jsonPath("$.[*].ngayMua").value(hasItem(DEFAULT_NGAY_MUA.toString())))
            .andExpect(jsonPath("$.[*].soLuong").value(hasItem(DEFAULT_SO_LUONG)))
            .andExpect(jsonPath("$.[*].khauHao").value(hasItem(DEFAULT_KHAU_HAO.doubleValue())))
            .andExpect(jsonPath("$.[*].soTienThanhLy").value(hasItem(DEFAULT_SO_TIEN_THANH_LY.doubleValue())))
            .andExpect(jsonPath("$.[*].chiPhiMua").value(hasItem(DEFAULT_CHI_PHI_MUA.doubleValue())));
    }

    @Test
    @Transactional
    void getChiTietGiamTSCD() throws Exception {
        // Initialize the database
        chiTietGiamTSCDRepository.saveAndFlush(chiTietGiamTSCD);

        // Get the chiTietGiamTSCD
        restChiTietGiamTSCDMockMvc
            .perform(get(ENTITY_API_URL_ID, chiTietGiamTSCD.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(chiTietGiamTSCD.getId().intValue()))
            .andExpect(jsonPath("$.ngayMua").value(DEFAULT_NGAY_MUA.toString()))
            .andExpect(jsonPath("$.soLuong").value(DEFAULT_SO_LUONG))
            .andExpect(jsonPath("$.khauHao").value(DEFAULT_KHAU_HAO.doubleValue()))
            .andExpect(jsonPath("$.soTienThanhLy").value(DEFAULT_SO_TIEN_THANH_LY.doubleValue()))
            .andExpect(jsonPath("$.chiPhiMua").value(DEFAULT_CHI_PHI_MUA.doubleValue()));
    }

    @Test
    @Transactional
    void getChiTietGiamTSCDSByIdFiltering() throws Exception {
        // Initialize the database
        chiTietGiamTSCDRepository.saveAndFlush(chiTietGiamTSCD);

        Long id = chiTietGiamTSCD.getId();

        defaultChiTietGiamTSCDShouldBeFound("id.equals=" + id);
        defaultChiTietGiamTSCDShouldNotBeFound("id.notEquals=" + id);

        defaultChiTietGiamTSCDShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultChiTietGiamTSCDShouldNotBeFound("id.greaterThan=" + id);

        defaultChiTietGiamTSCDShouldBeFound("id.lessThanOrEqual=" + id);
        defaultChiTietGiamTSCDShouldNotBeFound("id.lessThan=" + id);
    }

    @Test
    @Transactional
    void getAllChiTietGiamTSCDSByNgayMuaIsEqualToSomething() throws Exception {
        // Initialize the database
        chiTietGiamTSCDRepository.saveAndFlush(chiTietGiamTSCD);

        // Get all the chiTietGiamTSCDList where ngayMua equals to DEFAULT_NGAY_MUA
        defaultChiTietGiamTSCDShouldBeFound("ngayMua.equals=" + DEFAULT_NGAY_MUA);

        // Get all the chiTietGiamTSCDList where ngayMua equals to UPDATED_NGAY_MUA
        defaultChiTietGiamTSCDShouldNotBeFound("ngayMua.equals=" + UPDATED_NGAY_MUA);
    }

    @Test
    @Transactional
    void getAllChiTietGiamTSCDSByNgayMuaIsNotEqualToSomething() throws Exception {
        // Initialize the database
        chiTietGiamTSCDRepository.saveAndFlush(chiTietGiamTSCD);

        // Get all the chiTietGiamTSCDList where ngayMua not equals to DEFAULT_NGAY_MUA
        defaultChiTietGiamTSCDShouldNotBeFound("ngayMua.notEquals=" + DEFAULT_NGAY_MUA);

        // Get all the chiTietGiamTSCDList where ngayMua not equals to UPDATED_NGAY_MUA
        defaultChiTietGiamTSCDShouldBeFound("ngayMua.notEquals=" + UPDATED_NGAY_MUA);
    }

    @Test
    @Transactional
    void getAllChiTietGiamTSCDSByNgayMuaIsInShouldWork() throws Exception {
        // Initialize the database
        chiTietGiamTSCDRepository.saveAndFlush(chiTietGiamTSCD);

        // Get all the chiTietGiamTSCDList where ngayMua in DEFAULT_NGAY_MUA or UPDATED_NGAY_MUA
        defaultChiTietGiamTSCDShouldBeFound("ngayMua.in=" + DEFAULT_NGAY_MUA + "," + UPDATED_NGAY_MUA);

        // Get all the chiTietGiamTSCDList where ngayMua equals to UPDATED_NGAY_MUA
        defaultChiTietGiamTSCDShouldNotBeFound("ngayMua.in=" + UPDATED_NGAY_MUA);
    }

    @Test
    @Transactional
    void getAllChiTietGiamTSCDSByNgayMuaIsNullOrNotNull() throws Exception {
        // Initialize the database
        chiTietGiamTSCDRepository.saveAndFlush(chiTietGiamTSCD);

        // Get all the chiTietGiamTSCDList where ngayMua is not null
        defaultChiTietGiamTSCDShouldBeFound("ngayMua.specified=true");

        // Get all the chiTietGiamTSCDList where ngayMua is null
        defaultChiTietGiamTSCDShouldNotBeFound("ngayMua.specified=false");
    }

    @Test
    @Transactional
    void getAllChiTietGiamTSCDSBySoLuongIsEqualToSomething() throws Exception {
        // Initialize the database
        chiTietGiamTSCDRepository.saveAndFlush(chiTietGiamTSCD);

        // Get all the chiTietGiamTSCDList where soLuong equals to DEFAULT_SO_LUONG
        defaultChiTietGiamTSCDShouldBeFound("soLuong.equals=" + DEFAULT_SO_LUONG);

        // Get all the chiTietGiamTSCDList where soLuong equals to UPDATED_SO_LUONG
        defaultChiTietGiamTSCDShouldNotBeFound("soLuong.equals=" + UPDATED_SO_LUONG);
    }

    @Test
    @Transactional
    void getAllChiTietGiamTSCDSBySoLuongIsNotEqualToSomething() throws Exception {
        // Initialize the database
        chiTietGiamTSCDRepository.saveAndFlush(chiTietGiamTSCD);

        // Get all the chiTietGiamTSCDList where soLuong not equals to DEFAULT_SO_LUONG
        defaultChiTietGiamTSCDShouldNotBeFound("soLuong.notEquals=" + DEFAULT_SO_LUONG);

        // Get all the chiTietGiamTSCDList where soLuong not equals to UPDATED_SO_LUONG
        defaultChiTietGiamTSCDShouldBeFound("soLuong.notEquals=" + UPDATED_SO_LUONG);
    }

    @Test
    @Transactional
    void getAllChiTietGiamTSCDSBySoLuongIsInShouldWork() throws Exception {
        // Initialize the database
        chiTietGiamTSCDRepository.saveAndFlush(chiTietGiamTSCD);

        // Get all the chiTietGiamTSCDList where soLuong in DEFAULT_SO_LUONG or UPDATED_SO_LUONG
        defaultChiTietGiamTSCDShouldBeFound("soLuong.in=" + DEFAULT_SO_LUONG + "," + UPDATED_SO_LUONG);

        // Get all the chiTietGiamTSCDList where soLuong equals to UPDATED_SO_LUONG
        defaultChiTietGiamTSCDShouldNotBeFound("soLuong.in=" + UPDATED_SO_LUONG);
    }

    @Test
    @Transactional
    void getAllChiTietGiamTSCDSBySoLuongIsNullOrNotNull() throws Exception {
        // Initialize the database
        chiTietGiamTSCDRepository.saveAndFlush(chiTietGiamTSCD);

        // Get all the chiTietGiamTSCDList where soLuong is not null
        defaultChiTietGiamTSCDShouldBeFound("soLuong.specified=true");

        // Get all the chiTietGiamTSCDList where soLuong is null
        defaultChiTietGiamTSCDShouldNotBeFound("soLuong.specified=false");
    }

    @Test
    @Transactional
    void getAllChiTietGiamTSCDSBySoLuongIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        chiTietGiamTSCDRepository.saveAndFlush(chiTietGiamTSCD);

        // Get all the chiTietGiamTSCDList where soLuong is greater than or equal to DEFAULT_SO_LUONG
        defaultChiTietGiamTSCDShouldBeFound("soLuong.greaterThanOrEqual=" + DEFAULT_SO_LUONG);

        // Get all the chiTietGiamTSCDList where soLuong is greater than or equal to UPDATED_SO_LUONG
        defaultChiTietGiamTSCDShouldNotBeFound("soLuong.greaterThanOrEqual=" + UPDATED_SO_LUONG);
    }

    @Test
    @Transactional
    void getAllChiTietGiamTSCDSBySoLuongIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        chiTietGiamTSCDRepository.saveAndFlush(chiTietGiamTSCD);

        // Get all the chiTietGiamTSCDList where soLuong is less than or equal to DEFAULT_SO_LUONG
        defaultChiTietGiamTSCDShouldBeFound("soLuong.lessThanOrEqual=" + DEFAULT_SO_LUONG);

        // Get all the chiTietGiamTSCDList where soLuong is less than or equal to SMALLER_SO_LUONG
        defaultChiTietGiamTSCDShouldNotBeFound("soLuong.lessThanOrEqual=" + SMALLER_SO_LUONG);
    }

    @Test
    @Transactional
    void getAllChiTietGiamTSCDSBySoLuongIsLessThanSomething() throws Exception {
        // Initialize the database
        chiTietGiamTSCDRepository.saveAndFlush(chiTietGiamTSCD);

        // Get all the chiTietGiamTSCDList where soLuong is less than DEFAULT_SO_LUONG
        defaultChiTietGiamTSCDShouldNotBeFound("soLuong.lessThan=" + DEFAULT_SO_LUONG);

        // Get all the chiTietGiamTSCDList where soLuong is less than UPDATED_SO_LUONG
        defaultChiTietGiamTSCDShouldBeFound("soLuong.lessThan=" + UPDATED_SO_LUONG);
    }

    @Test
    @Transactional
    void getAllChiTietGiamTSCDSBySoLuongIsGreaterThanSomething() throws Exception {
        // Initialize the database
        chiTietGiamTSCDRepository.saveAndFlush(chiTietGiamTSCD);

        // Get all the chiTietGiamTSCDList where soLuong is greater than DEFAULT_SO_LUONG
        defaultChiTietGiamTSCDShouldNotBeFound("soLuong.greaterThan=" + DEFAULT_SO_LUONG);

        // Get all the chiTietGiamTSCDList where soLuong is greater than SMALLER_SO_LUONG
        defaultChiTietGiamTSCDShouldBeFound("soLuong.greaterThan=" + SMALLER_SO_LUONG);
    }

    @Test
    @Transactional
    void getAllChiTietGiamTSCDSByKhauHaoIsEqualToSomething() throws Exception {
        // Initialize the database
        chiTietGiamTSCDRepository.saveAndFlush(chiTietGiamTSCD);

        // Get all the chiTietGiamTSCDList where khauHao equals to DEFAULT_KHAU_HAO
        defaultChiTietGiamTSCDShouldBeFound("khauHao.equals=" + DEFAULT_KHAU_HAO);

        // Get all the chiTietGiamTSCDList where khauHao equals to UPDATED_KHAU_HAO
        defaultChiTietGiamTSCDShouldNotBeFound("khauHao.equals=" + UPDATED_KHAU_HAO);
    }

    @Test
    @Transactional
    void getAllChiTietGiamTSCDSByKhauHaoIsNotEqualToSomething() throws Exception {
        // Initialize the database
        chiTietGiamTSCDRepository.saveAndFlush(chiTietGiamTSCD);

        // Get all the chiTietGiamTSCDList where khauHao not equals to DEFAULT_KHAU_HAO
        defaultChiTietGiamTSCDShouldNotBeFound("khauHao.notEquals=" + DEFAULT_KHAU_HAO);

        // Get all the chiTietGiamTSCDList where khauHao not equals to UPDATED_KHAU_HAO
        defaultChiTietGiamTSCDShouldBeFound("khauHao.notEquals=" + UPDATED_KHAU_HAO);
    }

    @Test
    @Transactional
    void getAllChiTietGiamTSCDSByKhauHaoIsInShouldWork() throws Exception {
        // Initialize the database
        chiTietGiamTSCDRepository.saveAndFlush(chiTietGiamTSCD);

        // Get all the chiTietGiamTSCDList where khauHao in DEFAULT_KHAU_HAO or UPDATED_KHAU_HAO
        defaultChiTietGiamTSCDShouldBeFound("khauHao.in=" + DEFAULT_KHAU_HAO + "," + UPDATED_KHAU_HAO);

        // Get all the chiTietGiamTSCDList where khauHao equals to UPDATED_KHAU_HAO
        defaultChiTietGiamTSCDShouldNotBeFound("khauHao.in=" + UPDATED_KHAU_HAO);
    }

    @Test
    @Transactional
    void getAllChiTietGiamTSCDSByKhauHaoIsNullOrNotNull() throws Exception {
        // Initialize the database
        chiTietGiamTSCDRepository.saveAndFlush(chiTietGiamTSCD);

        // Get all the chiTietGiamTSCDList where khauHao is not null
        defaultChiTietGiamTSCDShouldBeFound("khauHao.specified=true");

        // Get all the chiTietGiamTSCDList where khauHao is null
        defaultChiTietGiamTSCDShouldNotBeFound("khauHao.specified=false");
    }

    @Test
    @Transactional
    void getAllChiTietGiamTSCDSByKhauHaoIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        chiTietGiamTSCDRepository.saveAndFlush(chiTietGiamTSCD);

        // Get all the chiTietGiamTSCDList where khauHao is greater than or equal to DEFAULT_KHAU_HAO
        defaultChiTietGiamTSCDShouldBeFound("khauHao.greaterThanOrEqual=" + DEFAULT_KHAU_HAO);

        // Get all the chiTietGiamTSCDList where khauHao is greater than or equal to UPDATED_KHAU_HAO
        defaultChiTietGiamTSCDShouldNotBeFound("khauHao.greaterThanOrEqual=" + UPDATED_KHAU_HAO);
    }

    @Test
    @Transactional
    void getAllChiTietGiamTSCDSByKhauHaoIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        chiTietGiamTSCDRepository.saveAndFlush(chiTietGiamTSCD);

        // Get all the chiTietGiamTSCDList where khauHao is less than or equal to DEFAULT_KHAU_HAO
        defaultChiTietGiamTSCDShouldBeFound("khauHao.lessThanOrEqual=" + DEFAULT_KHAU_HAO);

        // Get all the chiTietGiamTSCDList where khauHao is less than or equal to SMALLER_KHAU_HAO
        defaultChiTietGiamTSCDShouldNotBeFound("khauHao.lessThanOrEqual=" + SMALLER_KHAU_HAO);
    }

    @Test
    @Transactional
    void getAllChiTietGiamTSCDSByKhauHaoIsLessThanSomething() throws Exception {
        // Initialize the database
        chiTietGiamTSCDRepository.saveAndFlush(chiTietGiamTSCD);

        // Get all the chiTietGiamTSCDList where khauHao is less than DEFAULT_KHAU_HAO
        defaultChiTietGiamTSCDShouldNotBeFound("khauHao.lessThan=" + DEFAULT_KHAU_HAO);

        // Get all the chiTietGiamTSCDList where khauHao is less than UPDATED_KHAU_HAO
        defaultChiTietGiamTSCDShouldBeFound("khauHao.lessThan=" + UPDATED_KHAU_HAO);
    }

    @Test
    @Transactional
    void getAllChiTietGiamTSCDSByKhauHaoIsGreaterThanSomething() throws Exception {
        // Initialize the database
        chiTietGiamTSCDRepository.saveAndFlush(chiTietGiamTSCD);

        // Get all the chiTietGiamTSCDList where khauHao is greater than DEFAULT_KHAU_HAO
        defaultChiTietGiamTSCDShouldNotBeFound("khauHao.greaterThan=" + DEFAULT_KHAU_HAO);

        // Get all the chiTietGiamTSCDList where khauHao is greater than SMALLER_KHAU_HAO
        defaultChiTietGiamTSCDShouldBeFound("khauHao.greaterThan=" + SMALLER_KHAU_HAO);
    }

    @Test
    @Transactional
    void getAllChiTietGiamTSCDSBySoTienThanhLyIsEqualToSomething() throws Exception {
        // Initialize the database
        chiTietGiamTSCDRepository.saveAndFlush(chiTietGiamTSCD);

        // Get all the chiTietGiamTSCDList where soTienThanhLy equals to DEFAULT_SO_TIEN_THANH_LY
        defaultChiTietGiamTSCDShouldBeFound("soTienThanhLy.equals=" + DEFAULT_SO_TIEN_THANH_LY);

        // Get all the chiTietGiamTSCDList where soTienThanhLy equals to UPDATED_SO_TIEN_THANH_LY
        defaultChiTietGiamTSCDShouldNotBeFound("soTienThanhLy.equals=" + UPDATED_SO_TIEN_THANH_LY);
    }

    @Test
    @Transactional
    void getAllChiTietGiamTSCDSBySoTienThanhLyIsNotEqualToSomething() throws Exception {
        // Initialize the database
        chiTietGiamTSCDRepository.saveAndFlush(chiTietGiamTSCD);

        // Get all the chiTietGiamTSCDList where soTienThanhLy not equals to DEFAULT_SO_TIEN_THANH_LY
        defaultChiTietGiamTSCDShouldNotBeFound("soTienThanhLy.notEquals=" + DEFAULT_SO_TIEN_THANH_LY);

        // Get all the chiTietGiamTSCDList where soTienThanhLy not equals to UPDATED_SO_TIEN_THANH_LY
        defaultChiTietGiamTSCDShouldBeFound("soTienThanhLy.notEquals=" + UPDATED_SO_TIEN_THANH_LY);
    }

    @Test
    @Transactional
    void getAllChiTietGiamTSCDSBySoTienThanhLyIsInShouldWork() throws Exception {
        // Initialize the database
        chiTietGiamTSCDRepository.saveAndFlush(chiTietGiamTSCD);

        // Get all the chiTietGiamTSCDList where soTienThanhLy in DEFAULT_SO_TIEN_THANH_LY or UPDATED_SO_TIEN_THANH_LY
        defaultChiTietGiamTSCDShouldBeFound("soTienThanhLy.in=" + DEFAULT_SO_TIEN_THANH_LY + "," + UPDATED_SO_TIEN_THANH_LY);

        // Get all the chiTietGiamTSCDList where soTienThanhLy equals to UPDATED_SO_TIEN_THANH_LY
        defaultChiTietGiamTSCDShouldNotBeFound("soTienThanhLy.in=" + UPDATED_SO_TIEN_THANH_LY);
    }

    @Test
    @Transactional
    void getAllChiTietGiamTSCDSBySoTienThanhLyIsNullOrNotNull() throws Exception {
        // Initialize the database
        chiTietGiamTSCDRepository.saveAndFlush(chiTietGiamTSCD);

        // Get all the chiTietGiamTSCDList where soTienThanhLy is not null
        defaultChiTietGiamTSCDShouldBeFound("soTienThanhLy.specified=true");

        // Get all the chiTietGiamTSCDList where soTienThanhLy is null
        defaultChiTietGiamTSCDShouldNotBeFound("soTienThanhLy.specified=false");
    }

    @Test
    @Transactional
    void getAllChiTietGiamTSCDSBySoTienThanhLyIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        chiTietGiamTSCDRepository.saveAndFlush(chiTietGiamTSCD);

        // Get all the chiTietGiamTSCDList where soTienThanhLy is greater than or equal to DEFAULT_SO_TIEN_THANH_LY
        defaultChiTietGiamTSCDShouldBeFound("soTienThanhLy.greaterThanOrEqual=" + DEFAULT_SO_TIEN_THANH_LY);

        // Get all the chiTietGiamTSCDList where soTienThanhLy is greater than or equal to UPDATED_SO_TIEN_THANH_LY
        defaultChiTietGiamTSCDShouldNotBeFound("soTienThanhLy.greaterThanOrEqual=" + UPDATED_SO_TIEN_THANH_LY);
    }

    @Test
    @Transactional
    void getAllChiTietGiamTSCDSBySoTienThanhLyIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        chiTietGiamTSCDRepository.saveAndFlush(chiTietGiamTSCD);

        // Get all the chiTietGiamTSCDList where soTienThanhLy is less than or equal to DEFAULT_SO_TIEN_THANH_LY
        defaultChiTietGiamTSCDShouldBeFound("soTienThanhLy.lessThanOrEqual=" + DEFAULT_SO_TIEN_THANH_LY);

        // Get all the chiTietGiamTSCDList where soTienThanhLy is less than or equal to SMALLER_SO_TIEN_THANH_LY
        defaultChiTietGiamTSCDShouldNotBeFound("soTienThanhLy.lessThanOrEqual=" + SMALLER_SO_TIEN_THANH_LY);
    }

    @Test
    @Transactional
    void getAllChiTietGiamTSCDSBySoTienThanhLyIsLessThanSomething() throws Exception {
        // Initialize the database
        chiTietGiamTSCDRepository.saveAndFlush(chiTietGiamTSCD);

        // Get all the chiTietGiamTSCDList where soTienThanhLy is less than DEFAULT_SO_TIEN_THANH_LY
        defaultChiTietGiamTSCDShouldNotBeFound("soTienThanhLy.lessThan=" + DEFAULT_SO_TIEN_THANH_LY);

        // Get all the chiTietGiamTSCDList where soTienThanhLy is less than UPDATED_SO_TIEN_THANH_LY
        defaultChiTietGiamTSCDShouldBeFound("soTienThanhLy.lessThan=" + UPDATED_SO_TIEN_THANH_LY);
    }

    @Test
    @Transactional
    void getAllChiTietGiamTSCDSBySoTienThanhLyIsGreaterThanSomething() throws Exception {
        // Initialize the database
        chiTietGiamTSCDRepository.saveAndFlush(chiTietGiamTSCD);

        // Get all the chiTietGiamTSCDList where soTienThanhLy is greater than DEFAULT_SO_TIEN_THANH_LY
        defaultChiTietGiamTSCDShouldNotBeFound("soTienThanhLy.greaterThan=" + DEFAULT_SO_TIEN_THANH_LY);

        // Get all the chiTietGiamTSCDList where soTienThanhLy is greater than SMALLER_SO_TIEN_THANH_LY
        defaultChiTietGiamTSCDShouldBeFound("soTienThanhLy.greaterThan=" + SMALLER_SO_TIEN_THANH_LY);
    }

    @Test
    @Transactional
    void getAllChiTietGiamTSCDSByChiPhiMuaIsEqualToSomething() throws Exception {
        // Initialize the database
        chiTietGiamTSCDRepository.saveAndFlush(chiTietGiamTSCD);

        // Get all the chiTietGiamTSCDList where chiPhiMua equals to DEFAULT_CHI_PHI_MUA
        defaultChiTietGiamTSCDShouldBeFound("chiPhiMua.equals=" + DEFAULT_CHI_PHI_MUA);

        // Get all the chiTietGiamTSCDList where chiPhiMua equals to UPDATED_CHI_PHI_MUA
        defaultChiTietGiamTSCDShouldNotBeFound("chiPhiMua.equals=" + UPDATED_CHI_PHI_MUA);
    }

    @Test
    @Transactional
    void getAllChiTietGiamTSCDSByChiPhiMuaIsNotEqualToSomething() throws Exception {
        // Initialize the database
        chiTietGiamTSCDRepository.saveAndFlush(chiTietGiamTSCD);

        // Get all the chiTietGiamTSCDList where chiPhiMua not equals to DEFAULT_CHI_PHI_MUA
        defaultChiTietGiamTSCDShouldNotBeFound("chiPhiMua.notEquals=" + DEFAULT_CHI_PHI_MUA);

        // Get all the chiTietGiamTSCDList where chiPhiMua not equals to UPDATED_CHI_PHI_MUA
        defaultChiTietGiamTSCDShouldBeFound("chiPhiMua.notEquals=" + UPDATED_CHI_PHI_MUA);
    }

    @Test
    @Transactional
    void getAllChiTietGiamTSCDSByChiPhiMuaIsInShouldWork() throws Exception {
        // Initialize the database
        chiTietGiamTSCDRepository.saveAndFlush(chiTietGiamTSCD);

        // Get all the chiTietGiamTSCDList where chiPhiMua in DEFAULT_CHI_PHI_MUA or UPDATED_CHI_PHI_MUA
        defaultChiTietGiamTSCDShouldBeFound("chiPhiMua.in=" + DEFAULT_CHI_PHI_MUA + "," + UPDATED_CHI_PHI_MUA);

        // Get all the chiTietGiamTSCDList where chiPhiMua equals to UPDATED_CHI_PHI_MUA
        defaultChiTietGiamTSCDShouldNotBeFound("chiPhiMua.in=" + UPDATED_CHI_PHI_MUA);
    }

    @Test
    @Transactional
    void getAllChiTietGiamTSCDSByChiPhiMuaIsNullOrNotNull() throws Exception {
        // Initialize the database
        chiTietGiamTSCDRepository.saveAndFlush(chiTietGiamTSCD);

        // Get all the chiTietGiamTSCDList where chiPhiMua is not null
        defaultChiTietGiamTSCDShouldBeFound("chiPhiMua.specified=true");

        // Get all the chiTietGiamTSCDList where chiPhiMua is null
        defaultChiTietGiamTSCDShouldNotBeFound("chiPhiMua.specified=false");
    }

    @Test
    @Transactional
    void getAllChiTietGiamTSCDSByChiPhiMuaIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        chiTietGiamTSCDRepository.saveAndFlush(chiTietGiamTSCD);

        // Get all the chiTietGiamTSCDList where chiPhiMua is greater than or equal to DEFAULT_CHI_PHI_MUA
        defaultChiTietGiamTSCDShouldBeFound("chiPhiMua.greaterThanOrEqual=" + DEFAULT_CHI_PHI_MUA);

        // Get all the chiTietGiamTSCDList where chiPhiMua is greater than or equal to UPDATED_CHI_PHI_MUA
        defaultChiTietGiamTSCDShouldNotBeFound("chiPhiMua.greaterThanOrEqual=" + UPDATED_CHI_PHI_MUA);
    }

    @Test
    @Transactional
    void getAllChiTietGiamTSCDSByChiPhiMuaIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        chiTietGiamTSCDRepository.saveAndFlush(chiTietGiamTSCD);

        // Get all the chiTietGiamTSCDList where chiPhiMua is less than or equal to DEFAULT_CHI_PHI_MUA
        defaultChiTietGiamTSCDShouldBeFound("chiPhiMua.lessThanOrEqual=" + DEFAULT_CHI_PHI_MUA);

        // Get all the chiTietGiamTSCDList where chiPhiMua is less than or equal to SMALLER_CHI_PHI_MUA
        defaultChiTietGiamTSCDShouldNotBeFound("chiPhiMua.lessThanOrEqual=" + SMALLER_CHI_PHI_MUA);
    }

    @Test
    @Transactional
    void getAllChiTietGiamTSCDSByChiPhiMuaIsLessThanSomething() throws Exception {
        // Initialize the database
        chiTietGiamTSCDRepository.saveAndFlush(chiTietGiamTSCD);

        // Get all the chiTietGiamTSCDList where chiPhiMua is less than DEFAULT_CHI_PHI_MUA
        defaultChiTietGiamTSCDShouldNotBeFound("chiPhiMua.lessThan=" + DEFAULT_CHI_PHI_MUA);

        // Get all the chiTietGiamTSCDList where chiPhiMua is less than UPDATED_CHI_PHI_MUA
        defaultChiTietGiamTSCDShouldBeFound("chiPhiMua.lessThan=" + UPDATED_CHI_PHI_MUA);
    }

    @Test
    @Transactional
    void getAllChiTietGiamTSCDSByChiPhiMuaIsGreaterThanSomething() throws Exception {
        // Initialize the database
        chiTietGiamTSCDRepository.saveAndFlush(chiTietGiamTSCD);

        // Get all the chiTietGiamTSCDList where chiPhiMua is greater than DEFAULT_CHI_PHI_MUA
        defaultChiTietGiamTSCDShouldNotBeFound("chiPhiMua.greaterThan=" + DEFAULT_CHI_PHI_MUA);

        // Get all the chiTietGiamTSCDList where chiPhiMua is greater than SMALLER_CHI_PHI_MUA
        defaultChiTietGiamTSCDShouldBeFound("chiPhiMua.greaterThan=" + SMALLER_CHI_PHI_MUA);
    }

    @Test
    @Transactional
    void getAllChiTietGiamTSCDSByTaiSanCDIsEqualToSomething() throws Exception {
        // Initialize the database
        chiTietGiamTSCDRepository.saveAndFlush(chiTietGiamTSCD);
        TaiSanCD taiSanCD = TaiSanCDResourceIT.createEntity(em);
        em.persist(taiSanCD);
        em.flush();
        chiTietGiamTSCD.setTaiSanCD(taiSanCD);
        chiTietGiamTSCDRepository.saveAndFlush(chiTietGiamTSCD);
        Long taiSanCDId = taiSanCD.getId();

        // Get all the chiTietGiamTSCDList where taiSanCD equals to taiSanCDId
        defaultChiTietGiamTSCDShouldBeFound("taiSanCDId.equals=" + taiSanCDId);

        // Get all the chiTietGiamTSCDList where taiSanCD equals to (taiSanCDId + 1)
        defaultChiTietGiamTSCDShouldNotBeFound("taiSanCDId.equals=" + (taiSanCDId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultChiTietGiamTSCDShouldBeFound(String filter) throws Exception {
        restChiTietGiamTSCDMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(chiTietGiamTSCD.getId().intValue())))
            .andExpect(jsonPath("$.[*].ngayMua").value(hasItem(DEFAULT_NGAY_MUA.toString())))
            .andExpect(jsonPath("$.[*].soLuong").value(hasItem(DEFAULT_SO_LUONG)))
            .andExpect(jsonPath("$.[*].khauHao").value(hasItem(DEFAULT_KHAU_HAO.doubleValue())))
            .andExpect(jsonPath("$.[*].soTienThanhLy").value(hasItem(DEFAULT_SO_TIEN_THANH_LY.doubleValue())))
            .andExpect(jsonPath("$.[*].chiPhiMua").value(hasItem(DEFAULT_CHI_PHI_MUA.doubleValue())));

        // Check, that the count call also returns 1
        restChiTietGiamTSCDMockMvc
            .perform(get(ENTITY_API_URL + "/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultChiTietGiamTSCDShouldNotBeFound(String filter) throws Exception {
        restChiTietGiamTSCDMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restChiTietGiamTSCDMockMvc
            .perform(get(ENTITY_API_URL + "/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }

    @Test
    @Transactional
    void getNonExistingChiTietGiamTSCD() throws Exception {
        // Get the chiTietGiamTSCD
        restChiTietGiamTSCDMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putNewChiTietGiamTSCD() throws Exception {
        // Initialize the database
        chiTietGiamTSCDRepository.saveAndFlush(chiTietGiamTSCD);

        int databaseSizeBeforeUpdate = chiTietGiamTSCDRepository.findAll().size();

        // Update the chiTietGiamTSCD
        ChiTietGiamTSCD updatedChiTietGiamTSCD = chiTietGiamTSCDRepository.findById(chiTietGiamTSCD.getId()).get();
        // Disconnect from session so that the updates on updatedChiTietGiamTSCD are not directly saved in db
        em.detach(updatedChiTietGiamTSCD);
        updatedChiTietGiamTSCD
            .ngayMua(UPDATED_NGAY_MUA)
            .soLuong(UPDATED_SO_LUONG)
            .khauHao(UPDATED_KHAU_HAO)
            .soTienThanhLy(UPDATED_SO_TIEN_THANH_LY)
            .chiPhiMua(UPDATED_CHI_PHI_MUA);
        ChiTietGiamTSCDDTO chiTietGiamTSCDDTO = chiTietGiamTSCDMapper.toDto(updatedChiTietGiamTSCD);

        restChiTietGiamTSCDMockMvc
            .perform(
                put(ENTITY_API_URL_ID, chiTietGiamTSCDDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(chiTietGiamTSCDDTO))
            )
            .andExpect(status().isOk());

        // Validate the ChiTietGiamTSCD in the database
        List<ChiTietGiamTSCD> chiTietGiamTSCDList = chiTietGiamTSCDRepository.findAll();
        assertThat(chiTietGiamTSCDList).hasSize(databaseSizeBeforeUpdate);
        ChiTietGiamTSCD testChiTietGiamTSCD = chiTietGiamTSCDList.get(chiTietGiamTSCDList.size() - 1);
        assertThat(testChiTietGiamTSCD.getNgayMua()).isEqualTo(UPDATED_NGAY_MUA);
        assertThat(testChiTietGiamTSCD.getSoLuong()).isEqualTo(UPDATED_SO_LUONG);
        assertThat(testChiTietGiamTSCD.getKhauHao()).isEqualTo(UPDATED_KHAU_HAO);
        assertThat(testChiTietGiamTSCD.getSoTienThanhLy()).isEqualTo(UPDATED_SO_TIEN_THANH_LY);
        assertThat(testChiTietGiamTSCD.getChiPhiMua()).isEqualTo(UPDATED_CHI_PHI_MUA);
    }

    @Test
    @Transactional
    void putNonExistingChiTietGiamTSCD() throws Exception {
        int databaseSizeBeforeUpdate = chiTietGiamTSCDRepository.findAll().size();
        chiTietGiamTSCD.setId(count.incrementAndGet());

        // Create the ChiTietGiamTSCD
        ChiTietGiamTSCDDTO chiTietGiamTSCDDTO = chiTietGiamTSCDMapper.toDto(chiTietGiamTSCD);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restChiTietGiamTSCDMockMvc
            .perform(
                put(ENTITY_API_URL_ID, chiTietGiamTSCDDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(chiTietGiamTSCDDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the ChiTietGiamTSCD in the database
        List<ChiTietGiamTSCD> chiTietGiamTSCDList = chiTietGiamTSCDRepository.findAll();
        assertThat(chiTietGiamTSCDList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchChiTietGiamTSCD() throws Exception {
        int databaseSizeBeforeUpdate = chiTietGiamTSCDRepository.findAll().size();
        chiTietGiamTSCD.setId(count.incrementAndGet());

        // Create the ChiTietGiamTSCD
        ChiTietGiamTSCDDTO chiTietGiamTSCDDTO = chiTietGiamTSCDMapper.toDto(chiTietGiamTSCD);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restChiTietGiamTSCDMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(chiTietGiamTSCDDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the ChiTietGiamTSCD in the database
        List<ChiTietGiamTSCD> chiTietGiamTSCDList = chiTietGiamTSCDRepository.findAll();
        assertThat(chiTietGiamTSCDList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamChiTietGiamTSCD() throws Exception {
        int databaseSizeBeforeUpdate = chiTietGiamTSCDRepository.findAll().size();
        chiTietGiamTSCD.setId(count.incrementAndGet());

        // Create the ChiTietGiamTSCD
        ChiTietGiamTSCDDTO chiTietGiamTSCDDTO = chiTietGiamTSCDMapper.toDto(chiTietGiamTSCD);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restChiTietGiamTSCDMockMvc
            .perform(
                put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(chiTietGiamTSCDDTO))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the ChiTietGiamTSCD in the database
        List<ChiTietGiamTSCD> chiTietGiamTSCDList = chiTietGiamTSCDRepository.findAll();
        assertThat(chiTietGiamTSCDList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateChiTietGiamTSCDWithPatch() throws Exception {
        // Initialize the database
        chiTietGiamTSCDRepository.saveAndFlush(chiTietGiamTSCD);

        int databaseSizeBeforeUpdate = chiTietGiamTSCDRepository.findAll().size();

        // Update the chiTietGiamTSCD using partial update
        ChiTietGiamTSCD partialUpdatedChiTietGiamTSCD = new ChiTietGiamTSCD();
        partialUpdatedChiTietGiamTSCD.setId(chiTietGiamTSCD.getId());

        partialUpdatedChiTietGiamTSCD.soLuong(UPDATED_SO_LUONG);

        restChiTietGiamTSCDMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedChiTietGiamTSCD.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedChiTietGiamTSCD))
            )
            .andExpect(status().isOk());

        // Validate the ChiTietGiamTSCD in the database
        List<ChiTietGiamTSCD> chiTietGiamTSCDList = chiTietGiamTSCDRepository.findAll();
        assertThat(chiTietGiamTSCDList).hasSize(databaseSizeBeforeUpdate);
        ChiTietGiamTSCD testChiTietGiamTSCD = chiTietGiamTSCDList.get(chiTietGiamTSCDList.size() - 1);
        assertThat(testChiTietGiamTSCD.getNgayMua()).isEqualTo(DEFAULT_NGAY_MUA);
        assertThat(testChiTietGiamTSCD.getSoLuong()).isEqualTo(UPDATED_SO_LUONG);
        assertThat(testChiTietGiamTSCD.getKhauHao()).isEqualTo(DEFAULT_KHAU_HAO);
        assertThat(testChiTietGiamTSCD.getSoTienThanhLy()).isEqualTo(DEFAULT_SO_TIEN_THANH_LY);
        assertThat(testChiTietGiamTSCD.getChiPhiMua()).isEqualTo(DEFAULT_CHI_PHI_MUA);
    }

    @Test
    @Transactional
    void fullUpdateChiTietGiamTSCDWithPatch() throws Exception {
        // Initialize the database
        chiTietGiamTSCDRepository.saveAndFlush(chiTietGiamTSCD);

        int databaseSizeBeforeUpdate = chiTietGiamTSCDRepository.findAll().size();

        // Update the chiTietGiamTSCD using partial update
        ChiTietGiamTSCD partialUpdatedChiTietGiamTSCD = new ChiTietGiamTSCD();
        partialUpdatedChiTietGiamTSCD.setId(chiTietGiamTSCD.getId());

        partialUpdatedChiTietGiamTSCD
            .ngayMua(UPDATED_NGAY_MUA)
            .soLuong(UPDATED_SO_LUONG)
            .khauHao(UPDATED_KHAU_HAO)
            .soTienThanhLy(UPDATED_SO_TIEN_THANH_LY)
            .chiPhiMua(UPDATED_CHI_PHI_MUA);

        restChiTietGiamTSCDMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedChiTietGiamTSCD.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedChiTietGiamTSCD))
            )
            .andExpect(status().isOk());

        // Validate the ChiTietGiamTSCD in the database
        List<ChiTietGiamTSCD> chiTietGiamTSCDList = chiTietGiamTSCDRepository.findAll();
        assertThat(chiTietGiamTSCDList).hasSize(databaseSizeBeforeUpdate);
        ChiTietGiamTSCD testChiTietGiamTSCD = chiTietGiamTSCDList.get(chiTietGiamTSCDList.size() - 1);
        assertThat(testChiTietGiamTSCD.getNgayMua()).isEqualTo(UPDATED_NGAY_MUA);
        assertThat(testChiTietGiamTSCD.getSoLuong()).isEqualTo(UPDATED_SO_LUONG);
        assertThat(testChiTietGiamTSCD.getKhauHao()).isEqualTo(UPDATED_KHAU_HAO);
        assertThat(testChiTietGiamTSCD.getSoTienThanhLy()).isEqualTo(UPDATED_SO_TIEN_THANH_LY);
        assertThat(testChiTietGiamTSCD.getChiPhiMua()).isEqualTo(UPDATED_CHI_PHI_MUA);
    }

    @Test
    @Transactional
    void patchNonExistingChiTietGiamTSCD() throws Exception {
        int databaseSizeBeforeUpdate = chiTietGiamTSCDRepository.findAll().size();
        chiTietGiamTSCD.setId(count.incrementAndGet());

        // Create the ChiTietGiamTSCD
        ChiTietGiamTSCDDTO chiTietGiamTSCDDTO = chiTietGiamTSCDMapper.toDto(chiTietGiamTSCD);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restChiTietGiamTSCDMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, chiTietGiamTSCDDTO.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(chiTietGiamTSCDDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the ChiTietGiamTSCD in the database
        List<ChiTietGiamTSCD> chiTietGiamTSCDList = chiTietGiamTSCDRepository.findAll();
        assertThat(chiTietGiamTSCDList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchChiTietGiamTSCD() throws Exception {
        int databaseSizeBeforeUpdate = chiTietGiamTSCDRepository.findAll().size();
        chiTietGiamTSCD.setId(count.incrementAndGet());

        // Create the ChiTietGiamTSCD
        ChiTietGiamTSCDDTO chiTietGiamTSCDDTO = chiTietGiamTSCDMapper.toDto(chiTietGiamTSCD);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restChiTietGiamTSCDMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(chiTietGiamTSCDDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the ChiTietGiamTSCD in the database
        List<ChiTietGiamTSCD> chiTietGiamTSCDList = chiTietGiamTSCDRepository.findAll();
        assertThat(chiTietGiamTSCDList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamChiTietGiamTSCD() throws Exception {
        int databaseSizeBeforeUpdate = chiTietGiamTSCDRepository.findAll().size();
        chiTietGiamTSCD.setId(count.incrementAndGet());

        // Create the ChiTietGiamTSCD
        ChiTietGiamTSCDDTO chiTietGiamTSCDDTO = chiTietGiamTSCDMapper.toDto(chiTietGiamTSCD);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restChiTietGiamTSCDMockMvc
            .perform(
                patch(ENTITY_API_URL)
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(chiTietGiamTSCDDTO))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the ChiTietGiamTSCD in the database
        List<ChiTietGiamTSCD> chiTietGiamTSCDList = chiTietGiamTSCDRepository.findAll();
        assertThat(chiTietGiamTSCDList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteChiTietGiamTSCD() throws Exception {
        // Initialize the database
        chiTietGiamTSCDRepository.saveAndFlush(chiTietGiamTSCD);

        int databaseSizeBeforeDelete = chiTietGiamTSCDRepository.findAll().size();

        // Delete the chiTietGiamTSCD
        restChiTietGiamTSCDMockMvc
            .perform(delete(ENTITY_API_URL_ID, chiTietGiamTSCD.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<ChiTietGiamTSCD> chiTietGiamTSCDList = chiTietGiamTSCDRepository.findAll();
        assertThat(chiTietGiamTSCDList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
