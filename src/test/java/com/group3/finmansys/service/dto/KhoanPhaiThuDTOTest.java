package com.group3.finmansys.service.dto;

import static org.assertj.core.api.Assertions.assertThat;

import com.group3.finmansys.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class KhoanPhaiThuDTOTest {

    @Test
    void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(KhoanPhaiThuDTO.class);
        KhoanPhaiThuDTO khoanPhaiThuDTO1 = new KhoanPhaiThuDTO();
        khoanPhaiThuDTO1.setId(1L);
        KhoanPhaiThuDTO khoanPhaiThuDTO2 = new KhoanPhaiThuDTO();
        assertThat(khoanPhaiThuDTO1).isNotEqualTo(khoanPhaiThuDTO2);
        khoanPhaiThuDTO2.setId(khoanPhaiThuDTO1.getId());
        assertThat(khoanPhaiThuDTO1).isEqualTo(khoanPhaiThuDTO2);
        khoanPhaiThuDTO2.setId(2L);
        assertThat(khoanPhaiThuDTO1).isNotEqualTo(khoanPhaiThuDTO2);
        khoanPhaiThuDTO1.setId(null);
        assertThat(khoanPhaiThuDTO1).isNotEqualTo(khoanPhaiThuDTO2);
    }
}
