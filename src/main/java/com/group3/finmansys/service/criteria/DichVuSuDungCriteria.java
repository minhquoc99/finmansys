package com.group3.finmansys.service.criteria;

import java.io.Serializable;
import java.util.Objects;
import tech.jhipster.service.Criteria;
import tech.jhipster.service.filter.BooleanFilter;
import tech.jhipster.service.filter.DoubleFilter;
import tech.jhipster.service.filter.Filter;
import tech.jhipster.service.filter.FloatFilter;
import tech.jhipster.service.filter.InstantFilter;
import tech.jhipster.service.filter.IntegerFilter;
import tech.jhipster.service.filter.LongFilter;
import tech.jhipster.service.filter.StringFilter;

/**
 * Criteria class for the {@link com.group3.finmansys.domain.DichVuSuDung} entity. This class is used
 * in {@link com.group3.finmansys.web.rest.DichVuSuDungResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /dich-vu-su-dungs?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class DichVuSuDungCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private IntegerFilter soLuong;

    private DoubleFilter gia;

    private InstantFilter ngaySuDung;

    private LongFilter dichVuId;

    private LongFilter phongDuocDatId;

    public DichVuSuDungCriteria() {}

    public DichVuSuDungCriteria(DichVuSuDungCriteria other) {
        this.id = other.id == null ? null : other.id.copy();
        this.soLuong = other.soLuong == null ? null : other.soLuong.copy();
        this.gia = other.gia == null ? null : other.gia.copy();
        this.ngaySuDung = other.ngaySuDung == null ? null : other.ngaySuDung.copy();
        this.dichVuId = other.dichVuId == null ? null : other.dichVuId.copy();
        this.phongDuocDatId = other.phongDuocDatId == null ? null : other.phongDuocDatId.copy();
    }

    @Override
    public DichVuSuDungCriteria copy() {
        return new DichVuSuDungCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public LongFilter id() {
        if (id == null) {
            id = new LongFilter();
        }
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public IntegerFilter getSoLuong() {
        return soLuong;
    }

    public IntegerFilter soLuong() {
        if (soLuong == null) {
            soLuong = new IntegerFilter();
        }
        return soLuong;
    }

    public void setSoLuong(IntegerFilter soLuong) {
        this.soLuong = soLuong;
    }

    public DoubleFilter getGia() {
        return gia;
    }

    public DoubleFilter gia() {
        if (gia == null) {
            gia = new DoubleFilter();
        }
        return gia;
    }

    public void setGia(DoubleFilter gia) {
        this.gia = gia;
    }

    public InstantFilter getNgaySuDung() {
        return ngaySuDung;
    }

    public InstantFilter ngaySuDung() {
        if (ngaySuDung == null) {
            ngaySuDung = new InstantFilter();
        }
        return ngaySuDung;
    }

    public void setNgaySuDung(InstantFilter ngaySuDung) {
        this.ngaySuDung = ngaySuDung;
    }

    public LongFilter getDichVuId() {
        return dichVuId;
    }

    public LongFilter dichVuId() {
        if (dichVuId == null) {
            dichVuId = new LongFilter();
        }
        return dichVuId;
    }

    public void setDichVuId(LongFilter dichVuId) {
        this.dichVuId = dichVuId;
    }

    public LongFilter getPhongDuocDatId() {
        return phongDuocDatId;
    }

    public LongFilter phongDuocDatId() {
        if (phongDuocDatId == null) {
            phongDuocDatId = new LongFilter();
        }
        return phongDuocDatId;
    }

    public void setPhongDuocDatId(LongFilter phongDuocDatId) {
        this.phongDuocDatId = phongDuocDatId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final DichVuSuDungCriteria that = (DichVuSuDungCriteria) o;
        return (
            Objects.equals(id, that.id) &&
            Objects.equals(soLuong, that.soLuong) &&
            Objects.equals(gia, that.gia) &&
            Objects.equals(ngaySuDung, that.ngaySuDung) &&
            Objects.equals(dichVuId, that.dichVuId) &&
            Objects.equals(phongDuocDatId, that.phongDuocDatId)
        );
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, soLuong, gia, ngaySuDung, dichVuId, phongDuocDatId);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "DichVuSuDungCriteria{" +
            (id != null ? "id=" + id + ", " : "") +
            (soLuong != null ? "soLuong=" + soLuong + ", " : "") +
            (gia != null ? "gia=" + gia + ", " : "") +
            (ngaySuDung != null ? "ngaySuDung=" + ngaySuDung + ", " : "") +
            (dichVuId != null ? "dichVuId=" + dichVuId + ", " : "") +
            (phongDuocDatId != null ? "phongDuocDatId=" + phongDuocDatId + ", " : "") +
            "}";
    }
}
