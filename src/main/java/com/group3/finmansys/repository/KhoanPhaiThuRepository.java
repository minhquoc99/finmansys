package com.group3.finmansys.repository;

import com.group3.finmansys.domain.KhoanPhaiThu;
import java.time.Instant;
import java.util.List;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data SQL repository for the KhoanPhaiThu entity.
 */
@SuppressWarnings("unused")
@Repository
public interface KhoanPhaiThuRepository extends JpaRepository<KhoanPhaiThu, Long>, JpaSpecificationExecutor<KhoanPhaiThu> {
    @Query("SELECT kpt FROM KhoanPhaiThu kpt WHERE kpt.ngayTao < ?1 AND kpt.maKeToan.soHieu = ?2")
    List<KhoanPhaiThu> getBangCanDoiKeToan(Instant start, Double soHieu);
}
