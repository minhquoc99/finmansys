package com.group3.finmansys.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.group3.finmansys.IntegrationTest;
import com.group3.finmansys.domain.LoaiTaiSan;
import com.group3.finmansys.domain.MaKeToan;
import com.group3.finmansys.repository.LoaiTaiSanRepository;
import com.group3.finmansys.service.criteria.LoaiTaiSanCriteria;
import com.group3.finmansys.service.dto.LoaiTaiSanDTO;
import com.group3.finmansys.service.mapper.LoaiTaiSanMapper;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link LoaiTaiSanResource} REST controller.
 */
@IntegrationTest
@ExtendWith(MockitoExtension.class)
@AutoConfigureMockMvc
@WithMockUser
class LoaiTaiSanResourceIT {

    private static final String DEFAULT_TEN = "AAAAAAAAAA";
    private static final String UPDATED_TEN = "BBBBBBBBBB";

    private static final String DEFAULT_MO_TA = "AAAAAAAAAA";
    private static final String UPDATED_MO_TA = "BBBBBBBBBB";

    private static final String ENTITY_API_URL = "/api/loai-tai-sans";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private LoaiTaiSanRepository loaiTaiSanRepository;

    @Autowired
    private LoaiTaiSanMapper loaiTaiSanMapper;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restLoaiTaiSanMockMvc;

    private LoaiTaiSan loaiTaiSan;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static LoaiTaiSan createEntity(EntityManager em) {
        LoaiTaiSan loaiTaiSan = new LoaiTaiSan().ten(DEFAULT_TEN).moTa(DEFAULT_MO_TA);
        // Add required entity
        MaKeToan maKeToan;
        if (TestUtil.findAll(em, MaKeToan.class).isEmpty()) {
            maKeToan = MaKeToanResourceIT.createEntity(em);
            em.persist(maKeToan);
            em.flush();
        } else {
            maKeToan = TestUtil.findAll(em, MaKeToan.class).get(0);
        }
        loaiTaiSan.setMaKeToan(maKeToan);
        return loaiTaiSan;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static LoaiTaiSan createUpdatedEntity(EntityManager em) {
        LoaiTaiSan loaiTaiSan = new LoaiTaiSan().ten(UPDATED_TEN).moTa(UPDATED_MO_TA);
        // Add required entity
        MaKeToan maKeToan;
        if (TestUtil.findAll(em, MaKeToan.class).isEmpty()) {
            maKeToan = MaKeToanResourceIT.createUpdatedEntity(em);
            em.persist(maKeToan);
            em.flush();
        } else {
            maKeToan = TestUtil.findAll(em, MaKeToan.class).get(0);
        }
        loaiTaiSan.setMaKeToan(maKeToan);
        return loaiTaiSan;
    }

    @BeforeEach
    public void initTest() {
        loaiTaiSan = createEntity(em);
    }

    @Test
    @Transactional
    void createLoaiTaiSan() throws Exception {
        int databaseSizeBeforeCreate = loaiTaiSanRepository.findAll().size();
        // Create the LoaiTaiSan
        LoaiTaiSanDTO loaiTaiSanDTO = loaiTaiSanMapper.toDto(loaiTaiSan);
        restLoaiTaiSanMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(loaiTaiSanDTO)))
            .andExpect(status().isCreated());

        // Validate the LoaiTaiSan in the database
        List<LoaiTaiSan> loaiTaiSanList = loaiTaiSanRepository.findAll();
        assertThat(loaiTaiSanList).hasSize(databaseSizeBeforeCreate + 1);
        LoaiTaiSan testLoaiTaiSan = loaiTaiSanList.get(loaiTaiSanList.size() - 1);
        assertThat(testLoaiTaiSan.getTen()).isEqualTo(DEFAULT_TEN);
        assertThat(testLoaiTaiSan.getMoTa()).isEqualTo(DEFAULT_MO_TA);
    }

    @Test
    @Transactional
    void createLoaiTaiSanWithExistingId() throws Exception {
        // Create the LoaiTaiSan with an existing ID
        loaiTaiSan.setId(1L);
        LoaiTaiSanDTO loaiTaiSanDTO = loaiTaiSanMapper.toDto(loaiTaiSan);

        int databaseSizeBeforeCreate = loaiTaiSanRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restLoaiTaiSanMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(loaiTaiSanDTO)))
            .andExpect(status().isBadRequest());

        // Validate the LoaiTaiSan in the database
        List<LoaiTaiSan> loaiTaiSanList = loaiTaiSanRepository.findAll();
        assertThat(loaiTaiSanList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void getAllLoaiTaiSans() throws Exception {
        // Initialize the database
        loaiTaiSanRepository.saveAndFlush(loaiTaiSan);

        // Get all the loaiTaiSanList
        restLoaiTaiSanMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(loaiTaiSan.getId().intValue())))
            .andExpect(jsonPath("$.[*].ten").value(hasItem(DEFAULT_TEN)))
            .andExpect(jsonPath("$.[*].moTa").value(hasItem(DEFAULT_MO_TA)));
    }

    @Test
    @Transactional
    void getLoaiTaiSan() throws Exception {
        // Initialize the database
        loaiTaiSanRepository.saveAndFlush(loaiTaiSan);

        // Get the loaiTaiSan
        restLoaiTaiSanMockMvc
            .perform(get(ENTITY_API_URL_ID, loaiTaiSan.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(loaiTaiSan.getId().intValue()))
            .andExpect(jsonPath("$.ten").value(DEFAULT_TEN))
            .andExpect(jsonPath("$.moTa").value(DEFAULT_MO_TA));
    }

    @Test
    @Transactional
    void getLoaiTaiSansByIdFiltering() throws Exception {
        // Initialize the database
        loaiTaiSanRepository.saveAndFlush(loaiTaiSan);

        Long id = loaiTaiSan.getId();

        defaultLoaiTaiSanShouldBeFound("id.equals=" + id);
        defaultLoaiTaiSanShouldNotBeFound("id.notEquals=" + id);

        defaultLoaiTaiSanShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultLoaiTaiSanShouldNotBeFound("id.greaterThan=" + id);

        defaultLoaiTaiSanShouldBeFound("id.lessThanOrEqual=" + id);
        defaultLoaiTaiSanShouldNotBeFound("id.lessThan=" + id);
    }

    @Test
    @Transactional
    void getAllLoaiTaiSansByTenIsEqualToSomething() throws Exception {
        // Initialize the database
        loaiTaiSanRepository.saveAndFlush(loaiTaiSan);

        // Get all the loaiTaiSanList where ten equals to DEFAULT_TEN
        defaultLoaiTaiSanShouldBeFound("ten.equals=" + DEFAULT_TEN);

        // Get all the loaiTaiSanList where ten equals to UPDATED_TEN
        defaultLoaiTaiSanShouldNotBeFound("ten.equals=" + UPDATED_TEN);
    }

    @Test
    @Transactional
    void getAllLoaiTaiSansByTenIsNotEqualToSomething() throws Exception {
        // Initialize the database
        loaiTaiSanRepository.saveAndFlush(loaiTaiSan);

        // Get all the loaiTaiSanList where ten not equals to DEFAULT_TEN
        defaultLoaiTaiSanShouldNotBeFound("ten.notEquals=" + DEFAULT_TEN);

        // Get all the loaiTaiSanList where ten not equals to UPDATED_TEN
        defaultLoaiTaiSanShouldBeFound("ten.notEquals=" + UPDATED_TEN);
    }

    @Test
    @Transactional
    void getAllLoaiTaiSansByTenIsInShouldWork() throws Exception {
        // Initialize the database
        loaiTaiSanRepository.saveAndFlush(loaiTaiSan);

        // Get all the loaiTaiSanList where ten in DEFAULT_TEN or UPDATED_TEN
        defaultLoaiTaiSanShouldBeFound("ten.in=" + DEFAULT_TEN + "," + UPDATED_TEN);

        // Get all the loaiTaiSanList where ten equals to UPDATED_TEN
        defaultLoaiTaiSanShouldNotBeFound("ten.in=" + UPDATED_TEN);
    }

    @Test
    @Transactional
    void getAllLoaiTaiSansByTenIsNullOrNotNull() throws Exception {
        // Initialize the database
        loaiTaiSanRepository.saveAndFlush(loaiTaiSan);

        // Get all the loaiTaiSanList where ten is not null
        defaultLoaiTaiSanShouldBeFound("ten.specified=true");

        // Get all the loaiTaiSanList where ten is null
        defaultLoaiTaiSanShouldNotBeFound("ten.specified=false");
    }

    @Test
    @Transactional
    void getAllLoaiTaiSansByTenContainsSomething() throws Exception {
        // Initialize the database
        loaiTaiSanRepository.saveAndFlush(loaiTaiSan);

        // Get all the loaiTaiSanList where ten contains DEFAULT_TEN
        defaultLoaiTaiSanShouldBeFound("ten.contains=" + DEFAULT_TEN);

        // Get all the loaiTaiSanList where ten contains UPDATED_TEN
        defaultLoaiTaiSanShouldNotBeFound("ten.contains=" + UPDATED_TEN);
    }

    @Test
    @Transactional
    void getAllLoaiTaiSansByTenNotContainsSomething() throws Exception {
        // Initialize the database
        loaiTaiSanRepository.saveAndFlush(loaiTaiSan);

        // Get all the loaiTaiSanList where ten does not contain DEFAULT_TEN
        defaultLoaiTaiSanShouldNotBeFound("ten.doesNotContain=" + DEFAULT_TEN);

        // Get all the loaiTaiSanList where ten does not contain UPDATED_TEN
        defaultLoaiTaiSanShouldBeFound("ten.doesNotContain=" + UPDATED_TEN);
    }

    @Test
    @Transactional
    void getAllLoaiTaiSansByMoTaIsEqualToSomething() throws Exception {
        // Initialize the database
        loaiTaiSanRepository.saveAndFlush(loaiTaiSan);

        // Get all the loaiTaiSanList where moTa equals to DEFAULT_MO_TA
        defaultLoaiTaiSanShouldBeFound("moTa.equals=" + DEFAULT_MO_TA);

        // Get all the loaiTaiSanList where moTa equals to UPDATED_MO_TA
        defaultLoaiTaiSanShouldNotBeFound("moTa.equals=" + UPDATED_MO_TA);
    }

    @Test
    @Transactional
    void getAllLoaiTaiSansByMoTaIsNotEqualToSomething() throws Exception {
        // Initialize the database
        loaiTaiSanRepository.saveAndFlush(loaiTaiSan);

        // Get all the loaiTaiSanList where moTa not equals to DEFAULT_MO_TA
        defaultLoaiTaiSanShouldNotBeFound("moTa.notEquals=" + DEFAULT_MO_TA);

        // Get all the loaiTaiSanList where moTa not equals to UPDATED_MO_TA
        defaultLoaiTaiSanShouldBeFound("moTa.notEquals=" + UPDATED_MO_TA);
    }

    @Test
    @Transactional
    void getAllLoaiTaiSansByMoTaIsInShouldWork() throws Exception {
        // Initialize the database
        loaiTaiSanRepository.saveAndFlush(loaiTaiSan);

        // Get all the loaiTaiSanList where moTa in DEFAULT_MO_TA or UPDATED_MO_TA
        defaultLoaiTaiSanShouldBeFound("moTa.in=" + DEFAULT_MO_TA + "," + UPDATED_MO_TA);

        // Get all the loaiTaiSanList where moTa equals to UPDATED_MO_TA
        defaultLoaiTaiSanShouldNotBeFound("moTa.in=" + UPDATED_MO_TA);
    }

    @Test
    @Transactional
    void getAllLoaiTaiSansByMoTaIsNullOrNotNull() throws Exception {
        // Initialize the database
        loaiTaiSanRepository.saveAndFlush(loaiTaiSan);

        // Get all the loaiTaiSanList where moTa is not null
        defaultLoaiTaiSanShouldBeFound("moTa.specified=true");

        // Get all the loaiTaiSanList where moTa is null
        defaultLoaiTaiSanShouldNotBeFound("moTa.specified=false");
    }

    @Test
    @Transactional
    void getAllLoaiTaiSansByMoTaContainsSomething() throws Exception {
        // Initialize the database
        loaiTaiSanRepository.saveAndFlush(loaiTaiSan);

        // Get all the loaiTaiSanList where moTa contains DEFAULT_MO_TA
        defaultLoaiTaiSanShouldBeFound("moTa.contains=" + DEFAULT_MO_TA);

        // Get all the loaiTaiSanList where moTa contains UPDATED_MO_TA
        defaultLoaiTaiSanShouldNotBeFound("moTa.contains=" + UPDATED_MO_TA);
    }

    @Test
    @Transactional
    void getAllLoaiTaiSansByMoTaNotContainsSomething() throws Exception {
        // Initialize the database
        loaiTaiSanRepository.saveAndFlush(loaiTaiSan);

        // Get all the loaiTaiSanList where moTa does not contain DEFAULT_MO_TA
        defaultLoaiTaiSanShouldNotBeFound("moTa.doesNotContain=" + DEFAULT_MO_TA);

        // Get all the loaiTaiSanList where moTa does not contain UPDATED_MO_TA
        defaultLoaiTaiSanShouldBeFound("moTa.doesNotContain=" + UPDATED_MO_TA);
    }

    @Test
    @Transactional
    void getAllLoaiTaiSansByMaKeToanIsEqualToSomething() throws Exception {
        // Initialize the database
        loaiTaiSanRepository.saveAndFlush(loaiTaiSan);
        MaKeToan maKeToan = MaKeToanResourceIT.createEntity(em);
        em.persist(maKeToan);
        em.flush();
        loaiTaiSan.setMaKeToan(maKeToan);
        loaiTaiSanRepository.saveAndFlush(loaiTaiSan);
        Long maKeToanId = maKeToan.getId();

        // Get all the loaiTaiSanList where maKeToan equals to maKeToanId
        defaultLoaiTaiSanShouldBeFound("maKeToanId.equals=" + maKeToanId);

        // Get all the loaiTaiSanList where maKeToan equals to (maKeToanId + 1)
        defaultLoaiTaiSanShouldNotBeFound("maKeToanId.equals=" + (maKeToanId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultLoaiTaiSanShouldBeFound(String filter) throws Exception {
        restLoaiTaiSanMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(loaiTaiSan.getId().intValue())))
            .andExpect(jsonPath("$.[*].ten").value(hasItem(DEFAULT_TEN)))
            .andExpect(jsonPath("$.[*].moTa").value(hasItem(DEFAULT_MO_TA)));

        // Check, that the count call also returns 1
        restLoaiTaiSanMockMvc
            .perform(get(ENTITY_API_URL + "/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultLoaiTaiSanShouldNotBeFound(String filter) throws Exception {
        restLoaiTaiSanMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restLoaiTaiSanMockMvc
            .perform(get(ENTITY_API_URL + "/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }

    @Test
    @Transactional
    void getNonExistingLoaiTaiSan() throws Exception {
        // Get the loaiTaiSan
        restLoaiTaiSanMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putNewLoaiTaiSan() throws Exception {
        // Initialize the database
        loaiTaiSanRepository.saveAndFlush(loaiTaiSan);

        int databaseSizeBeforeUpdate = loaiTaiSanRepository.findAll().size();

        // Update the loaiTaiSan
        LoaiTaiSan updatedLoaiTaiSan = loaiTaiSanRepository.findById(loaiTaiSan.getId()).get();
        // Disconnect from session so that the updates on updatedLoaiTaiSan are not directly saved in db
        em.detach(updatedLoaiTaiSan);
        updatedLoaiTaiSan.ten(UPDATED_TEN).moTa(UPDATED_MO_TA);
        LoaiTaiSanDTO loaiTaiSanDTO = loaiTaiSanMapper.toDto(updatedLoaiTaiSan);

        restLoaiTaiSanMockMvc
            .perform(
                put(ENTITY_API_URL_ID, loaiTaiSanDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(loaiTaiSanDTO))
            )
            .andExpect(status().isOk());

        // Validate the LoaiTaiSan in the database
        List<LoaiTaiSan> loaiTaiSanList = loaiTaiSanRepository.findAll();
        assertThat(loaiTaiSanList).hasSize(databaseSizeBeforeUpdate);
        LoaiTaiSan testLoaiTaiSan = loaiTaiSanList.get(loaiTaiSanList.size() - 1);
        assertThat(testLoaiTaiSan.getTen()).isEqualTo(UPDATED_TEN);
        assertThat(testLoaiTaiSan.getMoTa()).isEqualTo(UPDATED_MO_TA);
    }

    @Test
    @Transactional
    void putNonExistingLoaiTaiSan() throws Exception {
        int databaseSizeBeforeUpdate = loaiTaiSanRepository.findAll().size();
        loaiTaiSan.setId(count.incrementAndGet());

        // Create the LoaiTaiSan
        LoaiTaiSanDTO loaiTaiSanDTO = loaiTaiSanMapper.toDto(loaiTaiSan);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restLoaiTaiSanMockMvc
            .perform(
                put(ENTITY_API_URL_ID, loaiTaiSanDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(loaiTaiSanDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the LoaiTaiSan in the database
        List<LoaiTaiSan> loaiTaiSanList = loaiTaiSanRepository.findAll();
        assertThat(loaiTaiSanList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchLoaiTaiSan() throws Exception {
        int databaseSizeBeforeUpdate = loaiTaiSanRepository.findAll().size();
        loaiTaiSan.setId(count.incrementAndGet());

        // Create the LoaiTaiSan
        LoaiTaiSanDTO loaiTaiSanDTO = loaiTaiSanMapper.toDto(loaiTaiSan);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restLoaiTaiSanMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(loaiTaiSanDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the LoaiTaiSan in the database
        List<LoaiTaiSan> loaiTaiSanList = loaiTaiSanRepository.findAll();
        assertThat(loaiTaiSanList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamLoaiTaiSan() throws Exception {
        int databaseSizeBeforeUpdate = loaiTaiSanRepository.findAll().size();
        loaiTaiSan.setId(count.incrementAndGet());

        // Create the LoaiTaiSan
        LoaiTaiSanDTO loaiTaiSanDTO = loaiTaiSanMapper.toDto(loaiTaiSan);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restLoaiTaiSanMockMvc
            .perform(put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(loaiTaiSanDTO)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the LoaiTaiSan in the database
        List<LoaiTaiSan> loaiTaiSanList = loaiTaiSanRepository.findAll();
        assertThat(loaiTaiSanList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateLoaiTaiSanWithPatch() throws Exception {
        // Initialize the database
        loaiTaiSanRepository.saveAndFlush(loaiTaiSan);

        int databaseSizeBeforeUpdate = loaiTaiSanRepository.findAll().size();

        // Update the loaiTaiSan using partial update
        LoaiTaiSan partialUpdatedLoaiTaiSan = new LoaiTaiSan();
        partialUpdatedLoaiTaiSan.setId(loaiTaiSan.getId());

        partialUpdatedLoaiTaiSan.moTa(UPDATED_MO_TA);

        restLoaiTaiSanMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedLoaiTaiSan.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedLoaiTaiSan))
            )
            .andExpect(status().isOk());

        // Validate the LoaiTaiSan in the database
        List<LoaiTaiSan> loaiTaiSanList = loaiTaiSanRepository.findAll();
        assertThat(loaiTaiSanList).hasSize(databaseSizeBeforeUpdate);
        LoaiTaiSan testLoaiTaiSan = loaiTaiSanList.get(loaiTaiSanList.size() - 1);
        assertThat(testLoaiTaiSan.getTen()).isEqualTo(DEFAULT_TEN);
        assertThat(testLoaiTaiSan.getMoTa()).isEqualTo(UPDATED_MO_TA);
    }

    @Test
    @Transactional
    void fullUpdateLoaiTaiSanWithPatch() throws Exception {
        // Initialize the database
        loaiTaiSanRepository.saveAndFlush(loaiTaiSan);

        int databaseSizeBeforeUpdate = loaiTaiSanRepository.findAll().size();

        // Update the loaiTaiSan using partial update
        LoaiTaiSan partialUpdatedLoaiTaiSan = new LoaiTaiSan();
        partialUpdatedLoaiTaiSan.setId(loaiTaiSan.getId());

        partialUpdatedLoaiTaiSan.ten(UPDATED_TEN).moTa(UPDATED_MO_TA);

        restLoaiTaiSanMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedLoaiTaiSan.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedLoaiTaiSan))
            )
            .andExpect(status().isOk());

        // Validate the LoaiTaiSan in the database
        List<LoaiTaiSan> loaiTaiSanList = loaiTaiSanRepository.findAll();
        assertThat(loaiTaiSanList).hasSize(databaseSizeBeforeUpdate);
        LoaiTaiSan testLoaiTaiSan = loaiTaiSanList.get(loaiTaiSanList.size() - 1);
        assertThat(testLoaiTaiSan.getTen()).isEqualTo(UPDATED_TEN);
        assertThat(testLoaiTaiSan.getMoTa()).isEqualTo(UPDATED_MO_TA);
    }

    @Test
    @Transactional
    void patchNonExistingLoaiTaiSan() throws Exception {
        int databaseSizeBeforeUpdate = loaiTaiSanRepository.findAll().size();
        loaiTaiSan.setId(count.incrementAndGet());

        // Create the LoaiTaiSan
        LoaiTaiSanDTO loaiTaiSanDTO = loaiTaiSanMapper.toDto(loaiTaiSan);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restLoaiTaiSanMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, loaiTaiSanDTO.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(loaiTaiSanDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the LoaiTaiSan in the database
        List<LoaiTaiSan> loaiTaiSanList = loaiTaiSanRepository.findAll();
        assertThat(loaiTaiSanList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchLoaiTaiSan() throws Exception {
        int databaseSizeBeforeUpdate = loaiTaiSanRepository.findAll().size();
        loaiTaiSan.setId(count.incrementAndGet());

        // Create the LoaiTaiSan
        LoaiTaiSanDTO loaiTaiSanDTO = loaiTaiSanMapper.toDto(loaiTaiSan);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restLoaiTaiSanMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(loaiTaiSanDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the LoaiTaiSan in the database
        List<LoaiTaiSan> loaiTaiSanList = loaiTaiSanRepository.findAll();
        assertThat(loaiTaiSanList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamLoaiTaiSan() throws Exception {
        int databaseSizeBeforeUpdate = loaiTaiSanRepository.findAll().size();
        loaiTaiSan.setId(count.incrementAndGet());

        // Create the LoaiTaiSan
        LoaiTaiSanDTO loaiTaiSanDTO = loaiTaiSanMapper.toDto(loaiTaiSan);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restLoaiTaiSanMockMvc
            .perform(
                patch(ENTITY_API_URL).contentType("application/merge-patch+json").content(TestUtil.convertObjectToJsonBytes(loaiTaiSanDTO))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the LoaiTaiSan in the database
        List<LoaiTaiSan> loaiTaiSanList = loaiTaiSanRepository.findAll();
        assertThat(loaiTaiSanList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteLoaiTaiSan() throws Exception {
        // Initialize the database
        loaiTaiSanRepository.saveAndFlush(loaiTaiSan);

        int databaseSizeBeforeDelete = loaiTaiSanRepository.findAll().size();

        // Delete the loaiTaiSan
        restLoaiTaiSanMockMvc
            .perform(delete(ENTITY_API_URL_ID, loaiTaiSan.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<LoaiTaiSan> loaiTaiSanList = loaiTaiSanRepository.findAll();
        assertThat(loaiTaiSanList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
