import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { UserRouteAccessService } from 'app/core/auth/user-route-access.service';
import { LoaiTaiSanComponent } from '../list/loai-tai-san.component';
import { LoaiTaiSanDetailComponent } from '../detail/loai-tai-san-detail.component';
import { LoaiTaiSanUpdateComponent } from '../update/loai-tai-san-update.component';
import { LoaiTaiSanRoutingResolveService } from './loai-tai-san-routing-resolve.service';

const loaiTaiSanRoute: Routes = [
  {
    path: '',
    component: LoaiTaiSanComponent,
    data: {
      defaultSort: 'id,asc',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: LoaiTaiSanDetailComponent,
    resolve: {
      loaiTaiSan: LoaiTaiSanRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: LoaiTaiSanUpdateComponent,
    resolve: {
      loaiTaiSan: LoaiTaiSanRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: LoaiTaiSanUpdateComponent,
    resolve: {
      loaiTaiSan: LoaiTaiSanRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
];

@NgModule({
  imports: [RouterModule.forChild(loaiTaiSanRoute)],
  exports: [RouterModule],
})
export class LoaiTaiSanRoutingModule {}
