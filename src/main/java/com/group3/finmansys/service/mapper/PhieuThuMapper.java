package com.group3.finmansys.service.mapper;

import com.group3.finmansys.domain.*;
import com.group3.finmansys.service.dto.PhieuThuDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link PhieuThu} and its DTO {@link PhieuThuDTO}.
 */
@Mapper(componentModel = "spring", uses = { KhoanPhaiThuMapper.class, QuyTienMapper.class, KhachHangMapper.class })
public interface PhieuThuMapper extends EntityMapper<PhieuThuDTO, PhieuThu> {
    @Mapping(target = "khoanPhaiThu", source = "khoanPhaiThu", qualifiedByName = "id")
    @Mapping(target = "quyTien", source = "quyTien", qualifiedByName = "ten")
    @Mapping(target = "khachHang", source = "khachHang", qualifiedByName = "ten")
    PhieuThuDTO toDto(PhieuThu s);
}
