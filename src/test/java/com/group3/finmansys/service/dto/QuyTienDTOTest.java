package com.group3.finmansys.service.dto;

import static org.assertj.core.api.Assertions.assertThat;

import com.group3.finmansys.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class QuyTienDTOTest {

    @Test
    void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(QuyTienDTO.class);
        QuyTienDTO quyTienDTO1 = new QuyTienDTO();
        quyTienDTO1.setId(1L);
        QuyTienDTO quyTienDTO2 = new QuyTienDTO();
        assertThat(quyTienDTO1).isNotEqualTo(quyTienDTO2);
        quyTienDTO2.setId(quyTienDTO1.getId());
        assertThat(quyTienDTO1).isEqualTo(quyTienDTO2);
        quyTienDTO2.setId(2L);
        assertThat(quyTienDTO1).isNotEqualTo(quyTienDTO2);
        quyTienDTO1.setId(null);
        assertThat(quyTienDTO1).isNotEqualTo(quyTienDTO2);
    }
}
