package com.group3.finmansys.service.dto;

import static org.assertj.core.api.Assertions.assertThat;

import com.group3.finmansys.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class DichVuSuDungDTOTest {

    @Test
    void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(DichVuSuDungDTO.class);
        DichVuSuDungDTO dichVuSuDungDTO1 = new DichVuSuDungDTO();
        dichVuSuDungDTO1.setId(1L);
        DichVuSuDungDTO dichVuSuDungDTO2 = new DichVuSuDungDTO();
        assertThat(dichVuSuDungDTO1).isNotEqualTo(dichVuSuDungDTO2);
        dichVuSuDungDTO2.setId(dichVuSuDungDTO1.getId());
        assertThat(dichVuSuDungDTO1).isEqualTo(dichVuSuDungDTO2);
        dichVuSuDungDTO2.setId(2L);
        assertThat(dichVuSuDungDTO1).isNotEqualTo(dichVuSuDungDTO2);
        dichVuSuDungDTO1.setId(null);
        assertThat(dichVuSuDungDTO1).isNotEqualTo(dichVuSuDungDTO2);
    }
}
