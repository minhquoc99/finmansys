package com.group3.finmansys.service.dto;

import java.io.Serializable;
import java.util.Objects;
import javax.persistence.Lob;
import javax.validation.constraints.*;

/**
 * A DTO for the {@link com.group3.finmansys.domain.TaiSanCD} entity.
 */
public class TaiSanCDDTO implements Serializable {

    private Long id;

    @NotNull
    @Size(max = 50)
    private String ten;

    @NotNull
    private String donViTuoiTho;

    @NotNull
    @DecimalMin(value = "1")
    private Double thoiGianSuDung;

    @Lob
    private byte[] hinhAnh;

    private String hinhAnhContentType;
    private KhachSanDTO khachSan;

    private LoaiTaiSanDTO loaiTaiSan;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTen() {
        return ten;
    }

    public void setTen(String ten) {
        this.ten = ten;
    }

    public String getDonViTuoiTho() {
        return donViTuoiTho;
    }

    public void setDonViTuoiTho(String donViTuoiTho) {
        this.donViTuoiTho = donViTuoiTho;
    }

    public Double getThoiGianSuDung() {
        return thoiGianSuDung;
    }

    public void setThoiGianSuDung(Double thoiGianSuDung) {
        this.thoiGianSuDung = thoiGianSuDung;
    }

    public byte[] getHinhAnh() {
        return hinhAnh;
    }

    public void setHinhAnh(byte[] hinhAnh) {
        this.hinhAnh = hinhAnh;
    }

    public String getHinhAnhContentType() {
        return hinhAnhContentType;
    }

    public void setHinhAnhContentType(String hinhAnhContentType) {
        this.hinhAnhContentType = hinhAnhContentType;
    }

    public KhachSanDTO getKhachSan() {
        return khachSan;
    }

    public void setKhachSan(KhachSanDTO khachSan) {
        this.khachSan = khachSan;
    }

    public LoaiTaiSanDTO getLoaiTaiSan() {
        return loaiTaiSan;
    }

    public void setLoaiTaiSan(LoaiTaiSanDTO loaiTaiSan) {
        this.loaiTaiSan = loaiTaiSan;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof TaiSanCDDTO)) {
            return false;
        }

        TaiSanCDDTO taiSanCDDTO = (TaiSanCDDTO) o;
        if (this.id == null) {
            return false;
        }
        return Objects.equals(this.id, taiSanCDDTO.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.id);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "TaiSanCDDTO{" +
            "id=" + getId() +
            ", ten='" + getTen() + "'" +
            ", donViTuoiTho='" + getDonViTuoiTho() + "'" +
            ", thoiGianSuDung=" + getThoiGianSuDung() +
            ", hinhAnh='" + getHinhAnh() + "'" +
            ", khachSan=" + getKhachSan() +
            ", loaiTaiSan=" + getLoaiTaiSan() +
            "}";
    }
}
