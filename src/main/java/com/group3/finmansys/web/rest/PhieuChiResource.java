package com.group3.finmansys.web.rest;

import com.group3.finmansys.repository.PhieuChiRepository;
import com.group3.finmansys.service.PhieuChiQueryService;
import com.group3.finmansys.service.PhieuChiService;
import com.group3.finmansys.service.criteria.PhieuChiCriteria;
import com.group3.finmansys.service.dto.PhieuChiDTO;
import com.group3.finmansys.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.PaginationUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link com.group3.finmansys.domain.PhieuChi}.
 */
@RestController
@RequestMapping("/api")
public class PhieuChiResource {

    private final Logger log = LoggerFactory.getLogger(PhieuChiResource.class);

    private static final String ENTITY_NAME = "phieuChi";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final PhieuChiService phieuChiService;

    private final PhieuChiRepository phieuChiRepository;

    private final PhieuChiQueryService phieuChiQueryService;

    public PhieuChiResource(
        PhieuChiService phieuChiService,
        PhieuChiRepository phieuChiRepository,
        PhieuChiQueryService phieuChiQueryService
    ) {
        this.phieuChiService = phieuChiService;
        this.phieuChiRepository = phieuChiRepository;
        this.phieuChiQueryService = phieuChiQueryService;
    }

    /**
     * {@code POST  /phieu-chis} : Create a new phieuChi.
     *
     * @param phieuChiDTO the phieuChiDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new phieuChiDTO, or with status {@code 400 (Bad Request)} if the phieuChi has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/phieu-chis")
    public ResponseEntity<PhieuChiDTO> createPhieuChi(@Valid @RequestBody PhieuChiDTO phieuChiDTO) throws URISyntaxException {
        log.debug("REST request to save PhieuChi : {}", phieuChiDTO);
        if (phieuChiDTO.getId() != null) {
            throw new BadRequestAlertException("A new phieuChi cannot already have an ID", ENTITY_NAME, "idexists");
        }
        PhieuChiDTO result = phieuChiService.save(phieuChiDTO);
        return ResponseEntity
            .created(new URI("/api/phieu-chis/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /phieu-chis/:id} : Updates an existing phieuChi.
     *
     * @param id the id of the phieuChiDTO to save.
     * @param phieuChiDTO the phieuChiDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated phieuChiDTO,
     * or with status {@code 400 (Bad Request)} if the phieuChiDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the phieuChiDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/phieu-chis/{id}")
    public ResponseEntity<PhieuChiDTO> updatePhieuChi(
        @PathVariable(value = "id", required = false) final Long id,
        @Valid @RequestBody PhieuChiDTO phieuChiDTO
    ) throws URISyntaxException {
        log.debug("REST request to update PhieuChi : {}, {}", id, phieuChiDTO);
        if (phieuChiDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, phieuChiDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!phieuChiRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        PhieuChiDTO result = phieuChiService.save(phieuChiDTO);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, phieuChiDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /phieu-chis/:id} : Partial updates given fields of an existing phieuChi, field will ignore if it is null
     *
     * @param id the id of the phieuChiDTO to save.
     * @param phieuChiDTO the phieuChiDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated phieuChiDTO,
     * or with status {@code 400 (Bad Request)} if the phieuChiDTO is not valid,
     * or with status {@code 404 (Not Found)} if the phieuChiDTO is not found,
     * or with status {@code 500 (Internal Server Error)} if the phieuChiDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/phieu-chis/{id}", consumes = "application/merge-patch+json")
    public ResponseEntity<PhieuChiDTO> partialUpdatePhieuChi(
        @PathVariable(value = "id", required = false) final Long id,
        @NotNull @RequestBody PhieuChiDTO phieuChiDTO
    ) throws URISyntaxException {
        log.debug("REST request to partial update PhieuChi partially : {}, {}", id, phieuChiDTO);
        if (phieuChiDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, phieuChiDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!phieuChiRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<PhieuChiDTO> result = phieuChiService.partialUpdate(phieuChiDTO);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, phieuChiDTO.getId().toString())
        );
    }

    /**
     * {@code GET  /phieu-chis} : get all the phieuChis.
     *
     * @param pageable the pagination information.
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of phieuChis in body.
     */
    @GetMapping("/phieu-chis")
    public ResponseEntity<List<PhieuChiDTO>> getAllPhieuChis(PhieuChiCriteria criteria, Pageable pageable) {
        log.debug("REST request to get PhieuChis by criteria: {}", criteria);
        Page<PhieuChiDTO> page = phieuChiQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /phieu-chis/count} : count all the phieuChis.
     *
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
     */
    @GetMapping("/phieu-chis/count")
    public ResponseEntity<Long> countPhieuChis(PhieuChiCriteria criteria) {
        log.debug("REST request to count PhieuChis by criteria: {}", criteria);
        return ResponseEntity.ok().body(phieuChiQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /phieu-chis/:id} : get the "id" phieuChi.
     *
     * @param id the id of the phieuChiDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the phieuChiDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/phieu-chis/{id}")
    public ResponseEntity<PhieuChiDTO> getPhieuChi(@PathVariable Long id) {
        log.debug("REST request to get PhieuChi : {}", id);
        Optional<PhieuChiDTO> phieuChiDTO = phieuChiService.findOne(id);
        return ResponseUtil.wrapOrNotFound(phieuChiDTO);
    }

    /**
     * {@code DELETE  /phieu-chis/:id} : delete the "id" phieuChi.
     *
     * @param id the id of the phieuChiDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/phieu-chis/{id}")
    public ResponseEntity<Void> deletePhieuChi(@PathVariable Long id) {
        log.debug("REST request to delete PhieuChi : {}", id);
        phieuChiService.delete(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
            .build();
    }
}
