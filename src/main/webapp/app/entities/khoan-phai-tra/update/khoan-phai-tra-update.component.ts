import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize, map } from 'rxjs/operators';

import * as dayjs from 'dayjs';
import { DATE_TIME_FORMAT } from 'app/config/input.constants';

import { IKhoanPhaiTra, KhoanPhaiTra } from '../khoan-phai-tra.model';
import { KhoanPhaiTraService } from '../service/khoan-phai-tra.service';
import { IKhachHang } from 'app/entities/khach-hang/khach-hang.model';
import { KhachHangService } from 'app/entities/khach-hang/service/khach-hang.service';
import { IMaKeToan } from 'app/entities/ma-ke-toan/ma-ke-toan.model';
import { MaKeToanService } from 'app/entities/ma-ke-toan/service/ma-ke-toan.service';

@Component({
  selector: 'jhi-khoan-phai-tra-update',
  templateUrl: './khoan-phai-tra-update.component.html',
})
export class KhoanPhaiTraUpdateComponent implements OnInit {
  isSaving = false;

  khachHangsSharedCollection: IKhachHang[] = [];
  maKeToansSharedCollection: IMaKeToan[] = [];

  editForm = this.fb.group({
    id: [],
    ngayTao: [null, [Validators.required]],
    ngayPhaiTra: [null, [Validators.required]],
    soTien: [null, [Validators.required, Validators.min(1)]],
    ghiChu: [],
    khachHang: [null, Validators.required],
    maKeToan: [null, Validators.required],
  });

  constructor(
    protected khoanPhaiTraService: KhoanPhaiTraService,
    protected khachHangService: KhachHangService,
    protected maKeToanService: MaKeToanService,
    protected activatedRoute: ActivatedRoute,
    protected fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ khoanPhaiTra }) => {
      if (khoanPhaiTra.id === undefined) {
        const today = dayjs().startOf('day');
        khoanPhaiTra.ngayTao = today;
        khoanPhaiTra.ngayPhaiTra = today;
      }

      this.updateForm(khoanPhaiTra);

      console.log(this.editForm);

      this.loadRelationshipsOptions();
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const khoanPhaiTra = this.createFromForm();
    if (khoanPhaiTra.id !== undefined) {
      this.subscribeToSaveResponse(this.khoanPhaiTraService.update(khoanPhaiTra));
    } else {
      this.subscribeToSaveResponse(this.khoanPhaiTraService.create(khoanPhaiTra));
    }
  }

  trackKhachHangById(index: number, item: IKhachHang): number {
    return item.id!;
  }

  trackMaKeToanById(index: number, item: IMaKeToan): number {
    return item.id!;
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IKhoanPhaiTra>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(khoanPhaiTra: IKhoanPhaiTra): void {
    this.editForm.patchValue({
      id: khoanPhaiTra.id,
      ngayTao: khoanPhaiTra.ngayTao ? khoanPhaiTra.ngayTao.format(DATE_TIME_FORMAT) : null,
      ngayPhaiTra: khoanPhaiTra.ngayPhaiTra ? khoanPhaiTra.ngayPhaiTra.format(DATE_TIME_FORMAT) : null,
      soTien: khoanPhaiTra.soTien,
      ghiChu: khoanPhaiTra.ghiChu,
      khachHang: khoanPhaiTra.khachHang,
      maKeToan: khoanPhaiTra.maKeToan,
    });

    this.khachHangsSharedCollection = this.khachHangService.addKhachHangToCollectionIfMissing(
      this.khachHangsSharedCollection,
      khoanPhaiTra.khachHang
    );
    this.maKeToansSharedCollection = this.maKeToanService.addMaKeToanToCollectionIfMissing(
      this.maKeToansSharedCollection,
      khoanPhaiTra.maKeToan
    );
  }

  protected loadRelationshipsOptions(): void {
    this.khachHangService
      .query({ page: 0, size: 10000, sort: ['id,asc'] })
      .pipe(map((res: HttpResponse<IKhachHang[]>) => res.body ?? []))
      .pipe(
        map((khachHangs: IKhachHang[]) =>
          this.khachHangService.addKhachHangToCollectionIfMissing(khachHangs, this.editForm.get('khachHang')!.value)
        )
      )
      .subscribe((khachHangs: IKhachHang[]) => (this.khachHangsSharedCollection = khachHangs));

    this.maKeToanService
      .query({ page: 0, size: 10000, sort: ['id,asc'] })
      .pipe(map((res: HttpResponse<IMaKeToan[]>) => res.body ?? []))
      .pipe(
        map((maKeToans: IMaKeToan[]) =>
          this.maKeToanService.addMaKeToanToCollectionIfMissing(maKeToans, this.editForm.get('maKeToan')!.value)
        )
      )
      .subscribe((maKeToans: IMaKeToan[]) => (this.maKeToansSharedCollection = maKeToans));
  }

  protected createFromForm(): IKhoanPhaiTra {
    return {
      ...new KhoanPhaiTra(),
      id: this.editForm.get(['id'])!.value,
      ngayTao: this.editForm.get(['ngayTao'])!.value ? dayjs(this.editForm.get(['ngayTao'])!.value, DATE_TIME_FORMAT) : undefined,
      ngayPhaiTra: this.editForm.get(['ngayPhaiTra'])!.value
        ? dayjs(this.editForm.get(['ngayPhaiTra'])!.value, DATE_TIME_FORMAT)
        : undefined,
      soTien: this.editForm.get(['soTien'])!.value,
      ghiChu: this.editForm.get(['ghiChu'])!.value,
      khachHang: this.editForm.get(['khachHang'])!.value,
      maKeToan: this.editForm.get(['maKeToan'])!.value,
    };
  }
}
