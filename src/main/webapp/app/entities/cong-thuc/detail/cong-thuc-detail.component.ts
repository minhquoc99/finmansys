import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { ICongThuc } from '../cong-thuc.model';

@Component({
  selector: 'jhi-cong-thuc-detail',
  templateUrl: './cong-thuc-detail.component.html',
})
export class CongThucDetailComponent implements OnInit {
  congThuc: ICongThuc | null = null;

  cap1 = {
    'font-weight': 'bold',
    'font-size': '20px',
  };

  checkCapCon = true;

  checkToanTu = true;

  cap2 = {
    'font-weight': 'bold',
  };

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ congThuc }) => {
      this.congThuc = congThuc;
      if (this.congThuc?.capCons?.length === 0) {
        this.checkCapCon = false;
      }

      if (this.congThuc?.toanTus?.length === 0) {
        this.checkToanTu = false;
      }
    });
  }

  getStyleCap(congThuc: ICongThuc): any {
    if (this.congThuc?.capCons?.length === 0) {
      this.checkCapCon = false;
    }

    if (this.congThuc?.toanTus?.length === 0) {
      this.checkToanTu = false;
    }

    if (congThuc.cap === 1) {
      return this.cap1;
    }
    if (congThuc.cap === 2) {
      return this.cap2;
    }
    return;
  }

  previousState(): void {
    window.history.back();
  }
}
