import { Component, OnInit } from '@angular/core';
import { HttpHeaders, HttpResponse } from '@angular/common/http';
import { ActivatedRoute, Router } from '@angular/router';
import { combineLatest } from 'rxjs';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { IKhoanPhaiTra } from '../khoan-phai-tra.model';

import { ITEMS_PER_PAGE } from 'app/config/pagination.constants';
import { KhoanPhaiTraService } from '../service/khoan-phai-tra.service';
import { KhoanPhaiTraDeleteDialogComponent } from '../delete/khoan-phai-tra-delete-dialog.component';
import * as dayjs from 'dayjs';
import { DATE_TIME_FORMAT } from 'app/config/input.constants';
import { FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'jhi-khoan-phai-tra',
  templateUrl: './khoan-phai-tra.component.html',
})
export class KhoanPhaiTraComponent implements OnInit {
  khoanPhaiTras?: IKhoanPhaiTra[];
  isLoading = false;
  totalItems = 0;
  itemsPerPage = ITEMS_PER_PAGE;
  page?: number;
  predicate!: string;
  ascending!: boolean;
  ngbPaginationPage = 1;
  bgRed = {
    'background-color': 'rgba(249,158,152,0.6)',
  };

  bgGreen = {
    'background-color': 'lightgreen',
  };

  bgWhite = {
    'background-color': 'white',
  };

  bgOrange = {
    'background-color': '#ffcc99',
  };

  editForm = this.fb.group({
    tuNgay: [dayjs(new Date(2021, 0, 1).toString()).format(DATE_TIME_FORMAT), [Validators.required]],
    denNgay: [dayjs(new Date().toString()).format(DATE_TIME_FORMAT), [Validators.required]],
  });

  constructor(
    protected khoanPhaiTraService: KhoanPhaiTraService,
    protected activatedRoute: ActivatedRoute,
    protected router: Router,
    protected modalService: NgbModal,
    protected fb: FormBuilder
  ) {}

  loadPage(page?: number, dontNavigate?: boolean): void {
    this.isLoading = true;
    const pageToLoad: number = page ?? this.page ?? 1;

    console.log(this.editForm);
    this.khoanPhaiTraService
      .query({
        page: pageToLoad - 1,
        size: this.itemsPerPage,
        sort: this.sort(),
        'ngayTao.greaterThan': new Date(this.editForm.get('tuNgay')!.value).toISOString(),
        'ngayTao.lessThan': new Date(this.editForm.get('denNgay')!.value).toISOString(),
      })
      .subscribe(
        (res: HttpResponse<IKhoanPhaiTra[]>) => {
          this.isLoading = false;
          this.onSuccess(res.body, res.headers, pageToLoad, !dontNavigate);
        },
        () => {
          this.isLoading = false;
          this.onError();
        }
      );
  }

  ngOnInit(): void {
    this.handleNavigation();
  }

  trackId(index: number, item: IKhoanPhaiTra): number {
    return item.id!;
  }

  delete(khoanPhaiTra: IKhoanPhaiTra): void {
    const modalRef = this.modalService.open(KhoanPhaiTraDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.khoanPhaiTra = khoanPhaiTra;
    // unsubscribe not needed because closed completes on modal close
    modalRef.closed.subscribe(reason => {
      if (reason === 'deleted') {
        this.loadPage();
      }
    });
  }

  checkDay(khoanPhaiTra: IKhoanPhaiTra): any {
    if (khoanPhaiTra.ngayPhaiTra) {
      if (khoanPhaiTra.dsPhieuChis) {
        let soTienDaTra = 0;
        for (const phieuChi of khoanPhaiTra.dsPhieuChis) {
          soTienDaTra = soTienDaTra + Number(phieuChi.soTien);
        }
        if (khoanPhaiTra.soTien && soTienDaTra >= khoanPhaiTra.soTien) {
          return this.bgGreen;
        }
      }

      if (khoanPhaiTra.ngayPhaiTra.toDate() < new Date()) {
        return this.bgRed;
      } else {
        if (khoanPhaiTra.ngayPhaiTra.toDate().valueOf() - new Date().valueOf() > 1209539868) {
          return this.bgWhite;
        } else {
          return this.bgOrange;
        }
      }
    }
    return this.bgGreen;
  }

  protected sort(): string[] {
    const result = [this.predicate + ',' + (this.ascending ? 'asc' : 'desc')];
    if (this.predicate !== 'id') {
      result.push('id');
    }
    return result;
  }

  protected handleNavigation(): void {
    combineLatest([this.activatedRoute.data, this.activatedRoute.queryParamMap]).subscribe(([data, params]) => {
      const page = params.get('page');
      const pageNumber = page !== null ? +page : 1;
      const sort = (params.get('sort') ?? data['defaultSort']).split(',');
      const predicate = sort[0];
      const ascending = sort[1] === 'asc';
      if (pageNumber !== this.page || predicate !== this.predicate || ascending !== this.ascending) {
        this.predicate = predicate;
        this.ascending = ascending;
        this.loadPage(pageNumber, true);
      }
    });
  }

  protected onSuccess(data: IKhoanPhaiTra[] | null, headers: HttpHeaders, page: number, navigate: boolean): void {
    this.totalItems = Number(headers.get('X-Total-Count'));
    this.page = page;
    if (navigate) {
      this.router.navigate(['/khoan-phai-tra'], {
        queryParams: {
          page: this.page,
          size: this.itemsPerPage,
          sort: this.predicate + ',' + (this.ascending ? 'asc' : 'desc'),
        },
      });
    }
    this.khoanPhaiTras = data ?? [];
    this.ngbPaginationPage = this.page;
  }

  protected onError(): void {
    this.ngbPaginationPage = this.page ?? 1;
  }
}
