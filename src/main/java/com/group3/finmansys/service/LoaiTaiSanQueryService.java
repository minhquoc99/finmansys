package com.group3.finmansys.service;

import com.group3.finmansys.domain.*; // for static metamodels
import com.group3.finmansys.domain.LoaiTaiSan;
import com.group3.finmansys.repository.LoaiTaiSanRepository;
import com.group3.finmansys.service.criteria.LoaiTaiSanCriteria;
import com.group3.finmansys.service.dto.LoaiTaiSanDTO;
import com.group3.finmansys.service.mapper.LoaiTaiSanMapper;
import java.util.List;
import javax.persistence.criteria.JoinType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tech.jhipster.service.QueryService;

/**
 * Service for executing complex queries for {@link LoaiTaiSan} entities in the database.
 * The main input is a {@link LoaiTaiSanCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link LoaiTaiSanDTO} or a {@link Page} of {@link LoaiTaiSanDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class LoaiTaiSanQueryService extends QueryService<LoaiTaiSan> {

    private final Logger log = LoggerFactory.getLogger(LoaiTaiSanQueryService.class);

    private final LoaiTaiSanRepository loaiTaiSanRepository;

    private final LoaiTaiSanMapper loaiTaiSanMapper;

    public LoaiTaiSanQueryService(LoaiTaiSanRepository loaiTaiSanRepository, LoaiTaiSanMapper loaiTaiSanMapper) {
        this.loaiTaiSanRepository = loaiTaiSanRepository;
        this.loaiTaiSanMapper = loaiTaiSanMapper;
    }

    /**
     * Return a {@link List} of {@link LoaiTaiSanDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<LoaiTaiSanDTO> findByCriteria(LoaiTaiSanCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<LoaiTaiSan> specification = createSpecification(criteria);
        return loaiTaiSanMapper.toDto(loaiTaiSanRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link LoaiTaiSanDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<LoaiTaiSanDTO> findByCriteria(LoaiTaiSanCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<LoaiTaiSan> specification = createSpecification(criteria);
        return loaiTaiSanRepository.findAll(specification, page).map(loaiTaiSanMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(LoaiTaiSanCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<LoaiTaiSan> specification = createSpecification(criteria);
        return loaiTaiSanRepository.count(specification);
    }

    /**
     * Function to convert {@link LoaiTaiSanCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<LoaiTaiSan> createSpecification(LoaiTaiSanCriteria criteria) {
        Specification<LoaiTaiSan> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getId(), LoaiTaiSan_.id));
            }
            if (criteria.getTen() != null) {
                specification = specification.and(buildStringSpecification(criteria.getTen(), LoaiTaiSan_.ten));
            }
            if (criteria.getMoTa() != null) {
                specification = specification.and(buildStringSpecification(criteria.getMoTa(), LoaiTaiSan_.moTa));
            }
            if (criteria.getMaKeToanId() != null) {
                specification =
                    specification.and(
                        buildSpecification(
                            criteria.getMaKeToanId(),
                            root -> root.join(LoaiTaiSan_.maKeToan, JoinType.LEFT).get(MaKeToan_.id)
                        )
                    );
            }
        }
        return specification;
    }
}
