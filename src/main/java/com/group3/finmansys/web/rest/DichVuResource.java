package com.group3.finmansys.web.rest;

import com.group3.finmansys.repository.DichVuRepository;
import com.group3.finmansys.service.DichVuQueryService;
import com.group3.finmansys.service.DichVuService;
import com.group3.finmansys.service.criteria.DichVuCriteria;
import com.group3.finmansys.service.dto.DichVuDTO;
import com.group3.finmansys.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.PaginationUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link com.group3.finmansys.domain.DichVu}.
 */
@RestController
@RequestMapping("/api")
public class DichVuResource {

    private final Logger log = LoggerFactory.getLogger(DichVuResource.class);

    private static final String ENTITY_NAME = "dichVu";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final DichVuService dichVuService;

    private final DichVuRepository dichVuRepository;

    private final DichVuQueryService dichVuQueryService;

    public DichVuResource(DichVuService dichVuService, DichVuRepository dichVuRepository, DichVuQueryService dichVuQueryService) {
        this.dichVuService = dichVuService;
        this.dichVuRepository = dichVuRepository;
        this.dichVuQueryService = dichVuQueryService;
    }

    /**
     * {@code POST  /dich-vus} : Create a new dichVu.
     *
     * @param dichVuDTO the dichVuDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new dichVuDTO, or with status {@code 400 (Bad Request)} if the dichVu has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/dich-vus")
    public ResponseEntity<DichVuDTO> createDichVu(@Valid @RequestBody DichVuDTO dichVuDTO) throws URISyntaxException {
        log.debug("REST request to save DichVu : {}", dichVuDTO);
        if (dichVuDTO.getId() != null) {
            throw new BadRequestAlertException("A new dichVu cannot already have an ID", ENTITY_NAME, "idexists");
        }
        DichVuDTO result = dichVuService.save(dichVuDTO);
        return ResponseEntity
            .created(new URI("/api/dich-vus/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /dich-vus/:id} : Updates an existing dichVu.
     *
     * @param id the id of the dichVuDTO to save.
     * @param dichVuDTO the dichVuDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated dichVuDTO,
     * or with status {@code 400 (Bad Request)} if the dichVuDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the dichVuDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/dich-vus/{id}")
    public ResponseEntity<DichVuDTO> updateDichVu(
        @PathVariable(value = "id", required = false) final Long id,
        @Valid @RequestBody DichVuDTO dichVuDTO
    ) throws URISyntaxException {
        log.debug("REST request to update DichVu : {}, {}", id, dichVuDTO);
        if (dichVuDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, dichVuDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!dichVuRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        DichVuDTO result = dichVuService.save(dichVuDTO);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, dichVuDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /dich-vus/:id} : Partial updates given fields of an existing dichVu, field will ignore if it is null
     *
     * @param id the id of the dichVuDTO to save.
     * @param dichVuDTO the dichVuDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated dichVuDTO,
     * or with status {@code 400 (Bad Request)} if the dichVuDTO is not valid,
     * or with status {@code 404 (Not Found)} if the dichVuDTO is not found,
     * or with status {@code 500 (Internal Server Error)} if the dichVuDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/dich-vus/{id}", consumes = "application/merge-patch+json")
    public ResponseEntity<DichVuDTO> partialUpdateDichVu(
        @PathVariable(value = "id", required = false) final Long id,
        @NotNull @RequestBody DichVuDTO dichVuDTO
    ) throws URISyntaxException {
        log.debug("REST request to partial update DichVu partially : {}, {}", id, dichVuDTO);
        if (dichVuDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, dichVuDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!dichVuRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<DichVuDTO> result = dichVuService.partialUpdate(dichVuDTO);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, dichVuDTO.getId().toString())
        );
    }

    /**
     * {@code GET  /dich-vus} : get all the dichVus.
     *
     * @param pageable the pagination information.
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of dichVus in body.
     */
    @GetMapping("/dich-vus")
    public ResponseEntity<List<DichVuDTO>> getAllDichVus(DichVuCriteria criteria, Pageable pageable) {
        log.debug("REST request to get DichVus by criteria: {}", criteria);
        Page<DichVuDTO> page = dichVuQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /dich-vus/count} : count all the dichVus.
     *
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
     */
    @GetMapping("/dich-vus/count")
    public ResponseEntity<Long> countDichVus(DichVuCriteria criteria) {
        log.debug("REST request to count DichVus by criteria: {}", criteria);
        return ResponseEntity.ok().body(dichVuQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /dich-vus/:id} : get the "id" dichVu.
     *
     * @param id the id of the dichVuDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the dichVuDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/dich-vus/{id}")
    public ResponseEntity<DichVuDTO> getDichVu(@PathVariable Long id) {
        log.debug("REST request to get DichVu : {}", id);
        Optional<DichVuDTO> dichVuDTO = dichVuService.findOne(id);
        return ResponseUtil.wrapOrNotFound(dichVuDTO);
    }

    /**
     * {@code DELETE  /dich-vus/:id} : delete the "id" dichVu.
     *
     * @param id the id of the dichVuDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/dich-vus/{id}")
    public ResponseEntity<Void> deleteDichVu(@PathVariable Long id) {
        log.debug("REST request to delete DichVu : {}", id);
        dichVuService.delete(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
            .build();
    }
}
