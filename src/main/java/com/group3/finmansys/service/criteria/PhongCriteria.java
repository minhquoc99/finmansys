package com.group3.finmansys.service.criteria;

import java.io.Serializable;
import java.util.Objects;
import tech.jhipster.service.Criteria;
import tech.jhipster.service.filter.BooleanFilter;
import tech.jhipster.service.filter.DoubleFilter;
import tech.jhipster.service.filter.Filter;
import tech.jhipster.service.filter.FloatFilter;
import tech.jhipster.service.filter.IntegerFilter;
import tech.jhipster.service.filter.LongFilter;
import tech.jhipster.service.filter.StringFilter;

/**
 * Criteria class for the {@link com.group3.finmansys.domain.Phong} entity. This class is used
 * in {@link com.group3.finmansys.web.rest.PhongResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /phongs?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class PhongCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter ten;

    private StringFilter loai;

    private DoubleFilter gia;

    private LongFilter khachSanId;

    public PhongCriteria() {}

    public PhongCriteria(PhongCriteria other) {
        this.id = other.id == null ? null : other.id.copy();
        this.ten = other.ten == null ? null : other.ten.copy();
        this.loai = other.loai == null ? null : other.loai.copy();
        this.gia = other.gia == null ? null : other.gia.copy();
        this.khachSanId = other.khachSanId == null ? null : other.khachSanId.copy();
    }

    @Override
    public PhongCriteria copy() {
        return new PhongCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public LongFilter id() {
        if (id == null) {
            id = new LongFilter();
        }
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getTen() {
        return ten;
    }

    public StringFilter ten() {
        if (ten == null) {
            ten = new StringFilter();
        }
        return ten;
    }

    public void setTen(StringFilter ten) {
        this.ten = ten;
    }

    public StringFilter getLoai() {
        return loai;
    }

    public StringFilter loai() {
        if (loai == null) {
            loai = new StringFilter();
        }
        return loai;
    }

    public void setLoai(StringFilter loai) {
        this.loai = loai;
    }

    public DoubleFilter getGia() {
        return gia;
    }

    public DoubleFilter gia() {
        if (gia == null) {
            gia = new DoubleFilter();
        }
        return gia;
    }

    public void setGia(DoubleFilter gia) {
        this.gia = gia;
    }

    public LongFilter getKhachSanId() {
        return khachSanId;
    }

    public LongFilter khachSanId() {
        if (khachSanId == null) {
            khachSanId = new LongFilter();
        }
        return khachSanId;
    }

    public void setKhachSanId(LongFilter khachSanId) {
        this.khachSanId = khachSanId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final PhongCriteria that = (PhongCriteria) o;
        return (
            Objects.equals(id, that.id) &&
            Objects.equals(ten, that.ten) &&
            Objects.equals(loai, that.loai) &&
            Objects.equals(gia, that.gia) &&
            Objects.equals(khachSanId, that.khachSanId)
        );
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, ten, loai, gia, khachSanId);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "PhongCriteria{" +
            (id != null ? "id=" + id + ", " : "") +
            (ten != null ? "ten=" + ten + ", " : "") +
            (loai != null ? "loai=" + loai + ", " : "") +
            (gia != null ? "gia=" + gia + ", " : "") +
            (khachSanId != null ? "khachSanId=" + khachSanId + ", " : "") +
            "}";
    }
}
