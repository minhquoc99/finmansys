jest.mock('@angular/router');

import { TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ActivatedRouteSnapshot, Router } from '@angular/router';
import { of } from 'rxjs';

import { IPhong, Phong } from '../phong.model';
import { PhongService } from '../service/phong.service';

import { PhongRoutingResolveService } from './phong-routing-resolve.service';

describe('Service Tests', () => {
  describe('Phong routing resolve service', () => {
    let mockRouter: Router;
    let mockActivatedRouteSnapshot: ActivatedRouteSnapshot;
    let routingResolveService: PhongRoutingResolveService;
    let service: PhongService;
    let resultPhong: IPhong | undefined;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule],
        providers: [Router, ActivatedRouteSnapshot],
      });
      mockRouter = TestBed.inject(Router);
      mockActivatedRouteSnapshot = TestBed.inject(ActivatedRouteSnapshot);
      routingResolveService = TestBed.inject(PhongRoutingResolveService);
      service = TestBed.inject(PhongService);
      resultPhong = undefined;
    });

    describe('resolve', () => {
      it('should return IPhong returned by find', () => {
        // GIVEN
        service.find = jest.fn(id => of(new HttpResponse({ body: { id } })));
        mockActivatedRouteSnapshot.params = { id: 123 };

        // WHEN
        routingResolveService.resolve(mockActivatedRouteSnapshot).subscribe(result => {
          resultPhong = result;
        });

        // THEN
        expect(service.find).toBeCalledWith(123);
        expect(resultPhong).toEqual({ id: 123 });
      });

      it('should return new IPhong if id is not provided', () => {
        // GIVEN
        service.find = jest.fn();
        mockActivatedRouteSnapshot.params = {};

        // WHEN
        routingResolveService.resolve(mockActivatedRouteSnapshot).subscribe(result => {
          resultPhong = result;
        });

        // THEN
        expect(service.find).not.toBeCalled();
        expect(resultPhong).toEqual(new Phong());
      });

      it('should route to 404 page if data not found in server', () => {
        // GIVEN
        spyOn(service, 'find').and.returnValue(of(new HttpResponse({ body: null })));
        mockActivatedRouteSnapshot.params = { id: 123 };

        // WHEN
        routingResolveService.resolve(mockActivatedRouteSnapshot).subscribe(result => {
          resultPhong = result;
        });

        // THEN
        expect(service.find).toBeCalledWith(123);
        expect(resultPhong).toEqual(undefined);
        expect(mockRouter.navigate).toHaveBeenCalledWith(['404']);
      });
    });
  });
});
