import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { isPresent } from 'app/core/util/operators';
import { ApplicationConfigService } from 'app/core/config/application-config.service';
import { createRequestOption } from 'app/core/request/request-util';
import { IDichVu, getDichVuIdentifier } from '../dich-vu.model';

export type EntityResponseType = HttpResponse<IDichVu>;
export type EntityArrayResponseType = HttpResponse<IDichVu[]>;

@Injectable({ providedIn: 'root' })
export class DichVuService {
  public resourceUrl = this.applicationConfigService.getEndpointFor('api/dich-vus');

  constructor(protected http: HttpClient, private applicationConfigService: ApplicationConfigService) {}

  create(dichVu: IDichVu): Observable<EntityResponseType> {
    return this.http.post<IDichVu>(this.resourceUrl, dichVu, { observe: 'response' });
  }

  update(dichVu: IDichVu): Observable<EntityResponseType> {
    return this.http.put<IDichVu>(`${this.resourceUrl}/${getDichVuIdentifier(dichVu) as number}`, dichVu, { observe: 'response' });
  }

  partialUpdate(dichVu: IDichVu): Observable<EntityResponseType> {
    return this.http.patch<IDichVu>(`${this.resourceUrl}/${getDichVuIdentifier(dichVu) as number}`, dichVu, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IDichVu>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IDichVu[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  addDichVuToCollectionIfMissing(dichVuCollection: IDichVu[], ...dichVusToCheck: (IDichVu | null | undefined)[]): IDichVu[] {
    const dichVus: IDichVu[] = dichVusToCheck.filter(isPresent);
    if (dichVus.length > 0) {
      const dichVuCollectionIdentifiers = dichVuCollection.map(dichVuItem => getDichVuIdentifier(dichVuItem)!);
      const dichVusToAdd = dichVus.filter(dichVuItem => {
        const dichVuIdentifier = getDichVuIdentifier(dichVuItem);
        if (dichVuIdentifier == null || dichVuCollectionIdentifiers.includes(dichVuIdentifier)) {
          return false;
        }
        dichVuCollectionIdentifiers.push(dichVuIdentifier);
        return true;
      });
      return [...dichVusToAdd, ...dichVuCollection];
    }
    return dichVuCollection;
  }
}
