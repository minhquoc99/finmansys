package com.group3.finmansys.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.time.Instant;
import javax.persistence.*;
import javax.validation.constraints.*;

/**
 * A ChiTietTangTSCD.
 */
@Entity
@Table(name = "chi_tiet_tangtscd")
public class ChiTietTangTSCD implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(name = "ngay_thu_mua", nullable = false)
    private Instant ngayThuMua;

    @NotNull
    @Min(value = 1)
    @Column(name = "so_luong", nullable = false)
    private Integer soLuong;

    @NotNull
    @DecimalMin(value = "1")
    @Column(name = "chi_phi_mua", nullable = false)
    private Double chiPhiMua;

    @ManyToOne(optional = false)
    @NotNull
    @JsonIgnoreProperties(value = { "khachSan", "loaiTaiSan" }, allowSetters = true)
    private TaiSanCD taiSanCD;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public ChiTietTangTSCD id(Long id) {
        this.id = id;
        return this;
    }

    public Instant getNgayThuMua() {
        return this.ngayThuMua;
    }

    public ChiTietTangTSCD ngayThuMua(Instant ngayThuMua) {
        this.ngayThuMua = ngayThuMua;
        return this;
    }

    public void setNgayThuMua(Instant ngayThuMua) {
        this.ngayThuMua = ngayThuMua;
    }

    public Integer getSoLuong() {
        return this.soLuong;
    }

    public ChiTietTangTSCD soLuong(Integer soLuong) {
        this.soLuong = soLuong;
        return this;
    }

    public void setSoLuong(Integer soLuong) {
        this.soLuong = soLuong;
    }

    public Double getChiPhiMua() {
        return this.chiPhiMua;
    }

    public ChiTietTangTSCD chiPhiMua(Double chiPhiMua) {
        this.chiPhiMua = chiPhiMua;
        return this;
    }

    public void setChiPhiMua(Double chiPhiMua) {
        this.chiPhiMua = chiPhiMua;
    }

    public TaiSanCD getTaiSanCD() {
        return this.taiSanCD;
    }

    public ChiTietTangTSCD taiSanCD(TaiSanCD taiSanCD) {
        this.setTaiSanCD(taiSanCD);
        return this;
    }

    public void setTaiSanCD(TaiSanCD taiSanCD) {
        this.taiSanCD = taiSanCD;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof ChiTietTangTSCD)) {
            return false;
        }
        return id != null && id.equals(((ChiTietTangTSCD) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "ChiTietTangTSCD{" +
            "id=" + getId() +
            ", ngayThuMua='" + getNgayThuMua() + "'" +
            ", soLuong=" + getSoLuong() +
            ", chiPhiMua=" + getChiPhiMua() +
            "}";
    }
}
