import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import * as dayjs from 'dayjs';

import { isPresent } from 'app/core/util/operators';
import { ApplicationConfigService } from 'app/core/config/application-config.service';
import { createRequestOption } from 'app/core/request/request-util';
import { IKhoanPhaiTra, getKhoanPhaiTraIdentifier } from '../khoan-phai-tra.model';

export type EntityResponseType = HttpResponse<IKhoanPhaiTra>;
export type EntityArrayResponseType = HttpResponse<IKhoanPhaiTra[]>;

@Injectable({ providedIn: 'root' })
export class KhoanPhaiTraService {
  public resourceUrl = this.applicationConfigService.getEndpointFor('api/khoan-phai-tras');

  constructor(protected http: HttpClient, private applicationConfigService: ApplicationConfigService) {}

  create(khoanPhaiTra: IKhoanPhaiTra): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(khoanPhaiTra);
    return this.http
      .post<IKhoanPhaiTra>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  update(khoanPhaiTra: IKhoanPhaiTra): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(khoanPhaiTra);
    return this.http
      .put<IKhoanPhaiTra>(`${this.resourceUrl}/${getKhoanPhaiTraIdentifier(khoanPhaiTra) as number}`, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  partialUpdate(khoanPhaiTra: IKhoanPhaiTra): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(khoanPhaiTra);
    return this.http
      .patch<IKhoanPhaiTra>(`${this.resourceUrl}/${getKhoanPhaiTraIdentifier(khoanPhaiTra) as number}`, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http
      .get<IKhoanPhaiTra>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<IKhoanPhaiTra[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  addKhoanPhaiTraToCollectionIfMissing(
    khoanPhaiTraCollection: IKhoanPhaiTra[],
    ...khoanPhaiTrasToCheck: (IKhoanPhaiTra | null | undefined)[]
  ): IKhoanPhaiTra[] {
    const khoanPhaiTras: IKhoanPhaiTra[] = khoanPhaiTrasToCheck.filter(isPresent);
    if (khoanPhaiTras.length > 0) {
      const khoanPhaiTraCollectionIdentifiers = khoanPhaiTraCollection.map(
        khoanPhaiTraItem => getKhoanPhaiTraIdentifier(khoanPhaiTraItem)!
      );
      const khoanPhaiTrasToAdd = khoanPhaiTras.filter(khoanPhaiTraItem => {
        const khoanPhaiTraIdentifier = getKhoanPhaiTraIdentifier(khoanPhaiTraItem);
        if (khoanPhaiTraIdentifier == null || khoanPhaiTraCollectionIdentifiers.includes(khoanPhaiTraIdentifier)) {
          return false;
        }
        khoanPhaiTraCollectionIdentifiers.push(khoanPhaiTraIdentifier);
        return true;
      });
      return [...khoanPhaiTrasToAdd, ...khoanPhaiTraCollection];
    }
    return khoanPhaiTraCollection;
  }

  protected convertDateFromClient(khoanPhaiTra: IKhoanPhaiTra): IKhoanPhaiTra {
    return Object.assign({}, khoanPhaiTra, {
      ngayTao: khoanPhaiTra.ngayTao?.isValid() ? khoanPhaiTra.ngayTao.toJSON() : undefined,
      ngayPhaiTra: khoanPhaiTra.ngayPhaiTra?.isValid() ? khoanPhaiTra.ngayPhaiTra.toJSON() : undefined,
    });
  }

  protected convertDateFromServer(res: EntityResponseType): EntityResponseType {
    if (res.body) {
      res.body.ngayTao = res.body.ngayTao ? dayjs(res.body.ngayTao) : undefined;
      res.body.ngayPhaiTra = res.body.ngayPhaiTra ? dayjs(res.body.ngayPhaiTra) : undefined;
    }
    return res;
  }

  protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
    if (res.body) {
      res.body.forEach((khoanPhaiTra: IKhoanPhaiTra) => {
        khoanPhaiTra.ngayTao = khoanPhaiTra.ngayTao ? dayjs(khoanPhaiTra.ngayTao) : undefined;
        khoanPhaiTra.ngayPhaiTra = khoanPhaiTra.ngayPhaiTra ? dayjs(khoanPhaiTra.ngayPhaiTra) : undefined;
      });
    }
    return res;
  }
}
