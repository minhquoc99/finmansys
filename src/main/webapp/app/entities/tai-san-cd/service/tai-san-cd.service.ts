import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { isPresent } from 'app/core/util/operators';
import { ApplicationConfigService } from 'app/core/config/application-config.service';
import { createRequestOption } from 'app/core/request/request-util';
import { ITaiSanCD, getTaiSanCDIdentifier } from '../tai-san-cd.model';

export type EntityResponseType = HttpResponse<ITaiSanCD>;
export type EntityArrayResponseType = HttpResponse<ITaiSanCD[]>;

@Injectable({ providedIn: 'root' })
export class TaiSanCDService {
  public resourceUrl = this.applicationConfigService.getEndpointFor('api/tai-san-cds');

  constructor(protected http: HttpClient, private applicationConfigService: ApplicationConfigService) {}

  create(taiSanCD: ITaiSanCD): Observable<EntityResponseType> {
    return this.http.post<ITaiSanCD>(this.resourceUrl, taiSanCD, { observe: 'response' });
  }

  update(taiSanCD: ITaiSanCD): Observable<EntityResponseType> {
    return this.http.put<ITaiSanCD>(`${this.resourceUrl}/${getTaiSanCDIdentifier(taiSanCD) as number}`, taiSanCD, { observe: 'response' });
  }

  partialUpdate(taiSanCD: ITaiSanCD): Observable<EntityResponseType> {
    return this.http.patch<ITaiSanCD>(`${this.resourceUrl}/${getTaiSanCDIdentifier(taiSanCD) as number}`, taiSanCD, {
      observe: 'response',
    });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<ITaiSanCD>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<ITaiSanCD[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  addTaiSanCDToCollectionIfMissing(taiSanCDCollection: ITaiSanCD[], ...taiSanCDSToCheck: (ITaiSanCD | null | undefined)[]): ITaiSanCD[] {
    const taiSanCDS: ITaiSanCD[] = taiSanCDSToCheck.filter(isPresent);
    if (taiSanCDS.length > 0) {
      const taiSanCDCollectionIdentifiers = taiSanCDCollection.map(taiSanCDItem => getTaiSanCDIdentifier(taiSanCDItem)!);
      const taiSanCDSToAdd = taiSanCDS.filter(taiSanCDItem => {
        const taiSanCDIdentifier = getTaiSanCDIdentifier(taiSanCDItem);
        if (taiSanCDIdentifier == null || taiSanCDCollectionIdentifiers.includes(taiSanCDIdentifier)) {
          return false;
        }
        taiSanCDCollectionIdentifiers.push(taiSanCDIdentifier);
        return true;
      });
      return [...taiSanCDSToAdd, ...taiSanCDCollection];
    }
    return taiSanCDCollection;
  }
}
