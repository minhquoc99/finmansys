export interface IKhachHang {
  id?: number;
  ten?: string;
  diaChi?: string | null;
  email?: string | null;
  sdt?: string | null;
}

export class KhachHang implements IKhachHang {
  constructor(
    public id?: number,
    public ten?: string,
    public diaChi?: string | null,
    public email?: string | null,
    public sdt?: string | null
  ) {}
}

export function getKhachHangIdentifier(khachHang: IKhachHang): number | undefined {
  return khachHang.id;
}
