package com.group3.finmansys.service;

import com.group3.finmansys.service.dto.ChiTietGiamTSCDDTO;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Service Interface for managing {@link com.group3.finmansys.domain.ChiTietGiamTSCD}.
 */
public interface ChiTietGiamTSCDService {
    /**
     * Save a chiTietGiamTSCD.
     *
     * @param chiTietGiamTSCDDTO the entity to save.
     * @return the persisted entity.
     */
    ChiTietGiamTSCDDTO save(ChiTietGiamTSCDDTO chiTietGiamTSCDDTO);

    /**
     * Partially updates a chiTietGiamTSCD.
     *
     * @param chiTietGiamTSCDDTO the entity to update partially.
     * @return the persisted entity.
     */
    Optional<ChiTietGiamTSCDDTO> partialUpdate(ChiTietGiamTSCDDTO chiTietGiamTSCDDTO);

    /**
     * Get all the chiTietGiamTSCDS.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<ChiTietGiamTSCDDTO> findAll(Pageable pageable);

    /**
     * Get the "id" chiTietGiamTSCD.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<ChiTietGiamTSCDDTO> findOne(Long id);

    /**
     * Delete the "id" chiTietGiamTSCD.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
