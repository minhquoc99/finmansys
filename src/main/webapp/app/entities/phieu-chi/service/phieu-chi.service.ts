import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import * as dayjs from 'dayjs';

import { isPresent } from 'app/core/util/operators';
import { ApplicationConfigService } from 'app/core/config/application-config.service';
import { createRequestOption } from 'app/core/request/request-util';
import { IPhieuChi, getPhieuChiIdentifier } from '../phieu-chi.model';

export type EntityResponseType = HttpResponse<IPhieuChi>;
export type EntityArrayResponseType = HttpResponse<IPhieuChi[]>;

@Injectable({ providedIn: 'root' })
export class PhieuChiService {
  public resourceUrl = this.applicationConfigService.getEndpointFor('api/phieu-chis');

  constructor(protected http: HttpClient, private applicationConfigService: ApplicationConfigService) {}

  create(phieuChi: IPhieuChi): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(phieuChi);
    return this.http
      .post<IPhieuChi>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  update(phieuChi: IPhieuChi): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(phieuChi);
    return this.http
      .put<IPhieuChi>(`${this.resourceUrl}/${getPhieuChiIdentifier(phieuChi) as number}`, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  partialUpdate(phieuChi: IPhieuChi): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(phieuChi);
    return this.http
      .patch<IPhieuChi>(`${this.resourceUrl}/${getPhieuChiIdentifier(phieuChi) as number}`, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http
      .get<IPhieuChi>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<IPhieuChi[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  addPhieuChiToCollectionIfMissing(phieuChiCollection: IPhieuChi[], ...phieuChisToCheck: (IPhieuChi | null | undefined)[]): IPhieuChi[] {
    const phieuChis: IPhieuChi[] = phieuChisToCheck.filter(isPresent);
    if (phieuChis.length > 0) {
      const phieuChiCollectionIdentifiers = phieuChiCollection.map(phieuChiItem => getPhieuChiIdentifier(phieuChiItem)!);
      const phieuChisToAdd = phieuChis.filter(phieuChiItem => {
        const phieuChiIdentifier = getPhieuChiIdentifier(phieuChiItem);
        if (phieuChiIdentifier == null || phieuChiCollectionIdentifiers.includes(phieuChiIdentifier)) {
          return false;
        }
        phieuChiCollectionIdentifiers.push(phieuChiIdentifier);
        return true;
      });
      return [...phieuChisToAdd, ...phieuChiCollection];
    }
    return phieuChiCollection;
  }

  protected convertDateFromClient(phieuChi: IPhieuChi): IPhieuChi {
    return Object.assign({}, phieuChi, {
      ngayChungTu: phieuChi.ngayChungTu?.isValid() ? phieuChi.ngayChungTu.toJSON() : undefined,
    });
  }

  protected convertDateFromServer(res: EntityResponseType): EntityResponseType {
    if (res.body) {
      res.body.ngayChungTu = res.body.ngayChungTu ? dayjs(res.body.ngayChungTu) : undefined;
    }
    return res;
  }

  protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
    if (res.body) {
      res.body.forEach((phieuChi: IPhieuChi) => {
        phieuChi.ngayChungTu = phieuChi.ngayChungTu ? dayjs(phieuChi.ngayChungTu) : undefined;
      });
    }
    return res;
  }
}
