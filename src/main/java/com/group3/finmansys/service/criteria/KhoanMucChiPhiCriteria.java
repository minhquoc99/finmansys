package com.group3.finmansys.service.criteria;

import java.io.Serializable;
import java.util.Objects;
import tech.jhipster.service.Criteria;
import tech.jhipster.service.filter.BooleanFilter;
import tech.jhipster.service.filter.DoubleFilter;
import tech.jhipster.service.filter.Filter;
import tech.jhipster.service.filter.FloatFilter;
import tech.jhipster.service.filter.IntegerFilter;
import tech.jhipster.service.filter.LongFilter;
import tech.jhipster.service.filter.StringFilter;

/**
 * Criteria class for the {@link com.group3.finmansys.domain.KhoanMucChiPhi} entity. This class is used
 * in {@link com.group3.finmansys.web.rest.KhoanMucChiPhiResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /khoan-muc-chi-phis?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class KhoanMucChiPhiCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter ten;

    private StringFilter moTa;

    public KhoanMucChiPhiCriteria() {}

    public KhoanMucChiPhiCriteria(KhoanMucChiPhiCriteria other) {
        this.id = other.id == null ? null : other.id.copy();
        this.ten = other.ten == null ? null : other.ten.copy();
        this.moTa = other.moTa == null ? null : other.moTa.copy();
    }

    @Override
    public KhoanMucChiPhiCriteria copy() {
        return new KhoanMucChiPhiCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public LongFilter id() {
        if (id == null) {
            id = new LongFilter();
        }
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getTen() {
        return ten;
    }

    public StringFilter ten() {
        if (ten == null) {
            ten = new StringFilter();
        }
        return ten;
    }

    public void setTen(StringFilter ten) {
        this.ten = ten;
    }

    public StringFilter getMoTa() {
        return moTa;
    }

    public StringFilter moTa() {
        if (moTa == null) {
            moTa = new StringFilter();
        }
        return moTa;
    }

    public void setMoTa(StringFilter moTa) {
        this.moTa = moTa;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final KhoanMucChiPhiCriteria that = (KhoanMucChiPhiCriteria) o;
        return Objects.equals(id, that.id) && Objects.equals(ten, that.ten) && Objects.equals(moTa, that.moTa);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, ten, moTa);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "KhoanMucChiPhiCriteria{" +
            (id != null ? "id=" + id + ", " : "") +
            (ten != null ? "ten=" + ten + ", " : "") +
            (moTa != null ? "moTa=" + moTa + ", " : "") +
            "}";
    }
}
