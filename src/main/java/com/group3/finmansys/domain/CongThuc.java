package com.group3.finmansys.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.*;
import javax.validation.constraints.*;

/**
 * A CongThuc.
 */
@Entity
@Table(name = "cong_thuc")
public class CongThuc implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(name = "ma_so", nullable = false)
    private Integer maSo;

    @NotNull
    @Column(name = "ten_chi_tieu", nullable = false)
    private String tenChiTieu;

    @NotNull
    @Column(name = "cap", nullable = false)
    private Integer cap;

    @Column(name = "so_dau_ky")
    private Double soDauKy;

    @Column(name = "so_cuoi_ky")
    private Double soCuoiKy;

    @OneToMany(mappedBy = "congThuc")
    @JsonIgnoreProperties(value = { "maKeToan", "congThuc" }, allowSetters = true)
    private Set<ToanTu> toanTus = new HashSet<>();

    @ManyToOne
    @JsonIgnoreProperties(value = { "toanTus", "capCha", "capCons" }, allowSetters = true)
    private CongThuc capCha;

    @OneToMany(mappedBy = "capCha")
    @JsonIgnoreProperties(value = { "toanTus", "capCha", "capCons" }, allowSetters = true)
    private Set<CongThuc> capCons = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public CongThuc id(Long id) {
        this.id = id;
        return this;
    }

    public Integer getMaSo() {
        return this.maSo;
    }

    public CongThuc maSo(Integer maSo) {
        this.maSo = maSo;
        return this;
    }

    public void setMaSo(Integer maSo) {
        this.maSo = maSo;
    }

    public String getTenChiTieu() {
        return this.tenChiTieu;
    }

    public CongThuc tenChiTieu(String tenChiTieu) {
        this.tenChiTieu = tenChiTieu;
        return this;
    }

    public void setTenChiTieu(String tenChiTieu) {
        this.tenChiTieu = tenChiTieu;
    }

    public Integer getCap() {
        return this.cap;
    }

    public CongThuc cap(Integer cap) {
        this.cap = cap;
        return this;
    }

    public void setCap(Integer cap) {
        this.cap = cap;
    }

    public Double getSoDauKy() {
        return this.soDauKy;
    }

    public CongThuc soDauKy(Double soDauKy) {
        this.soDauKy = soDauKy;
        return this;
    }

    public void setSoDauKy(Double soDauKy) {
        this.soDauKy = soDauKy;
    }

    public Double getSoCuoiKy() {
        return this.soCuoiKy;
    }

    public CongThuc soCuoiKy(Double soCuoiKy) {
        this.soCuoiKy = soCuoiKy;
        return this;
    }

    public void setSoCuoiKy(Double soCuoiKy) {
        this.soCuoiKy = soCuoiKy;
    }

    public Set<ToanTu> getToanTus() {
        return this.toanTus;
    }

    public CongThuc toanTus(Set<ToanTu> toanTus) {
        this.setToanTus(toanTus);
        return this;
    }

    public CongThuc addToanTu(ToanTu toanTu) {
        this.toanTus.add(toanTu);
        toanTu.setCongThuc(this);
        return this;
    }

    public CongThuc removeToanTu(ToanTu toanTu) {
        this.toanTus.remove(toanTu);
        toanTu.setCongThuc(null);
        return this;
    }

    public void setToanTus(Set<ToanTu> toanTus) {
        if (this.toanTus != null) {
            this.toanTus.forEach(i -> i.setCongThuc(null));
        }
        if (toanTus != null) {
            toanTus.forEach(i -> i.setCongThuc(this));
        }
        this.toanTus = toanTus;
    }

    public CongThuc getCapCha() {
        return this.capCha;
    }

    public CongThuc capCha(CongThuc congThuc) {
        this.setCapCha(congThuc);
        return this;
    }

    public void setCapCha(CongThuc congThuc) {
        this.capCha = congThuc;
    }

    public Set<CongThuc> getCapCons() {
        return this.capCons;
    }

    public CongThuc capCons(Set<CongThuc> congThucs) {
        this.setCapCons(congThucs);
        return this;
    }

    public CongThuc addCapCon(CongThuc congThuc) {
        this.capCons.add(congThuc);
        congThuc.setCapCha(this);
        return this;
    }

    public CongThuc removeCapCon(CongThuc congThuc) {
        this.capCons.remove(congThuc);
        congThuc.setCapCha(null);
        return this;
    }

    public void setCapCons(Set<CongThuc> congThucs) {
        if (this.capCons != null) {
            this.capCons.forEach(i -> i.setCapCha(null));
        }
        if (congThucs != null) {
            congThucs.forEach(i -> i.setCapCha(this));
        }
        this.capCons = congThucs;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof CongThuc)) {
            return false;
        }
        return id != null && id.equals(((CongThuc) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "CongThuc{" +
            "id=" + getId() +
            ", maSo=" + getMaSo() +
            ", tenChiTieu='" + getTenChiTieu() + "'" +
            ", cap=" + getCap() +
            ", soDauKy=" + getSoDauKy() +
            ", soCuoiKy=" + getSoCuoiKy() +
            "}";
    }
}
