import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { isPresent } from 'app/core/util/operators';
import { ApplicationConfigService } from 'app/core/config/application-config.service';
import { createRequestOption } from 'app/core/request/request-util';
import { IKhachSan, getKhachSanIdentifier } from '../khach-san.model';

export type EntityResponseType = HttpResponse<IKhachSan>;
export type EntityArrayResponseType = HttpResponse<IKhachSan[]>;

@Injectable({ providedIn: 'root' })
export class KhachSanService {
  public resourceUrl = this.applicationConfigService.getEndpointFor('api/khach-sans');

  constructor(protected http: HttpClient, private applicationConfigService: ApplicationConfigService) {}

  create(khachSan: IKhachSan): Observable<EntityResponseType> {
    return this.http.post<IKhachSan>(this.resourceUrl, khachSan, { observe: 'response' });
  }

  update(khachSan: IKhachSan): Observable<EntityResponseType> {
    return this.http.put<IKhachSan>(`${this.resourceUrl}/${getKhachSanIdentifier(khachSan) as number}`, khachSan, { observe: 'response' });
  }

  partialUpdate(khachSan: IKhachSan): Observable<EntityResponseType> {
    return this.http.patch<IKhachSan>(`${this.resourceUrl}/${getKhachSanIdentifier(khachSan) as number}`, khachSan, {
      observe: 'response',
    });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IKhachSan>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IKhachSan[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  addKhachSanToCollectionIfMissing(khachSanCollection: IKhachSan[], ...khachSansToCheck: (IKhachSan | null | undefined)[]): IKhachSan[] {
    const khachSans: IKhachSan[] = khachSansToCheck.filter(isPresent);
    if (khachSans.length > 0) {
      const khachSanCollectionIdentifiers = khachSanCollection.map(khachSanItem => getKhachSanIdentifier(khachSanItem)!);
      const khachSansToAdd = khachSans.filter(khachSanItem => {
        const khachSanIdentifier = getKhachSanIdentifier(khachSanItem);
        if (khachSanIdentifier == null || khachSanCollectionIdentifiers.includes(khachSanIdentifier)) {
          return false;
        }
        khachSanCollectionIdentifiers.push(khachSanIdentifier);
        return true;
      });
      return [...khachSansToAdd, ...khachSanCollection];
    }
    return khachSanCollection;
  }
}
