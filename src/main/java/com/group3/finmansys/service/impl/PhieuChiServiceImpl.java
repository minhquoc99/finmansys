package com.group3.finmansys.service.impl;

import com.group3.finmansys.domain.PhieuChi;
import com.group3.finmansys.repository.PhieuChiRepository;
import com.group3.finmansys.service.PhieuChiService;
import com.group3.finmansys.service.dto.PhieuChiDTO;
import com.group3.finmansys.service.mapper.PhieuChiMapper;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link PhieuChi}.
 */
@Service
@Transactional
public class PhieuChiServiceImpl implements PhieuChiService {

    private final Logger log = LoggerFactory.getLogger(PhieuChiServiceImpl.class);

    private final PhieuChiRepository phieuChiRepository;

    private final PhieuChiMapper phieuChiMapper;

    public PhieuChiServiceImpl(PhieuChiRepository phieuChiRepository, PhieuChiMapper phieuChiMapper) {
        this.phieuChiRepository = phieuChiRepository;
        this.phieuChiMapper = phieuChiMapper;
    }

    @Override
    public PhieuChiDTO save(PhieuChiDTO phieuChiDTO) {
        log.debug("Request to save PhieuChi : {}", phieuChiDTO);
        PhieuChi phieuChi = phieuChiMapper.toEntity(phieuChiDTO);
        phieuChi = phieuChiRepository.save(phieuChi);
        return phieuChiMapper.toDto(phieuChi);
    }

    @Override
    public Optional<PhieuChiDTO> partialUpdate(PhieuChiDTO phieuChiDTO) {
        log.debug("Request to partially update PhieuChi : {}", phieuChiDTO);

        return phieuChiRepository
            .findById(phieuChiDTO.getId())
            .map(
                existingPhieuChi -> {
                    phieuChiMapper.partialUpdate(existingPhieuChi, phieuChiDTO);
                    return existingPhieuChi;
                }
            )
            .map(phieuChiRepository::save)
            .map(phieuChiMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<PhieuChiDTO> findAll(Pageable pageable) {
        log.debug("Request to get all PhieuChis");
        return phieuChiRepository.findAll(pageable).map(phieuChiMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<PhieuChiDTO> findOne(Long id) {
        log.debug("Request to get PhieuChi : {}", id);
        return phieuChiRepository.findById(id).map(phieuChiMapper::toDto);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete PhieuChi : {}", id);
        phieuChiRepository.deleteById(id);
    }
}
