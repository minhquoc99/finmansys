package com.group3.finmansys.domain;

import static org.assertj.core.api.Assertions.assertThat;

import com.group3.finmansys.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class KhoanPhaiThuTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(KhoanPhaiThu.class);
        KhoanPhaiThu khoanPhaiThu1 = new KhoanPhaiThu();
        khoanPhaiThu1.setId(1L);
        KhoanPhaiThu khoanPhaiThu2 = new KhoanPhaiThu();
        khoanPhaiThu2.setId(khoanPhaiThu1.getId());
        assertThat(khoanPhaiThu1).isEqualTo(khoanPhaiThu2);
        khoanPhaiThu2.setId(2L);
        assertThat(khoanPhaiThu1).isNotEqualTo(khoanPhaiThu2);
        khoanPhaiThu1.setId(null);
        assertThat(khoanPhaiThu1).isNotEqualTo(khoanPhaiThu2);
    }
}
