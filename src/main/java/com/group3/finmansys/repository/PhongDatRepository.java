package com.group3.finmansys.repository;

import com.group3.finmansys.domain.PhongDat;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data SQL repository for the PhongDat entity.
 */
@SuppressWarnings("unused")
@Repository
public interface PhongDatRepository extends JpaRepository<PhongDat, Long>, JpaSpecificationExecutor<PhongDat> {}
