package com.group3.finmansys.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.group3.finmansys.IntegrationTest;
import com.group3.finmansys.domain.KhachHang;
import com.group3.finmansys.domain.KhoanPhaiThu;
import com.group3.finmansys.domain.MaKeToan;
import com.group3.finmansys.domain.PhieuThu;
import com.group3.finmansys.domain.PhongDuocDat;
import com.group3.finmansys.repository.KhoanPhaiThuRepository;
import com.group3.finmansys.service.criteria.KhoanPhaiThuCriteria;
import com.group3.finmansys.service.dto.KhoanPhaiThuDTO;
import com.group3.finmansys.service.mapper.KhoanPhaiThuMapper;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link KhoanPhaiThuResource} REST controller.
 */
@IntegrationTest
@ExtendWith(MockitoExtension.class)
@AutoConfigureMockMvc
@WithMockUser
class KhoanPhaiThuResourceIT {

    private static final Double DEFAULT_SO_TIEN = 1D;
    private static final Double UPDATED_SO_TIEN = 2D;
    private static final Double SMALLER_SO_TIEN = 1D - 1D;

    private static final Instant DEFAULT_NGAY_TAO = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_NGAY_TAO = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final Instant DEFAULT_NGAY_PHAI_THU = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_NGAY_PHAI_THU = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final String DEFAULT_GHI_CHU = "AAAAAAAAAA";
    private static final String UPDATED_GHI_CHU = "BBBBBBBBBB";

    private static final String ENTITY_API_URL = "/api/khoan-phai-thus";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private KhoanPhaiThuRepository khoanPhaiThuRepository;

    @Autowired
    private KhoanPhaiThuMapper khoanPhaiThuMapper;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restKhoanPhaiThuMockMvc;

    private KhoanPhaiThu khoanPhaiThu;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static KhoanPhaiThu createEntity(EntityManager em) {
        KhoanPhaiThu khoanPhaiThu = new KhoanPhaiThu()
            .soTien(DEFAULT_SO_TIEN)
            .ngayTao(DEFAULT_NGAY_TAO)
            .ngayPhaiThu(DEFAULT_NGAY_PHAI_THU)
            .ghiChu(DEFAULT_GHI_CHU);
        // Add required entity
        KhachHang khachHang;
        if (TestUtil.findAll(em, KhachHang.class).isEmpty()) {
            khachHang = KhachHangResourceIT.createEntity(em);
            em.persist(khachHang);
            em.flush();
        } else {
            khachHang = TestUtil.findAll(em, KhachHang.class).get(0);
        }
        khoanPhaiThu.setKhachHang(khachHang);
        // Add required entity
        MaKeToan maKeToan;
        if (TestUtil.findAll(em, MaKeToan.class).isEmpty()) {
            maKeToan = MaKeToanResourceIT.createEntity(em);
            em.persist(maKeToan);
            em.flush();
        } else {
            maKeToan = TestUtil.findAll(em, MaKeToan.class).get(0);
        }
        khoanPhaiThu.setMaKeToan(maKeToan);
        return khoanPhaiThu;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static KhoanPhaiThu createUpdatedEntity(EntityManager em) {
        KhoanPhaiThu khoanPhaiThu = new KhoanPhaiThu()
            .soTien(UPDATED_SO_TIEN)
            .ngayTao(UPDATED_NGAY_TAO)
            .ngayPhaiThu(UPDATED_NGAY_PHAI_THU)
            .ghiChu(UPDATED_GHI_CHU);
        // Add required entity
        KhachHang khachHang;
        if (TestUtil.findAll(em, KhachHang.class).isEmpty()) {
            khachHang = KhachHangResourceIT.createUpdatedEntity(em);
            em.persist(khachHang);
            em.flush();
        } else {
            khachHang = TestUtil.findAll(em, KhachHang.class).get(0);
        }
        khoanPhaiThu.setKhachHang(khachHang);
        // Add required entity
        MaKeToan maKeToan;
        if (TestUtil.findAll(em, MaKeToan.class).isEmpty()) {
            maKeToan = MaKeToanResourceIT.createUpdatedEntity(em);
            em.persist(maKeToan);
            em.flush();
        } else {
            maKeToan = TestUtil.findAll(em, MaKeToan.class).get(0);
        }
        khoanPhaiThu.setMaKeToan(maKeToan);
        return khoanPhaiThu;
    }

    @BeforeEach
    public void initTest() {
        khoanPhaiThu = createEntity(em);
    }

    @Test
    @Transactional
    void createKhoanPhaiThu() throws Exception {
        int databaseSizeBeforeCreate = khoanPhaiThuRepository.findAll().size();
        // Create the KhoanPhaiThu
        KhoanPhaiThuDTO khoanPhaiThuDTO = khoanPhaiThuMapper.toDto(khoanPhaiThu);
        restKhoanPhaiThuMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(khoanPhaiThuDTO))
            )
            .andExpect(status().isCreated());

        // Validate the KhoanPhaiThu in the database
        List<KhoanPhaiThu> khoanPhaiThuList = khoanPhaiThuRepository.findAll();
        assertThat(khoanPhaiThuList).hasSize(databaseSizeBeforeCreate + 1);
        KhoanPhaiThu testKhoanPhaiThu = khoanPhaiThuList.get(khoanPhaiThuList.size() - 1);
        assertThat(testKhoanPhaiThu.getSoTien()).isEqualTo(DEFAULT_SO_TIEN);
        assertThat(testKhoanPhaiThu.getNgayTao()).isEqualTo(DEFAULT_NGAY_TAO);
        assertThat(testKhoanPhaiThu.getNgayPhaiThu()).isEqualTo(DEFAULT_NGAY_PHAI_THU);
        assertThat(testKhoanPhaiThu.getGhiChu()).isEqualTo(DEFAULT_GHI_CHU);
    }

    @Test
    @Transactional
    void createKhoanPhaiThuWithExistingId() throws Exception {
        // Create the KhoanPhaiThu with an existing ID
        khoanPhaiThu.setId(1L);
        KhoanPhaiThuDTO khoanPhaiThuDTO = khoanPhaiThuMapper.toDto(khoanPhaiThu);

        int databaseSizeBeforeCreate = khoanPhaiThuRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restKhoanPhaiThuMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(khoanPhaiThuDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the KhoanPhaiThu in the database
        List<KhoanPhaiThu> khoanPhaiThuList = khoanPhaiThuRepository.findAll();
        assertThat(khoanPhaiThuList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void checkSoTienIsRequired() throws Exception {
        int databaseSizeBeforeTest = khoanPhaiThuRepository.findAll().size();
        // set the field null
        khoanPhaiThu.setSoTien(null);

        // Create the KhoanPhaiThu, which fails.
        KhoanPhaiThuDTO khoanPhaiThuDTO = khoanPhaiThuMapper.toDto(khoanPhaiThu);

        restKhoanPhaiThuMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(khoanPhaiThuDTO))
            )
            .andExpect(status().isBadRequest());

        List<KhoanPhaiThu> khoanPhaiThuList = khoanPhaiThuRepository.findAll();
        assertThat(khoanPhaiThuList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkNgayTaoIsRequired() throws Exception {
        int databaseSizeBeforeTest = khoanPhaiThuRepository.findAll().size();
        // set the field null
        khoanPhaiThu.setNgayTao(null);

        // Create the KhoanPhaiThu, which fails.
        KhoanPhaiThuDTO khoanPhaiThuDTO = khoanPhaiThuMapper.toDto(khoanPhaiThu);

        restKhoanPhaiThuMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(khoanPhaiThuDTO))
            )
            .andExpect(status().isBadRequest());

        List<KhoanPhaiThu> khoanPhaiThuList = khoanPhaiThuRepository.findAll();
        assertThat(khoanPhaiThuList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkNgayPhaiThuIsRequired() throws Exception {
        int databaseSizeBeforeTest = khoanPhaiThuRepository.findAll().size();
        // set the field null
        khoanPhaiThu.setNgayPhaiThu(null);

        // Create the KhoanPhaiThu, which fails.
        KhoanPhaiThuDTO khoanPhaiThuDTO = khoanPhaiThuMapper.toDto(khoanPhaiThu);

        restKhoanPhaiThuMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(khoanPhaiThuDTO))
            )
            .andExpect(status().isBadRequest());

        List<KhoanPhaiThu> khoanPhaiThuList = khoanPhaiThuRepository.findAll();
        assertThat(khoanPhaiThuList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void getAllKhoanPhaiThus() throws Exception {
        // Initialize the database
        khoanPhaiThuRepository.saveAndFlush(khoanPhaiThu);

        // Get all the khoanPhaiThuList
        restKhoanPhaiThuMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(khoanPhaiThu.getId().intValue())))
            .andExpect(jsonPath("$.[*].soTien").value(hasItem(DEFAULT_SO_TIEN.doubleValue())))
            .andExpect(jsonPath("$.[*].ngayTao").value(hasItem(DEFAULT_NGAY_TAO.toString())))
            .andExpect(jsonPath("$.[*].ngayPhaiThu").value(hasItem(DEFAULT_NGAY_PHAI_THU.toString())))
            .andExpect(jsonPath("$.[*].ghiChu").value(hasItem(DEFAULT_GHI_CHU)));
    }

    @Test
    @Transactional
    void getKhoanPhaiThu() throws Exception {
        // Initialize the database
        khoanPhaiThuRepository.saveAndFlush(khoanPhaiThu);

        // Get the khoanPhaiThu
        restKhoanPhaiThuMockMvc
            .perform(get(ENTITY_API_URL_ID, khoanPhaiThu.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(khoanPhaiThu.getId().intValue()))
            .andExpect(jsonPath("$.soTien").value(DEFAULT_SO_TIEN.doubleValue()))
            .andExpect(jsonPath("$.ngayTao").value(DEFAULT_NGAY_TAO.toString()))
            .andExpect(jsonPath("$.ngayPhaiThu").value(DEFAULT_NGAY_PHAI_THU.toString()))
            .andExpect(jsonPath("$.ghiChu").value(DEFAULT_GHI_CHU));
    }

    @Test
    @Transactional
    void getKhoanPhaiThusByIdFiltering() throws Exception {
        // Initialize the database
        khoanPhaiThuRepository.saveAndFlush(khoanPhaiThu);

        Long id = khoanPhaiThu.getId();

        defaultKhoanPhaiThuShouldBeFound("id.equals=" + id);
        defaultKhoanPhaiThuShouldNotBeFound("id.notEquals=" + id);

        defaultKhoanPhaiThuShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultKhoanPhaiThuShouldNotBeFound("id.greaterThan=" + id);

        defaultKhoanPhaiThuShouldBeFound("id.lessThanOrEqual=" + id);
        defaultKhoanPhaiThuShouldNotBeFound("id.lessThan=" + id);
    }

    @Test
    @Transactional
    void getAllKhoanPhaiThusBySoTienIsEqualToSomething() throws Exception {
        // Initialize the database
        khoanPhaiThuRepository.saveAndFlush(khoanPhaiThu);

        // Get all the khoanPhaiThuList where soTien equals to DEFAULT_SO_TIEN
        defaultKhoanPhaiThuShouldBeFound("soTien.equals=" + DEFAULT_SO_TIEN);

        // Get all the khoanPhaiThuList where soTien equals to UPDATED_SO_TIEN
        defaultKhoanPhaiThuShouldNotBeFound("soTien.equals=" + UPDATED_SO_TIEN);
    }

    @Test
    @Transactional
    void getAllKhoanPhaiThusBySoTienIsNotEqualToSomething() throws Exception {
        // Initialize the database
        khoanPhaiThuRepository.saveAndFlush(khoanPhaiThu);

        // Get all the khoanPhaiThuList where soTien not equals to DEFAULT_SO_TIEN
        defaultKhoanPhaiThuShouldNotBeFound("soTien.notEquals=" + DEFAULT_SO_TIEN);

        // Get all the khoanPhaiThuList where soTien not equals to UPDATED_SO_TIEN
        defaultKhoanPhaiThuShouldBeFound("soTien.notEquals=" + UPDATED_SO_TIEN);
    }

    @Test
    @Transactional
    void getAllKhoanPhaiThusBySoTienIsInShouldWork() throws Exception {
        // Initialize the database
        khoanPhaiThuRepository.saveAndFlush(khoanPhaiThu);

        // Get all the khoanPhaiThuList where soTien in DEFAULT_SO_TIEN or UPDATED_SO_TIEN
        defaultKhoanPhaiThuShouldBeFound("soTien.in=" + DEFAULT_SO_TIEN + "," + UPDATED_SO_TIEN);

        // Get all the khoanPhaiThuList where soTien equals to UPDATED_SO_TIEN
        defaultKhoanPhaiThuShouldNotBeFound("soTien.in=" + UPDATED_SO_TIEN);
    }

    @Test
    @Transactional
    void getAllKhoanPhaiThusBySoTienIsNullOrNotNull() throws Exception {
        // Initialize the database
        khoanPhaiThuRepository.saveAndFlush(khoanPhaiThu);

        // Get all the khoanPhaiThuList where soTien is not null
        defaultKhoanPhaiThuShouldBeFound("soTien.specified=true");

        // Get all the khoanPhaiThuList where soTien is null
        defaultKhoanPhaiThuShouldNotBeFound("soTien.specified=false");
    }

    @Test
    @Transactional
    void getAllKhoanPhaiThusBySoTienIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        khoanPhaiThuRepository.saveAndFlush(khoanPhaiThu);

        // Get all the khoanPhaiThuList where soTien is greater than or equal to DEFAULT_SO_TIEN
        defaultKhoanPhaiThuShouldBeFound("soTien.greaterThanOrEqual=" + DEFAULT_SO_TIEN);

        // Get all the khoanPhaiThuList where soTien is greater than or equal to UPDATED_SO_TIEN
        defaultKhoanPhaiThuShouldNotBeFound("soTien.greaterThanOrEqual=" + UPDATED_SO_TIEN);
    }

    @Test
    @Transactional
    void getAllKhoanPhaiThusBySoTienIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        khoanPhaiThuRepository.saveAndFlush(khoanPhaiThu);

        // Get all the khoanPhaiThuList where soTien is less than or equal to DEFAULT_SO_TIEN
        defaultKhoanPhaiThuShouldBeFound("soTien.lessThanOrEqual=" + DEFAULT_SO_TIEN);

        // Get all the khoanPhaiThuList where soTien is less than or equal to SMALLER_SO_TIEN
        defaultKhoanPhaiThuShouldNotBeFound("soTien.lessThanOrEqual=" + SMALLER_SO_TIEN);
    }

    @Test
    @Transactional
    void getAllKhoanPhaiThusBySoTienIsLessThanSomething() throws Exception {
        // Initialize the database
        khoanPhaiThuRepository.saveAndFlush(khoanPhaiThu);

        // Get all the khoanPhaiThuList where soTien is less than DEFAULT_SO_TIEN
        defaultKhoanPhaiThuShouldNotBeFound("soTien.lessThan=" + DEFAULT_SO_TIEN);

        // Get all the khoanPhaiThuList where soTien is less than UPDATED_SO_TIEN
        defaultKhoanPhaiThuShouldBeFound("soTien.lessThan=" + UPDATED_SO_TIEN);
    }

    @Test
    @Transactional
    void getAllKhoanPhaiThusBySoTienIsGreaterThanSomething() throws Exception {
        // Initialize the database
        khoanPhaiThuRepository.saveAndFlush(khoanPhaiThu);

        // Get all the khoanPhaiThuList where soTien is greater than DEFAULT_SO_TIEN
        defaultKhoanPhaiThuShouldNotBeFound("soTien.greaterThan=" + DEFAULT_SO_TIEN);

        // Get all the khoanPhaiThuList where soTien is greater than SMALLER_SO_TIEN
        defaultKhoanPhaiThuShouldBeFound("soTien.greaterThan=" + SMALLER_SO_TIEN);
    }

    @Test
    @Transactional
    void getAllKhoanPhaiThusByNgayTaoIsEqualToSomething() throws Exception {
        // Initialize the database
        khoanPhaiThuRepository.saveAndFlush(khoanPhaiThu);

        // Get all the khoanPhaiThuList where ngayTao equals to DEFAULT_NGAY_TAO
        defaultKhoanPhaiThuShouldBeFound("ngayTao.equals=" + DEFAULT_NGAY_TAO);

        // Get all the khoanPhaiThuList where ngayTao equals to UPDATED_NGAY_TAO
        defaultKhoanPhaiThuShouldNotBeFound("ngayTao.equals=" + UPDATED_NGAY_TAO);
    }

    @Test
    @Transactional
    void getAllKhoanPhaiThusByNgayTaoIsNotEqualToSomething() throws Exception {
        // Initialize the database
        khoanPhaiThuRepository.saveAndFlush(khoanPhaiThu);

        // Get all the khoanPhaiThuList where ngayTao not equals to DEFAULT_NGAY_TAO
        defaultKhoanPhaiThuShouldNotBeFound("ngayTao.notEquals=" + DEFAULT_NGAY_TAO);

        // Get all the khoanPhaiThuList where ngayTao not equals to UPDATED_NGAY_TAO
        defaultKhoanPhaiThuShouldBeFound("ngayTao.notEquals=" + UPDATED_NGAY_TAO);
    }

    @Test
    @Transactional
    void getAllKhoanPhaiThusByNgayTaoIsInShouldWork() throws Exception {
        // Initialize the database
        khoanPhaiThuRepository.saveAndFlush(khoanPhaiThu);

        // Get all the khoanPhaiThuList where ngayTao in DEFAULT_NGAY_TAO or UPDATED_NGAY_TAO
        defaultKhoanPhaiThuShouldBeFound("ngayTao.in=" + DEFAULT_NGAY_TAO + "," + UPDATED_NGAY_TAO);

        // Get all the khoanPhaiThuList where ngayTao equals to UPDATED_NGAY_TAO
        defaultKhoanPhaiThuShouldNotBeFound("ngayTao.in=" + UPDATED_NGAY_TAO);
    }

    @Test
    @Transactional
    void getAllKhoanPhaiThusByNgayTaoIsNullOrNotNull() throws Exception {
        // Initialize the database
        khoanPhaiThuRepository.saveAndFlush(khoanPhaiThu);

        // Get all the khoanPhaiThuList where ngayTao is not null
        defaultKhoanPhaiThuShouldBeFound("ngayTao.specified=true");

        // Get all the khoanPhaiThuList where ngayTao is null
        defaultKhoanPhaiThuShouldNotBeFound("ngayTao.specified=false");
    }

    @Test
    @Transactional
    void getAllKhoanPhaiThusByNgayPhaiThuIsEqualToSomething() throws Exception {
        // Initialize the database
        khoanPhaiThuRepository.saveAndFlush(khoanPhaiThu);

        // Get all the khoanPhaiThuList where ngayPhaiThu equals to DEFAULT_NGAY_PHAI_THU
        defaultKhoanPhaiThuShouldBeFound("ngayPhaiThu.equals=" + DEFAULT_NGAY_PHAI_THU);

        // Get all the khoanPhaiThuList where ngayPhaiThu equals to UPDATED_NGAY_PHAI_THU
        defaultKhoanPhaiThuShouldNotBeFound("ngayPhaiThu.equals=" + UPDATED_NGAY_PHAI_THU);
    }

    @Test
    @Transactional
    void getAllKhoanPhaiThusByNgayPhaiThuIsNotEqualToSomething() throws Exception {
        // Initialize the database
        khoanPhaiThuRepository.saveAndFlush(khoanPhaiThu);

        // Get all the khoanPhaiThuList where ngayPhaiThu not equals to DEFAULT_NGAY_PHAI_THU
        defaultKhoanPhaiThuShouldNotBeFound("ngayPhaiThu.notEquals=" + DEFAULT_NGAY_PHAI_THU);

        // Get all the khoanPhaiThuList where ngayPhaiThu not equals to UPDATED_NGAY_PHAI_THU
        defaultKhoanPhaiThuShouldBeFound("ngayPhaiThu.notEquals=" + UPDATED_NGAY_PHAI_THU);
    }

    @Test
    @Transactional
    void getAllKhoanPhaiThusByNgayPhaiThuIsInShouldWork() throws Exception {
        // Initialize the database
        khoanPhaiThuRepository.saveAndFlush(khoanPhaiThu);

        // Get all the khoanPhaiThuList where ngayPhaiThu in DEFAULT_NGAY_PHAI_THU or UPDATED_NGAY_PHAI_THU
        defaultKhoanPhaiThuShouldBeFound("ngayPhaiThu.in=" + DEFAULT_NGAY_PHAI_THU + "," + UPDATED_NGAY_PHAI_THU);

        // Get all the khoanPhaiThuList where ngayPhaiThu equals to UPDATED_NGAY_PHAI_THU
        defaultKhoanPhaiThuShouldNotBeFound("ngayPhaiThu.in=" + UPDATED_NGAY_PHAI_THU);
    }

    @Test
    @Transactional
    void getAllKhoanPhaiThusByNgayPhaiThuIsNullOrNotNull() throws Exception {
        // Initialize the database
        khoanPhaiThuRepository.saveAndFlush(khoanPhaiThu);

        // Get all the khoanPhaiThuList where ngayPhaiThu is not null
        defaultKhoanPhaiThuShouldBeFound("ngayPhaiThu.specified=true");

        // Get all the khoanPhaiThuList where ngayPhaiThu is null
        defaultKhoanPhaiThuShouldNotBeFound("ngayPhaiThu.specified=false");
    }

    @Test
    @Transactional
    void getAllKhoanPhaiThusByGhiChuIsEqualToSomething() throws Exception {
        // Initialize the database
        khoanPhaiThuRepository.saveAndFlush(khoanPhaiThu);

        // Get all the khoanPhaiThuList where ghiChu equals to DEFAULT_GHI_CHU
        defaultKhoanPhaiThuShouldBeFound("ghiChu.equals=" + DEFAULT_GHI_CHU);

        // Get all the khoanPhaiThuList where ghiChu equals to UPDATED_GHI_CHU
        defaultKhoanPhaiThuShouldNotBeFound("ghiChu.equals=" + UPDATED_GHI_CHU);
    }

    @Test
    @Transactional
    void getAllKhoanPhaiThusByGhiChuIsNotEqualToSomething() throws Exception {
        // Initialize the database
        khoanPhaiThuRepository.saveAndFlush(khoanPhaiThu);

        // Get all the khoanPhaiThuList where ghiChu not equals to DEFAULT_GHI_CHU
        defaultKhoanPhaiThuShouldNotBeFound("ghiChu.notEquals=" + DEFAULT_GHI_CHU);

        // Get all the khoanPhaiThuList where ghiChu not equals to UPDATED_GHI_CHU
        defaultKhoanPhaiThuShouldBeFound("ghiChu.notEquals=" + UPDATED_GHI_CHU);
    }

    @Test
    @Transactional
    void getAllKhoanPhaiThusByGhiChuIsInShouldWork() throws Exception {
        // Initialize the database
        khoanPhaiThuRepository.saveAndFlush(khoanPhaiThu);

        // Get all the khoanPhaiThuList where ghiChu in DEFAULT_GHI_CHU or UPDATED_GHI_CHU
        defaultKhoanPhaiThuShouldBeFound("ghiChu.in=" + DEFAULT_GHI_CHU + "," + UPDATED_GHI_CHU);

        // Get all the khoanPhaiThuList where ghiChu equals to UPDATED_GHI_CHU
        defaultKhoanPhaiThuShouldNotBeFound("ghiChu.in=" + UPDATED_GHI_CHU);
    }

    @Test
    @Transactional
    void getAllKhoanPhaiThusByGhiChuIsNullOrNotNull() throws Exception {
        // Initialize the database
        khoanPhaiThuRepository.saveAndFlush(khoanPhaiThu);

        // Get all the khoanPhaiThuList where ghiChu is not null
        defaultKhoanPhaiThuShouldBeFound("ghiChu.specified=true");

        // Get all the khoanPhaiThuList where ghiChu is null
        defaultKhoanPhaiThuShouldNotBeFound("ghiChu.specified=false");
    }

    @Test
    @Transactional
    void getAllKhoanPhaiThusByGhiChuContainsSomething() throws Exception {
        // Initialize the database
        khoanPhaiThuRepository.saveAndFlush(khoanPhaiThu);

        // Get all the khoanPhaiThuList where ghiChu contains DEFAULT_GHI_CHU
        defaultKhoanPhaiThuShouldBeFound("ghiChu.contains=" + DEFAULT_GHI_CHU);

        // Get all the khoanPhaiThuList where ghiChu contains UPDATED_GHI_CHU
        defaultKhoanPhaiThuShouldNotBeFound("ghiChu.contains=" + UPDATED_GHI_CHU);
    }

    @Test
    @Transactional
    void getAllKhoanPhaiThusByGhiChuNotContainsSomething() throws Exception {
        // Initialize the database
        khoanPhaiThuRepository.saveAndFlush(khoanPhaiThu);

        // Get all the khoanPhaiThuList where ghiChu does not contain DEFAULT_GHI_CHU
        defaultKhoanPhaiThuShouldNotBeFound("ghiChu.doesNotContain=" + DEFAULT_GHI_CHU);

        // Get all the khoanPhaiThuList where ghiChu does not contain UPDATED_GHI_CHU
        defaultKhoanPhaiThuShouldBeFound("ghiChu.doesNotContain=" + UPDATED_GHI_CHU);
    }

    @Test
    @Transactional
    void getAllKhoanPhaiThusByKhachHangIsEqualToSomething() throws Exception {
        // Initialize the database
        khoanPhaiThuRepository.saveAndFlush(khoanPhaiThu);
        KhachHang khachHang = KhachHangResourceIT.createEntity(em);
        em.persist(khachHang);
        em.flush();
        khoanPhaiThu.setKhachHang(khachHang);
        khoanPhaiThuRepository.saveAndFlush(khoanPhaiThu);
        Long khachHangId = khachHang.getId();

        // Get all the khoanPhaiThuList where khachHang equals to khachHangId
        defaultKhoanPhaiThuShouldBeFound("khachHangId.equals=" + khachHangId);

        // Get all the khoanPhaiThuList where khachHang equals to (khachHangId + 1)
        defaultKhoanPhaiThuShouldNotBeFound("khachHangId.equals=" + (khachHangId + 1));
    }

    @Test
    @Transactional
    void getAllKhoanPhaiThusByMaKeToanIsEqualToSomething() throws Exception {
        // Initialize the database
        khoanPhaiThuRepository.saveAndFlush(khoanPhaiThu);
        MaKeToan maKeToan = MaKeToanResourceIT.createEntity(em);
        em.persist(maKeToan);
        em.flush();
        khoanPhaiThu.setMaKeToan(maKeToan);
        khoanPhaiThuRepository.saveAndFlush(khoanPhaiThu);
        Long maKeToanId = maKeToan.getId();

        // Get all the khoanPhaiThuList where maKeToan equals to maKeToanId
        defaultKhoanPhaiThuShouldBeFound("maKeToanId.equals=" + maKeToanId);

        // Get all the khoanPhaiThuList where maKeToan equals to (maKeToanId + 1)
        defaultKhoanPhaiThuShouldNotBeFound("maKeToanId.equals=" + (maKeToanId + 1));
    }

    @Test
    @Transactional
    void getAllKhoanPhaiThusByPhieuThuIsEqualToSomething() throws Exception {
        // Initialize the database
        khoanPhaiThuRepository.saveAndFlush(khoanPhaiThu);
        PhieuThu phieuThu = PhieuThuResourceIT.createEntity(em);
        em.persist(phieuThu);
        em.flush();
        khoanPhaiThu.addPhieuThu(phieuThu);
        khoanPhaiThuRepository.saveAndFlush(khoanPhaiThu);
        Long phieuThuId = phieuThu.getId();

        // Get all the khoanPhaiThuList where phieuThu equals to phieuThuId
        defaultKhoanPhaiThuShouldBeFound("phieuThuId.equals=" + phieuThuId);

        // Get all the khoanPhaiThuList where phieuThu equals to (phieuThuId + 1)
        defaultKhoanPhaiThuShouldNotBeFound("phieuThuId.equals=" + (phieuThuId + 1));
    }

    @Test
    @Transactional
    void getAllKhoanPhaiThusByPhongDuocDatIsEqualToSomething() throws Exception {
        // Initialize the database
        khoanPhaiThuRepository.saveAndFlush(khoanPhaiThu);
        PhongDuocDat phongDuocDat = PhongDuocDatResourceIT.createEntity(em);
        em.persist(phongDuocDat);
        em.flush();
        khoanPhaiThu.setPhongDuocDat(phongDuocDat);
        khoanPhaiThuRepository.saveAndFlush(khoanPhaiThu);
        Long phongDuocDatId = phongDuocDat.getId();

        // Get all the khoanPhaiThuList where phongDuocDat equals to phongDuocDatId
        defaultKhoanPhaiThuShouldBeFound("phongDuocDatId.equals=" + phongDuocDatId);

        // Get all the khoanPhaiThuList where phongDuocDat equals to (phongDuocDatId + 1)
        defaultKhoanPhaiThuShouldNotBeFound("phongDuocDatId.equals=" + (phongDuocDatId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultKhoanPhaiThuShouldBeFound(String filter) throws Exception {
        restKhoanPhaiThuMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(khoanPhaiThu.getId().intValue())))
            .andExpect(jsonPath("$.[*].soTien").value(hasItem(DEFAULT_SO_TIEN.doubleValue())))
            .andExpect(jsonPath("$.[*].ngayTao").value(hasItem(DEFAULT_NGAY_TAO.toString())))
            .andExpect(jsonPath("$.[*].ngayPhaiThu").value(hasItem(DEFAULT_NGAY_PHAI_THU.toString())))
            .andExpect(jsonPath("$.[*].ghiChu").value(hasItem(DEFAULT_GHI_CHU)));

        // Check, that the count call also returns 1
        restKhoanPhaiThuMockMvc
            .perform(get(ENTITY_API_URL + "/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultKhoanPhaiThuShouldNotBeFound(String filter) throws Exception {
        restKhoanPhaiThuMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restKhoanPhaiThuMockMvc
            .perform(get(ENTITY_API_URL + "/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }

    @Test
    @Transactional
    void getNonExistingKhoanPhaiThu() throws Exception {
        // Get the khoanPhaiThu
        restKhoanPhaiThuMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putNewKhoanPhaiThu() throws Exception {
        // Initialize the database
        khoanPhaiThuRepository.saveAndFlush(khoanPhaiThu);

        int databaseSizeBeforeUpdate = khoanPhaiThuRepository.findAll().size();

        // Update the khoanPhaiThu
        KhoanPhaiThu updatedKhoanPhaiThu = khoanPhaiThuRepository.findById(khoanPhaiThu.getId()).get();
        // Disconnect from session so that the updates on updatedKhoanPhaiThu are not directly saved in db
        em.detach(updatedKhoanPhaiThu);
        updatedKhoanPhaiThu.soTien(UPDATED_SO_TIEN).ngayTao(UPDATED_NGAY_TAO).ngayPhaiThu(UPDATED_NGAY_PHAI_THU).ghiChu(UPDATED_GHI_CHU);
        KhoanPhaiThuDTO khoanPhaiThuDTO = khoanPhaiThuMapper.toDto(updatedKhoanPhaiThu);

        restKhoanPhaiThuMockMvc
            .perform(
                put(ENTITY_API_URL_ID, khoanPhaiThuDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(khoanPhaiThuDTO))
            )
            .andExpect(status().isOk());

        // Validate the KhoanPhaiThu in the database
        List<KhoanPhaiThu> khoanPhaiThuList = khoanPhaiThuRepository.findAll();
        assertThat(khoanPhaiThuList).hasSize(databaseSizeBeforeUpdate);
        KhoanPhaiThu testKhoanPhaiThu = khoanPhaiThuList.get(khoanPhaiThuList.size() - 1);
        assertThat(testKhoanPhaiThu.getSoTien()).isEqualTo(UPDATED_SO_TIEN);
        assertThat(testKhoanPhaiThu.getNgayTao()).isEqualTo(UPDATED_NGAY_TAO);
        assertThat(testKhoanPhaiThu.getNgayPhaiThu()).isEqualTo(UPDATED_NGAY_PHAI_THU);
        assertThat(testKhoanPhaiThu.getGhiChu()).isEqualTo(UPDATED_GHI_CHU);
    }

    @Test
    @Transactional
    void putNonExistingKhoanPhaiThu() throws Exception {
        int databaseSizeBeforeUpdate = khoanPhaiThuRepository.findAll().size();
        khoanPhaiThu.setId(count.incrementAndGet());

        // Create the KhoanPhaiThu
        KhoanPhaiThuDTO khoanPhaiThuDTO = khoanPhaiThuMapper.toDto(khoanPhaiThu);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restKhoanPhaiThuMockMvc
            .perform(
                put(ENTITY_API_URL_ID, khoanPhaiThuDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(khoanPhaiThuDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the KhoanPhaiThu in the database
        List<KhoanPhaiThu> khoanPhaiThuList = khoanPhaiThuRepository.findAll();
        assertThat(khoanPhaiThuList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchKhoanPhaiThu() throws Exception {
        int databaseSizeBeforeUpdate = khoanPhaiThuRepository.findAll().size();
        khoanPhaiThu.setId(count.incrementAndGet());

        // Create the KhoanPhaiThu
        KhoanPhaiThuDTO khoanPhaiThuDTO = khoanPhaiThuMapper.toDto(khoanPhaiThu);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restKhoanPhaiThuMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(khoanPhaiThuDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the KhoanPhaiThu in the database
        List<KhoanPhaiThu> khoanPhaiThuList = khoanPhaiThuRepository.findAll();
        assertThat(khoanPhaiThuList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamKhoanPhaiThu() throws Exception {
        int databaseSizeBeforeUpdate = khoanPhaiThuRepository.findAll().size();
        khoanPhaiThu.setId(count.incrementAndGet());

        // Create the KhoanPhaiThu
        KhoanPhaiThuDTO khoanPhaiThuDTO = khoanPhaiThuMapper.toDto(khoanPhaiThu);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restKhoanPhaiThuMockMvc
            .perform(
                put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(khoanPhaiThuDTO))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the KhoanPhaiThu in the database
        List<KhoanPhaiThu> khoanPhaiThuList = khoanPhaiThuRepository.findAll();
        assertThat(khoanPhaiThuList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateKhoanPhaiThuWithPatch() throws Exception {
        // Initialize the database
        khoanPhaiThuRepository.saveAndFlush(khoanPhaiThu);

        int databaseSizeBeforeUpdate = khoanPhaiThuRepository.findAll().size();

        // Update the khoanPhaiThu using partial update
        KhoanPhaiThu partialUpdatedKhoanPhaiThu = new KhoanPhaiThu();
        partialUpdatedKhoanPhaiThu.setId(khoanPhaiThu.getId());

        partialUpdatedKhoanPhaiThu.soTien(UPDATED_SO_TIEN).ngayTao(UPDATED_NGAY_TAO);

        restKhoanPhaiThuMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedKhoanPhaiThu.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedKhoanPhaiThu))
            )
            .andExpect(status().isOk());

        // Validate the KhoanPhaiThu in the database
        List<KhoanPhaiThu> khoanPhaiThuList = khoanPhaiThuRepository.findAll();
        assertThat(khoanPhaiThuList).hasSize(databaseSizeBeforeUpdate);
        KhoanPhaiThu testKhoanPhaiThu = khoanPhaiThuList.get(khoanPhaiThuList.size() - 1);
        assertThat(testKhoanPhaiThu.getSoTien()).isEqualTo(UPDATED_SO_TIEN);
        assertThat(testKhoanPhaiThu.getNgayTao()).isEqualTo(UPDATED_NGAY_TAO);
        assertThat(testKhoanPhaiThu.getNgayPhaiThu()).isEqualTo(DEFAULT_NGAY_PHAI_THU);
        assertThat(testKhoanPhaiThu.getGhiChu()).isEqualTo(DEFAULT_GHI_CHU);
    }

    @Test
    @Transactional
    void fullUpdateKhoanPhaiThuWithPatch() throws Exception {
        // Initialize the database
        khoanPhaiThuRepository.saveAndFlush(khoanPhaiThu);

        int databaseSizeBeforeUpdate = khoanPhaiThuRepository.findAll().size();

        // Update the khoanPhaiThu using partial update
        KhoanPhaiThu partialUpdatedKhoanPhaiThu = new KhoanPhaiThu();
        partialUpdatedKhoanPhaiThu.setId(khoanPhaiThu.getId());

        partialUpdatedKhoanPhaiThu
            .soTien(UPDATED_SO_TIEN)
            .ngayTao(UPDATED_NGAY_TAO)
            .ngayPhaiThu(UPDATED_NGAY_PHAI_THU)
            .ghiChu(UPDATED_GHI_CHU);

        restKhoanPhaiThuMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedKhoanPhaiThu.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedKhoanPhaiThu))
            )
            .andExpect(status().isOk());

        // Validate the KhoanPhaiThu in the database
        List<KhoanPhaiThu> khoanPhaiThuList = khoanPhaiThuRepository.findAll();
        assertThat(khoanPhaiThuList).hasSize(databaseSizeBeforeUpdate);
        KhoanPhaiThu testKhoanPhaiThu = khoanPhaiThuList.get(khoanPhaiThuList.size() - 1);
        assertThat(testKhoanPhaiThu.getSoTien()).isEqualTo(UPDATED_SO_TIEN);
        assertThat(testKhoanPhaiThu.getNgayTao()).isEqualTo(UPDATED_NGAY_TAO);
        assertThat(testKhoanPhaiThu.getNgayPhaiThu()).isEqualTo(UPDATED_NGAY_PHAI_THU);
        assertThat(testKhoanPhaiThu.getGhiChu()).isEqualTo(UPDATED_GHI_CHU);
    }

    @Test
    @Transactional
    void patchNonExistingKhoanPhaiThu() throws Exception {
        int databaseSizeBeforeUpdate = khoanPhaiThuRepository.findAll().size();
        khoanPhaiThu.setId(count.incrementAndGet());

        // Create the KhoanPhaiThu
        KhoanPhaiThuDTO khoanPhaiThuDTO = khoanPhaiThuMapper.toDto(khoanPhaiThu);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restKhoanPhaiThuMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, khoanPhaiThuDTO.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(khoanPhaiThuDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the KhoanPhaiThu in the database
        List<KhoanPhaiThu> khoanPhaiThuList = khoanPhaiThuRepository.findAll();
        assertThat(khoanPhaiThuList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchKhoanPhaiThu() throws Exception {
        int databaseSizeBeforeUpdate = khoanPhaiThuRepository.findAll().size();
        khoanPhaiThu.setId(count.incrementAndGet());

        // Create the KhoanPhaiThu
        KhoanPhaiThuDTO khoanPhaiThuDTO = khoanPhaiThuMapper.toDto(khoanPhaiThu);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restKhoanPhaiThuMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(khoanPhaiThuDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the KhoanPhaiThu in the database
        List<KhoanPhaiThu> khoanPhaiThuList = khoanPhaiThuRepository.findAll();
        assertThat(khoanPhaiThuList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamKhoanPhaiThu() throws Exception {
        int databaseSizeBeforeUpdate = khoanPhaiThuRepository.findAll().size();
        khoanPhaiThu.setId(count.incrementAndGet());

        // Create the KhoanPhaiThu
        KhoanPhaiThuDTO khoanPhaiThuDTO = khoanPhaiThuMapper.toDto(khoanPhaiThu);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restKhoanPhaiThuMockMvc
            .perform(
                patch(ENTITY_API_URL)
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(khoanPhaiThuDTO))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the KhoanPhaiThu in the database
        List<KhoanPhaiThu> khoanPhaiThuList = khoanPhaiThuRepository.findAll();
        assertThat(khoanPhaiThuList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteKhoanPhaiThu() throws Exception {
        // Initialize the database
        khoanPhaiThuRepository.saveAndFlush(khoanPhaiThu);

        int databaseSizeBeforeDelete = khoanPhaiThuRepository.findAll().size();

        // Delete the khoanPhaiThu
        restKhoanPhaiThuMockMvc
            .perform(delete(ENTITY_API_URL_ID, khoanPhaiThu.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<KhoanPhaiThu> khoanPhaiThuList = khoanPhaiThuRepository.findAll();
        assertThat(khoanPhaiThuList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
