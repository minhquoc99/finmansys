import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { ToanTuDetailComponent } from './toan-tu-detail.component';

describe('Component Tests', () => {
  describe('ToanTu Management Detail Component', () => {
    let comp: ToanTuDetailComponent;
    let fixture: ComponentFixture<ToanTuDetailComponent>;

    beforeEach(() => {
      TestBed.configureTestingModule({
        declarations: [ToanTuDetailComponent],
        providers: [
          {
            provide: ActivatedRoute,
            useValue: { data: of({ toanTu: { id: 123 } }) },
          },
        ],
      })
        .overrideTemplate(ToanTuDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(ToanTuDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should load toanTu on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.toanTu).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
