import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { MaKeToanDetailComponent } from './ma-ke-toan-detail.component';

describe('Component Tests', () => {
  describe('MaKeToan Management Detail Component', () => {
    let comp: MaKeToanDetailComponent;
    let fixture: ComponentFixture<MaKeToanDetailComponent>;

    beforeEach(() => {
      TestBed.configureTestingModule({
        declarations: [MaKeToanDetailComponent],
        providers: [
          {
            provide: ActivatedRoute,
            useValue: { data: of({ maKeToan: { id: 123 } }) },
          },
        ],
      })
        .overrideTemplate(MaKeToanDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(MaKeToanDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should load maKeToan on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.maKeToan).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
