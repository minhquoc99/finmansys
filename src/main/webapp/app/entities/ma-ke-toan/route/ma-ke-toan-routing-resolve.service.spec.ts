jest.mock('@angular/router');

import { TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ActivatedRouteSnapshot, Router } from '@angular/router';
import { of } from 'rxjs';

import { IMaKeToan, MaKeToan } from '../ma-ke-toan.model';
import { MaKeToanService } from '../service/ma-ke-toan.service';

import { MaKeToanRoutingResolveService } from './ma-ke-toan-routing-resolve.service';

describe('Service Tests', () => {
  describe('MaKeToan routing resolve service', () => {
    let mockRouter: Router;
    let mockActivatedRouteSnapshot: ActivatedRouteSnapshot;
    let routingResolveService: MaKeToanRoutingResolveService;
    let service: MaKeToanService;
    let resultMaKeToan: IMaKeToan | undefined;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule],
        providers: [Router, ActivatedRouteSnapshot],
      });
      mockRouter = TestBed.inject(Router);
      mockActivatedRouteSnapshot = TestBed.inject(ActivatedRouteSnapshot);
      routingResolveService = TestBed.inject(MaKeToanRoutingResolveService);
      service = TestBed.inject(MaKeToanService);
      resultMaKeToan = undefined;
    });

    describe('resolve', () => {
      it('should return IMaKeToan returned by find', () => {
        // GIVEN
        service.find = jest.fn(id => of(new HttpResponse({ body: { id } })));
        mockActivatedRouteSnapshot.params = { id: 123 };

        // WHEN
        routingResolveService.resolve(mockActivatedRouteSnapshot).subscribe(result => {
          resultMaKeToan = result;
        });

        // THEN
        expect(service.find).toBeCalledWith(123);
        expect(resultMaKeToan).toEqual({ id: 123 });
      });

      it('should return new IMaKeToan if id is not provided', () => {
        // GIVEN
        service.find = jest.fn();
        mockActivatedRouteSnapshot.params = {};

        // WHEN
        routingResolveService.resolve(mockActivatedRouteSnapshot).subscribe(result => {
          resultMaKeToan = result;
        });

        // THEN
        expect(service.find).not.toBeCalled();
        expect(resultMaKeToan).toEqual(new MaKeToan());
      });

      it('should route to 404 page if data not found in server', () => {
        // GIVEN
        spyOn(service, 'find').and.returnValue(of(new HttpResponse({ body: null })));
        mockActivatedRouteSnapshot.params = { id: 123 };

        // WHEN
        routingResolveService.resolve(mockActivatedRouteSnapshot).subscribe(result => {
          resultMaKeToan = result;
        });

        // THEN
        expect(service.find).toBeCalledWith(123);
        expect(resultMaKeToan).toEqual(undefined);
        expect(mockRouter.navigate).toHaveBeenCalledWith(['404']);
      });
    });
  });
});
