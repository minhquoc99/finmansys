import { Component, OnInit } from '@angular/core';
import { PhieuThuService } from 'app/entities/phieu-thu/service/phieu-thu.service';
import { KhachSanService } from 'app/entities/khach-san/service/khach-san.service';
import { IPhieuThu } from 'app/entities/phieu-thu/phieu-thu.model';
import { IKhachSan } from 'app/entities/khach-san/khach-san.model';
import { HttpResponse } from '@angular/common/http';
import { IKhachHang } from 'app/entities/khach-hang/khach-hang.model';

@Component({
  selector: 'jhi-bao-cao-doanh-thu',
  templateUrl: './bao-cao-doanh-thu.component.html',
  styleUrls: ['./bao-cao-doanh-thu.component.scss'],
})
export class BaoCaoDoanhThuComponent implements OnInit {
  phieuThus: IPhieuThu[] = [];

  khachSans: IKhachSan[] = [];

  month = 11;

  year = 2018;

  khachSanId = 1;

  predicate = 101230000;

  constructor(protected phieuThuService: PhieuThuService, protected khachSanService: KhachSanService) {}

  ngOnInit(): void {
    this.khachSanService
      .query({
        page: 0,
        size: 10000,
        sort: ['id,asc'],
      })
      .subscribe((res: HttpResponse<IKhachSan[]>) => {
        this.khachSans = res.body ?? [];
        console.log(this.khachSans);
        this.getData();
      });
  }

  getData(): void {
    console.log(this.month, ' ', this.year, ' ', this.khachSanId);
    let endMonth = this.month + 1;
    let endYear = this.year;
    if (endMonth === 13) {
      endMonth = 1;
      endYear = this.year + 1;
    }
    this.phieuThuService
      .query({
        page: 0,
        size: 10000,
        sort: ['id,asc'],
        'ngayChungTu.greaterThan': new Date(this.year, this.month, 1).toISOString(),
        'ngayChungTu.lessThan': new Date(endYear, endMonth, 1).toISOString(),
      })
      .subscribe((res: HttpResponse<IPhieuThu[]>) => {
        this.phieuThus = res.body ?? [];
      });
    this.phieuThuService.getPredictions(this.year, this.month, this.khachSanId).subscribe((res: HttpResponse<any>) => {
      this.predicate = Number(res.body.predictions) * 1000000;
    });
  }

  getTongDoanhThu(): number {
    let sum = 0;
    for (const phieuThu of this.phieuThus) {
      sum += Number(phieuThu.soTien);
    }
    return sum;
  }

  getPhieuThuMax(): IPhieuThu {
    let max = 0;
    let maxPhieuThu = this.phieuThus[0];
    for (const phieuThu of this.phieuThus) {
      if (phieuThu.soTien) {
        if (max < phieuThu.soTien) {
          max = phieuThu.soTien;
          maxPhieuThu = phieuThu;
        }
      }
    }
    return maxPhieuThu;
  }
}
