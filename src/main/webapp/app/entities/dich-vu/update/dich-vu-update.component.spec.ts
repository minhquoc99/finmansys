jest.mock('@angular/router');

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { of, Subject } from 'rxjs';

import { DichVuService } from '../service/dich-vu.service';
import { IDichVu, DichVu } from '../dich-vu.model';

import { DichVuUpdateComponent } from './dich-vu-update.component';

describe('Component Tests', () => {
  describe('DichVu Management Update Component', () => {
    let comp: DichVuUpdateComponent;
    let fixture: ComponentFixture<DichVuUpdateComponent>;
    let activatedRoute: ActivatedRoute;
    let dichVuService: DichVuService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule],
        declarations: [DichVuUpdateComponent],
        providers: [FormBuilder, ActivatedRoute],
      })
        .overrideTemplate(DichVuUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(DichVuUpdateComponent);
      activatedRoute = TestBed.inject(ActivatedRoute);
      dichVuService = TestBed.inject(DichVuService);

      comp = fixture.componentInstance;
    });

    describe('ngOnInit', () => {
      it('Should update editForm', () => {
        const dichVu: IDichVu = { id: 456 };

        activatedRoute.data = of({ dichVu });
        comp.ngOnInit();

        expect(comp.editForm.value).toEqual(expect.objectContaining(dichVu));
      });
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', () => {
        // GIVEN
        const saveSubject = new Subject();
        const dichVu = { id: 123 };
        spyOn(dichVuService, 'update').and.returnValue(saveSubject);
        spyOn(comp, 'previousState');
        activatedRoute.data = of({ dichVu });
        comp.ngOnInit();

        // WHEN
        comp.save();
        expect(comp.isSaving).toEqual(true);
        saveSubject.next(new HttpResponse({ body: dichVu }));
        saveSubject.complete();

        // THEN
        expect(comp.previousState).toHaveBeenCalled();
        expect(dichVuService.update).toHaveBeenCalledWith(dichVu);
        expect(comp.isSaving).toEqual(false);
      });

      it('Should call create service on save for new entity', () => {
        // GIVEN
        const saveSubject = new Subject();
        const dichVu = new DichVu();
        spyOn(dichVuService, 'create').and.returnValue(saveSubject);
        spyOn(comp, 'previousState');
        activatedRoute.data = of({ dichVu });
        comp.ngOnInit();

        // WHEN
        comp.save();
        expect(comp.isSaving).toEqual(true);
        saveSubject.next(new HttpResponse({ body: dichVu }));
        saveSubject.complete();

        // THEN
        expect(dichVuService.create).toHaveBeenCalledWith(dichVu);
        expect(comp.isSaving).toEqual(false);
        expect(comp.previousState).toHaveBeenCalled();
      });

      it('Should set isSaving to false on error', () => {
        // GIVEN
        const saveSubject = new Subject();
        const dichVu = { id: 123 };
        spyOn(dichVuService, 'update').and.returnValue(saveSubject);
        spyOn(comp, 'previousState');
        activatedRoute.data = of({ dichVu });
        comp.ngOnInit();

        // WHEN
        comp.save();
        expect(comp.isSaving).toEqual(true);
        saveSubject.error('This is an error!');

        // THEN
        expect(dichVuService.update).toHaveBeenCalledWith(dichVu);
        expect(comp.isSaving).toEqual(false);
        expect(comp.previousState).not.toHaveBeenCalled();
      });
    });
  });
});
