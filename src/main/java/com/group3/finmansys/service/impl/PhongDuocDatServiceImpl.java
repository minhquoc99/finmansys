package com.group3.finmansys.service.impl;

import com.group3.finmansys.domain.PhongDuocDat;
import com.group3.finmansys.repository.PhongDuocDatRepository;
import com.group3.finmansys.service.PhongDuocDatService;
import com.group3.finmansys.service.dto.PhongDuocDatDTO;
import com.group3.finmansys.service.mapper.PhongDuocDatMapper;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link PhongDuocDat}.
 */
@Service
@Transactional
public class PhongDuocDatServiceImpl implements PhongDuocDatService {

    private final Logger log = LoggerFactory.getLogger(PhongDuocDatServiceImpl.class);

    private final PhongDuocDatRepository phongDuocDatRepository;

    private final PhongDuocDatMapper phongDuocDatMapper;

    public PhongDuocDatServiceImpl(PhongDuocDatRepository phongDuocDatRepository, PhongDuocDatMapper phongDuocDatMapper) {
        this.phongDuocDatRepository = phongDuocDatRepository;
        this.phongDuocDatMapper = phongDuocDatMapper;
    }

    @Override
    public PhongDuocDatDTO save(PhongDuocDatDTO phongDuocDatDTO) {
        log.debug("Request to save PhongDuocDat : {}", phongDuocDatDTO);
        PhongDuocDat phongDuocDat = phongDuocDatMapper.toEntity(phongDuocDatDTO);
        phongDuocDat = phongDuocDatRepository.save(phongDuocDat);
        return phongDuocDatMapper.toDto(phongDuocDat);
    }

    @Override
    public Optional<PhongDuocDatDTO> partialUpdate(PhongDuocDatDTO phongDuocDatDTO) {
        log.debug("Request to partially update PhongDuocDat : {}", phongDuocDatDTO);

        return phongDuocDatRepository
            .findById(phongDuocDatDTO.getId())
            .map(
                existingPhongDuocDat -> {
                    phongDuocDatMapper.partialUpdate(existingPhongDuocDat, phongDuocDatDTO);
                    return existingPhongDuocDat;
                }
            )
            .map(phongDuocDatRepository::save)
            .map(phongDuocDatMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<PhongDuocDatDTO> findAll(Pageable pageable) {
        log.debug("Request to get all PhongDuocDats");
        return phongDuocDatRepository.findAll(pageable).map(phongDuocDatMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<PhongDuocDatDTO> findOne(Long id) {
        log.debug("Request to get PhongDuocDat : {}", id);
        return phongDuocDatRepository.findById(id).map(phongDuocDatMapper::toDto);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete PhongDuocDat : {}", id);
        phongDuocDatRepository.deleteById(id);
    }
}
