package com.group3.finmansys.repository;

import com.group3.finmansys.domain.ToanTu;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data SQL repository for the ToanTu entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ToanTuRepository extends JpaRepository<ToanTu, Long>, JpaSpecificationExecutor<ToanTu> {}
