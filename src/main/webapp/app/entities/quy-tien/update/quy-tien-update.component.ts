import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize, map } from 'rxjs/operators';

import { IQuyTien, QuyTien } from '../quy-tien.model';
import { QuyTienService } from '../service/quy-tien.service';
import { ILoaiQuy } from 'app/entities/loai-quy/loai-quy.model';
import { LoaiQuyService } from 'app/entities/loai-quy/service/loai-quy.service';
import { IMaKeToan } from 'app/entities/ma-ke-toan/ma-ke-toan.model';
import { MaKeToanService } from 'app/entities/ma-ke-toan/service/ma-ke-toan.service';
import { IKhachSan } from 'app/entities/khach-san/khach-san.model';
import { KhachSanService } from 'app/entities/khach-san/service/khach-san.service';

@Component({
  selector: 'jhi-quy-tien-update',
  templateUrl: './quy-tien-update.component.html',
})
export class QuyTienUpdateComponent implements OnInit {
  isSaving = false;

  loaiQuiesSharedCollection: ILoaiQuy[] = [];
  maKeToansSharedCollection: IMaKeToan[] = [];
  khachSansSharedCollection: IKhachSan[] = [];

  editForm = this.fb.group({
    id: [],
    soTien: [null, [Validators.required, Validators.min(1)]],
    ghiChu: [],
    ten: [null, [Validators.required]],
    loaiQuy: [null, Validators.required],
    maKeToan: [null, Validators.required],
    khachSan: [null, Validators.required],
  });

  constructor(
    protected quyTienService: QuyTienService,
    protected loaiQuyService: LoaiQuyService,
    protected maKeToanService: MaKeToanService,
    protected khachSanService: KhachSanService,
    protected activatedRoute: ActivatedRoute,
    protected fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ quyTien }) => {
      this.updateForm(quyTien);

      this.loadRelationshipsOptions();
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const quyTien = this.createFromForm();
    if (quyTien.id !== undefined) {
      this.subscribeToSaveResponse(this.quyTienService.update(quyTien));
    } else {
      this.subscribeToSaveResponse(this.quyTienService.create(quyTien));
    }
  }

  trackLoaiQuyById(index: number, item: ILoaiQuy): number {
    return item.id!;
  }

  trackMaKeToanById(index: number, item: IMaKeToan): number {
    return item.id!;
  }

  trackKhachSanById(index: number, item: IKhachSan): number {
    return item.id!;
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IQuyTien>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(quyTien: IQuyTien): void {
    this.editForm.patchValue({
      id: quyTien.id,
      soTien: quyTien.soTien,
      ghiChu: quyTien.ghiChu,
      ten: quyTien.ten,
      loaiQuy: quyTien.loaiQuy,
      maKeToan: quyTien.maKeToan,
      khachSan: quyTien.khachSan,
    });

    this.loaiQuiesSharedCollection = this.loaiQuyService.addLoaiQuyToCollectionIfMissing(this.loaiQuiesSharedCollection, quyTien.loaiQuy);
    this.maKeToansSharedCollection = this.maKeToanService.addMaKeToanToCollectionIfMissing(
      this.maKeToansSharedCollection,
      quyTien.maKeToan
    );
    this.khachSansSharedCollection = this.khachSanService.addKhachSanToCollectionIfMissing(
      this.khachSansSharedCollection,
      quyTien.khachSan
    );
  }

  protected loadRelationshipsOptions(): void {
    this.loaiQuyService
      .query({
        page: 0,
        size: 10000,
        sort: ['id', 'asc'],
      })
      .pipe(map((res: HttpResponse<ILoaiQuy[]>) => res.body ?? []))
      .pipe(
        map((loaiQuies: ILoaiQuy[]) => this.loaiQuyService.addLoaiQuyToCollectionIfMissing(loaiQuies, this.editForm.get('loaiQuy')!.value))
      )
      .subscribe((loaiQuies: ILoaiQuy[]) => (this.loaiQuiesSharedCollection = loaiQuies));

    this.maKeToanService
      .query({
        page: 0,
        size: 10000,
        sort: ['id', 'asc'],
      })
      .pipe(map((res: HttpResponse<IMaKeToan[]>) => res.body ?? []))
      .pipe(
        map((maKeToans: IMaKeToan[]) =>
          this.maKeToanService.addMaKeToanToCollectionIfMissing(maKeToans, this.editForm.get('maKeToan')!.value)
        )
      )
      .subscribe((maKeToans: IMaKeToan[]) => (this.maKeToansSharedCollection = maKeToans));

    this.khachSanService
      .query({
        page: 0,
        size: 10000,
        sort: ['id', 'asc'],
      })
      .pipe(map((res: HttpResponse<IKhachSan[]>) => res.body ?? []))
      .pipe(
        map((khachSans: IKhachSan[]) =>
          this.khachSanService.addKhachSanToCollectionIfMissing(khachSans, this.editForm.get('khachSan')!.value)
        )
      )
      .subscribe((khachSans: IKhachSan[]) => (this.khachSansSharedCollection = khachSans));
  }

  protected createFromForm(): IQuyTien {
    return {
      ...new QuyTien(),
      id: this.editForm.get(['id'])!.value,
      soTien: this.editForm.get(['soTien'])!.value,
      ghiChu: this.editForm.get(['ghiChu'])!.value,
      ten: this.editForm.get(['ten'])!.value,
      loaiQuy: this.editForm.get(['loaiQuy'])!.value,
      maKeToan: this.editForm.get(['maKeToan'])!.value,
      khachSan: this.editForm.get(['khachSan'])!.value,
    };
  }
}
