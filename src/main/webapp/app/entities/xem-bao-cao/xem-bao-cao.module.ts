import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { XemBaoCaoRoutingModule } from './xem-bao-cao-routing.module';
import { BangCanDoiKeToanComponent } from './bang-can-doi-ke-toan/bang-can-doi-ke-toan.component';
import { BaoCaoDoanhThuComponent } from './bao-cao-doanh-thu/bao-cao-doanh-thu.component';
import { SharedModule } from 'app/shared/shared.module';

@NgModule({
  declarations: [BangCanDoiKeToanComponent, BaoCaoDoanhThuComponent],
  imports: [CommonModule, XemBaoCaoRoutingModule, SharedModule],
})
export class XemBaoCaoModule {}
