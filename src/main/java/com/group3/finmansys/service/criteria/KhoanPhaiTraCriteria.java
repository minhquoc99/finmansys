package com.group3.finmansys.service.criteria;

import java.io.Serializable;
import java.util.Objects;
import tech.jhipster.service.Criteria;
import tech.jhipster.service.filter.BooleanFilter;
import tech.jhipster.service.filter.DoubleFilter;
import tech.jhipster.service.filter.Filter;
import tech.jhipster.service.filter.FloatFilter;
import tech.jhipster.service.filter.InstantFilter;
import tech.jhipster.service.filter.IntegerFilter;
import tech.jhipster.service.filter.LongFilter;
import tech.jhipster.service.filter.StringFilter;

/**
 * Criteria class for the {@link com.group3.finmansys.domain.KhoanPhaiTra} entity. This class is used
 * in {@link com.group3.finmansys.web.rest.KhoanPhaiTraResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /khoan-phai-tras?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class KhoanPhaiTraCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private InstantFilter ngayTao;

    private InstantFilter ngayPhaiTra;

    private DoubleFilter soTien;

    private StringFilter ghiChu;

    private LongFilter khachHangId;

    private LongFilter maKeToanId;

    private LongFilter dsPhieuChiId;

    public KhoanPhaiTraCriteria() {}

    public KhoanPhaiTraCriteria(KhoanPhaiTraCriteria other) {
        this.id = other.id == null ? null : other.id.copy();
        this.ngayTao = other.ngayTao == null ? null : other.ngayTao.copy();
        this.ngayPhaiTra = other.ngayPhaiTra == null ? null : other.ngayPhaiTra.copy();
        this.soTien = other.soTien == null ? null : other.soTien.copy();
        this.ghiChu = other.ghiChu == null ? null : other.ghiChu.copy();
        this.khachHangId = other.khachHangId == null ? null : other.khachHangId.copy();
        this.maKeToanId = other.maKeToanId == null ? null : other.maKeToanId.copy();
        this.dsPhieuChiId = other.dsPhieuChiId == null ? null : other.dsPhieuChiId.copy();
    }

    @Override
    public KhoanPhaiTraCriteria copy() {
        return new KhoanPhaiTraCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public LongFilter id() {
        if (id == null) {
            id = new LongFilter();
        }
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public InstantFilter getNgayTao() {
        return ngayTao;
    }

    public InstantFilter ngayTao() {
        if (ngayTao == null) {
            ngayTao = new InstantFilter();
        }
        return ngayTao;
    }

    public void setNgayTao(InstantFilter ngayTao) {
        this.ngayTao = ngayTao;
    }

    public InstantFilter getNgayPhaiTra() {
        return ngayPhaiTra;
    }

    public InstantFilter ngayPhaiTra() {
        if (ngayPhaiTra == null) {
            ngayPhaiTra = new InstantFilter();
        }
        return ngayPhaiTra;
    }

    public void setNgayPhaiTra(InstantFilter ngayPhaiTra) {
        this.ngayPhaiTra = ngayPhaiTra;
    }

    public DoubleFilter getSoTien() {
        return soTien;
    }

    public DoubleFilter soTien() {
        if (soTien == null) {
            soTien = new DoubleFilter();
        }
        return soTien;
    }

    public void setSoTien(DoubleFilter soTien) {
        this.soTien = soTien;
    }

    public StringFilter getGhiChu() {
        return ghiChu;
    }

    public StringFilter ghiChu() {
        if (ghiChu == null) {
            ghiChu = new StringFilter();
        }
        return ghiChu;
    }

    public void setGhiChu(StringFilter ghiChu) {
        this.ghiChu = ghiChu;
    }

    public LongFilter getKhachHangId() {
        return khachHangId;
    }

    public LongFilter khachHangId() {
        if (khachHangId == null) {
            khachHangId = new LongFilter();
        }
        return khachHangId;
    }

    public void setKhachHangId(LongFilter khachHangId) {
        this.khachHangId = khachHangId;
    }

    public LongFilter getMaKeToanId() {
        return maKeToanId;
    }

    public LongFilter maKeToanId() {
        if (maKeToanId == null) {
            maKeToanId = new LongFilter();
        }
        return maKeToanId;
    }

    public void setMaKeToanId(LongFilter maKeToanId) {
        this.maKeToanId = maKeToanId;
    }

    public LongFilter getDsPhieuChiId() {
        return dsPhieuChiId;
    }

    public LongFilter dsPhieuChiId() {
        if (dsPhieuChiId == null) {
            dsPhieuChiId = new LongFilter();
        }
        return dsPhieuChiId;
    }

    public void setDsPhieuChiId(LongFilter dsPhieuChiId) {
        this.dsPhieuChiId = dsPhieuChiId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final KhoanPhaiTraCriteria that = (KhoanPhaiTraCriteria) o;
        return (
            Objects.equals(id, that.id) &&
            Objects.equals(ngayTao, that.ngayTao) &&
            Objects.equals(ngayPhaiTra, that.ngayPhaiTra) &&
            Objects.equals(soTien, that.soTien) &&
            Objects.equals(ghiChu, that.ghiChu) &&
            Objects.equals(khachHangId, that.khachHangId) &&
            Objects.equals(maKeToanId, that.maKeToanId) &&
            Objects.equals(dsPhieuChiId, that.dsPhieuChiId)
        );
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, ngayTao, ngayPhaiTra, soTien, ghiChu, khachHangId, maKeToanId, dsPhieuChiId);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "KhoanPhaiTraCriteria{" +
            (id != null ? "id=" + id + ", " : "") +
            (ngayTao != null ? "ngayTao=" + ngayTao + ", " : "") +
            (ngayPhaiTra != null ? "ngayPhaiTra=" + ngayPhaiTra + ", " : "") +
            (soTien != null ? "soTien=" + soTien + ", " : "") +
            (ghiChu != null ? "ghiChu=" + ghiChu + ", " : "") +
            (khachHangId != null ? "khachHangId=" + khachHangId + ", " : "") +
            (maKeToanId != null ? "maKeToanId=" + maKeToanId + ", " : "") +
            (dsPhieuChiId != null ? "dsPhieuChiId=" + dsPhieuChiId + ", " : "") +
            "}";
    }
}
