import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { IDichVu, DichVu } from '../dich-vu.model';

import { DichVuService } from './dich-vu.service';

describe('Service Tests', () => {
  describe('DichVu Service', () => {
    let service: DichVuService;
    let httpMock: HttpTestingController;
    let elemDefault: IDichVu;
    let expectedResult: IDichVu | IDichVu[] | boolean | null;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule],
      });
      expectedResult = null;
      service = TestBed.inject(DichVuService);
      httpMock = TestBed.inject(HttpTestingController);

      elemDefault = {
        id: 0,
        ten: 'AAAAAAA',
        gia: 0,
        moTa: 'AAAAAAA',
      };
    });

    describe('Service methods', () => {
      it('should find an element', () => {
        const returnedFromService = Object.assign({}, elemDefault);

        service.find(123).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(elemDefault);
      });

      it('should create a DichVu', () => {
        const returnedFromService = Object.assign(
          {
            id: 0,
          },
          elemDefault
        );

        const expected = Object.assign({}, returnedFromService);

        service.create(new DichVu()).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'POST' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should update a DichVu', () => {
        const returnedFromService = Object.assign(
          {
            id: 1,
            ten: 'BBBBBB',
            gia: 1,
            moTa: 'BBBBBB',
          },
          elemDefault
        );

        const expected = Object.assign({}, returnedFromService);

        service.update(expected).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'PUT' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should partial update a DichVu', () => {
        const patchObject = Object.assign({}, new DichVu());

        const returnedFromService = Object.assign(patchObject, elemDefault);

        const expected = Object.assign({}, returnedFromService);

        service.partialUpdate(patchObject).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'PATCH' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should return a list of DichVu', () => {
        const returnedFromService = Object.assign(
          {
            id: 1,
            ten: 'BBBBBB',
            gia: 1,
            moTa: 'BBBBBB',
          },
          elemDefault
        );

        const expected = Object.assign({}, returnedFromService);

        service.query().subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush([returnedFromService]);
        httpMock.verify();
        expect(expectedResult).toContainEqual(expected);
      });

      it('should delete a DichVu', () => {
        service.delete(123).subscribe(resp => (expectedResult = resp.ok));

        const req = httpMock.expectOne({ method: 'DELETE' });
        req.flush({ status: 200 });
        expect(expectedResult);
      });

      describe('addDichVuToCollectionIfMissing', () => {
        it('should add a DichVu to an empty array', () => {
          const dichVu: IDichVu = { id: 123 };
          expectedResult = service.addDichVuToCollectionIfMissing([], dichVu);
          expect(expectedResult).toHaveLength(1);
          expect(expectedResult).toContain(dichVu);
        });

        it('should not add a DichVu to an array that contains it', () => {
          const dichVu: IDichVu = { id: 123 };
          const dichVuCollection: IDichVu[] = [
            {
              ...dichVu,
            },
            { id: 456 },
          ];
          expectedResult = service.addDichVuToCollectionIfMissing(dichVuCollection, dichVu);
          expect(expectedResult).toHaveLength(2);
        });

        it("should add a DichVu to an array that doesn't contain it", () => {
          const dichVu: IDichVu = { id: 123 };
          const dichVuCollection: IDichVu[] = [{ id: 456 }];
          expectedResult = service.addDichVuToCollectionIfMissing(dichVuCollection, dichVu);
          expect(expectedResult).toHaveLength(2);
          expect(expectedResult).toContain(dichVu);
        });

        it('should add only unique DichVu to an array', () => {
          const dichVuArray: IDichVu[] = [{ id: 123 }, { id: 456 }, { id: 73724 }];
          const dichVuCollection: IDichVu[] = [{ id: 123 }];
          expectedResult = service.addDichVuToCollectionIfMissing(dichVuCollection, ...dichVuArray);
          expect(expectedResult).toHaveLength(3);
        });

        it('should accept varargs', () => {
          const dichVu: IDichVu = { id: 123 };
          const dichVu2: IDichVu = { id: 456 };
          expectedResult = service.addDichVuToCollectionIfMissing([], dichVu, dichVu2);
          expect(expectedResult).toHaveLength(2);
          expect(expectedResult).toContain(dichVu);
          expect(expectedResult).toContain(dichVu2);
        });

        it('should accept null and undefined values', () => {
          const dichVu: IDichVu = { id: 123 };
          expectedResult = service.addDichVuToCollectionIfMissing([], null, dichVu, undefined);
          expect(expectedResult).toHaveLength(1);
          expect(expectedResult).toContain(dichVu);
        });
      });
    });

    afterEach(() => {
      httpMock.verify();
    });
  });
});
