package com.group3.finmansys.service.impl;

import com.group3.finmansys.domain.PhongDat;
import com.group3.finmansys.repository.PhongDatRepository;
import com.group3.finmansys.service.PhongDatService;
import com.group3.finmansys.service.dto.PhongDatDTO;
import com.group3.finmansys.service.mapper.PhongDatMapper;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link PhongDat}.
 */
@Service
@Transactional
public class PhongDatServiceImpl implements PhongDatService {

    private final Logger log = LoggerFactory.getLogger(PhongDatServiceImpl.class);

    private final PhongDatRepository phongDatRepository;

    private final PhongDatMapper phongDatMapper;

    public PhongDatServiceImpl(PhongDatRepository phongDatRepository, PhongDatMapper phongDatMapper) {
        this.phongDatRepository = phongDatRepository;
        this.phongDatMapper = phongDatMapper;
    }

    @Override
    public PhongDatDTO save(PhongDatDTO phongDatDTO) {
        log.debug("Request to save PhongDat : {}", phongDatDTO);
        PhongDat phongDat = phongDatMapper.toEntity(phongDatDTO);
        phongDat = phongDatRepository.save(phongDat);
        return phongDatMapper.toDto(phongDat);
    }

    @Override
    public Optional<PhongDatDTO> partialUpdate(PhongDatDTO phongDatDTO) {
        log.debug("Request to partially update PhongDat : {}", phongDatDTO);

        return phongDatRepository
            .findById(phongDatDTO.getId())
            .map(
                existingPhongDat -> {
                    phongDatMapper.partialUpdate(existingPhongDat, phongDatDTO);
                    return existingPhongDat;
                }
            )
            .map(phongDatRepository::save)
            .map(phongDatMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<PhongDatDTO> findAll(Pageable pageable) {
        log.debug("Request to get all PhongDats");
        return phongDatRepository.findAll(pageable).map(phongDatMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<PhongDatDTO> findOne(Long id) {
        log.debug("Request to get PhongDat : {}", id);
        return phongDatRepository.findById(id).map(phongDatMapper::toDto);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete PhongDat : {}", id);
        phongDatRepository.deleteById(id);
    }
}
