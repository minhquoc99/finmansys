import { Component, OnInit } from '@angular/core';
import { NganSachNamService } from 'app/entities/ngan-sach-nam/service/ngan-sach-nam.service';
import { INganSachNam } from 'app/entities/ngan-sach-nam/ngan-sach-nam.model';
import { HttpResponse } from '@angular/common/http';
import { IKhoanMucChiPhi } from 'app/entities/khoan-muc-chi-phi/khoan-muc-chi-phi.model';
import { INganSachThang } from 'app/entities/ngan-sach-thang/ngan-sach-thang.model';
import { KhoanMucChiPhiService } from 'app/entities/khoan-muc-chi-phi/service/khoan-muc-chi-phi.service';

@Component({
  selector: 'jhi-hoach-toan',
  templateUrl: './hoach-toan.component.html',
  styleUrls: ['./hoach-toan.component.scss'],
})
export class HoachToanComponent implements OnInit {
  nganSachNams?: INganSachNam[];

  nganSachNamChiTieu?: INganSachNam;

  listMonth = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12];

  khoanMucChiPhis: IKhoanMucChiPhi[] = [];

  nganSachThangs: INganSachThang[] = [];

  nganSachThangChiTieus: INganSachThang[] = [];

  reset = true;

  constructor(protected nganSachNamService: NganSachNamService, protected khoanMucChiPhiService: KhoanMucChiPhiService) {}

  ngOnInit(): void {
    this.khoanMucChiPhiService
      .query({
        page: 0,
        size: 10000,
        sort: ['id', 'asc'],
      })
      .subscribe(
        (res: HttpResponse<IKhoanMucChiPhi[]>) => {
          this.khoanMucChiPhis = res.body ?? [];
        },
        () => {
          this.khoanMucChiPhis = [];
        }
      );
    this.nganSachNamService
      .query({
        page: 0,
        size: 10000,
        sort: ['id', 'asc'],
      })
      .subscribe(
        (res: HttpResponse<INganSachNam[]>) => {
          this.nganSachNams = res.body ?? [];
          this.nganSachThangs = this.nganSachNams[0].nganSachThangs ?? [];
          this.nganSachNamService.getChiTieu(this.nganSachNams[0]?.id ?? 1).subscribe(
            (resChiTieu: HttpResponse<INganSachNam>) => {
              this.nganSachNamChiTieu = resChiTieu.body ?? undefined;
              this.nganSachThangChiTieus = this.nganSachNamChiTieu?.nganSachThangs ?? [];
            },
            () => {
              this.nganSachNamChiTieu = undefined;
              this.nganSachThangChiTieus = [];
            }
          );
          this.reset = false;
          setTimeout(() => (this.reset = true), 0);
        },
        () => {
          this.nganSachNams = [];
        }
      );
  }

  onChangegNSNam(event: any): void {
    if (this.nganSachNams) {
      for (const nganSachNam of this.nganSachNams) {
        if (nganSachNam.id === Number(event.target.value)) {
          this.nganSachThangs = nganSachNam.nganSachThangs ?? [];
          this.nganSachNamService.getChiTieu(nganSachNam.id).subscribe(
            (res: HttpResponse<INganSachNam>) => {
              this.nganSachNamChiTieu = res.body ?? undefined;
              this.nganSachThangChiTieus = this.nganSachNamChiTieu?.nganSachThangs ?? [];
            },
            () => {
              this.nganSachNamChiTieu = undefined;
              this.nganSachThangChiTieus = [];
            }
          );
          this.reset = false;
          setTimeout(() => (this.reset = true), 0);
          return;
        }
      }
    }
  }

  getMoney(thang: number, khoanMucChiPhi: IKhoanMucChiPhi): number {
    for (const nganSachThang of this.nganSachThangs) {
      if (nganSachThang.thang === thang && nganSachThang.khoanMucChiPhi?.id === khoanMucChiPhi.id) {
        return nganSachThang.soTien ?? 0;
      }
    }
    return 0;
  }

  getTotal(khoanMucChiPhi: IKhoanMucChiPhi): number {
    let total: number;
    total = 0;
    for (const nganSachThang of this.nganSachThangs) {
      if (nganSachThang.khoanMucChiPhi?.id === khoanMucChiPhi.id) {
        total = total + Number(nganSachThang.soTien ?? 0);
      }
    }
    return total;
  }

  getMoneyChiTieu(thang: number, khoanMucChiPhi: IKhoanMucChiPhi): number {
    for (const nganSachThang of this.nganSachThangChiTieus) {
      if (nganSachThang.thang === thang && nganSachThang.khoanMucChiPhi?.id === khoanMucChiPhi.id) {
        return nganSachThang.soTien ?? 0;
      }
    }
    return 0;
  }

  getTotalChiTieu(khoanMucChiPhi: IKhoanMucChiPhi): number {
    let total: number;
    total = 0;
    for (const nganSachThang of this.nganSachThangChiTieus) {
      if (nganSachThang.khoanMucChiPhi?.id === khoanMucChiPhi.id) {
        total = total + Number(nganSachThang.soTien ?? 0);
      }
    }
    return total;
  }
}
