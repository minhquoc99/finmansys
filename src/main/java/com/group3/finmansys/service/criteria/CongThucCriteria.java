package com.group3.finmansys.service.criteria;

import java.io.Serializable;
import java.util.Objects;
import tech.jhipster.service.Criteria;
import tech.jhipster.service.filter.BooleanFilter;
import tech.jhipster.service.filter.DoubleFilter;
import tech.jhipster.service.filter.Filter;
import tech.jhipster.service.filter.FloatFilter;
import tech.jhipster.service.filter.IntegerFilter;
import tech.jhipster.service.filter.LongFilter;
import tech.jhipster.service.filter.StringFilter;

/**
 * Criteria class for the {@link com.group3.finmansys.domain.CongThuc} entity. This class is used
 * in {@link com.group3.finmansys.web.rest.CongThucResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /cong-thucs?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class CongThucCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private IntegerFilter maSo;

    private StringFilter tenChiTieu;

    private IntegerFilter cap;

    private DoubleFilter soDauKy;

    private DoubleFilter soCuoiKy;

    private LongFilter toanTuId;

    private LongFilter capChaId;

    private LongFilter capConId;

    public CongThucCriteria() {}

    public CongThucCriteria(CongThucCriteria other) {
        this.id = other.id == null ? null : other.id.copy();
        this.maSo = other.maSo == null ? null : other.maSo.copy();
        this.tenChiTieu = other.tenChiTieu == null ? null : other.tenChiTieu.copy();
        this.cap = other.cap == null ? null : other.cap.copy();
        this.soDauKy = other.soDauKy == null ? null : other.soDauKy.copy();
        this.soCuoiKy = other.soCuoiKy == null ? null : other.soCuoiKy.copy();
        this.toanTuId = other.toanTuId == null ? null : other.toanTuId.copy();
        this.capChaId = other.capChaId == null ? null : other.capChaId.copy();
        this.capConId = other.capConId == null ? null : other.capConId.copy();
    }

    @Override
    public CongThucCriteria copy() {
        return new CongThucCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public LongFilter id() {
        if (id == null) {
            id = new LongFilter();
        }
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public IntegerFilter getMaSo() {
        return maSo;
    }

    public IntegerFilter maSo() {
        if (maSo == null) {
            maSo = new IntegerFilter();
        }
        return maSo;
    }

    public void setMaSo(IntegerFilter maSo) {
        this.maSo = maSo;
    }

    public StringFilter getTenChiTieu() {
        return tenChiTieu;
    }

    public StringFilter tenChiTieu() {
        if (tenChiTieu == null) {
            tenChiTieu = new StringFilter();
        }
        return tenChiTieu;
    }

    public void setTenChiTieu(StringFilter tenChiTieu) {
        this.tenChiTieu = tenChiTieu;
    }

    public IntegerFilter getCap() {
        return cap;
    }

    public IntegerFilter cap() {
        if (cap == null) {
            cap = new IntegerFilter();
        }
        return cap;
    }

    public void setCap(IntegerFilter cap) {
        this.cap = cap;
    }

    public DoubleFilter getSoDauKy() {
        return soDauKy;
    }

    public DoubleFilter soDauKy() {
        if (soDauKy == null) {
            soDauKy = new DoubleFilter();
        }
        return soDauKy;
    }

    public void setSoDauKy(DoubleFilter soDauKy) {
        this.soDauKy = soDauKy;
    }

    public DoubleFilter getSoCuoiKy() {
        return soCuoiKy;
    }

    public DoubleFilter soCuoiKy() {
        if (soCuoiKy == null) {
            soCuoiKy = new DoubleFilter();
        }
        return soCuoiKy;
    }

    public void setSoCuoiKy(DoubleFilter soCuoiKy) {
        this.soCuoiKy = soCuoiKy;
    }

    public LongFilter getToanTuId() {
        return toanTuId;
    }

    public LongFilter toanTuId() {
        if (toanTuId == null) {
            toanTuId = new LongFilter();
        }
        return toanTuId;
    }

    public void setToanTuId(LongFilter toanTuId) {
        this.toanTuId = toanTuId;
    }

    public LongFilter getCapChaId() {
        return capChaId;
    }

    public LongFilter capChaId() {
        if (capChaId == null) {
            capChaId = new LongFilter();
        }
        return capChaId;
    }

    public void setCapChaId(LongFilter capChaId) {
        this.capChaId = capChaId;
    }

    public LongFilter getCapConId() {
        return capConId;
    }

    public LongFilter capConId() {
        if (capConId == null) {
            capConId = new LongFilter();
        }
        return capConId;
    }

    public void setCapConId(LongFilter capConId) {
        this.capConId = capConId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final CongThucCriteria that = (CongThucCriteria) o;
        return (
            Objects.equals(id, that.id) &&
            Objects.equals(maSo, that.maSo) &&
            Objects.equals(tenChiTieu, that.tenChiTieu) &&
            Objects.equals(cap, that.cap) &&
            Objects.equals(soDauKy, that.soDauKy) &&
            Objects.equals(soCuoiKy, that.soCuoiKy) &&
            Objects.equals(toanTuId, that.toanTuId) &&
            Objects.equals(capChaId, that.capChaId) &&
            Objects.equals(capConId, that.capConId)
        );
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, maSo, tenChiTieu, cap, soDauKy, soCuoiKy, toanTuId, capChaId, capConId);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "CongThucCriteria{" +
            (id != null ? "id=" + id + ", " : "") +
            (maSo != null ? "maSo=" + maSo + ", " : "") +
            (tenChiTieu != null ? "tenChiTieu=" + tenChiTieu + ", " : "") +
            (cap != null ? "cap=" + cap + ", " : "") +
            (soDauKy != null ? "soDauKy=" + soDauKy + ", " : "") +
            (soCuoiKy != null ? "soCuoiKy=" + soCuoiKy + ", " : "") +
            (toanTuId != null ? "toanTuId=" + toanTuId + ", " : "") +
            (capChaId != null ? "capChaId=" + capChaId + ", " : "") +
            (capConId != null ? "capConId=" + capConId + ", " : "") +
            "}";
    }
}
