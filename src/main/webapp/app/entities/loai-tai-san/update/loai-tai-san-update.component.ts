import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize, map } from 'rxjs/operators';

import { ILoaiTaiSan, LoaiTaiSan } from '../loai-tai-san.model';
import { LoaiTaiSanService } from '../service/loai-tai-san.service';
import { IMaKeToan } from 'app/entities/ma-ke-toan/ma-ke-toan.model';
import { MaKeToanService } from 'app/entities/ma-ke-toan/service/ma-ke-toan.service';

@Component({
  selector: 'jhi-loai-tai-san-update',
  templateUrl: './loai-tai-san-update.component.html',
})
export class LoaiTaiSanUpdateComponent implements OnInit {
  isSaving = false;

  maKeToansSharedCollection: IMaKeToan[] = [];

  editForm = this.fb.group({
    id: [],
    ten: [],
    moTa: [],
    maKeToan: [null, Validators.required],
  });

  constructor(
    protected loaiTaiSanService: LoaiTaiSanService,
    protected maKeToanService: MaKeToanService,
    protected activatedRoute: ActivatedRoute,
    protected fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ loaiTaiSan }) => {
      this.updateForm(loaiTaiSan);

      this.loadRelationshipsOptions();
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const loaiTaiSan = this.createFromForm();
    if (loaiTaiSan.id !== undefined) {
      this.subscribeToSaveResponse(this.loaiTaiSanService.update(loaiTaiSan));
    } else {
      this.subscribeToSaveResponse(this.loaiTaiSanService.create(loaiTaiSan));
    }
  }

  trackMaKeToanById(index: number, item: IMaKeToan): number {
    return item.id!;
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<ILoaiTaiSan>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(loaiTaiSan: ILoaiTaiSan): void {
    this.editForm.patchValue({
      id: loaiTaiSan.id,
      ten: loaiTaiSan.ten,
      moTa: loaiTaiSan.moTa,
      maKeToan: loaiTaiSan.maKeToan,
    });

    this.maKeToansSharedCollection = this.maKeToanService.addMaKeToanToCollectionIfMissing(
      this.maKeToansSharedCollection,
      loaiTaiSan.maKeToan
    );
  }

  protected loadRelationshipsOptions(): void {
    this.maKeToanService
      .query()
      .pipe(map((res: HttpResponse<IMaKeToan[]>) => res.body ?? []))
      .pipe(
        map((maKeToans: IMaKeToan[]) =>
          this.maKeToanService.addMaKeToanToCollectionIfMissing(maKeToans, this.editForm.get('maKeToan')!.value)
        )
      )
      .subscribe((maKeToans: IMaKeToan[]) => (this.maKeToansSharedCollection = maKeToans));
  }

  protected createFromForm(): ILoaiTaiSan {
    return {
      ...new LoaiTaiSan(),
      id: this.editForm.get(['id'])!.value,
      ten: this.editForm.get(['ten'])!.value,
      moTa: this.editForm.get(['moTa'])!.value,
      maKeToan: this.editForm.get(['maKeToan'])!.value,
    };
  }
}
