import { IKhachSan } from 'app/entities/khach-san/khach-san.model';
import { INganSachThang } from 'app/entities/ngan-sach-thang/ngan-sach-thang.model';

export interface INganSachNam {
  id?: number;
  ten?: string;
  nam?: number;
  soTien?: number;
  khachSan?: IKhachSan;
  nganSachThangs?: INganSachThang[] | null;
}

export class NganSachNam implements INganSachNam {
  constructor(
    public id?: number,
    public ten?: string,
    public nam?: number,
    public soTien?: number,
    public khachSan?: IKhachSan,
    public nganSachThangs?: INganSachThang[] | null
  ) {}
}

export function getNganSachNamIdentifier(nganSachNam: INganSachNam): number | undefined {
  return nganSachNam.id;
}
