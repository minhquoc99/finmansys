package com.group3.finmansys.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.group3.finmansys.IntegrationTest;
import com.group3.finmansys.domain.CongThuc;
import com.group3.finmansys.domain.MaKeToan;
import com.group3.finmansys.domain.ToanTu;
import com.group3.finmansys.repository.ToanTuRepository;
import com.group3.finmansys.service.criteria.ToanTuCriteria;
import com.group3.finmansys.service.dto.ToanTuDTO;
import com.group3.finmansys.service.mapper.ToanTuMapper;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link ToanTuResource} REST controller.
 */
@IntegrationTest
@ExtendWith(MockitoExtension.class)
@AutoConfigureMockMvc
@WithMockUser
class ToanTuResourceIT {

    private static final String DEFAULT_DAU = "AAAAAAAAAA";
    private static final String UPDATED_DAU = "BBBBBBBBBB";

    private static final String ENTITY_API_URL = "/api/toan-tus";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private ToanTuRepository toanTuRepository;

    @Autowired
    private ToanTuMapper toanTuMapper;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restToanTuMockMvc;

    private ToanTu toanTu;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ToanTu createEntity(EntityManager em) {
        ToanTu toanTu = new ToanTu().dau(DEFAULT_DAU);
        // Add required entity
        MaKeToan maKeToan;
        if (TestUtil.findAll(em, MaKeToan.class).isEmpty()) {
            maKeToan = MaKeToanResourceIT.createEntity(em);
            em.persist(maKeToan);
            em.flush();
        } else {
            maKeToan = TestUtil.findAll(em, MaKeToan.class).get(0);
        }
        toanTu.setMaKeToan(maKeToan);
        // Add required entity
        CongThuc congThuc;
        if (TestUtil.findAll(em, CongThuc.class).isEmpty()) {
            congThuc = CongThucResourceIT.createEntity(em);
            em.persist(congThuc);
            em.flush();
        } else {
            congThuc = TestUtil.findAll(em, CongThuc.class).get(0);
        }
        toanTu.setCongThuc(congThuc);
        return toanTu;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ToanTu createUpdatedEntity(EntityManager em) {
        ToanTu toanTu = new ToanTu().dau(UPDATED_DAU);
        // Add required entity
        MaKeToan maKeToan;
        if (TestUtil.findAll(em, MaKeToan.class).isEmpty()) {
            maKeToan = MaKeToanResourceIT.createUpdatedEntity(em);
            em.persist(maKeToan);
            em.flush();
        } else {
            maKeToan = TestUtil.findAll(em, MaKeToan.class).get(0);
        }
        toanTu.setMaKeToan(maKeToan);
        // Add required entity
        CongThuc congThuc;
        if (TestUtil.findAll(em, CongThuc.class).isEmpty()) {
            congThuc = CongThucResourceIT.createUpdatedEntity(em);
            em.persist(congThuc);
            em.flush();
        } else {
            congThuc = TestUtil.findAll(em, CongThuc.class).get(0);
        }
        toanTu.setCongThuc(congThuc);
        return toanTu;
    }

    @BeforeEach
    public void initTest() {
        toanTu = createEntity(em);
    }

    @Test
    @Transactional
    void createToanTu() throws Exception {
        int databaseSizeBeforeCreate = toanTuRepository.findAll().size();
        // Create the ToanTu
        ToanTuDTO toanTuDTO = toanTuMapper.toDto(toanTu);
        restToanTuMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(toanTuDTO)))
            .andExpect(status().isCreated());

        // Validate the ToanTu in the database
        List<ToanTu> toanTuList = toanTuRepository.findAll();
        assertThat(toanTuList).hasSize(databaseSizeBeforeCreate + 1);
        ToanTu testToanTu = toanTuList.get(toanTuList.size() - 1);
        assertThat(testToanTu.getDau()).isEqualTo(DEFAULT_DAU);
    }

    @Test
    @Transactional
    void createToanTuWithExistingId() throws Exception {
        // Create the ToanTu with an existing ID
        toanTu.setId(1L);
        ToanTuDTO toanTuDTO = toanTuMapper.toDto(toanTu);

        int databaseSizeBeforeCreate = toanTuRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restToanTuMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(toanTuDTO)))
            .andExpect(status().isBadRequest());

        // Validate the ToanTu in the database
        List<ToanTu> toanTuList = toanTuRepository.findAll();
        assertThat(toanTuList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void checkDauIsRequired() throws Exception {
        int databaseSizeBeforeTest = toanTuRepository.findAll().size();
        // set the field null
        toanTu.setDau(null);

        // Create the ToanTu, which fails.
        ToanTuDTO toanTuDTO = toanTuMapper.toDto(toanTu);

        restToanTuMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(toanTuDTO)))
            .andExpect(status().isBadRequest());

        List<ToanTu> toanTuList = toanTuRepository.findAll();
        assertThat(toanTuList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void getAllToanTus() throws Exception {
        // Initialize the database
        toanTuRepository.saveAndFlush(toanTu);

        // Get all the toanTuList
        restToanTuMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(toanTu.getId().intValue())))
            .andExpect(jsonPath("$.[*].dau").value(hasItem(DEFAULT_DAU)));
    }

    @Test
    @Transactional
    void getToanTu() throws Exception {
        // Initialize the database
        toanTuRepository.saveAndFlush(toanTu);

        // Get the toanTu
        restToanTuMockMvc
            .perform(get(ENTITY_API_URL_ID, toanTu.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(toanTu.getId().intValue()))
            .andExpect(jsonPath("$.dau").value(DEFAULT_DAU));
    }

    @Test
    @Transactional
    void getToanTusByIdFiltering() throws Exception {
        // Initialize the database
        toanTuRepository.saveAndFlush(toanTu);

        Long id = toanTu.getId();

        defaultToanTuShouldBeFound("id.equals=" + id);
        defaultToanTuShouldNotBeFound("id.notEquals=" + id);

        defaultToanTuShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultToanTuShouldNotBeFound("id.greaterThan=" + id);

        defaultToanTuShouldBeFound("id.lessThanOrEqual=" + id);
        defaultToanTuShouldNotBeFound("id.lessThan=" + id);
    }

    @Test
    @Transactional
    void getAllToanTusByDauIsEqualToSomething() throws Exception {
        // Initialize the database
        toanTuRepository.saveAndFlush(toanTu);

        // Get all the toanTuList where dau equals to DEFAULT_DAU
        defaultToanTuShouldBeFound("dau.equals=" + DEFAULT_DAU);

        // Get all the toanTuList where dau equals to UPDATED_DAU
        defaultToanTuShouldNotBeFound("dau.equals=" + UPDATED_DAU);
    }

    @Test
    @Transactional
    void getAllToanTusByDauIsNotEqualToSomething() throws Exception {
        // Initialize the database
        toanTuRepository.saveAndFlush(toanTu);

        // Get all the toanTuList where dau not equals to DEFAULT_DAU
        defaultToanTuShouldNotBeFound("dau.notEquals=" + DEFAULT_DAU);

        // Get all the toanTuList where dau not equals to UPDATED_DAU
        defaultToanTuShouldBeFound("dau.notEquals=" + UPDATED_DAU);
    }

    @Test
    @Transactional
    void getAllToanTusByDauIsInShouldWork() throws Exception {
        // Initialize the database
        toanTuRepository.saveAndFlush(toanTu);

        // Get all the toanTuList where dau in DEFAULT_DAU or UPDATED_DAU
        defaultToanTuShouldBeFound("dau.in=" + DEFAULT_DAU + "," + UPDATED_DAU);

        // Get all the toanTuList where dau equals to UPDATED_DAU
        defaultToanTuShouldNotBeFound("dau.in=" + UPDATED_DAU);
    }

    @Test
    @Transactional
    void getAllToanTusByDauIsNullOrNotNull() throws Exception {
        // Initialize the database
        toanTuRepository.saveAndFlush(toanTu);

        // Get all the toanTuList where dau is not null
        defaultToanTuShouldBeFound("dau.specified=true");

        // Get all the toanTuList where dau is null
        defaultToanTuShouldNotBeFound("dau.specified=false");
    }

    @Test
    @Transactional
    void getAllToanTusByDauContainsSomething() throws Exception {
        // Initialize the database
        toanTuRepository.saveAndFlush(toanTu);

        // Get all the toanTuList where dau contains DEFAULT_DAU
        defaultToanTuShouldBeFound("dau.contains=" + DEFAULT_DAU);

        // Get all the toanTuList where dau contains UPDATED_DAU
        defaultToanTuShouldNotBeFound("dau.contains=" + UPDATED_DAU);
    }

    @Test
    @Transactional
    void getAllToanTusByDauNotContainsSomething() throws Exception {
        // Initialize the database
        toanTuRepository.saveAndFlush(toanTu);

        // Get all the toanTuList where dau does not contain DEFAULT_DAU
        defaultToanTuShouldNotBeFound("dau.doesNotContain=" + DEFAULT_DAU);

        // Get all the toanTuList where dau does not contain UPDATED_DAU
        defaultToanTuShouldBeFound("dau.doesNotContain=" + UPDATED_DAU);
    }

    @Test
    @Transactional
    void getAllToanTusByMaKeToanIsEqualToSomething() throws Exception {
        // Initialize the database
        toanTuRepository.saveAndFlush(toanTu);
        MaKeToan maKeToan = MaKeToanResourceIT.createEntity(em);
        em.persist(maKeToan);
        em.flush();
        toanTu.setMaKeToan(maKeToan);
        toanTuRepository.saveAndFlush(toanTu);
        Long maKeToanId = maKeToan.getId();

        // Get all the toanTuList where maKeToan equals to maKeToanId
        defaultToanTuShouldBeFound("maKeToanId.equals=" + maKeToanId);

        // Get all the toanTuList where maKeToan equals to (maKeToanId + 1)
        defaultToanTuShouldNotBeFound("maKeToanId.equals=" + (maKeToanId + 1));
    }

    @Test
    @Transactional
    void getAllToanTusByCongThucIsEqualToSomething() throws Exception {
        // Initialize the database
        toanTuRepository.saveAndFlush(toanTu);
        CongThuc congThuc = CongThucResourceIT.createEntity(em);
        em.persist(congThuc);
        em.flush();
        toanTu.setCongThuc(congThuc);
        toanTuRepository.saveAndFlush(toanTu);
        Long congThucId = congThuc.getId();

        // Get all the toanTuList where congThuc equals to congThucId
        defaultToanTuShouldBeFound("congThucId.equals=" + congThucId);

        // Get all the toanTuList where congThuc equals to (congThucId + 1)
        defaultToanTuShouldNotBeFound("congThucId.equals=" + (congThucId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultToanTuShouldBeFound(String filter) throws Exception {
        restToanTuMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(toanTu.getId().intValue())))
            .andExpect(jsonPath("$.[*].dau").value(hasItem(DEFAULT_DAU)));

        // Check, that the count call also returns 1
        restToanTuMockMvc
            .perform(get(ENTITY_API_URL + "/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultToanTuShouldNotBeFound(String filter) throws Exception {
        restToanTuMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restToanTuMockMvc
            .perform(get(ENTITY_API_URL + "/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }

    @Test
    @Transactional
    void getNonExistingToanTu() throws Exception {
        // Get the toanTu
        restToanTuMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putNewToanTu() throws Exception {
        // Initialize the database
        toanTuRepository.saveAndFlush(toanTu);

        int databaseSizeBeforeUpdate = toanTuRepository.findAll().size();

        // Update the toanTu
        ToanTu updatedToanTu = toanTuRepository.findById(toanTu.getId()).get();
        // Disconnect from session so that the updates on updatedToanTu are not directly saved in db
        em.detach(updatedToanTu);
        updatedToanTu.dau(UPDATED_DAU);
        ToanTuDTO toanTuDTO = toanTuMapper.toDto(updatedToanTu);

        restToanTuMockMvc
            .perform(
                put(ENTITY_API_URL_ID, toanTuDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(toanTuDTO))
            )
            .andExpect(status().isOk());

        // Validate the ToanTu in the database
        List<ToanTu> toanTuList = toanTuRepository.findAll();
        assertThat(toanTuList).hasSize(databaseSizeBeforeUpdate);
        ToanTu testToanTu = toanTuList.get(toanTuList.size() - 1);
        assertThat(testToanTu.getDau()).isEqualTo(UPDATED_DAU);
    }

    @Test
    @Transactional
    void putNonExistingToanTu() throws Exception {
        int databaseSizeBeforeUpdate = toanTuRepository.findAll().size();
        toanTu.setId(count.incrementAndGet());

        // Create the ToanTu
        ToanTuDTO toanTuDTO = toanTuMapper.toDto(toanTu);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restToanTuMockMvc
            .perform(
                put(ENTITY_API_URL_ID, toanTuDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(toanTuDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the ToanTu in the database
        List<ToanTu> toanTuList = toanTuRepository.findAll();
        assertThat(toanTuList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchToanTu() throws Exception {
        int databaseSizeBeforeUpdate = toanTuRepository.findAll().size();
        toanTu.setId(count.incrementAndGet());

        // Create the ToanTu
        ToanTuDTO toanTuDTO = toanTuMapper.toDto(toanTu);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restToanTuMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(toanTuDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the ToanTu in the database
        List<ToanTu> toanTuList = toanTuRepository.findAll();
        assertThat(toanTuList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamToanTu() throws Exception {
        int databaseSizeBeforeUpdate = toanTuRepository.findAll().size();
        toanTu.setId(count.incrementAndGet());

        // Create the ToanTu
        ToanTuDTO toanTuDTO = toanTuMapper.toDto(toanTu);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restToanTuMockMvc
            .perform(put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(toanTuDTO)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the ToanTu in the database
        List<ToanTu> toanTuList = toanTuRepository.findAll();
        assertThat(toanTuList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateToanTuWithPatch() throws Exception {
        // Initialize the database
        toanTuRepository.saveAndFlush(toanTu);

        int databaseSizeBeforeUpdate = toanTuRepository.findAll().size();

        // Update the toanTu using partial update
        ToanTu partialUpdatedToanTu = new ToanTu();
        partialUpdatedToanTu.setId(toanTu.getId());

        restToanTuMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedToanTu.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedToanTu))
            )
            .andExpect(status().isOk());

        // Validate the ToanTu in the database
        List<ToanTu> toanTuList = toanTuRepository.findAll();
        assertThat(toanTuList).hasSize(databaseSizeBeforeUpdate);
        ToanTu testToanTu = toanTuList.get(toanTuList.size() - 1);
        assertThat(testToanTu.getDau()).isEqualTo(DEFAULT_DAU);
    }

    @Test
    @Transactional
    void fullUpdateToanTuWithPatch() throws Exception {
        // Initialize the database
        toanTuRepository.saveAndFlush(toanTu);

        int databaseSizeBeforeUpdate = toanTuRepository.findAll().size();

        // Update the toanTu using partial update
        ToanTu partialUpdatedToanTu = new ToanTu();
        partialUpdatedToanTu.setId(toanTu.getId());

        partialUpdatedToanTu.dau(UPDATED_DAU);

        restToanTuMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedToanTu.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedToanTu))
            )
            .andExpect(status().isOk());

        // Validate the ToanTu in the database
        List<ToanTu> toanTuList = toanTuRepository.findAll();
        assertThat(toanTuList).hasSize(databaseSizeBeforeUpdate);
        ToanTu testToanTu = toanTuList.get(toanTuList.size() - 1);
        assertThat(testToanTu.getDau()).isEqualTo(UPDATED_DAU);
    }

    @Test
    @Transactional
    void patchNonExistingToanTu() throws Exception {
        int databaseSizeBeforeUpdate = toanTuRepository.findAll().size();
        toanTu.setId(count.incrementAndGet());

        // Create the ToanTu
        ToanTuDTO toanTuDTO = toanTuMapper.toDto(toanTu);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restToanTuMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, toanTuDTO.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(toanTuDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the ToanTu in the database
        List<ToanTu> toanTuList = toanTuRepository.findAll();
        assertThat(toanTuList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchToanTu() throws Exception {
        int databaseSizeBeforeUpdate = toanTuRepository.findAll().size();
        toanTu.setId(count.incrementAndGet());

        // Create the ToanTu
        ToanTuDTO toanTuDTO = toanTuMapper.toDto(toanTu);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restToanTuMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(toanTuDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the ToanTu in the database
        List<ToanTu> toanTuList = toanTuRepository.findAll();
        assertThat(toanTuList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamToanTu() throws Exception {
        int databaseSizeBeforeUpdate = toanTuRepository.findAll().size();
        toanTu.setId(count.incrementAndGet());

        // Create the ToanTu
        ToanTuDTO toanTuDTO = toanTuMapper.toDto(toanTu);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restToanTuMockMvc
            .perform(
                patch(ENTITY_API_URL).contentType("application/merge-patch+json").content(TestUtil.convertObjectToJsonBytes(toanTuDTO))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the ToanTu in the database
        List<ToanTu> toanTuList = toanTuRepository.findAll();
        assertThat(toanTuList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteToanTu() throws Exception {
        // Initialize the database
        toanTuRepository.saveAndFlush(toanTu);

        int databaseSizeBeforeDelete = toanTuRepository.findAll().size();

        // Delete the toanTu
        restToanTuMockMvc
            .perform(delete(ENTITY_API_URL_ID, toanTu.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<ToanTu> toanTuList = toanTuRepository.findAll();
        assertThat(toanTuList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
