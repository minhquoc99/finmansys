import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize, map } from 'rxjs/operators';

import { INganSachNam, NganSachNam } from '../ngan-sach-nam.model';
import { NganSachNamService } from '../service/ngan-sach-nam.service';
import { IKhachSan } from 'app/entities/khach-san/khach-san.model';
import { KhachSanService } from 'app/entities/khach-san/service/khach-san.service';
import { KhoanMucChiPhiService } from 'app/entities/khoan-muc-chi-phi/service/khoan-muc-chi-phi.service';
import { IKhoanMucChiPhi } from 'app/entities/khoan-muc-chi-phi/khoan-muc-chi-phi.model';
import { INganSachThang, NganSachThang } from 'app/entities/ngan-sach-thang/ngan-sach-thang.model';

@Component({
  selector: 'jhi-ngan-sach-nam-update',
  templateUrl: './ngan-sach-nam-update.component.html',
})
export class NganSachNamUpdateComponent implements OnInit {
  isSaving = false;

  listMonth = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12];

  listTotal: number[] = [0];

  khachSansSharedCollection: IKhachSan[] = [];

  nganSachNam: INganSachNam | null = null;

  khoanMucChiPhis: IKhoanMucChiPhi[] = [];

  nganSachThangs: INganSachThang[] = [];

  editForm = this.fb.group({
    id: [],
    ten: [null, [Validators.required]],
    nam: [null, [Validators.required, Validators.min(1)]],
    soTien: [null, [Validators.required, Validators.min(1)]],
    khachSan: [null, Validators.required],
  });

  constructor(
    protected nganSachNamService: NganSachNamService,
    protected khachSanService: KhachSanService,
    protected khoanMucChiPhiService: KhoanMucChiPhiService,
    protected activatedRoute: ActivatedRoute,
    protected fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ nganSachNam }) => {
      this.updateForm(nganSachNam);
      this.nganSachNam = nganSachNam;
      if (this.nganSachNam?.id) {
        this.nganSachThangs = nganSachNam.nganSachThangs;
      }

      this.loadRelationshipsOptions();
    });

    this.khoanMucChiPhiService.query().subscribe(
      (res: HttpResponse<IKhoanMucChiPhi[]>) => {
        this.khoanMucChiPhis = res.body ?? [];
        if (!this.nganSachNam?.id) {
          for (const khoanMucChiPhi of this.khoanMucChiPhis) {
            this.listTotal.push(0);
            for (let i = 1; i <= 12; i++) {
              const nganSachThang = {
                ...new NganSachThang(),
                thang: i,
                khoanMucChiPhi,
                soTien: 0,
              };
              this.nganSachThangs.push(nganSachThang);
            }
          }
        } else {
          let i = 1;
          for (const khoanMucChiPhiIte of this.khoanMucChiPhis) {
            this.listTotal[i] = this.getTotal(khoanMucChiPhiIte);
            i++;
          }
        }
      },
      () => {
        this.khoanMucChiPhis = [];
      }
    );
  }

  onChangeMoney(thang: number, khoanMucChiPhi: IKhoanMucChiPhi, event: any): void {
    for (const nganSachThang of this.nganSachThangs) {
      if (nganSachThang.thang === thang && nganSachThang.khoanMucChiPhi?.id === khoanMucChiPhi.id) {
        nganSachThang.soTien = event.target.value;
      }
    }

    let i = 1;
    for (const khoanMucChiPhiIte of this.khoanMucChiPhis) {
      this.listTotal[i] = this.getTotal(khoanMucChiPhiIte);
      i++;
    }
  }

  getNganSachThang(thang: number, khoanMucChiPhi: IKhoanMucChiPhi): INganSachThang {
    const nganSachThangTmp = new NganSachThang();
    nganSachThangTmp.soTien = 0;
    for (const nganSachThang of this.nganSachThangs) {
      if (nganSachThang.thang === thang && nganSachThang.khoanMucChiPhi?.id === khoanMucChiPhi.id) {
        return nganSachThang;
      }
    }
    return nganSachThangTmp;
  }

  getMoney(thang: number, khoanMucChiPhi: IKhoanMucChiPhi): number {
    for (const nganSachThang of this.nganSachThangs) {
      if (nganSachThang.thang === thang && nganSachThang.khoanMucChiPhi?.id === khoanMucChiPhi.id) {
        return nganSachThang.soTien ?? 0;
      }
    }
    return 0;
  }

  getTotal(khoanMucChiPhi: IKhoanMucChiPhi): number {
    let total: number;
    total = 0;
    for (const nganSachThang of this.nganSachThangs) {
      if (nganSachThang.khoanMucChiPhi?.id === khoanMucChiPhi.id) {
        total = total + Number(nganSachThang.soTien ?? 0);
      }
    }
    return total;
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const nganSachNam = this.createFromForm();
    if (nganSachNam.id !== undefined) {
      this.subscribeToSaveResponse(this.nganSachNamService.update(nganSachNam));
    } else {
      this.subscribeToSaveResponse(this.nganSachNamService.create(nganSachNam));
    }
  }

  trackKhachSanById(index: number, item: IKhachSan): number {
    return item.id!;
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<INganSachNam>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(nganSachNam: INganSachNam): void {
    this.editForm.patchValue({
      id: nganSachNam.id,
      ten: nganSachNam.ten,
      nam: nganSachNam.nam,
      soTien: nganSachNam.soTien,
      khachSan: nganSachNam.khachSan,
    });

    this.khachSansSharedCollection = this.khachSanService.addKhachSanToCollectionIfMissing(
      this.khachSansSharedCollection,
      nganSachNam.khachSan
    );
  }

  protected loadRelationshipsOptions(): void {
    this.khachSanService
      .query()
      .pipe(map((res: HttpResponse<IKhachSan[]>) => res.body ?? []))
      .pipe(
        map((khachSans: IKhachSan[]) =>
          this.khachSanService.addKhachSanToCollectionIfMissing(khachSans, this.editForm.get('khachSan')!.value)
        )
      )
      .subscribe((khachSans: IKhachSan[]) => (this.khachSansSharedCollection = khachSans));
  }

  protected createFromForm(): INganSachNam {
    return {
      ...new NganSachNam(),
      id: this.editForm.get(['id'])!.value,
      ten: this.editForm.get(['ten'])!.value,
      nam: this.editForm.get(['nam'])!.value,
      soTien: this.editForm.get(['soTien'])!.value,
      khachSan: this.editForm.get(['khachSan'])!.value,
      nganSachThangs: this.nganSachThangs,
    };
  }
}
