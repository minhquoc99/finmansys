import * as dayjs from 'dayjs';
import { IKhachHang } from 'app/entities/khach-hang/khach-hang.model';
import { IMaKeToan } from 'app/entities/ma-ke-toan/ma-ke-toan.model';
import { IPhieuThu } from 'app/entities/phieu-thu/phieu-thu.model';
import { IPhongDuocDat } from 'app/entities/phong-duoc-dat/phong-duoc-dat.model';

export interface IKhoanPhaiThu {
  id?: number;
  soTien?: number;
  ngayTao?: dayjs.Dayjs;
  ngayPhaiThu?: dayjs.Dayjs;
  ghiChu?: string | null;
  khachHang?: IKhachHang;
  maKeToan?: IMaKeToan;
  phieuThus?: IPhieuThu[] | null;
  phongDuocDat?: IPhongDuocDat | null;
}

export class KhoanPhaiThu implements IKhoanPhaiThu {
  constructor(
    public id?: number,
    public soTien?: number,
    public ngayTao?: dayjs.Dayjs,
    public ngayPhaiThu?: dayjs.Dayjs,
    public ghiChu?: string | null,
    public khachHang?: IKhachHang,
    public maKeToan?: IMaKeToan,
    public phieuThus?: IPhieuThu[] | null,
    public phongDuocDat?: IPhongDuocDat | null
  ) {}
}

export function getKhoanPhaiThuIdentifier(khoanPhaiThu: IKhoanPhaiThu): number | undefined {
  return khoanPhaiThu.id;
}
