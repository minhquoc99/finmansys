package com.group3.finmansys.service;

import com.group3.finmansys.domain.*; // for static metamodels
import com.group3.finmansys.domain.TaiSanCD;
import com.group3.finmansys.repository.TaiSanCDRepository;
import com.group3.finmansys.service.criteria.TaiSanCDCriteria;
import com.group3.finmansys.service.dto.TaiSanCDDTO;
import com.group3.finmansys.service.mapper.TaiSanCDMapper;
import java.util.List;
import javax.persistence.criteria.JoinType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tech.jhipster.service.QueryService;

/**
 * Service for executing complex queries for {@link TaiSanCD} entities in the database.
 * The main input is a {@link TaiSanCDCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link TaiSanCDDTO} or a {@link Page} of {@link TaiSanCDDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class TaiSanCDQueryService extends QueryService<TaiSanCD> {

    private final Logger log = LoggerFactory.getLogger(TaiSanCDQueryService.class);

    private final TaiSanCDRepository taiSanCDRepository;

    private final TaiSanCDMapper taiSanCDMapper;

    public TaiSanCDQueryService(TaiSanCDRepository taiSanCDRepository, TaiSanCDMapper taiSanCDMapper) {
        this.taiSanCDRepository = taiSanCDRepository;
        this.taiSanCDMapper = taiSanCDMapper;
    }

    /**
     * Return a {@link List} of {@link TaiSanCDDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<TaiSanCDDTO> findByCriteria(TaiSanCDCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<TaiSanCD> specification = createSpecification(criteria);
        return taiSanCDMapper.toDto(taiSanCDRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link TaiSanCDDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<TaiSanCDDTO> findByCriteria(TaiSanCDCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<TaiSanCD> specification = createSpecification(criteria);
        return taiSanCDRepository.findAll(specification, page).map(taiSanCDMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(TaiSanCDCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<TaiSanCD> specification = createSpecification(criteria);
        return taiSanCDRepository.count(specification);
    }

    /**
     * Function to convert {@link TaiSanCDCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<TaiSanCD> createSpecification(TaiSanCDCriteria criteria) {
        Specification<TaiSanCD> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getId(), TaiSanCD_.id));
            }
            if (criteria.getTen() != null) {
                specification = specification.and(buildStringSpecification(criteria.getTen(), TaiSanCD_.ten));
            }
            if (criteria.getDonViTuoiTho() != null) {
                specification = specification.and(buildStringSpecification(criteria.getDonViTuoiTho(), TaiSanCD_.donViTuoiTho));
            }
            if (criteria.getThoiGianSuDung() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getThoiGianSuDung(), TaiSanCD_.thoiGianSuDung));
            }
            if (criteria.getKhachSanId() != null) {
                specification =
                    specification.and(
                        buildSpecification(criteria.getKhachSanId(), root -> root.join(TaiSanCD_.khachSan, JoinType.LEFT).get(KhachSan_.id))
                    );
            }
            if (criteria.getLoaiTaiSanId() != null) {
                specification =
                    specification.and(
                        buildSpecification(
                            criteria.getLoaiTaiSanId(),
                            root -> root.join(TaiSanCD_.loaiTaiSan, JoinType.LEFT).get(LoaiTaiSan_.id)
                        )
                    );
            }
        }
        return specification;
    }
}
