import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize, map } from 'rxjs/operators';

import * as dayjs from 'dayjs';
import { DATE_TIME_FORMAT } from 'app/config/input.constants';

import { IPhieuChi, PhieuChi } from '../phieu-chi.model';
import { PhieuChiService } from '../service/phieu-chi.service';
import { IKhachHang } from 'app/entities/khach-hang/khach-hang.model';
import { KhachHangService } from 'app/entities/khach-hang/service/khach-hang.service';
import { IKhoanMucChiPhi } from 'app/entities/khoan-muc-chi-phi/khoan-muc-chi-phi.model';
import { KhoanMucChiPhiService } from 'app/entities/khoan-muc-chi-phi/service/khoan-muc-chi-phi.service';
import { IKhoanPhaiTra } from 'app/entities/khoan-phai-tra/khoan-phai-tra.model';
import { KhoanPhaiTraService } from 'app/entities/khoan-phai-tra/service/khoan-phai-tra.service';
import { IQuyTien } from 'app/entities/quy-tien/quy-tien.model';
import { QuyTienService } from 'app/entities/quy-tien/service/quy-tien.service';

@Component({
  selector: 'jhi-phieu-chi-update',
  templateUrl: './phieu-chi-update.component.html',
})
export class PhieuChiUpdateComponent implements OnInit {
  isSaving = false;

  khachHangsSharedCollection: IKhachHang[] = [];
  khoanMucChiPhisSharedCollection: IKhoanMucChiPhi[] = [];
  khoanPhaiTrasSharedCollection: IKhoanPhaiTra[] = [];
  quyTiensSharedCollection: IQuyTien[] = [];

  khoanPhaiTraViews: IKhoanPhaiTra[] = [];

  editForm = this.fb.group({
    id: [],
    ngayChungTu: [null, [Validators.required]],
    soTien: [null, [Validators.required, Validators.min(1)]],
    khachHang: [null, Validators.required],
    khoanMucChiPhi: [null, Validators.required],
    khoanPhaiTra: [],
    quyTien: [null, Validators.required],
  });

  constructor(
    protected phieuChiService: PhieuChiService,
    protected khachHangService: KhachHangService,
    protected khoanMucChiPhiService: KhoanMucChiPhiService,
    protected khoanPhaiTraService: KhoanPhaiTraService,
    protected quyTienService: QuyTienService,
    protected activatedRoute: ActivatedRoute,
    protected fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ phieuChi }) => {
      if (phieuChi.id === undefined) {
        const today = dayjs().startOf('day');
        phieuChi.ngayChungTu = today;
      }

      this.updateForm(phieuChi);

      this.loadRelationshipsOptions();
    });
  }

  onChangeKhachHang(): void {
    this.khoanPhaiTraViews = [];
    let checked = true;
    for (const khoanPhaiTra of this.khoanPhaiTrasSharedCollection) {
      if (khoanPhaiTra.khachHang?.id === this.editForm.get('khachHang')!.value.id) {
        this.khoanPhaiTraViews.push(khoanPhaiTra);
        if (!this.editForm.get('khoanPhaiTra')!.value?.id) {
          checked = false;
        } else {
          if (khoanPhaiTra.id === this.editForm.get('khoanPhaiTra')!.value.id) {
            checked = false;
          }
        }
      }
    }

    if (this.editForm.get('khoanPhaiTra') && checked) {
      this.editForm.get('khoanPhaiTra')?.patchValue(this.khoanPhaiTraViews[0]);
    }
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const phieuChi = this.createFromForm();
    if (phieuChi.id !== undefined) {
      this.subscribeToSaveResponse(this.phieuChiService.update(phieuChi));
    } else {
      this.subscribeToSaveResponse(this.phieuChiService.create(phieuChi));
    }
  }

  trackKhachHangById(index: number, item: IKhachHang): number {
    return item.id!;
  }

  trackKhoanMucChiPhiById(index: number, item: IKhoanMucChiPhi): number {
    return item.id!;
  }

  trackKhoanPhaiTraById(index: number, item: IKhoanPhaiTra): number {
    return item.id!;
  }

  trackQuyTienById(index: number, item: IQuyTien): number {
    return item.id!;
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IPhieuChi>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(phieuChi: IPhieuChi): void {
    this.editForm.patchValue({
      id: phieuChi.id,
      ngayChungTu: phieuChi.ngayChungTu ? phieuChi.ngayChungTu.format(DATE_TIME_FORMAT) : null,
      soTien: phieuChi.soTien,
      khachHang: phieuChi.khachHang,
      khoanMucChiPhi: phieuChi.khoanMucChiPhi,
      khoanPhaiTra: phieuChi.khoanPhaiTra,
      quyTien: phieuChi.quyTien,
    });

    this.khachHangsSharedCollection = this.khachHangService.addKhachHangToCollectionIfMissing(
      this.khachHangsSharedCollection,
      phieuChi.khachHang
    );
    this.khoanMucChiPhisSharedCollection = this.khoanMucChiPhiService.addKhoanMucChiPhiToCollectionIfMissing(
      this.khoanMucChiPhisSharedCollection,
      phieuChi.khoanMucChiPhi
    );
    this.khoanPhaiTrasSharedCollection = this.khoanPhaiTraService.addKhoanPhaiTraToCollectionIfMissing(
      this.khoanPhaiTrasSharedCollection,
      phieuChi.khoanPhaiTra
    );
    this.quyTiensSharedCollection = this.quyTienService.addQuyTienToCollectionIfMissing(this.quyTiensSharedCollection, phieuChi.quyTien);
  }

  protected loadRelationshipsOptions(): void {
    this.khachHangService
      .query({ page: 0, size: 10000, sort: ['id,asc'] })
      .pipe(map((res: HttpResponse<IKhachHang[]>) => res.body ?? []))
      .pipe(
        map((khachHangs: IKhachHang[]) =>
          this.khachHangService.addKhachHangToCollectionIfMissing(khachHangs, this.editForm.get('khachHang')!.value)
        )
      )
      .subscribe((khachHangs: IKhachHang[]) => {
        this.khachHangsSharedCollection = khachHangs;
        if (!this.editForm.get('khachHang')!.value) {
          this.editForm.get('khachHang')?.patchValue(this.khachHangsSharedCollection[0]);
        }
        this.khoanPhaiTraService
          .query({ page: 0, size: 10000, sort: ['id,asc'] })
          .pipe(map((res: HttpResponse<IKhoanPhaiTra[]>) => res.body ?? []))
          .pipe(
            map((khoanPhaiTras: IKhoanPhaiTra[]) =>
              this.khoanPhaiTraService.addKhoanPhaiTraToCollectionIfMissing(khoanPhaiTras, this.editForm.get('khoanPhaiTra')!.value)
            )
          )
          .subscribe((khoanPhaiTras: IKhoanPhaiTra[]) => {
            this.khoanPhaiTrasSharedCollection = khoanPhaiTras;
            this.onChangeKhachHang();
          });
      });

    this.khoanMucChiPhiService
      .query({ page: 0, size: 10000, sort: ['id,asc'] })
      .pipe(map((res: HttpResponse<IKhoanMucChiPhi[]>) => res.body ?? []))
      .pipe(
        map((khoanMucChiPhis: IKhoanMucChiPhi[]) =>
          this.khoanMucChiPhiService.addKhoanMucChiPhiToCollectionIfMissing(khoanMucChiPhis, this.editForm.get('khoanMucChiPhi')!.value)
        )
      )
      .subscribe((khoanMucChiPhis: IKhoanMucChiPhi[]) => (this.khoanMucChiPhisSharedCollection = khoanMucChiPhis));

    this.quyTienService
      .query({ page: 0, size: 10000, sort: ['id,asc'] })
      .pipe(map((res: HttpResponse<IQuyTien[]>) => res.body ?? []))
      .pipe(
        map((quyTiens: IQuyTien[]) => this.quyTienService.addQuyTienToCollectionIfMissing(quyTiens, this.editForm.get('quyTien')!.value))
      )
      .subscribe((quyTiens: IQuyTien[]) => (this.quyTiensSharedCollection = quyTiens));
  }

  protected createFromForm(): IPhieuChi {
    return {
      ...new PhieuChi(),
      id: this.editForm.get(['id'])!.value,
      ngayChungTu: this.editForm.get(['ngayChungTu'])!.value
        ? dayjs(this.editForm.get(['ngayChungTu'])!.value, DATE_TIME_FORMAT)
        : undefined,
      soTien: this.editForm.get(['soTien'])!.value,
      khachHang: this.editForm.get(['khachHang'])!.value,
      khoanMucChiPhi: this.editForm.get(['khoanMucChiPhi'])!.value,
      khoanPhaiTra: this.editForm.get(['khoanPhaiTra'])!.value,
      quyTien: this.editForm.get(['quyTien'])!.value,
    };
  }
}
