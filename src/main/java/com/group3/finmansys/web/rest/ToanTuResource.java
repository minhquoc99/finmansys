package com.group3.finmansys.web.rest;

import com.group3.finmansys.repository.ToanTuRepository;
import com.group3.finmansys.service.ToanTuQueryService;
import com.group3.finmansys.service.ToanTuService;
import com.group3.finmansys.service.criteria.ToanTuCriteria;
import com.group3.finmansys.service.dto.ToanTuDTO;
import com.group3.finmansys.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.PaginationUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link com.group3.finmansys.domain.ToanTu}.
 */
@RestController
@RequestMapping("/api")
public class ToanTuResource {

    private final Logger log = LoggerFactory.getLogger(ToanTuResource.class);

    private static final String ENTITY_NAME = "toanTu";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final ToanTuService toanTuService;

    private final ToanTuRepository toanTuRepository;

    private final ToanTuQueryService toanTuQueryService;

    public ToanTuResource(ToanTuService toanTuService, ToanTuRepository toanTuRepository, ToanTuQueryService toanTuQueryService) {
        this.toanTuService = toanTuService;
        this.toanTuRepository = toanTuRepository;
        this.toanTuQueryService = toanTuQueryService;
    }

    /**
     * {@code POST  /toan-tus} : Create a new toanTu.
     *
     * @param toanTuDTO the toanTuDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new toanTuDTO, or with status {@code 400 (Bad Request)} if the toanTu has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/toan-tus")
    public ResponseEntity<ToanTuDTO> createToanTu(@Valid @RequestBody ToanTuDTO toanTuDTO) throws URISyntaxException {
        log.debug("REST request to save ToanTu : {}", toanTuDTO);
        if (toanTuDTO.getId() != null) {
            throw new BadRequestAlertException("A new toanTu cannot already have an ID", ENTITY_NAME, "idexists");
        }
        ToanTuDTO result = toanTuService.save(toanTuDTO);
        return ResponseEntity
            .created(new URI("/api/toan-tus/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /toan-tus/:id} : Updates an existing toanTu.
     *
     * @param id the id of the toanTuDTO to save.
     * @param toanTuDTO the toanTuDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated toanTuDTO,
     * or with status {@code 400 (Bad Request)} if the toanTuDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the toanTuDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/toan-tus/{id}")
    public ResponseEntity<ToanTuDTO> updateToanTu(
        @PathVariable(value = "id", required = false) final Long id,
        @Valid @RequestBody ToanTuDTO toanTuDTO
    ) throws URISyntaxException {
        log.debug("REST request to update ToanTu : {}, {}", id, toanTuDTO);
        if (toanTuDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, toanTuDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!toanTuRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        ToanTuDTO result = toanTuService.save(toanTuDTO);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, toanTuDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /toan-tus/:id} : Partial updates given fields of an existing toanTu, field will ignore if it is null
     *
     * @param id the id of the toanTuDTO to save.
     * @param toanTuDTO the toanTuDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated toanTuDTO,
     * or with status {@code 400 (Bad Request)} if the toanTuDTO is not valid,
     * or with status {@code 404 (Not Found)} if the toanTuDTO is not found,
     * or with status {@code 500 (Internal Server Error)} if the toanTuDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/toan-tus/{id}", consumes = "application/merge-patch+json")
    public ResponseEntity<ToanTuDTO> partialUpdateToanTu(
        @PathVariable(value = "id", required = false) final Long id,
        @NotNull @RequestBody ToanTuDTO toanTuDTO
    ) throws URISyntaxException {
        log.debug("REST request to partial update ToanTu partially : {}, {}", id, toanTuDTO);
        if (toanTuDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, toanTuDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!toanTuRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<ToanTuDTO> result = toanTuService.partialUpdate(toanTuDTO);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, toanTuDTO.getId().toString())
        );
    }

    /**
     * {@code GET  /toan-tus} : get all the toanTus.
     *
     * @param pageable the pagination information.
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of toanTus in body.
     */
    @GetMapping("/toan-tus")
    public ResponseEntity<List<ToanTuDTO>> getAllToanTus(ToanTuCriteria criteria, Pageable pageable) {
        log.debug("REST request to get ToanTus by criteria: {}", criteria);
        Page<ToanTuDTO> page = toanTuQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /toan-tus/count} : count all the toanTus.
     *
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
     */
    @GetMapping("/toan-tus/count")
    public ResponseEntity<Long> countToanTus(ToanTuCriteria criteria) {
        log.debug("REST request to count ToanTus by criteria: {}", criteria);
        return ResponseEntity.ok().body(toanTuQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /toan-tus/:id} : get the "id" toanTu.
     *
     * @param id the id of the toanTuDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the toanTuDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/toan-tus/{id}")
    public ResponseEntity<ToanTuDTO> getToanTu(@PathVariable Long id) {
        log.debug("REST request to get ToanTu : {}", id);
        Optional<ToanTuDTO> toanTuDTO = toanTuService.findOne(id);
        return ResponseUtil.wrapOrNotFound(toanTuDTO);
    }

    /**
     * {@code DELETE  /toan-tus/:id} : delete the "id" toanTu.
     *
     * @param id the id of the toanTuDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/toan-tus/{id}")
    public ResponseEntity<Void> deleteToanTu(@PathVariable Long id) {
        log.debug("REST request to delete ToanTu : {}", id);
        toanTuService.delete(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
            .build();
    }
}
