package com.group3.finmansys.service.dto;

import static org.assertj.core.api.Assertions.assertThat;

import com.group3.finmansys.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class NganSachThangDTOTest {

    @Test
    void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(NganSachThangDTO.class);
        NganSachThangDTO nganSachThangDTO1 = new NganSachThangDTO();
        nganSachThangDTO1.setId(1L);
        NganSachThangDTO nganSachThangDTO2 = new NganSachThangDTO();
        assertThat(nganSachThangDTO1).isNotEqualTo(nganSachThangDTO2);
        nganSachThangDTO2.setId(nganSachThangDTO1.getId());
        assertThat(nganSachThangDTO1).isEqualTo(nganSachThangDTO2);
        nganSachThangDTO2.setId(2L);
        assertThat(nganSachThangDTO1).isNotEqualTo(nganSachThangDTO2);
        nganSachThangDTO1.setId(null);
        assertThat(nganSachThangDTO1).isNotEqualTo(nganSachThangDTO2);
    }
}
