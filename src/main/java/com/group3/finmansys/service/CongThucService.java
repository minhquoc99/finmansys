package com.group3.finmansys.service;

import com.group3.finmansys.service.dto.CongThucDTO;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Service Interface for managing {@link com.group3.finmansys.domain.CongThuc}.
 */
public interface CongThucService {
    /**
     * Save a congThuc.
     *
     * @param congThucDTO the entity to save.
     * @return the persisted entity.
     */
    CongThucDTO save(CongThucDTO congThucDTO);

    /**
     * Partially updates a congThuc.
     *
     * @param congThucDTO the entity to update partially.
     * @return the persisted entity.
     */
    Optional<CongThucDTO> partialUpdate(CongThucDTO congThucDTO);

    /**
     * Get all the congThucs.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<CongThucDTO> findAll(Pageable pageable);

    /**
     * Get the "id" congThuc.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<CongThucDTO> findOne(Long id);

    /**
     * Delete the "id" congThuc.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
