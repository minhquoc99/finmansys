import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { IKhoanMucChiPhi, KhoanMucChiPhi } from '../khoan-muc-chi-phi.model';

import { KhoanMucChiPhiService } from './khoan-muc-chi-phi.service';

describe('Service Tests', () => {
  describe('KhoanMucChiPhi Service', () => {
    let service: KhoanMucChiPhiService;
    let httpMock: HttpTestingController;
    let elemDefault: IKhoanMucChiPhi;
    let expectedResult: IKhoanMucChiPhi | IKhoanMucChiPhi[] | boolean | null;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule],
      });
      expectedResult = null;
      service = TestBed.inject(KhoanMucChiPhiService);
      httpMock = TestBed.inject(HttpTestingController);

      elemDefault = {
        id: 0,
        ten: 'AAAAAAA',
        moTa: 'AAAAAAA',
      };
    });

    describe('Service methods', () => {
      it('should find an element', () => {
        const returnedFromService = Object.assign({}, elemDefault);

        service.find(123).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(elemDefault);
      });

      it('should create a KhoanMucChiPhi', () => {
        const returnedFromService = Object.assign(
          {
            id: 0,
          },
          elemDefault
        );

        const expected = Object.assign({}, returnedFromService);

        service.create(new KhoanMucChiPhi()).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'POST' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should update a KhoanMucChiPhi', () => {
        const returnedFromService = Object.assign(
          {
            id: 1,
            ten: 'BBBBBB',
            moTa: 'BBBBBB',
          },
          elemDefault
        );

        const expected = Object.assign({}, returnedFromService);

        service.update(expected).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'PUT' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should partial update a KhoanMucChiPhi', () => {
        const patchObject = Object.assign({}, new KhoanMucChiPhi());

        const returnedFromService = Object.assign(patchObject, elemDefault);

        const expected = Object.assign({}, returnedFromService);

        service.partialUpdate(patchObject).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'PATCH' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should return a list of KhoanMucChiPhi', () => {
        const returnedFromService = Object.assign(
          {
            id: 1,
            ten: 'BBBBBB',
            moTa: 'BBBBBB',
          },
          elemDefault
        );

        const expected = Object.assign({}, returnedFromService);

        service.query().subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush([returnedFromService]);
        httpMock.verify();
        expect(expectedResult).toContainEqual(expected);
      });

      it('should delete a KhoanMucChiPhi', () => {
        service.delete(123).subscribe(resp => (expectedResult = resp.ok));

        const req = httpMock.expectOne({ method: 'DELETE' });
        req.flush({ status: 200 });
        expect(expectedResult);
      });

      describe('addKhoanMucChiPhiToCollectionIfMissing', () => {
        it('should add a KhoanMucChiPhi to an empty array', () => {
          const khoanMucChiPhi: IKhoanMucChiPhi = { id: 123 };
          expectedResult = service.addKhoanMucChiPhiToCollectionIfMissing([], khoanMucChiPhi);
          expect(expectedResult).toHaveLength(1);
          expect(expectedResult).toContain(khoanMucChiPhi);
        });

        it('should not add a KhoanMucChiPhi to an array that contains it', () => {
          const khoanMucChiPhi: IKhoanMucChiPhi = { id: 123 };
          const khoanMucChiPhiCollection: IKhoanMucChiPhi[] = [
            {
              ...khoanMucChiPhi,
            },
            { id: 456 },
          ];
          expectedResult = service.addKhoanMucChiPhiToCollectionIfMissing(khoanMucChiPhiCollection, khoanMucChiPhi);
          expect(expectedResult).toHaveLength(2);
        });

        it("should add a KhoanMucChiPhi to an array that doesn't contain it", () => {
          const khoanMucChiPhi: IKhoanMucChiPhi = { id: 123 };
          const khoanMucChiPhiCollection: IKhoanMucChiPhi[] = [{ id: 456 }];
          expectedResult = service.addKhoanMucChiPhiToCollectionIfMissing(khoanMucChiPhiCollection, khoanMucChiPhi);
          expect(expectedResult).toHaveLength(2);
          expect(expectedResult).toContain(khoanMucChiPhi);
        });

        it('should add only unique KhoanMucChiPhi to an array', () => {
          const khoanMucChiPhiArray: IKhoanMucChiPhi[] = [{ id: 123 }, { id: 456 }, { id: 20396 }];
          const khoanMucChiPhiCollection: IKhoanMucChiPhi[] = [{ id: 123 }];
          expectedResult = service.addKhoanMucChiPhiToCollectionIfMissing(khoanMucChiPhiCollection, ...khoanMucChiPhiArray);
          expect(expectedResult).toHaveLength(3);
        });

        it('should accept varargs', () => {
          const khoanMucChiPhi: IKhoanMucChiPhi = { id: 123 };
          const khoanMucChiPhi2: IKhoanMucChiPhi = { id: 456 };
          expectedResult = service.addKhoanMucChiPhiToCollectionIfMissing([], khoanMucChiPhi, khoanMucChiPhi2);
          expect(expectedResult).toHaveLength(2);
          expect(expectedResult).toContain(khoanMucChiPhi);
          expect(expectedResult).toContain(khoanMucChiPhi2);
        });

        it('should accept null and undefined values', () => {
          const khoanMucChiPhi: IKhoanMucChiPhi = { id: 123 };
          expectedResult = service.addKhoanMucChiPhiToCollectionIfMissing([], null, khoanMucChiPhi, undefined);
          expect(expectedResult).toHaveLength(1);
          expect(expectedResult).toContain(khoanMucChiPhi);
        });
      });
    });

    afterEach(() => {
      httpMock.verify();
    });
  });
});
