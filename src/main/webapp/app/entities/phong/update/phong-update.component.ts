import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize, map } from 'rxjs/operators';

import { IPhong, Phong } from '../phong.model';
import { PhongService } from '../service/phong.service';
import { IKhachSan } from 'app/entities/khach-san/khach-san.model';
import { KhachSanService } from 'app/entities/khach-san/service/khach-san.service';

@Component({
  selector: 'jhi-phong-update',
  templateUrl: './phong-update.component.html',
})
export class PhongUpdateComponent implements OnInit {
  isSaving = false;

  khachSansSharedCollection: IKhachSan[] = [];

  editForm = this.fb.group({
    id: [],
    ten: [null, [Validators.required]],
    loai: [null, [Validators.required]],
    gia: [null, [Validators.required, Validators.min(1)]],
    khachSan: [null, Validators.required],
  });

  constructor(
    protected phongService: PhongService,
    protected khachSanService: KhachSanService,
    protected activatedRoute: ActivatedRoute,
    protected fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ phong }) => {
      this.updateForm(phong);

      this.loadRelationshipsOptions();
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const phong = this.createFromForm();
    if (phong.id !== undefined) {
      this.subscribeToSaveResponse(this.phongService.update(phong));
    } else {
      this.subscribeToSaveResponse(this.phongService.create(phong));
    }
  }

  trackKhachSanById(index: number, item: IKhachSan): number {
    return item.id!;
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IPhong>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(phong: IPhong): void {
    this.editForm.patchValue({
      id: phong.id,
      ten: phong.ten,
      loai: phong.loai,
      gia: phong.gia,
      khachSan: phong.khachSan,
    });

    this.khachSansSharedCollection = this.khachSanService.addKhachSanToCollectionIfMissing(this.khachSansSharedCollection, phong.khachSan);
  }

  protected loadRelationshipsOptions(): void {
    this.khachSanService
      .query()
      .pipe(map((res: HttpResponse<IKhachSan[]>) => res.body ?? []))
      .pipe(
        map((khachSans: IKhachSan[]) =>
          this.khachSanService.addKhachSanToCollectionIfMissing(khachSans, this.editForm.get('khachSan')!.value)
        )
      )
      .subscribe((khachSans: IKhachSan[]) => (this.khachSansSharedCollection = khachSans));
  }

  protected createFromForm(): IPhong {
    return {
      ...new Phong(),
      id: this.editForm.get(['id'])!.value,
      ten: this.editForm.get(['ten'])!.value,
      loai: this.editForm.get(['loai'])!.value,
      gia: this.editForm.get(['gia'])!.value,
      khachSan: this.editForm.get(['khachSan'])!.value,
    };
  }
}
