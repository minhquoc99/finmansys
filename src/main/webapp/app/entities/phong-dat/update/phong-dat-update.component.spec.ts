jest.mock('@angular/router');

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { of, Subject } from 'rxjs';

import { PhongDatService } from '../service/phong-dat.service';
import { IPhongDat, PhongDat } from '../phong-dat.model';
import { IPhong } from 'app/entities/phong/phong.model';
import { PhongService } from 'app/entities/phong/service/phong.service';
import { IPhongDuocDat } from 'app/entities/phong-duoc-dat/phong-duoc-dat.model';
import { PhongDuocDatService } from 'app/entities/phong-duoc-dat/service/phong-duoc-dat.service';

import { PhongDatUpdateComponent } from './phong-dat-update.component';

describe('Component Tests', () => {
  describe('PhongDat Management Update Component', () => {
    let comp: PhongDatUpdateComponent;
    let fixture: ComponentFixture<PhongDatUpdateComponent>;
    let activatedRoute: ActivatedRoute;
    let phongDatService: PhongDatService;
    let phongService: PhongService;
    let phongDuocDatService: PhongDuocDatService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule],
        declarations: [PhongDatUpdateComponent],
        providers: [FormBuilder, ActivatedRoute],
      })
        .overrideTemplate(PhongDatUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(PhongDatUpdateComponent);
      activatedRoute = TestBed.inject(ActivatedRoute);
      phongDatService = TestBed.inject(PhongDatService);
      phongService = TestBed.inject(PhongService);
      phongDuocDatService = TestBed.inject(PhongDuocDatService);

      comp = fixture.componentInstance;
    });

    describe('ngOnInit', () => {
      it('Should call Phong query and add missing value', () => {
        const phongDat: IPhongDat = { id: 456 };
        const phong: IPhong = { id: 62841 };
        phongDat.phong = phong;

        const phongCollection: IPhong[] = [{ id: 43112 }];
        spyOn(phongService, 'query').and.returnValue(of(new HttpResponse({ body: phongCollection })));
        const additionalPhongs = [phong];
        const expectedCollection: IPhong[] = [...additionalPhongs, ...phongCollection];
        spyOn(phongService, 'addPhongToCollectionIfMissing').and.returnValue(expectedCollection);

        activatedRoute.data = of({ phongDat });
        comp.ngOnInit();

        expect(phongService.query).toHaveBeenCalled();
        expect(phongService.addPhongToCollectionIfMissing).toHaveBeenCalledWith(phongCollection, ...additionalPhongs);
        expect(comp.phongsSharedCollection).toEqual(expectedCollection);
      });

      it('Should call PhongDuocDat query and add missing value', () => {
        const phongDat: IPhongDat = { id: 456 };
        const phongDuocDat: IPhongDuocDat = { id: 91569 };
        phongDat.phongDuocDat = phongDuocDat;

        const phongDuocDatCollection: IPhongDuocDat[] = [{ id: 12420 }];
        spyOn(phongDuocDatService, 'query').and.returnValue(of(new HttpResponse({ body: phongDuocDatCollection })));
        const additionalPhongDuocDats = [phongDuocDat];
        const expectedCollection: IPhongDuocDat[] = [...additionalPhongDuocDats, ...phongDuocDatCollection];
        spyOn(phongDuocDatService, 'addPhongDuocDatToCollectionIfMissing').and.returnValue(expectedCollection);

        activatedRoute.data = of({ phongDat });
        comp.ngOnInit();

        expect(phongDuocDatService.query).toHaveBeenCalled();
        expect(phongDuocDatService.addPhongDuocDatToCollectionIfMissing).toHaveBeenCalledWith(
          phongDuocDatCollection,
          ...additionalPhongDuocDats
        );
        expect(comp.phongDuocDatsSharedCollection).toEqual(expectedCollection);
      });

      it('Should update editForm', () => {
        const phongDat: IPhongDat = { id: 456 };
        const phong: IPhong = { id: 48161 };
        phongDat.phong = phong;
        const phongDuocDat: IPhongDuocDat = { id: 25943 };
        phongDat.phongDuocDat = phongDuocDat;

        activatedRoute.data = of({ phongDat });
        comp.ngOnInit();

        expect(comp.editForm.value).toEqual(expect.objectContaining(phongDat));
        expect(comp.phongsSharedCollection).toContain(phong);
        expect(comp.phongDuocDatsSharedCollection).toContain(phongDuocDat);
      });
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', () => {
        // GIVEN
        const saveSubject = new Subject();
        const phongDat = { id: 123 };
        spyOn(phongDatService, 'update').and.returnValue(saveSubject);
        spyOn(comp, 'previousState');
        activatedRoute.data = of({ phongDat });
        comp.ngOnInit();

        // WHEN
        comp.save();
        expect(comp.isSaving).toEqual(true);
        saveSubject.next(new HttpResponse({ body: phongDat }));
        saveSubject.complete();

        // THEN
        expect(comp.previousState).toHaveBeenCalled();
        expect(phongDatService.update).toHaveBeenCalledWith(phongDat);
        expect(comp.isSaving).toEqual(false);
      });

      it('Should call create service on save for new entity', () => {
        // GIVEN
        const saveSubject = new Subject();
        const phongDat = new PhongDat();
        spyOn(phongDatService, 'create').and.returnValue(saveSubject);
        spyOn(comp, 'previousState');
        activatedRoute.data = of({ phongDat });
        comp.ngOnInit();

        // WHEN
        comp.save();
        expect(comp.isSaving).toEqual(true);
        saveSubject.next(new HttpResponse({ body: phongDat }));
        saveSubject.complete();

        // THEN
        expect(phongDatService.create).toHaveBeenCalledWith(phongDat);
        expect(comp.isSaving).toEqual(false);
        expect(comp.previousState).toHaveBeenCalled();
      });

      it('Should set isSaving to false on error', () => {
        // GIVEN
        const saveSubject = new Subject();
        const phongDat = { id: 123 };
        spyOn(phongDatService, 'update').and.returnValue(saveSubject);
        spyOn(comp, 'previousState');
        activatedRoute.data = of({ phongDat });
        comp.ngOnInit();

        // WHEN
        comp.save();
        expect(comp.isSaving).toEqual(true);
        saveSubject.error('This is an error!');

        // THEN
        expect(phongDatService.update).toHaveBeenCalledWith(phongDat);
        expect(comp.isSaving).toEqual(false);
        expect(comp.previousState).not.toHaveBeenCalled();
      });
    });

    describe('Tracking relationships identifiers', () => {
      describe('trackPhongById', () => {
        it('Should return tracked Phong primary key', () => {
          const entity = { id: 123 };
          const trackResult = comp.trackPhongById(0, entity);
          expect(trackResult).toEqual(entity.id);
        });
      });

      describe('trackPhongDuocDatById', () => {
        it('Should return tracked PhongDuocDat primary key', () => {
          const entity = { id: 123 };
          const trackResult = comp.trackPhongDuocDatById(0, entity);
          expect(trackResult).toEqual(entity.id);
        });
      });
    });
  });
});
