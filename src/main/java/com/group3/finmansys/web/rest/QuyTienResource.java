package com.group3.finmansys.web.rest;

import com.group3.finmansys.repository.QuyTienRepository;
import com.group3.finmansys.service.QuyTienQueryService;
import com.group3.finmansys.service.QuyTienService;
import com.group3.finmansys.service.criteria.QuyTienCriteria;
import com.group3.finmansys.service.dto.QuyTienDTO;
import com.group3.finmansys.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.PaginationUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link com.group3.finmansys.domain.QuyTien}.
 */
@RestController
@RequestMapping("/api")
public class QuyTienResource {

    private final Logger log = LoggerFactory.getLogger(QuyTienResource.class);

    private static final String ENTITY_NAME = "quyTien";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final QuyTienService quyTienService;

    private final QuyTienRepository quyTienRepository;

    private final QuyTienQueryService quyTienQueryService;

    public QuyTienResource(QuyTienService quyTienService, QuyTienRepository quyTienRepository, QuyTienQueryService quyTienQueryService) {
        this.quyTienService = quyTienService;
        this.quyTienRepository = quyTienRepository;
        this.quyTienQueryService = quyTienQueryService;
    }

    /**
     * {@code POST  /quy-tiens} : Create a new quyTien.
     *
     * @param quyTienDTO the quyTienDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new quyTienDTO, or with status {@code 400 (Bad Request)} if the quyTien has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/quy-tiens")
    public ResponseEntity<QuyTienDTO> createQuyTien(@Valid @RequestBody QuyTienDTO quyTienDTO) throws URISyntaxException {
        log.debug("REST request to save QuyTien : {}", quyTienDTO);
        if (quyTienDTO.getId() != null) {
            throw new BadRequestAlertException("A new quyTien cannot already have an ID", ENTITY_NAME, "idexists");
        }
        QuyTienDTO result = quyTienService.save(quyTienDTO);
        return ResponseEntity
            .created(new URI("/api/quy-tiens/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /quy-tiens/:id} : Updates an existing quyTien.
     *
     * @param id the id of the quyTienDTO to save.
     * @param quyTienDTO the quyTienDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated quyTienDTO,
     * or with status {@code 400 (Bad Request)} if the quyTienDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the quyTienDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/quy-tiens/{id}")
    public ResponseEntity<QuyTienDTO> updateQuyTien(
        @PathVariable(value = "id", required = false) final Long id,
        @Valid @RequestBody QuyTienDTO quyTienDTO
    ) throws URISyntaxException {
        log.debug("REST request to update QuyTien : {}, {}", id, quyTienDTO);
        if (quyTienDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, quyTienDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!quyTienRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        QuyTienDTO result = quyTienService.save(quyTienDTO);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, quyTienDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /quy-tiens/:id} : Partial updates given fields of an existing quyTien, field will ignore if it is null
     *
     * @param id the id of the quyTienDTO to save.
     * @param quyTienDTO the quyTienDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated quyTienDTO,
     * or with status {@code 400 (Bad Request)} if the quyTienDTO is not valid,
     * or with status {@code 404 (Not Found)} if the quyTienDTO is not found,
     * or with status {@code 500 (Internal Server Error)} if the quyTienDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/quy-tiens/{id}", consumes = "application/merge-patch+json")
    public ResponseEntity<QuyTienDTO> partialUpdateQuyTien(
        @PathVariable(value = "id", required = false) final Long id,
        @NotNull @RequestBody QuyTienDTO quyTienDTO
    ) throws URISyntaxException {
        log.debug("REST request to partial update QuyTien partially : {}, {}", id, quyTienDTO);
        if (quyTienDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, quyTienDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!quyTienRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<QuyTienDTO> result = quyTienService.partialUpdate(quyTienDTO);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, quyTienDTO.getId().toString())
        );
    }

    /**
     * {@code GET  /quy-tiens} : get all the quyTiens.
     *
     * @param pageable the pagination information.
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of quyTiens in body.
     */
    @GetMapping("/quy-tiens")
    public ResponseEntity<List<QuyTienDTO>> getAllQuyTiens(QuyTienCriteria criteria, Pageable pageable) {
        log.debug("REST request to get QuyTiens by criteria: {}", criteria);
        Page<QuyTienDTO> page = quyTienQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /quy-tiens/count} : count all the quyTiens.
     *
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
     */
    @GetMapping("/quy-tiens/count")
    public ResponseEntity<Long> countQuyTiens(QuyTienCriteria criteria) {
        log.debug("REST request to count QuyTiens by criteria: {}", criteria);
        return ResponseEntity.ok().body(quyTienQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /quy-tiens/:id} : get the "id" quyTien.
     *
     * @param id the id of the quyTienDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the quyTienDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/quy-tiens/{id}")
    public ResponseEntity<QuyTienDTO> getQuyTien(@PathVariable Long id) {
        log.debug("REST request to get QuyTien : {}", id);
        Optional<QuyTienDTO> quyTienDTO = quyTienService.findOne(id);
        return ResponseUtil.wrapOrNotFound(quyTienDTO);
    }

    /**
     * {@code DELETE  /quy-tiens/:id} : delete the "id" quyTien.
     *
     * @param id the id of the quyTienDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/quy-tiens/{id}")
    public ResponseEntity<Void> deleteQuyTien(@PathVariable Long id) {
        log.debug("REST request to delete QuyTien : {}", id);
        quyTienService.delete(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
            .build();
    }
}
