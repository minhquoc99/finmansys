import { NgModule } from '@angular/core';

import { SharedModule } from 'app/shared/shared.module';
import { PhongDatComponent } from './list/phong-dat.component';
import { PhongDatDetailComponent } from './detail/phong-dat-detail.component';
import { PhongDatUpdateComponent } from './update/phong-dat-update.component';
import { PhongDatDeleteDialogComponent } from './delete/phong-dat-delete-dialog.component';
import { PhongDatRoutingModule } from './route/phong-dat-routing.module';

@NgModule({
  imports: [SharedModule, PhongDatRoutingModule],
  declarations: [PhongDatComponent, PhongDatDetailComponent, PhongDatUpdateComponent, PhongDatDeleteDialogComponent],
  entryComponents: [PhongDatDeleteDialogComponent],
})
export class PhongDatModule {}
