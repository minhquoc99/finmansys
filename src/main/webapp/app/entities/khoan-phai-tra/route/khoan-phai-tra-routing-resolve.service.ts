import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { mergeMap } from 'rxjs/operators';

import { IKhoanPhaiTra, KhoanPhaiTra } from '../khoan-phai-tra.model';
import { KhoanPhaiTraService } from '../service/khoan-phai-tra.service';

@Injectable({ providedIn: 'root' })
export class KhoanPhaiTraRoutingResolveService implements Resolve<IKhoanPhaiTra> {
  constructor(protected service: KhoanPhaiTraService, protected router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IKhoanPhaiTra> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        mergeMap((khoanPhaiTra: HttpResponse<KhoanPhaiTra>) => {
          if (khoanPhaiTra.body) {
            return of(khoanPhaiTra.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new KhoanPhaiTra());
  }
}
