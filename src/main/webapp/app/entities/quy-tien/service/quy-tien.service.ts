import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { isPresent } from 'app/core/util/operators';
import { ApplicationConfigService } from 'app/core/config/application-config.service';
import { createRequestOption } from 'app/core/request/request-util';
import { IQuyTien, getQuyTienIdentifier } from '../quy-tien.model';

export type EntityResponseType = HttpResponse<IQuyTien>;
export type EntityArrayResponseType = HttpResponse<IQuyTien[]>;

@Injectable({ providedIn: 'root' })
export class QuyTienService {
  public resourceUrl = this.applicationConfigService.getEndpointFor('api/quy-tiens');

  constructor(protected http: HttpClient, private applicationConfigService: ApplicationConfigService) {}

  create(quyTien: IQuyTien): Observable<EntityResponseType> {
    return this.http.post<IQuyTien>(this.resourceUrl, quyTien, { observe: 'response' });
  }

  update(quyTien: IQuyTien): Observable<EntityResponseType> {
    return this.http.put<IQuyTien>(`${this.resourceUrl}/${getQuyTienIdentifier(quyTien) as number}`, quyTien, { observe: 'response' });
  }

  partialUpdate(quyTien: IQuyTien): Observable<EntityResponseType> {
    return this.http.patch<IQuyTien>(`${this.resourceUrl}/${getQuyTienIdentifier(quyTien) as number}`, quyTien, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IQuyTien>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IQuyTien[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  addQuyTienToCollectionIfMissing(quyTienCollection: IQuyTien[], ...quyTiensToCheck: (IQuyTien | null | undefined)[]): IQuyTien[] {
    const quyTiens: IQuyTien[] = quyTiensToCheck.filter(isPresent);
    if (quyTiens.length > 0) {
      const quyTienCollectionIdentifiers = quyTienCollection.map(quyTienItem => getQuyTienIdentifier(quyTienItem)!);
      const quyTiensToAdd = quyTiens.filter(quyTienItem => {
        const quyTienIdentifier = getQuyTienIdentifier(quyTienItem);
        if (quyTienIdentifier == null || quyTienCollectionIdentifiers.includes(quyTienIdentifier)) {
          return false;
        }
        quyTienCollectionIdentifiers.push(quyTienIdentifier);
        return true;
      });
      return [...quyTiensToAdd, ...quyTienCollection];
    }
    return quyTienCollection;
  }
}
