package com.group3.finmansys.service;

import com.group3.finmansys.service.dto.KhoanPhaiTraDTO;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Service Interface for managing {@link com.group3.finmansys.domain.KhoanPhaiTra}.
 */
public interface KhoanPhaiTraService {
    /**
     * Save a khoanPhaiTra.
     *
     * @param khoanPhaiTraDTO the entity to save.
     * @return the persisted entity.
     */
    KhoanPhaiTraDTO save(KhoanPhaiTraDTO khoanPhaiTraDTO);

    /**
     * Partially updates a khoanPhaiTra.
     *
     * @param khoanPhaiTraDTO the entity to update partially.
     * @return the persisted entity.
     */
    Optional<KhoanPhaiTraDTO> partialUpdate(KhoanPhaiTraDTO khoanPhaiTraDTO);

    /**
     * Get all the khoanPhaiTras.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<KhoanPhaiTraDTO> findAll(Pageable pageable);

    /**
     * Get the "id" khoanPhaiTra.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<KhoanPhaiTraDTO> findOne(Long id);

    /**
     * Delete the "id" khoanPhaiTra.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
