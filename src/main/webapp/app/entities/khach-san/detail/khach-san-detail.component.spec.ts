import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { KhachSanDetailComponent } from './khach-san-detail.component';

describe('Component Tests', () => {
  describe('KhachSan Management Detail Component', () => {
    let comp: KhachSanDetailComponent;
    let fixture: ComponentFixture<KhachSanDetailComponent>;

    beforeEach(() => {
      TestBed.configureTestingModule({
        declarations: [KhachSanDetailComponent],
        providers: [
          {
            provide: ActivatedRoute,
            useValue: { data: of({ khachSan: { id: 123 } }) },
          },
        ],
      })
        .overrideTemplate(KhachSanDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(KhachSanDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should load khachSan on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.khachSan).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
