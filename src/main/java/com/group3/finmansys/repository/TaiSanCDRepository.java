package com.group3.finmansys.repository;

import com.group3.finmansys.domain.TaiSanCD;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data SQL repository for the TaiSanCD entity.
 */
@SuppressWarnings("unused")
@Repository
public interface TaiSanCDRepository extends JpaRepository<TaiSanCD, Long>, JpaSpecificationExecutor<TaiSanCD> {}
