package com.group3.finmansys.service;

import com.group3.finmansys.service.dto.NganSachThangDTO;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Service Interface for managing {@link com.group3.finmansys.domain.NganSachThang}.
 */
public interface NganSachThangService {
    /**
     * Save a nganSachThang.
     *
     * @param nganSachThangDTO the entity to save.
     * @return the persisted entity.
     */
    NganSachThangDTO save(NganSachThangDTO nganSachThangDTO);

    /**
     * Partially updates a nganSachThang.
     *
     * @param nganSachThangDTO the entity to update partially.
     * @return the persisted entity.
     */
    Optional<NganSachThangDTO> partialUpdate(NganSachThangDTO nganSachThangDTO);

    /**
     * Get all the nganSachThangs.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<NganSachThangDTO> findAll(Pageable pageable);

    /**
     * Get the "id" nganSachThang.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<NganSachThangDTO> findOne(Long id);

    /**
     * Delete the "id" nganSachThang.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
