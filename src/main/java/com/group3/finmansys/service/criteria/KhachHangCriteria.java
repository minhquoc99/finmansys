package com.group3.finmansys.service.criteria;

import java.io.Serializable;
import java.util.Objects;
import tech.jhipster.service.Criteria;
import tech.jhipster.service.filter.BooleanFilter;
import tech.jhipster.service.filter.DoubleFilter;
import tech.jhipster.service.filter.Filter;
import tech.jhipster.service.filter.FloatFilter;
import tech.jhipster.service.filter.IntegerFilter;
import tech.jhipster.service.filter.LongFilter;
import tech.jhipster.service.filter.StringFilter;

/**
 * Criteria class for the {@link com.group3.finmansys.domain.KhachHang} entity. This class is used
 * in {@link com.group3.finmansys.web.rest.KhachHangResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /khach-hangs?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class KhachHangCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter ten;

    private StringFilter diaChi;

    private StringFilter email;

    private StringFilter sdt;

    public KhachHangCriteria() {}

    public KhachHangCriteria(KhachHangCriteria other) {
        this.id = other.id == null ? null : other.id.copy();
        this.ten = other.ten == null ? null : other.ten.copy();
        this.diaChi = other.diaChi == null ? null : other.diaChi.copy();
        this.email = other.email == null ? null : other.email.copy();
        this.sdt = other.sdt == null ? null : other.sdt.copy();
    }

    @Override
    public KhachHangCriteria copy() {
        return new KhachHangCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public LongFilter id() {
        if (id == null) {
            id = new LongFilter();
        }
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getTen() {
        return ten;
    }

    public StringFilter ten() {
        if (ten == null) {
            ten = new StringFilter();
        }
        return ten;
    }

    public void setTen(StringFilter ten) {
        this.ten = ten;
    }

    public StringFilter getDiaChi() {
        return diaChi;
    }

    public StringFilter diaChi() {
        if (diaChi == null) {
            diaChi = new StringFilter();
        }
        return diaChi;
    }

    public void setDiaChi(StringFilter diaChi) {
        this.diaChi = diaChi;
    }

    public StringFilter getEmail() {
        return email;
    }

    public StringFilter email() {
        if (email == null) {
            email = new StringFilter();
        }
        return email;
    }

    public void setEmail(StringFilter email) {
        this.email = email;
    }

    public StringFilter getSdt() {
        return sdt;
    }

    public StringFilter sdt() {
        if (sdt == null) {
            sdt = new StringFilter();
        }
        return sdt;
    }

    public void setSdt(StringFilter sdt) {
        this.sdt = sdt;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final KhachHangCriteria that = (KhachHangCriteria) o;
        return (
            Objects.equals(id, that.id) &&
            Objects.equals(ten, that.ten) &&
            Objects.equals(diaChi, that.diaChi) &&
            Objects.equals(email, that.email) &&
            Objects.equals(sdt, that.sdt)
        );
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, ten, diaChi, email, sdt);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "KhachHangCriteria{" +
            (id != null ? "id=" + id + ", " : "") +
            (ten != null ? "ten=" + ten + ", " : "") +
            (diaChi != null ? "diaChi=" + diaChi + ", " : "") +
            (email != null ? "email=" + email + ", " : "") +
            (sdt != null ? "sdt=" + sdt + ", " : "") +
            "}";
    }
}
