package com.group3.finmansys.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.group3.finmansys.IntegrationTest;
import com.group3.finmansys.domain.DichVuSuDung;
import com.group3.finmansys.domain.KhachHang;
import com.group3.finmansys.domain.PhongDat;
import com.group3.finmansys.domain.PhongDuocDat;
import com.group3.finmansys.repository.PhongDuocDatRepository;
import com.group3.finmansys.service.criteria.PhongDuocDatCriteria;
import com.group3.finmansys.service.dto.PhongDuocDatDTO;
import com.group3.finmansys.service.mapper.PhongDuocDatMapper;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link PhongDuocDatResource} REST controller.
 */
@IntegrationTest
@ExtendWith(MockitoExtension.class)
@AutoConfigureMockMvc
@WithMockUser
class PhongDuocDatResourceIT {

    private static final Instant DEFAULT_NGAY_DAT = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_NGAY_DAT = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final Double DEFAULT_KHUYEN_MAI = 0D;
    private static final Double UPDATED_KHUYEN_MAI = 1D;
    private static final Double SMALLER_KHUYEN_MAI = 0D - 1D;

    private static final Instant DEFAULT_CHECK_IN = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_CHECK_IN = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final Instant DEFAULT_CHECK_OUT = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_CHECK_OUT = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final Double DEFAULT_GIA = 1D;
    private static final Double UPDATED_GIA = 2D;
    private static final Double SMALLER_GIA = 1D - 1D;

    private static final String ENTITY_API_URL = "/api/phong-duoc-dats";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private PhongDuocDatRepository phongDuocDatRepository;

    @Autowired
    private PhongDuocDatMapper phongDuocDatMapper;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restPhongDuocDatMockMvc;

    private PhongDuocDat phongDuocDat;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static PhongDuocDat createEntity(EntityManager em) {
        PhongDuocDat phongDuocDat = new PhongDuocDat()
            .ngayDat(DEFAULT_NGAY_DAT)
            .khuyenMai(DEFAULT_KHUYEN_MAI)
            .checkIn(DEFAULT_CHECK_IN)
            .checkOut(DEFAULT_CHECK_OUT)
            .gia(DEFAULT_GIA);
        // Add required entity
        KhachHang khachHang;
        if (TestUtil.findAll(em, KhachHang.class).isEmpty()) {
            khachHang = KhachHangResourceIT.createEntity(em);
            em.persist(khachHang);
            em.flush();
        } else {
            khachHang = TestUtil.findAll(em, KhachHang.class).get(0);
        }
        phongDuocDat.setKhachHang(khachHang);
        return phongDuocDat;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static PhongDuocDat createUpdatedEntity(EntityManager em) {
        PhongDuocDat phongDuocDat = new PhongDuocDat()
            .ngayDat(UPDATED_NGAY_DAT)
            .khuyenMai(UPDATED_KHUYEN_MAI)
            .checkIn(UPDATED_CHECK_IN)
            .checkOut(UPDATED_CHECK_OUT)
            .gia(UPDATED_GIA);
        // Add required entity
        KhachHang khachHang;
        if (TestUtil.findAll(em, KhachHang.class).isEmpty()) {
            khachHang = KhachHangResourceIT.createUpdatedEntity(em);
            em.persist(khachHang);
            em.flush();
        } else {
            khachHang = TestUtil.findAll(em, KhachHang.class).get(0);
        }
        phongDuocDat.setKhachHang(khachHang);
        return phongDuocDat;
    }

    @BeforeEach
    public void initTest() {
        phongDuocDat = createEntity(em);
    }

    @Test
    @Transactional
    void createPhongDuocDat() throws Exception {
        int databaseSizeBeforeCreate = phongDuocDatRepository.findAll().size();
        // Create the PhongDuocDat
        PhongDuocDatDTO phongDuocDatDTO = phongDuocDatMapper.toDto(phongDuocDat);
        restPhongDuocDatMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(phongDuocDatDTO))
            )
            .andExpect(status().isCreated());

        // Validate the PhongDuocDat in the database
        List<PhongDuocDat> phongDuocDatList = phongDuocDatRepository.findAll();
        assertThat(phongDuocDatList).hasSize(databaseSizeBeforeCreate + 1);
        PhongDuocDat testPhongDuocDat = phongDuocDatList.get(phongDuocDatList.size() - 1);
        assertThat(testPhongDuocDat.getNgayDat()).isEqualTo(DEFAULT_NGAY_DAT);
        assertThat(testPhongDuocDat.getKhuyenMai()).isEqualTo(DEFAULT_KHUYEN_MAI);
        assertThat(testPhongDuocDat.getCheckIn()).isEqualTo(DEFAULT_CHECK_IN);
        assertThat(testPhongDuocDat.getCheckOut()).isEqualTo(DEFAULT_CHECK_OUT);
        assertThat(testPhongDuocDat.getGia()).isEqualTo(DEFAULT_GIA);
    }

    @Test
    @Transactional
    void createPhongDuocDatWithExistingId() throws Exception {
        // Create the PhongDuocDat with an existing ID
        phongDuocDat.setId(1L);
        PhongDuocDatDTO phongDuocDatDTO = phongDuocDatMapper.toDto(phongDuocDat);

        int databaseSizeBeforeCreate = phongDuocDatRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restPhongDuocDatMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(phongDuocDatDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the PhongDuocDat in the database
        List<PhongDuocDat> phongDuocDatList = phongDuocDatRepository.findAll();
        assertThat(phongDuocDatList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void checkNgayDatIsRequired() throws Exception {
        int databaseSizeBeforeTest = phongDuocDatRepository.findAll().size();
        // set the field null
        phongDuocDat.setNgayDat(null);

        // Create the PhongDuocDat, which fails.
        PhongDuocDatDTO phongDuocDatDTO = phongDuocDatMapper.toDto(phongDuocDat);

        restPhongDuocDatMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(phongDuocDatDTO))
            )
            .andExpect(status().isBadRequest());

        List<PhongDuocDat> phongDuocDatList = phongDuocDatRepository.findAll();
        assertThat(phongDuocDatList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkCheckInIsRequired() throws Exception {
        int databaseSizeBeforeTest = phongDuocDatRepository.findAll().size();
        // set the field null
        phongDuocDat.setCheckIn(null);

        // Create the PhongDuocDat, which fails.
        PhongDuocDatDTO phongDuocDatDTO = phongDuocDatMapper.toDto(phongDuocDat);

        restPhongDuocDatMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(phongDuocDatDTO))
            )
            .andExpect(status().isBadRequest());

        List<PhongDuocDat> phongDuocDatList = phongDuocDatRepository.findAll();
        assertThat(phongDuocDatList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkCheckOutIsRequired() throws Exception {
        int databaseSizeBeforeTest = phongDuocDatRepository.findAll().size();
        // set the field null
        phongDuocDat.setCheckOut(null);

        // Create the PhongDuocDat, which fails.
        PhongDuocDatDTO phongDuocDatDTO = phongDuocDatMapper.toDto(phongDuocDat);

        restPhongDuocDatMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(phongDuocDatDTO))
            )
            .andExpect(status().isBadRequest());

        List<PhongDuocDat> phongDuocDatList = phongDuocDatRepository.findAll();
        assertThat(phongDuocDatList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkGiaIsRequired() throws Exception {
        int databaseSizeBeforeTest = phongDuocDatRepository.findAll().size();
        // set the field null
        phongDuocDat.setGia(null);

        // Create the PhongDuocDat, which fails.
        PhongDuocDatDTO phongDuocDatDTO = phongDuocDatMapper.toDto(phongDuocDat);

        restPhongDuocDatMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(phongDuocDatDTO))
            )
            .andExpect(status().isBadRequest());

        List<PhongDuocDat> phongDuocDatList = phongDuocDatRepository.findAll();
        assertThat(phongDuocDatList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void getAllPhongDuocDats() throws Exception {
        // Initialize the database
        phongDuocDatRepository.saveAndFlush(phongDuocDat);

        // Get all the phongDuocDatList
        restPhongDuocDatMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(phongDuocDat.getId().intValue())))
            .andExpect(jsonPath("$.[*].ngayDat").value(hasItem(DEFAULT_NGAY_DAT.toString())))
            .andExpect(jsonPath("$.[*].khuyenMai").value(hasItem(DEFAULT_KHUYEN_MAI.doubleValue())))
            .andExpect(jsonPath("$.[*].checkIn").value(hasItem(DEFAULT_CHECK_IN.toString())))
            .andExpect(jsonPath("$.[*].checkOut").value(hasItem(DEFAULT_CHECK_OUT.toString())))
            .andExpect(jsonPath("$.[*].gia").value(hasItem(DEFAULT_GIA.doubleValue())));
    }

    @Test
    @Transactional
    void getPhongDuocDat() throws Exception {
        // Initialize the database
        phongDuocDatRepository.saveAndFlush(phongDuocDat);

        // Get the phongDuocDat
        restPhongDuocDatMockMvc
            .perform(get(ENTITY_API_URL_ID, phongDuocDat.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(phongDuocDat.getId().intValue()))
            .andExpect(jsonPath("$.ngayDat").value(DEFAULT_NGAY_DAT.toString()))
            .andExpect(jsonPath("$.khuyenMai").value(DEFAULT_KHUYEN_MAI.doubleValue()))
            .andExpect(jsonPath("$.checkIn").value(DEFAULT_CHECK_IN.toString()))
            .andExpect(jsonPath("$.checkOut").value(DEFAULT_CHECK_OUT.toString()))
            .andExpect(jsonPath("$.gia").value(DEFAULT_GIA.doubleValue()));
    }

    @Test
    @Transactional
    void getPhongDuocDatsByIdFiltering() throws Exception {
        // Initialize the database
        phongDuocDatRepository.saveAndFlush(phongDuocDat);

        Long id = phongDuocDat.getId();

        defaultPhongDuocDatShouldBeFound("id.equals=" + id);
        defaultPhongDuocDatShouldNotBeFound("id.notEquals=" + id);

        defaultPhongDuocDatShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultPhongDuocDatShouldNotBeFound("id.greaterThan=" + id);

        defaultPhongDuocDatShouldBeFound("id.lessThanOrEqual=" + id);
        defaultPhongDuocDatShouldNotBeFound("id.lessThan=" + id);
    }

    @Test
    @Transactional
    void getAllPhongDuocDatsByNgayDatIsEqualToSomething() throws Exception {
        // Initialize the database
        phongDuocDatRepository.saveAndFlush(phongDuocDat);

        // Get all the phongDuocDatList where ngayDat equals to DEFAULT_NGAY_DAT
        defaultPhongDuocDatShouldBeFound("ngayDat.equals=" + DEFAULT_NGAY_DAT);

        // Get all the phongDuocDatList where ngayDat equals to UPDATED_NGAY_DAT
        defaultPhongDuocDatShouldNotBeFound("ngayDat.equals=" + UPDATED_NGAY_DAT);
    }

    @Test
    @Transactional
    void getAllPhongDuocDatsByNgayDatIsNotEqualToSomething() throws Exception {
        // Initialize the database
        phongDuocDatRepository.saveAndFlush(phongDuocDat);

        // Get all the phongDuocDatList where ngayDat not equals to DEFAULT_NGAY_DAT
        defaultPhongDuocDatShouldNotBeFound("ngayDat.notEquals=" + DEFAULT_NGAY_DAT);

        // Get all the phongDuocDatList where ngayDat not equals to UPDATED_NGAY_DAT
        defaultPhongDuocDatShouldBeFound("ngayDat.notEquals=" + UPDATED_NGAY_DAT);
    }

    @Test
    @Transactional
    void getAllPhongDuocDatsByNgayDatIsInShouldWork() throws Exception {
        // Initialize the database
        phongDuocDatRepository.saveAndFlush(phongDuocDat);

        // Get all the phongDuocDatList where ngayDat in DEFAULT_NGAY_DAT or UPDATED_NGAY_DAT
        defaultPhongDuocDatShouldBeFound("ngayDat.in=" + DEFAULT_NGAY_DAT + "," + UPDATED_NGAY_DAT);

        // Get all the phongDuocDatList where ngayDat equals to UPDATED_NGAY_DAT
        defaultPhongDuocDatShouldNotBeFound("ngayDat.in=" + UPDATED_NGAY_DAT);
    }

    @Test
    @Transactional
    void getAllPhongDuocDatsByNgayDatIsNullOrNotNull() throws Exception {
        // Initialize the database
        phongDuocDatRepository.saveAndFlush(phongDuocDat);

        // Get all the phongDuocDatList where ngayDat is not null
        defaultPhongDuocDatShouldBeFound("ngayDat.specified=true");

        // Get all the phongDuocDatList where ngayDat is null
        defaultPhongDuocDatShouldNotBeFound("ngayDat.specified=false");
    }

    @Test
    @Transactional
    void getAllPhongDuocDatsByKhuyenMaiIsEqualToSomething() throws Exception {
        // Initialize the database
        phongDuocDatRepository.saveAndFlush(phongDuocDat);

        // Get all the phongDuocDatList where khuyenMai equals to DEFAULT_KHUYEN_MAI
        defaultPhongDuocDatShouldBeFound("khuyenMai.equals=" + DEFAULT_KHUYEN_MAI);

        // Get all the phongDuocDatList where khuyenMai equals to UPDATED_KHUYEN_MAI
        defaultPhongDuocDatShouldNotBeFound("khuyenMai.equals=" + UPDATED_KHUYEN_MAI);
    }

    @Test
    @Transactional
    void getAllPhongDuocDatsByKhuyenMaiIsNotEqualToSomething() throws Exception {
        // Initialize the database
        phongDuocDatRepository.saveAndFlush(phongDuocDat);

        // Get all the phongDuocDatList where khuyenMai not equals to DEFAULT_KHUYEN_MAI
        defaultPhongDuocDatShouldNotBeFound("khuyenMai.notEquals=" + DEFAULT_KHUYEN_MAI);

        // Get all the phongDuocDatList where khuyenMai not equals to UPDATED_KHUYEN_MAI
        defaultPhongDuocDatShouldBeFound("khuyenMai.notEquals=" + UPDATED_KHUYEN_MAI);
    }

    @Test
    @Transactional
    void getAllPhongDuocDatsByKhuyenMaiIsInShouldWork() throws Exception {
        // Initialize the database
        phongDuocDatRepository.saveAndFlush(phongDuocDat);

        // Get all the phongDuocDatList where khuyenMai in DEFAULT_KHUYEN_MAI or UPDATED_KHUYEN_MAI
        defaultPhongDuocDatShouldBeFound("khuyenMai.in=" + DEFAULT_KHUYEN_MAI + "," + UPDATED_KHUYEN_MAI);

        // Get all the phongDuocDatList where khuyenMai equals to UPDATED_KHUYEN_MAI
        defaultPhongDuocDatShouldNotBeFound("khuyenMai.in=" + UPDATED_KHUYEN_MAI);
    }

    @Test
    @Transactional
    void getAllPhongDuocDatsByKhuyenMaiIsNullOrNotNull() throws Exception {
        // Initialize the database
        phongDuocDatRepository.saveAndFlush(phongDuocDat);

        // Get all the phongDuocDatList where khuyenMai is not null
        defaultPhongDuocDatShouldBeFound("khuyenMai.specified=true");

        // Get all the phongDuocDatList where khuyenMai is null
        defaultPhongDuocDatShouldNotBeFound("khuyenMai.specified=false");
    }

    @Test
    @Transactional
    void getAllPhongDuocDatsByKhuyenMaiIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        phongDuocDatRepository.saveAndFlush(phongDuocDat);

        // Get all the phongDuocDatList where khuyenMai is greater than or equal to DEFAULT_KHUYEN_MAI
        defaultPhongDuocDatShouldBeFound("khuyenMai.greaterThanOrEqual=" + DEFAULT_KHUYEN_MAI);

        // Get all the phongDuocDatList where khuyenMai is greater than or equal to (DEFAULT_KHUYEN_MAI + 1)
        defaultPhongDuocDatShouldNotBeFound("khuyenMai.greaterThanOrEqual=" + (DEFAULT_KHUYEN_MAI + 1));
    }

    @Test
    @Transactional
    void getAllPhongDuocDatsByKhuyenMaiIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        phongDuocDatRepository.saveAndFlush(phongDuocDat);

        // Get all the phongDuocDatList where khuyenMai is less than or equal to DEFAULT_KHUYEN_MAI
        defaultPhongDuocDatShouldBeFound("khuyenMai.lessThanOrEqual=" + DEFAULT_KHUYEN_MAI);

        // Get all the phongDuocDatList where khuyenMai is less than or equal to SMALLER_KHUYEN_MAI
        defaultPhongDuocDatShouldNotBeFound("khuyenMai.lessThanOrEqual=" + SMALLER_KHUYEN_MAI);
    }

    @Test
    @Transactional
    void getAllPhongDuocDatsByKhuyenMaiIsLessThanSomething() throws Exception {
        // Initialize the database
        phongDuocDatRepository.saveAndFlush(phongDuocDat);

        // Get all the phongDuocDatList where khuyenMai is less than DEFAULT_KHUYEN_MAI
        defaultPhongDuocDatShouldNotBeFound("khuyenMai.lessThan=" + DEFAULT_KHUYEN_MAI);

        // Get all the phongDuocDatList where khuyenMai is less than (DEFAULT_KHUYEN_MAI + 1)
        defaultPhongDuocDatShouldBeFound("khuyenMai.lessThan=" + (DEFAULT_KHUYEN_MAI + 1));
    }

    @Test
    @Transactional
    void getAllPhongDuocDatsByKhuyenMaiIsGreaterThanSomething() throws Exception {
        // Initialize the database
        phongDuocDatRepository.saveAndFlush(phongDuocDat);

        // Get all the phongDuocDatList where khuyenMai is greater than DEFAULT_KHUYEN_MAI
        defaultPhongDuocDatShouldNotBeFound("khuyenMai.greaterThan=" + DEFAULT_KHUYEN_MAI);

        // Get all the phongDuocDatList where khuyenMai is greater than SMALLER_KHUYEN_MAI
        defaultPhongDuocDatShouldBeFound("khuyenMai.greaterThan=" + SMALLER_KHUYEN_MAI);
    }

    @Test
    @Transactional
    void getAllPhongDuocDatsByCheckInIsEqualToSomething() throws Exception {
        // Initialize the database
        phongDuocDatRepository.saveAndFlush(phongDuocDat);

        // Get all the phongDuocDatList where checkIn equals to DEFAULT_CHECK_IN
        defaultPhongDuocDatShouldBeFound("checkIn.equals=" + DEFAULT_CHECK_IN);

        // Get all the phongDuocDatList where checkIn equals to UPDATED_CHECK_IN
        defaultPhongDuocDatShouldNotBeFound("checkIn.equals=" + UPDATED_CHECK_IN);
    }

    @Test
    @Transactional
    void getAllPhongDuocDatsByCheckInIsNotEqualToSomething() throws Exception {
        // Initialize the database
        phongDuocDatRepository.saveAndFlush(phongDuocDat);

        // Get all the phongDuocDatList where checkIn not equals to DEFAULT_CHECK_IN
        defaultPhongDuocDatShouldNotBeFound("checkIn.notEquals=" + DEFAULT_CHECK_IN);

        // Get all the phongDuocDatList where checkIn not equals to UPDATED_CHECK_IN
        defaultPhongDuocDatShouldBeFound("checkIn.notEquals=" + UPDATED_CHECK_IN);
    }

    @Test
    @Transactional
    void getAllPhongDuocDatsByCheckInIsInShouldWork() throws Exception {
        // Initialize the database
        phongDuocDatRepository.saveAndFlush(phongDuocDat);

        // Get all the phongDuocDatList where checkIn in DEFAULT_CHECK_IN or UPDATED_CHECK_IN
        defaultPhongDuocDatShouldBeFound("checkIn.in=" + DEFAULT_CHECK_IN + "," + UPDATED_CHECK_IN);

        // Get all the phongDuocDatList where checkIn equals to UPDATED_CHECK_IN
        defaultPhongDuocDatShouldNotBeFound("checkIn.in=" + UPDATED_CHECK_IN);
    }

    @Test
    @Transactional
    void getAllPhongDuocDatsByCheckInIsNullOrNotNull() throws Exception {
        // Initialize the database
        phongDuocDatRepository.saveAndFlush(phongDuocDat);

        // Get all the phongDuocDatList where checkIn is not null
        defaultPhongDuocDatShouldBeFound("checkIn.specified=true");

        // Get all the phongDuocDatList where checkIn is null
        defaultPhongDuocDatShouldNotBeFound("checkIn.specified=false");
    }

    @Test
    @Transactional
    void getAllPhongDuocDatsByCheckOutIsEqualToSomething() throws Exception {
        // Initialize the database
        phongDuocDatRepository.saveAndFlush(phongDuocDat);

        // Get all the phongDuocDatList where checkOut equals to DEFAULT_CHECK_OUT
        defaultPhongDuocDatShouldBeFound("checkOut.equals=" + DEFAULT_CHECK_OUT);

        // Get all the phongDuocDatList where checkOut equals to UPDATED_CHECK_OUT
        defaultPhongDuocDatShouldNotBeFound("checkOut.equals=" + UPDATED_CHECK_OUT);
    }

    @Test
    @Transactional
    void getAllPhongDuocDatsByCheckOutIsNotEqualToSomething() throws Exception {
        // Initialize the database
        phongDuocDatRepository.saveAndFlush(phongDuocDat);

        // Get all the phongDuocDatList where checkOut not equals to DEFAULT_CHECK_OUT
        defaultPhongDuocDatShouldNotBeFound("checkOut.notEquals=" + DEFAULT_CHECK_OUT);

        // Get all the phongDuocDatList where checkOut not equals to UPDATED_CHECK_OUT
        defaultPhongDuocDatShouldBeFound("checkOut.notEquals=" + UPDATED_CHECK_OUT);
    }

    @Test
    @Transactional
    void getAllPhongDuocDatsByCheckOutIsInShouldWork() throws Exception {
        // Initialize the database
        phongDuocDatRepository.saveAndFlush(phongDuocDat);

        // Get all the phongDuocDatList where checkOut in DEFAULT_CHECK_OUT or UPDATED_CHECK_OUT
        defaultPhongDuocDatShouldBeFound("checkOut.in=" + DEFAULT_CHECK_OUT + "," + UPDATED_CHECK_OUT);

        // Get all the phongDuocDatList where checkOut equals to UPDATED_CHECK_OUT
        defaultPhongDuocDatShouldNotBeFound("checkOut.in=" + UPDATED_CHECK_OUT);
    }

    @Test
    @Transactional
    void getAllPhongDuocDatsByCheckOutIsNullOrNotNull() throws Exception {
        // Initialize the database
        phongDuocDatRepository.saveAndFlush(phongDuocDat);

        // Get all the phongDuocDatList where checkOut is not null
        defaultPhongDuocDatShouldBeFound("checkOut.specified=true");

        // Get all the phongDuocDatList where checkOut is null
        defaultPhongDuocDatShouldNotBeFound("checkOut.specified=false");
    }

    @Test
    @Transactional
    void getAllPhongDuocDatsByGiaIsEqualToSomething() throws Exception {
        // Initialize the database
        phongDuocDatRepository.saveAndFlush(phongDuocDat);

        // Get all the phongDuocDatList where gia equals to DEFAULT_GIA
        defaultPhongDuocDatShouldBeFound("gia.equals=" + DEFAULT_GIA);

        // Get all the phongDuocDatList where gia equals to UPDATED_GIA
        defaultPhongDuocDatShouldNotBeFound("gia.equals=" + UPDATED_GIA);
    }

    @Test
    @Transactional
    void getAllPhongDuocDatsByGiaIsNotEqualToSomething() throws Exception {
        // Initialize the database
        phongDuocDatRepository.saveAndFlush(phongDuocDat);

        // Get all the phongDuocDatList where gia not equals to DEFAULT_GIA
        defaultPhongDuocDatShouldNotBeFound("gia.notEquals=" + DEFAULT_GIA);

        // Get all the phongDuocDatList where gia not equals to UPDATED_GIA
        defaultPhongDuocDatShouldBeFound("gia.notEquals=" + UPDATED_GIA);
    }

    @Test
    @Transactional
    void getAllPhongDuocDatsByGiaIsInShouldWork() throws Exception {
        // Initialize the database
        phongDuocDatRepository.saveAndFlush(phongDuocDat);

        // Get all the phongDuocDatList where gia in DEFAULT_GIA or UPDATED_GIA
        defaultPhongDuocDatShouldBeFound("gia.in=" + DEFAULT_GIA + "," + UPDATED_GIA);

        // Get all the phongDuocDatList where gia equals to UPDATED_GIA
        defaultPhongDuocDatShouldNotBeFound("gia.in=" + UPDATED_GIA);
    }

    @Test
    @Transactional
    void getAllPhongDuocDatsByGiaIsNullOrNotNull() throws Exception {
        // Initialize the database
        phongDuocDatRepository.saveAndFlush(phongDuocDat);

        // Get all the phongDuocDatList where gia is not null
        defaultPhongDuocDatShouldBeFound("gia.specified=true");

        // Get all the phongDuocDatList where gia is null
        defaultPhongDuocDatShouldNotBeFound("gia.specified=false");
    }

    @Test
    @Transactional
    void getAllPhongDuocDatsByGiaIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        phongDuocDatRepository.saveAndFlush(phongDuocDat);

        // Get all the phongDuocDatList where gia is greater than or equal to DEFAULT_GIA
        defaultPhongDuocDatShouldBeFound("gia.greaterThanOrEqual=" + DEFAULT_GIA);

        // Get all the phongDuocDatList where gia is greater than or equal to UPDATED_GIA
        defaultPhongDuocDatShouldNotBeFound("gia.greaterThanOrEqual=" + UPDATED_GIA);
    }

    @Test
    @Transactional
    void getAllPhongDuocDatsByGiaIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        phongDuocDatRepository.saveAndFlush(phongDuocDat);

        // Get all the phongDuocDatList where gia is less than or equal to DEFAULT_GIA
        defaultPhongDuocDatShouldBeFound("gia.lessThanOrEqual=" + DEFAULT_GIA);

        // Get all the phongDuocDatList where gia is less than or equal to SMALLER_GIA
        defaultPhongDuocDatShouldNotBeFound("gia.lessThanOrEqual=" + SMALLER_GIA);
    }

    @Test
    @Transactional
    void getAllPhongDuocDatsByGiaIsLessThanSomething() throws Exception {
        // Initialize the database
        phongDuocDatRepository.saveAndFlush(phongDuocDat);

        // Get all the phongDuocDatList where gia is less than DEFAULT_GIA
        defaultPhongDuocDatShouldNotBeFound("gia.lessThan=" + DEFAULT_GIA);

        // Get all the phongDuocDatList where gia is less than UPDATED_GIA
        defaultPhongDuocDatShouldBeFound("gia.lessThan=" + UPDATED_GIA);
    }

    @Test
    @Transactional
    void getAllPhongDuocDatsByGiaIsGreaterThanSomething() throws Exception {
        // Initialize the database
        phongDuocDatRepository.saveAndFlush(phongDuocDat);

        // Get all the phongDuocDatList where gia is greater than DEFAULT_GIA
        defaultPhongDuocDatShouldNotBeFound("gia.greaterThan=" + DEFAULT_GIA);

        // Get all the phongDuocDatList where gia is greater than SMALLER_GIA
        defaultPhongDuocDatShouldBeFound("gia.greaterThan=" + SMALLER_GIA);
    }

    @Test
    @Transactional
    void getAllPhongDuocDatsByKhachHangIsEqualToSomething() throws Exception {
        // Initialize the database
        phongDuocDatRepository.saveAndFlush(phongDuocDat);
        KhachHang khachHang = KhachHangResourceIT.createEntity(em);
        em.persist(khachHang);
        em.flush();
        phongDuocDat.setKhachHang(khachHang);
        phongDuocDatRepository.saveAndFlush(phongDuocDat);
        Long khachHangId = khachHang.getId();

        // Get all the phongDuocDatList where khachHang equals to khachHangId
        defaultPhongDuocDatShouldBeFound("khachHangId.equals=" + khachHangId);

        // Get all the phongDuocDatList where khachHang equals to (khachHangId + 1)
        defaultPhongDuocDatShouldNotBeFound("khachHangId.equals=" + (khachHangId + 1));
    }

    @Test
    @Transactional
    void getAllPhongDuocDatsByPhongDatIsEqualToSomething() throws Exception {
        // Initialize the database
        phongDuocDatRepository.saveAndFlush(phongDuocDat);
        PhongDat phongDat = PhongDatResourceIT.createEntity(em);
        em.persist(phongDat);
        em.flush();
        phongDuocDat.addPhongDat(phongDat);
        phongDuocDatRepository.saveAndFlush(phongDuocDat);
        Long phongDatId = phongDat.getId();

        // Get all the phongDuocDatList where phongDat equals to phongDatId
        defaultPhongDuocDatShouldBeFound("phongDatId.equals=" + phongDatId);

        // Get all the phongDuocDatList where phongDat equals to (phongDatId + 1)
        defaultPhongDuocDatShouldNotBeFound("phongDatId.equals=" + (phongDatId + 1));
    }

    @Test
    @Transactional
    void getAllPhongDuocDatsByDichVuSuDungIsEqualToSomething() throws Exception {
        // Initialize the database
        phongDuocDatRepository.saveAndFlush(phongDuocDat);
        DichVuSuDung dichVuSuDung = DichVuSuDungResourceIT.createEntity(em);
        em.persist(dichVuSuDung);
        em.flush();
        phongDuocDat.addDichVuSuDung(dichVuSuDung);
        phongDuocDatRepository.saveAndFlush(phongDuocDat);
        Long dichVuSuDungId = dichVuSuDung.getId();

        // Get all the phongDuocDatList where dichVuSuDung equals to dichVuSuDungId
        defaultPhongDuocDatShouldBeFound("dichVuSuDungId.equals=" + dichVuSuDungId);

        // Get all the phongDuocDatList where dichVuSuDung equals to (dichVuSuDungId + 1)
        defaultPhongDuocDatShouldNotBeFound("dichVuSuDungId.equals=" + (dichVuSuDungId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultPhongDuocDatShouldBeFound(String filter) throws Exception {
        restPhongDuocDatMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(phongDuocDat.getId().intValue())))
            .andExpect(jsonPath("$.[*].ngayDat").value(hasItem(DEFAULT_NGAY_DAT.toString())))
            .andExpect(jsonPath("$.[*].khuyenMai").value(hasItem(DEFAULT_KHUYEN_MAI.doubleValue())))
            .andExpect(jsonPath("$.[*].checkIn").value(hasItem(DEFAULT_CHECK_IN.toString())))
            .andExpect(jsonPath("$.[*].checkOut").value(hasItem(DEFAULT_CHECK_OUT.toString())))
            .andExpect(jsonPath("$.[*].gia").value(hasItem(DEFAULT_GIA.doubleValue())));

        // Check, that the count call also returns 1
        restPhongDuocDatMockMvc
            .perform(get(ENTITY_API_URL + "/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultPhongDuocDatShouldNotBeFound(String filter) throws Exception {
        restPhongDuocDatMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restPhongDuocDatMockMvc
            .perform(get(ENTITY_API_URL + "/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }

    @Test
    @Transactional
    void getNonExistingPhongDuocDat() throws Exception {
        // Get the phongDuocDat
        restPhongDuocDatMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putNewPhongDuocDat() throws Exception {
        // Initialize the database
        phongDuocDatRepository.saveAndFlush(phongDuocDat);

        int databaseSizeBeforeUpdate = phongDuocDatRepository.findAll().size();

        // Update the phongDuocDat
        PhongDuocDat updatedPhongDuocDat = phongDuocDatRepository.findById(phongDuocDat.getId()).get();
        // Disconnect from session so that the updates on updatedPhongDuocDat are not directly saved in db
        em.detach(updatedPhongDuocDat);
        updatedPhongDuocDat
            .ngayDat(UPDATED_NGAY_DAT)
            .khuyenMai(UPDATED_KHUYEN_MAI)
            .checkIn(UPDATED_CHECK_IN)
            .checkOut(UPDATED_CHECK_OUT)
            .gia(UPDATED_GIA);
        PhongDuocDatDTO phongDuocDatDTO = phongDuocDatMapper.toDto(updatedPhongDuocDat);

        restPhongDuocDatMockMvc
            .perform(
                put(ENTITY_API_URL_ID, phongDuocDatDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(phongDuocDatDTO))
            )
            .andExpect(status().isOk());

        // Validate the PhongDuocDat in the database
        List<PhongDuocDat> phongDuocDatList = phongDuocDatRepository.findAll();
        assertThat(phongDuocDatList).hasSize(databaseSizeBeforeUpdate);
        PhongDuocDat testPhongDuocDat = phongDuocDatList.get(phongDuocDatList.size() - 1);
        assertThat(testPhongDuocDat.getNgayDat()).isEqualTo(UPDATED_NGAY_DAT);
        assertThat(testPhongDuocDat.getKhuyenMai()).isEqualTo(UPDATED_KHUYEN_MAI);
        assertThat(testPhongDuocDat.getCheckIn()).isEqualTo(UPDATED_CHECK_IN);
        assertThat(testPhongDuocDat.getCheckOut()).isEqualTo(UPDATED_CHECK_OUT);
        assertThat(testPhongDuocDat.getGia()).isEqualTo(UPDATED_GIA);
    }

    @Test
    @Transactional
    void putNonExistingPhongDuocDat() throws Exception {
        int databaseSizeBeforeUpdate = phongDuocDatRepository.findAll().size();
        phongDuocDat.setId(count.incrementAndGet());

        // Create the PhongDuocDat
        PhongDuocDatDTO phongDuocDatDTO = phongDuocDatMapper.toDto(phongDuocDat);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restPhongDuocDatMockMvc
            .perform(
                put(ENTITY_API_URL_ID, phongDuocDatDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(phongDuocDatDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the PhongDuocDat in the database
        List<PhongDuocDat> phongDuocDatList = phongDuocDatRepository.findAll();
        assertThat(phongDuocDatList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchPhongDuocDat() throws Exception {
        int databaseSizeBeforeUpdate = phongDuocDatRepository.findAll().size();
        phongDuocDat.setId(count.incrementAndGet());

        // Create the PhongDuocDat
        PhongDuocDatDTO phongDuocDatDTO = phongDuocDatMapper.toDto(phongDuocDat);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restPhongDuocDatMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(phongDuocDatDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the PhongDuocDat in the database
        List<PhongDuocDat> phongDuocDatList = phongDuocDatRepository.findAll();
        assertThat(phongDuocDatList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamPhongDuocDat() throws Exception {
        int databaseSizeBeforeUpdate = phongDuocDatRepository.findAll().size();
        phongDuocDat.setId(count.incrementAndGet());

        // Create the PhongDuocDat
        PhongDuocDatDTO phongDuocDatDTO = phongDuocDatMapper.toDto(phongDuocDat);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restPhongDuocDatMockMvc
            .perform(
                put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(phongDuocDatDTO))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the PhongDuocDat in the database
        List<PhongDuocDat> phongDuocDatList = phongDuocDatRepository.findAll();
        assertThat(phongDuocDatList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdatePhongDuocDatWithPatch() throws Exception {
        // Initialize the database
        phongDuocDatRepository.saveAndFlush(phongDuocDat);

        int databaseSizeBeforeUpdate = phongDuocDatRepository.findAll().size();

        // Update the phongDuocDat using partial update
        PhongDuocDat partialUpdatedPhongDuocDat = new PhongDuocDat();
        partialUpdatedPhongDuocDat.setId(phongDuocDat.getId());

        partialUpdatedPhongDuocDat.khuyenMai(UPDATED_KHUYEN_MAI).checkOut(UPDATED_CHECK_OUT);

        restPhongDuocDatMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedPhongDuocDat.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedPhongDuocDat))
            )
            .andExpect(status().isOk());

        // Validate the PhongDuocDat in the database
        List<PhongDuocDat> phongDuocDatList = phongDuocDatRepository.findAll();
        assertThat(phongDuocDatList).hasSize(databaseSizeBeforeUpdate);
        PhongDuocDat testPhongDuocDat = phongDuocDatList.get(phongDuocDatList.size() - 1);
        assertThat(testPhongDuocDat.getNgayDat()).isEqualTo(DEFAULT_NGAY_DAT);
        assertThat(testPhongDuocDat.getKhuyenMai()).isEqualTo(UPDATED_KHUYEN_MAI);
        assertThat(testPhongDuocDat.getCheckIn()).isEqualTo(DEFAULT_CHECK_IN);
        assertThat(testPhongDuocDat.getCheckOut()).isEqualTo(UPDATED_CHECK_OUT);
        assertThat(testPhongDuocDat.getGia()).isEqualTo(DEFAULT_GIA);
    }

    @Test
    @Transactional
    void fullUpdatePhongDuocDatWithPatch() throws Exception {
        // Initialize the database
        phongDuocDatRepository.saveAndFlush(phongDuocDat);

        int databaseSizeBeforeUpdate = phongDuocDatRepository.findAll().size();

        // Update the phongDuocDat using partial update
        PhongDuocDat partialUpdatedPhongDuocDat = new PhongDuocDat();
        partialUpdatedPhongDuocDat.setId(phongDuocDat.getId());

        partialUpdatedPhongDuocDat
            .ngayDat(UPDATED_NGAY_DAT)
            .khuyenMai(UPDATED_KHUYEN_MAI)
            .checkIn(UPDATED_CHECK_IN)
            .checkOut(UPDATED_CHECK_OUT)
            .gia(UPDATED_GIA);

        restPhongDuocDatMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedPhongDuocDat.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedPhongDuocDat))
            )
            .andExpect(status().isOk());

        // Validate the PhongDuocDat in the database
        List<PhongDuocDat> phongDuocDatList = phongDuocDatRepository.findAll();
        assertThat(phongDuocDatList).hasSize(databaseSizeBeforeUpdate);
        PhongDuocDat testPhongDuocDat = phongDuocDatList.get(phongDuocDatList.size() - 1);
        assertThat(testPhongDuocDat.getNgayDat()).isEqualTo(UPDATED_NGAY_DAT);
        assertThat(testPhongDuocDat.getKhuyenMai()).isEqualTo(UPDATED_KHUYEN_MAI);
        assertThat(testPhongDuocDat.getCheckIn()).isEqualTo(UPDATED_CHECK_IN);
        assertThat(testPhongDuocDat.getCheckOut()).isEqualTo(UPDATED_CHECK_OUT);
        assertThat(testPhongDuocDat.getGia()).isEqualTo(UPDATED_GIA);
    }

    @Test
    @Transactional
    void patchNonExistingPhongDuocDat() throws Exception {
        int databaseSizeBeforeUpdate = phongDuocDatRepository.findAll().size();
        phongDuocDat.setId(count.incrementAndGet());

        // Create the PhongDuocDat
        PhongDuocDatDTO phongDuocDatDTO = phongDuocDatMapper.toDto(phongDuocDat);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restPhongDuocDatMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, phongDuocDatDTO.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(phongDuocDatDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the PhongDuocDat in the database
        List<PhongDuocDat> phongDuocDatList = phongDuocDatRepository.findAll();
        assertThat(phongDuocDatList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchPhongDuocDat() throws Exception {
        int databaseSizeBeforeUpdate = phongDuocDatRepository.findAll().size();
        phongDuocDat.setId(count.incrementAndGet());

        // Create the PhongDuocDat
        PhongDuocDatDTO phongDuocDatDTO = phongDuocDatMapper.toDto(phongDuocDat);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restPhongDuocDatMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(phongDuocDatDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the PhongDuocDat in the database
        List<PhongDuocDat> phongDuocDatList = phongDuocDatRepository.findAll();
        assertThat(phongDuocDatList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamPhongDuocDat() throws Exception {
        int databaseSizeBeforeUpdate = phongDuocDatRepository.findAll().size();
        phongDuocDat.setId(count.incrementAndGet());

        // Create the PhongDuocDat
        PhongDuocDatDTO phongDuocDatDTO = phongDuocDatMapper.toDto(phongDuocDat);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restPhongDuocDatMockMvc
            .perform(
                patch(ENTITY_API_URL)
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(phongDuocDatDTO))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the PhongDuocDat in the database
        List<PhongDuocDat> phongDuocDatList = phongDuocDatRepository.findAll();
        assertThat(phongDuocDatList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deletePhongDuocDat() throws Exception {
        // Initialize the database
        phongDuocDatRepository.saveAndFlush(phongDuocDat);

        int databaseSizeBeforeDelete = phongDuocDatRepository.findAll().size();

        // Delete the phongDuocDat
        restPhongDuocDatMockMvc
            .perform(delete(ENTITY_API_URL_ID, phongDuocDat.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<PhongDuocDat> phongDuocDatList = phongDuocDatRepository.findAll();
        assertThat(phongDuocDatList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
