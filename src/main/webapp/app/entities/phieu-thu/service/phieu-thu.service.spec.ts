import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import * as dayjs from 'dayjs';

import { DATE_TIME_FORMAT } from 'app/config/input.constants';
import { IPhieuThu, PhieuThu } from '../phieu-thu.model';

import { PhieuThuService } from './phieu-thu.service';

describe('Service Tests', () => {
  describe('PhieuThu Service', () => {
    let service: PhieuThuService;
    let httpMock: HttpTestingController;
    let elemDefault: IPhieuThu;
    let expectedResult: IPhieuThu | IPhieuThu[] | boolean | null;
    let currentDate: dayjs.Dayjs;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule],
      });
      expectedResult = null;
      service = TestBed.inject(PhieuThuService);
      httpMock = TestBed.inject(HttpTestingController);
      currentDate = dayjs();

      elemDefault = {
        id: 0,
        ngayChungTu: currentDate,
        soTien: 0,
      };
    });

    describe('Service methods', () => {
      it('should find an element', () => {
        const returnedFromService = Object.assign(
          {
            ngayChungTu: currentDate.format(DATE_TIME_FORMAT),
          },
          elemDefault
        );

        service.find(123).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(elemDefault);
      });

      it('should create a PhieuThu', () => {
        const returnedFromService = Object.assign(
          {
            id: 0,
            ngayChungTu: currentDate.format(DATE_TIME_FORMAT),
          },
          elemDefault
        );

        const expected = Object.assign(
          {
            ngayChungTu: currentDate,
          },
          returnedFromService
        );

        service.create(new PhieuThu()).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'POST' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should update a PhieuThu', () => {
        const returnedFromService = Object.assign(
          {
            id: 1,
            ngayChungTu: currentDate.format(DATE_TIME_FORMAT),
            soTien: 1,
          },
          elemDefault
        );

        const expected = Object.assign(
          {
            ngayChungTu: currentDate,
          },
          returnedFromService
        );

        service.update(expected).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'PUT' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should partial update a PhieuThu', () => {
        const patchObject = Object.assign({}, new PhieuThu());

        const returnedFromService = Object.assign(patchObject, elemDefault);

        const expected = Object.assign(
          {
            ngayChungTu: currentDate,
          },
          returnedFromService
        );

        service.partialUpdate(patchObject).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'PATCH' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should return a list of PhieuThu', () => {
        const returnedFromService = Object.assign(
          {
            id: 1,
            ngayChungTu: currentDate.format(DATE_TIME_FORMAT),
            soTien: 1,
          },
          elemDefault
        );

        const expected = Object.assign(
          {
            ngayChungTu: currentDate,
          },
          returnedFromService
        );

        service.query().subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush([returnedFromService]);
        httpMock.verify();
        expect(expectedResult).toContainEqual(expected);
      });

      it('should delete a PhieuThu', () => {
        service.delete(123).subscribe(resp => (expectedResult = resp.ok));

        const req = httpMock.expectOne({ method: 'DELETE' });
        req.flush({ status: 200 });
        expect(expectedResult);
      });

      describe('addPhieuThuToCollectionIfMissing', () => {
        it('should add a PhieuThu to an empty array', () => {
          const phieuThu: IPhieuThu = { id: 123 };
          expectedResult = service.addPhieuThuToCollectionIfMissing([], phieuThu);
          expect(expectedResult).toHaveLength(1);
          expect(expectedResult).toContain(phieuThu);
        });

        it('should not add a PhieuThu to an array that contains it', () => {
          const phieuThu: IPhieuThu = { id: 123 };
          const phieuThuCollection: IPhieuThu[] = [
            {
              ...phieuThu,
            },
            { id: 456 },
          ];
          expectedResult = service.addPhieuThuToCollectionIfMissing(phieuThuCollection, phieuThu);
          expect(expectedResult).toHaveLength(2);
        });

        it("should add a PhieuThu to an array that doesn't contain it", () => {
          const phieuThu: IPhieuThu = { id: 123 };
          const phieuThuCollection: IPhieuThu[] = [{ id: 456 }];
          expectedResult = service.addPhieuThuToCollectionIfMissing(phieuThuCollection, phieuThu);
          expect(expectedResult).toHaveLength(2);
          expect(expectedResult).toContain(phieuThu);
        });

        it('should add only unique PhieuThu to an array', () => {
          const phieuThuArray: IPhieuThu[] = [{ id: 123 }, { id: 456 }, { id: 69272 }];
          const phieuThuCollection: IPhieuThu[] = [{ id: 123 }];
          expectedResult = service.addPhieuThuToCollectionIfMissing(phieuThuCollection, ...phieuThuArray);
          expect(expectedResult).toHaveLength(3);
        });

        it('should accept varargs', () => {
          const phieuThu: IPhieuThu = { id: 123 };
          const phieuThu2: IPhieuThu = { id: 456 };
          expectedResult = service.addPhieuThuToCollectionIfMissing([], phieuThu, phieuThu2);
          expect(expectedResult).toHaveLength(2);
          expect(expectedResult).toContain(phieuThu);
          expect(expectedResult).toContain(phieuThu2);
        });

        it('should accept null and undefined values', () => {
          const phieuThu: IPhieuThu = { id: 123 };
          expectedResult = service.addPhieuThuToCollectionIfMissing([], null, phieuThu, undefined);
          expect(expectedResult).toHaveLength(1);
          expect(expectedResult).toContain(phieuThu);
        });
      });
    });

    afterEach(() => {
      httpMock.verify();
    });
  });
});
