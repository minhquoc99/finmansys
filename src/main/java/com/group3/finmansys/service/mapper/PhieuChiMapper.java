package com.group3.finmansys.service.mapper;

import com.group3.finmansys.domain.*;
import com.group3.finmansys.service.dto.PhieuChiDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link PhieuChi} and its DTO {@link PhieuChiDTO}.
 */
@Mapper(
    componentModel = "spring",
    uses = { KhachHangMapper.class, KhoanMucChiPhiMapper.class, KhoanPhaiTraMapper.class, QuyTienMapper.class }
)
public interface PhieuChiMapper extends EntityMapper<PhieuChiDTO, PhieuChi> {
    @Mapping(target = "khachHang", source = "khachHang", qualifiedByName = "ten")
    @Mapping(target = "khoanMucChiPhi", source = "khoanMucChiPhi", qualifiedByName = "ten")
    @Mapping(target = "khoanPhaiTra", source = "khoanPhaiTra", qualifiedByName = "id")
    @Mapping(target = "quyTien", source = "quyTien", qualifiedByName = "ten")
    PhieuChiDTO toDto(PhieuChi s);
}
