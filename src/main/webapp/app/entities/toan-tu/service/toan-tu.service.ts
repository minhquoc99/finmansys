import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { isPresent } from 'app/core/util/operators';
import { ApplicationConfigService } from 'app/core/config/application-config.service';
import { createRequestOption } from 'app/core/request/request-util';
import { IToanTu, getToanTuIdentifier } from '../toan-tu.model';

export type EntityResponseType = HttpResponse<IToanTu>;
export type EntityArrayResponseType = HttpResponse<IToanTu[]>;

@Injectable({ providedIn: 'root' })
export class ToanTuService {
  public resourceUrl = this.applicationConfigService.getEndpointFor('api/toan-tus');

  constructor(protected http: HttpClient, private applicationConfigService: ApplicationConfigService) {}

  create(toanTu: IToanTu): Observable<EntityResponseType> {
    return this.http.post<IToanTu>(this.resourceUrl, toanTu, { observe: 'response' });
  }

  update(toanTu: IToanTu): Observable<EntityResponseType> {
    return this.http.put<IToanTu>(`${this.resourceUrl}/${getToanTuIdentifier(toanTu) as number}`, toanTu, { observe: 'response' });
  }

  partialUpdate(toanTu: IToanTu): Observable<EntityResponseType> {
    return this.http.patch<IToanTu>(`${this.resourceUrl}/${getToanTuIdentifier(toanTu) as number}`, toanTu, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IToanTu>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IToanTu[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  addToanTuToCollectionIfMissing(toanTuCollection: IToanTu[], ...toanTusToCheck: (IToanTu | null | undefined)[]): IToanTu[] {
    const toanTus: IToanTu[] = toanTusToCheck.filter(isPresent);
    if (toanTus.length > 0) {
      const toanTuCollectionIdentifiers = toanTuCollection.map(toanTuItem => getToanTuIdentifier(toanTuItem)!);
      const toanTusToAdd = toanTus.filter(toanTuItem => {
        const toanTuIdentifier = getToanTuIdentifier(toanTuItem);
        if (toanTuIdentifier == null || toanTuCollectionIdentifiers.includes(toanTuIdentifier)) {
          return false;
        }
        toanTuCollectionIdentifiers.push(toanTuIdentifier);
        return true;
      });
      return [...toanTusToAdd, ...toanTuCollection];
    }
    return toanTuCollection;
  }
}
