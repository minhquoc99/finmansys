package com.group3.finmansys.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.time.Instant;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.*;
import javax.validation.constraints.*;

/**
 * A KhoanPhaiTra.
 */
@Entity
@Table(name = "khoan_phai_tra")
public class KhoanPhaiTra implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(name = "ngay_tao", nullable = false)
    private Instant ngayTao;

    @NotNull
    @Column(name = "ngay_phai_tra", nullable = false)
    private Instant ngayPhaiTra;

    @NotNull
    @DecimalMin(value = "1")
    @Column(name = "so_tien", nullable = false)
    private Double soTien;

    @Column(name = "ghi_chu")
    private String ghiChu;

    @ManyToOne(optional = false)
    @NotNull
    private KhachHang khachHang;

    @ManyToOne(optional = false)
    @NotNull
    private MaKeToan maKeToan;

    @OneToMany(mappedBy = "khoanPhaiTra")
    @JsonIgnoreProperties(value = { "khachHang", "maKeToan", "khoanMucChiPhi", "khoanPhaiTra" }, allowSetters = true)
    private Set<PhieuChi> dsPhieuChis = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public KhoanPhaiTra id(Long id) {
        this.id = id;
        return this;
    }

    public Instant getNgayTao() {
        return this.ngayTao;
    }

    public KhoanPhaiTra ngayTao(Instant ngayTao) {
        this.ngayTao = ngayTao;
        return this;
    }

    public void setNgayTao(Instant ngayTao) {
        this.ngayTao = ngayTao;
    }

    public Instant getNgayPhaiTra() {
        return this.ngayPhaiTra;
    }

    public KhoanPhaiTra ngayPhaiTra(Instant ngayPhaiTra) {
        this.ngayPhaiTra = ngayPhaiTra;
        return this;
    }

    public void setNgayPhaiTra(Instant ngayPhaiTra) {
        this.ngayPhaiTra = ngayPhaiTra;
    }

    public Double getSoTien() {
        return this.soTien;
    }

    public KhoanPhaiTra soTien(Double soTien) {
        this.soTien = soTien;
        return this;
    }

    public void setSoTien(Double soTien) {
        this.soTien = soTien;
    }

    public String getGhiChu() {
        return this.ghiChu;
    }

    public KhoanPhaiTra ghiChu(String ghiChu) {
        this.ghiChu = ghiChu;
        return this;
    }

    public void setGhiChu(String ghiChu) {
        this.ghiChu = ghiChu;
    }

    public KhachHang getKhachHang() {
        return this.khachHang;
    }

    public KhoanPhaiTra khachHang(KhachHang khachHang) {
        this.setKhachHang(khachHang);
        return this;
    }

    public void setKhachHang(KhachHang khachHang) {
        this.khachHang = khachHang;
    }

    public MaKeToan getMaKeToan() {
        return this.maKeToan;
    }

    public KhoanPhaiTra maKeToan(MaKeToan maKeToan) {
        this.setMaKeToan(maKeToan);
        return this;
    }

    public void setMaKeToan(MaKeToan maKeToan) {
        this.maKeToan = maKeToan;
    }

    public Set<PhieuChi> getDsPhieuChis() {
        return this.dsPhieuChis;
    }

    public KhoanPhaiTra dsPhieuChis(Set<PhieuChi> phieuChis) {
        this.setDsPhieuChis(phieuChis);
        return this;
    }

    public KhoanPhaiTra addDsPhieuChi(PhieuChi phieuChi) {
        this.dsPhieuChis.add(phieuChi);
        phieuChi.setKhoanPhaiTra(this);
        return this;
    }

    public KhoanPhaiTra removeDsPhieuChi(PhieuChi phieuChi) {
        this.dsPhieuChis.remove(phieuChi);
        phieuChi.setKhoanPhaiTra(null);
        return this;
    }

    public void setDsPhieuChis(Set<PhieuChi> phieuChis) {
        if (this.dsPhieuChis != null) {
            this.dsPhieuChis.forEach(i -> i.setKhoanPhaiTra(null));
        }
        if (phieuChis != null) {
            phieuChis.forEach(i -> i.setKhoanPhaiTra(this));
        }
        this.dsPhieuChis = phieuChis;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof KhoanPhaiTra)) {
            return false;
        }
        return id != null && id.equals(((KhoanPhaiTra) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "KhoanPhaiTra{" +
            "id=" + getId() +
            ", ngayTao='" + getNgayTao() + "'" +
            ", ngayPhaiTra='" + getNgayPhaiTra() + "'" +
            ", soTien=" + getSoTien() +
            ", ghiChu='" + getGhiChu() + "'" +
            "}";
    }
}
