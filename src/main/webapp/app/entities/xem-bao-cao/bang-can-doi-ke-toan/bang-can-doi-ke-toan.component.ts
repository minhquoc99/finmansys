import { Component, OnInit } from '@angular/core';
import { IMaKeToan } from 'app/entities/ma-ke-toan/ma-ke-toan.model';
import { MaKeToanService } from 'app/entities/ma-ke-toan/service/ma-ke-toan.service';
import { HttpResponse } from '@angular/common/http';
import { CongThucService } from 'app/entities/cong-thuc/service/cong-thuc.service';
import { ICongThuc } from 'app/entities/cong-thuc/cong-thuc.model';
import * as dayjs from 'dayjs';
import { DATE_TIME_FORMAT } from 'app/config/input.constants';

@Component({
  selector: 'jhi-bang-can-doi-ke-toan',
  templateUrl: './bang-can-doi-ke-toan.component.html',
  styleUrls: ['./bang-can-doi-ke-toan.component.scss'],
})
export class BangCanDoiKeToanComponent implements OnInit {
  year = 2021;
  type = 0;

  congThucs: ICongThuc[] = [];

  cap1 = {
    'font-weight': 'bold',
    'font-size': '20px',
  };

  cap2 = {
    'font-weight': 'bold',
    'font-size': '15px',
  };

  constructor(protected maKeToanService: MaKeToanService, protected congThucService: CongThucService) {}

  ngOnInit(): void {
    this.getData();
  }

  getData(): void {
    const startYear = this.year;
    let startMonth = '01';
    let endYear = this.year;
    let endMonth = '12';
    switch (Number(this.type)) {
      case 1: {
        startMonth = '01';
        endMonth = '04';
        break;
      }
      case 2: {
        startMonth = '04';
        endMonth = '07';
        break;
      }
      case 3: {
        startMonth = '07';
        endMonth = '10';
        break;
      }
      case 4: {
        startMonth = '10';
        endMonth = '01';
        endYear = endYear + 1;
        break;
      }
      case 0: {
        startMonth = '01';
        endMonth = '01';
        endYear = endYear + 1;
        break;
      }
    }
    const startDate = `${startYear}-${startMonth}-01T00:00:00.00Z`;
    const endDate = `${endYear}-${endMonth}-01T00:00:00.00Z`;
    this.congThucService
      .getBangCanDoiKeToan({
        startDate,
        endDate,
      })
      .subscribe((res: HttpResponse<ICongThuc[]>) => {
        this.congThucs = res.body ?? [];
      });
  }

  getStyleCap(congThuc: ICongThuc): any {
    if (congThuc.cap === 1) {
      return this.cap1;
    }
    if (congThuc.cap === 2) {
      return this.cap2;
    }
    return;
  }

  getCongThuc(congThuc: ICongThuc): string {
    let result = '';
    if (congThuc.capCons) {
      if (congThuc.capCons.length > 0) {
        for (const congThucCon of congThuc.capCons) {
          result = result + `MS[${congThucCon.maSo ?? 0}] + `;
        }
      }
      result = result.slice(0, result.length - 3);
    }

    if (congThuc.toanTus) {
      if (congThuc.toanTus.length > 0) {
        for (const toanTu of congThuc.toanTus) {
          result = result + ` ${toanTu.dau ?? ''} MKT[${toanTu.maKeToan?.soHieu ?? 0}]`;
        }
      }
    }
    return result;
  }
}
