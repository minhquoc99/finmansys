package com.group3.finmansys.service.mapper;

import com.group3.finmansys.domain.*;
import com.group3.finmansys.service.dto.KhachHangDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link KhachHang} and its DTO {@link KhachHangDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface KhachHangMapper extends EntityMapper<KhachHangDTO, KhachHang> {
    @Named("ten")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    @Mapping(target = "ten", source = "ten")
    KhachHangDTO toDtoTen(KhachHang khachHang);
}
