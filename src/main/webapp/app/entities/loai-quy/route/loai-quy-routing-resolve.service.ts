import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { mergeMap } from 'rxjs/operators';

import { ILoaiQuy, LoaiQuy } from '../loai-quy.model';
import { LoaiQuyService } from '../service/loai-quy.service';

@Injectable({ providedIn: 'root' })
export class LoaiQuyRoutingResolveService implements Resolve<ILoaiQuy> {
  constructor(protected service: LoaiQuyService, protected router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<ILoaiQuy> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        mergeMap((loaiQuy: HttpResponse<LoaiQuy>) => {
          if (loaiQuy.body) {
            return of(loaiQuy.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new LoaiQuy());
  }
}
