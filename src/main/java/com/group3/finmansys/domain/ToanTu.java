package com.group3.finmansys.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import javax.persistence.*;
import javax.validation.constraints.*;

/**
 * A ToanTu.
 */
@Entity
@Table(name = "toan_tu")
public class ToanTu implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(name = "dau", nullable = false)
    private String dau;

    @ManyToOne(optional = false)
    @NotNull
    private MaKeToan maKeToan;

    @ManyToOne(optional = false)
    @NotNull
    @JsonIgnoreProperties(value = { "toanTus" }, allowSetters = true)
    private CongThuc congThuc;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public ToanTu id(Long id) {
        this.id = id;
        return this;
    }

    public String getDau() {
        return this.dau;
    }

    public ToanTu dau(String dau) {
        this.dau = dau;
        return this;
    }

    public void setDau(String dau) {
        this.dau = dau;
    }

    public MaKeToan getMaKeToan() {
        return this.maKeToan;
    }

    public ToanTu maKeToan(MaKeToan maKeToan) {
        this.setMaKeToan(maKeToan);
        return this;
    }

    public void setMaKeToan(MaKeToan maKeToan) {
        this.maKeToan = maKeToan;
    }

    public CongThuc getCongThuc() {
        return this.congThuc;
    }

    public ToanTu congThuc(CongThuc congThuc) {
        this.setCongThuc(congThuc);
        return this;
    }

    public void setCongThuc(CongThuc congThuc) {
        this.congThuc = congThuc;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof ToanTu)) {
            return false;
        }
        return id != null && id.equals(((ToanTu) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "ToanTu{" +
            "id=" + getId() +
            ", dau='" + getDau() + "'" +
            "}";
    }
}
