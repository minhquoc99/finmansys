package com.group3.finmansys.domain;

import static org.assertj.core.api.Assertions.assertThat;

import com.group3.finmansys.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class QuyTienTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(QuyTien.class);
        QuyTien quyTien1 = new QuyTien();
        quyTien1.setId(1L);
        QuyTien quyTien2 = new QuyTien();
        quyTien2.setId(quyTien1.getId());
        assertThat(quyTien1).isEqualTo(quyTien2);
        quyTien2.setId(2L);
        assertThat(quyTien1).isNotEqualTo(quyTien2);
        quyTien1.setId(null);
        assertThat(quyTien1).isNotEqualTo(quyTien2);
    }
}
