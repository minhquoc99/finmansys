jest.mock('@angular/router');

import { TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ActivatedRouteSnapshot, Router } from '@angular/router';
import { of } from 'rxjs';

import { IQuyTien, QuyTien } from '../quy-tien.model';
import { QuyTienService } from '../service/quy-tien.service';

import { QuyTienRoutingResolveService } from './quy-tien-routing-resolve.service';

describe('Service Tests', () => {
  describe('QuyTien routing resolve service', () => {
    let mockRouter: Router;
    let mockActivatedRouteSnapshot: ActivatedRouteSnapshot;
    let routingResolveService: QuyTienRoutingResolveService;
    let service: QuyTienService;
    let resultQuyTien: IQuyTien | undefined;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule],
        providers: [Router, ActivatedRouteSnapshot],
      });
      mockRouter = TestBed.inject(Router);
      mockActivatedRouteSnapshot = TestBed.inject(ActivatedRouteSnapshot);
      routingResolveService = TestBed.inject(QuyTienRoutingResolveService);
      service = TestBed.inject(QuyTienService);
      resultQuyTien = undefined;
    });

    describe('resolve', () => {
      it('should return IQuyTien returned by find', () => {
        // GIVEN
        service.find = jest.fn(id => of(new HttpResponse({ body: { id } })));
        mockActivatedRouteSnapshot.params = { id: 123 };

        // WHEN
        routingResolveService.resolve(mockActivatedRouteSnapshot).subscribe(result => {
          resultQuyTien = result;
        });

        // THEN
        expect(service.find).toBeCalledWith(123);
        expect(resultQuyTien).toEqual({ id: 123 });
      });

      it('should return new IQuyTien if id is not provided', () => {
        // GIVEN
        service.find = jest.fn();
        mockActivatedRouteSnapshot.params = {};

        // WHEN
        routingResolveService.resolve(mockActivatedRouteSnapshot).subscribe(result => {
          resultQuyTien = result;
        });

        // THEN
        expect(service.find).not.toBeCalled();
        expect(resultQuyTien).toEqual(new QuyTien());
      });

      it('should route to 404 page if data not found in server', () => {
        // GIVEN
        spyOn(service, 'find').and.returnValue(of(new HttpResponse({ body: null })));
        mockActivatedRouteSnapshot.params = { id: 123 };

        // WHEN
        routingResolveService.resolve(mockActivatedRouteSnapshot).subscribe(result => {
          resultQuyTien = result;
        });

        // THEN
        expect(service.find).toBeCalledWith(123);
        expect(resultQuyTien).toEqual(undefined);
        expect(mockRouter.navigate).toHaveBeenCalledWith(['404']);
      });
    });
  });
});
