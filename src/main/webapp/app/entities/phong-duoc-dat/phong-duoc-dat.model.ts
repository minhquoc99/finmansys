import * as dayjs from 'dayjs';
import { IKhachHang } from 'app/entities/khach-hang/khach-hang.model';
import { IPhongDat } from 'app/entities/phong-dat/phong-dat.model';
import { IDichVuSuDung } from 'app/entities/dich-vu-su-dung/dich-vu-su-dung.model';

export interface IPhongDuocDat {
  id?: number;
  ngayDat?: dayjs.Dayjs;
  khuyenMai?: number | null;
  checkIn?: dayjs.Dayjs;
  checkOut?: dayjs.Dayjs;
  gia?: number;
  khachHang?: IKhachHang;
  phongDats?: IPhongDat[] | null;
  dichVuSuDungs?: IDichVuSuDung[] | null;
}

export class PhongDuocDat implements IPhongDuocDat {
  constructor(
    public id?: number,
    public ngayDat?: dayjs.Dayjs,
    public khuyenMai?: number | null,
    public checkIn?: dayjs.Dayjs,
    public checkOut?: dayjs.Dayjs,
    public gia?: number,
    public khachHang?: IKhachHang,
    public phongDats?: IPhongDat[] | null,
    public dichVuSuDungs?: IDichVuSuDung[] | null
  ) {}
}

export function getPhongDuocDatIdentifier(phongDuocDat: IPhongDuocDat): number | undefined {
  return phongDuocDat.id;
}
