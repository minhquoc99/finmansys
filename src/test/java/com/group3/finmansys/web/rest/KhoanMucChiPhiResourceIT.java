package com.group3.finmansys.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.group3.finmansys.IntegrationTest;
import com.group3.finmansys.domain.KhoanMucChiPhi;
import com.group3.finmansys.repository.KhoanMucChiPhiRepository;
import com.group3.finmansys.service.criteria.KhoanMucChiPhiCriteria;
import com.group3.finmansys.service.dto.KhoanMucChiPhiDTO;
import com.group3.finmansys.service.mapper.KhoanMucChiPhiMapper;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link KhoanMucChiPhiResource} REST controller.
 */
@IntegrationTest
@ExtendWith(MockitoExtension.class)
@AutoConfigureMockMvc
@WithMockUser
class KhoanMucChiPhiResourceIT {

    private static final String DEFAULT_TEN = "AAAAAAAAAA";
    private static final String UPDATED_TEN = "BBBBBBBBBB";

    private static final String DEFAULT_MO_TA = "AAAAAAAAAA";
    private static final String UPDATED_MO_TA = "BBBBBBBBBB";

    private static final String ENTITY_API_URL = "/api/khoan-muc-chi-phis";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private KhoanMucChiPhiRepository khoanMucChiPhiRepository;

    @Autowired
    private KhoanMucChiPhiMapper khoanMucChiPhiMapper;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restKhoanMucChiPhiMockMvc;

    private KhoanMucChiPhi khoanMucChiPhi;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static KhoanMucChiPhi createEntity(EntityManager em) {
        KhoanMucChiPhi khoanMucChiPhi = new KhoanMucChiPhi().ten(DEFAULT_TEN).moTa(DEFAULT_MO_TA);
        return khoanMucChiPhi;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static KhoanMucChiPhi createUpdatedEntity(EntityManager em) {
        KhoanMucChiPhi khoanMucChiPhi = new KhoanMucChiPhi().ten(UPDATED_TEN).moTa(UPDATED_MO_TA);
        return khoanMucChiPhi;
    }

    @BeforeEach
    public void initTest() {
        khoanMucChiPhi = createEntity(em);
    }

    @Test
    @Transactional
    void createKhoanMucChiPhi() throws Exception {
        int databaseSizeBeforeCreate = khoanMucChiPhiRepository.findAll().size();
        // Create the KhoanMucChiPhi
        KhoanMucChiPhiDTO khoanMucChiPhiDTO = khoanMucChiPhiMapper.toDto(khoanMucChiPhi);
        restKhoanMucChiPhiMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(khoanMucChiPhiDTO))
            )
            .andExpect(status().isCreated());

        // Validate the KhoanMucChiPhi in the database
        List<KhoanMucChiPhi> khoanMucChiPhiList = khoanMucChiPhiRepository.findAll();
        assertThat(khoanMucChiPhiList).hasSize(databaseSizeBeforeCreate + 1);
        KhoanMucChiPhi testKhoanMucChiPhi = khoanMucChiPhiList.get(khoanMucChiPhiList.size() - 1);
        assertThat(testKhoanMucChiPhi.getTen()).isEqualTo(DEFAULT_TEN);
        assertThat(testKhoanMucChiPhi.getMoTa()).isEqualTo(DEFAULT_MO_TA);
    }

    @Test
    @Transactional
    void createKhoanMucChiPhiWithExistingId() throws Exception {
        // Create the KhoanMucChiPhi with an existing ID
        khoanMucChiPhi.setId(1L);
        KhoanMucChiPhiDTO khoanMucChiPhiDTO = khoanMucChiPhiMapper.toDto(khoanMucChiPhi);

        int databaseSizeBeforeCreate = khoanMucChiPhiRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restKhoanMucChiPhiMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(khoanMucChiPhiDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the KhoanMucChiPhi in the database
        List<KhoanMucChiPhi> khoanMucChiPhiList = khoanMucChiPhiRepository.findAll();
        assertThat(khoanMucChiPhiList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void checkTenIsRequired() throws Exception {
        int databaseSizeBeforeTest = khoanMucChiPhiRepository.findAll().size();
        // set the field null
        khoanMucChiPhi.setTen(null);

        // Create the KhoanMucChiPhi, which fails.
        KhoanMucChiPhiDTO khoanMucChiPhiDTO = khoanMucChiPhiMapper.toDto(khoanMucChiPhi);

        restKhoanMucChiPhiMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(khoanMucChiPhiDTO))
            )
            .andExpect(status().isBadRequest());

        List<KhoanMucChiPhi> khoanMucChiPhiList = khoanMucChiPhiRepository.findAll();
        assertThat(khoanMucChiPhiList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void getAllKhoanMucChiPhis() throws Exception {
        // Initialize the database
        khoanMucChiPhiRepository.saveAndFlush(khoanMucChiPhi);

        // Get all the khoanMucChiPhiList
        restKhoanMucChiPhiMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(khoanMucChiPhi.getId().intValue())))
            .andExpect(jsonPath("$.[*].ten").value(hasItem(DEFAULT_TEN)))
            .andExpect(jsonPath("$.[*].moTa").value(hasItem(DEFAULT_MO_TA)));
    }

    @Test
    @Transactional
    void getKhoanMucChiPhi() throws Exception {
        // Initialize the database
        khoanMucChiPhiRepository.saveAndFlush(khoanMucChiPhi);

        // Get the khoanMucChiPhi
        restKhoanMucChiPhiMockMvc
            .perform(get(ENTITY_API_URL_ID, khoanMucChiPhi.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(khoanMucChiPhi.getId().intValue()))
            .andExpect(jsonPath("$.ten").value(DEFAULT_TEN))
            .andExpect(jsonPath("$.moTa").value(DEFAULT_MO_TA));
    }

    @Test
    @Transactional
    void getKhoanMucChiPhisByIdFiltering() throws Exception {
        // Initialize the database
        khoanMucChiPhiRepository.saveAndFlush(khoanMucChiPhi);

        Long id = khoanMucChiPhi.getId();

        defaultKhoanMucChiPhiShouldBeFound("id.equals=" + id);
        defaultKhoanMucChiPhiShouldNotBeFound("id.notEquals=" + id);

        defaultKhoanMucChiPhiShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultKhoanMucChiPhiShouldNotBeFound("id.greaterThan=" + id);

        defaultKhoanMucChiPhiShouldBeFound("id.lessThanOrEqual=" + id);
        defaultKhoanMucChiPhiShouldNotBeFound("id.lessThan=" + id);
    }

    @Test
    @Transactional
    void getAllKhoanMucChiPhisByTenIsEqualToSomething() throws Exception {
        // Initialize the database
        khoanMucChiPhiRepository.saveAndFlush(khoanMucChiPhi);

        // Get all the khoanMucChiPhiList where ten equals to DEFAULT_TEN
        defaultKhoanMucChiPhiShouldBeFound("ten.equals=" + DEFAULT_TEN);

        // Get all the khoanMucChiPhiList where ten equals to UPDATED_TEN
        defaultKhoanMucChiPhiShouldNotBeFound("ten.equals=" + UPDATED_TEN);
    }

    @Test
    @Transactional
    void getAllKhoanMucChiPhisByTenIsNotEqualToSomething() throws Exception {
        // Initialize the database
        khoanMucChiPhiRepository.saveAndFlush(khoanMucChiPhi);

        // Get all the khoanMucChiPhiList where ten not equals to DEFAULT_TEN
        defaultKhoanMucChiPhiShouldNotBeFound("ten.notEquals=" + DEFAULT_TEN);

        // Get all the khoanMucChiPhiList where ten not equals to UPDATED_TEN
        defaultKhoanMucChiPhiShouldBeFound("ten.notEquals=" + UPDATED_TEN);
    }

    @Test
    @Transactional
    void getAllKhoanMucChiPhisByTenIsInShouldWork() throws Exception {
        // Initialize the database
        khoanMucChiPhiRepository.saveAndFlush(khoanMucChiPhi);

        // Get all the khoanMucChiPhiList where ten in DEFAULT_TEN or UPDATED_TEN
        defaultKhoanMucChiPhiShouldBeFound("ten.in=" + DEFAULT_TEN + "," + UPDATED_TEN);

        // Get all the khoanMucChiPhiList where ten equals to UPDATED_TEN
        defaultKhoanMucChiPhiShouldNotBeFound("ten.in=" + UPDATED_TEN);
    }

    @Test
    @Transactional
    void getAllKhoanMucChiPhisByTenIsNullOrNotNull() throws Exception {
        // Initialize the database
        khoanMucChiPhiRepository.saveAndFlush(khoanMucChiPhi);

        // Get all the khoanMucChiPhiList where ten is not null
        defaultKhoanMucChiPhiShouldBeFound("ten.specified=true");

        // Get all the khoanMucChiPhiList where ten is null
        defaultKhoanMucChiPhiShouldNotBeFound("ten.specified=false");
    }

    @Test
    @Transactional
    void getAllKhoanMucChiPhisByTenContainsSomething() throws Exception {
        // Initialize the database
        khoanMucChiPhiRepository.saveAndFlush(khoanMucChiPhi);

        // Get all the khoanMucChiPhiList where ten contains DEFAULT_TEN
        defaultKhoanMucChiPhiShouldBeFound("ten.contains=" + DEFAULT_TEN);

        // Get all the khoanMucChiPhiList where ten contains UPDATED_TEN
        defaultKhoanMucChiPhiShouldNotBeFound("ten.contains=" + UPDATED_TEN);
    }

    @Test
    @Transactional
    void getAllKhoanMucChiPhisByTenNotContainsSomething() throws Exception {
        // Initialize the database
        khoanMucChiPhiRepository.saveAndFlush(khoanMucChiPhi);

        // Get all the khoanMucChiPhiList where ten does not contain DEFAULT_TEN
        defaultKhoanMucChiPhiShouldNotBeFound("ten.doesNotContain=" + DEFAULT_TEN);

        // Get all the khoanMucChiPhiList where ten does not contain UPDATED_TEN
        defaultKhoanMucChiPhiShouldBeFound("ten.doesNotContain=" + UPDATED_TEN);
    }

    @Test
    @Transactional
    void getAllKhoanMucChiPhisByMoTaIsEqualToSomething() throws Exception {
        // Initialize the database
        khoanMucChiPhiRepository.saveAndFlush(khoanMucChiPhi);

        // Get all the khoanMucChiPhiList where moTa equals to DEFAULT_MO_TA
        defaultKhoanMucChiPhiShouldBeFound("moTa.equals=" + DEFAULT_MO_TA);

        // Get all the khoanMucChiPhiList where moTa equals to UPDATED_MO_TA
        defaultKhoanMucChiPhiShouldNotBeFound("moTa.equals=" + UPDATED_MO_TA);
    }

    @Test
    @Transactional
    void getAllKhoanMucChiPhisByMoTaIsNotEqualToSomething() throws Exception {
        // Initialize the database
        khoanMucChiPhiRepository.saveAndFlush(khoanMucChiPhi);

        // Get all the khoanMucChiPhiList where moTa not equals to DEFAULT_MO_TA
        defaultKhoanMucChiPhiShouldNotBeFound("moTa.notEquals=" + DEFAULT_MO_TA);

        // Get all the khoanMucChiPhiList where moTa not equals to UPDATED_MO_TA
        defaultKhoanMucChiPhiShouldBeFound("moTa.notEquals=" + UPDATED_MO_TA);
    }

    @Test
    @Transactional
    void getAllKhoanMucChiPhisByMoTaIsInShouldWork() throws Exception {
        // Initialize the database
        khoanMucChiPhiRepository.saveAndFlush(khoanMucChiPhi);

        // Get all the khoanMucChiPhiList where moTa in DEFAULT_MO_TA or UPDATED_MO_TA
        defaultKhoanMucChiPhiShouldBeFound("moTa.in=" + DEFAULT_MO_TA + "," + UPDATED_MO_TA);

        // Get all the khoanMucChiPhiList where moTa equals to UPDATED_MO_TA
        defaultKhoanMucChiPhiShouldNotBeFound("moTa.in=" + UPDATED_MO_TA);
    }

    @Test
    @Transactional
    void getAllKhoanMucChiPhisByMoTaIsNullOrNotNull() throws Exception {
        // Initialize the database
        khoanMucChiPhiRepository.saveAndFlush(khoanMucChiPhi);

        // Get all the khoanMucChiPhiList where moTa is not null
        defaultKhoanMucChiPhiShouldBeFound("moTa.specified=true");

        // Get all the khoanMucChiPhiList where moTa is null
        defaultKhoanMucChiPhiShouldNotBeFound("moTa.specified=false");
    }

    @Test
    @Transactional
    void getAllKhoanMucChiPhisByMoTaContainsSomething() throws Exception {
        // Initialize the database
        khoanMucChiPhiRepository.saveAndFlush(khoanMucChiPhi);

        // Get all the khoanMucChiPhiList where moTa contains DEFAULT_MO_TA
        defaultKhoanMucChiPhiShouldBeFound("moTa.contains=" + DEFAULT_MO_TA);

        // Get all the khoanMucChiPhiList where moTa contains UPDATED_MO_TA
        defaultKhoanMucChiPhiShouldNotBeFound("moTa.contains=" + UPDATED_MO_TA);
    }

    @Test
    @Transactional
    void getAllKhoanMucChiPhisByMoTaNotContainsSomething() throws Exception {
        // Initialize the database
        khoanMucChiPhiRepository.saveAndFlush(khoanMucChiPhi);

        // Get all the khoanMucChiPhiList where moTa does not contain DEFAULT_MO_TA
        defaultKhoanMucChiPhiShouldNotBeFound("moTa.doesNotContain=" + DEFAULT_MO_TA);

        // Get all the khoanMucChiPhiList where moTa does not contain UPDATED_MO_TA
        defaultKhoanMucChiPhiShouldBeFound("moTa.doesNotContain=" + UPDATED_MO_TA);
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultKhoanMucChiPhiShouldBeFound(String filter) throws Exception {
        restKhoanMucChiPhiMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(khoanMucChiPhi.getId().intValue())))
            .andExpect(jsonPath("$.[*].ten").value(hasItem(DEFAULT_TEN)))
            .andExpect(jsonPath("$.[*].moTa").value(hasItem(DEFAULT_MO_TA)));

        // Check, that the count call also returns 1
        restKhoanMucChiPhiMockMvc
            .perform(get(ENTITY_API_URL + "/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultKhoanMucChiPhiShouldNotBeFound(String filter) throws Exception {
        restKhoanMucChiPhiMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restKhoanMucChiPhiMockMvc
            .perform(get(ENTITY_API_URL + "/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }

    @Test
    @Transactional
    void getNonExistingKhoanMucChiPhi() throws Exception {
        // Get the khoanMucChiPhi
        restKhoanMucChiPhiMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putNewKhoanMucChiPhi() throws Exception {
        // Initialize the database
        khoanMucChiPhiRepository.saveAndFlush(khoanMucChiPhi);

        int databaseSizeBeforeUpdate = khoanMucChiPhiRepository.findAll().size();

        // Update the khoanMucChiPhi
        KhoanMucChiPhi updatedKhoanMucChiPhi = khoanMucChiPhiRepository.findById(khoanMucChiPhi.getId()).get();
        // Disconnect from session so that the updates on updatedKhoanMucChiPhi are not directly saved in db
        em.detach(updatedKhoanMucChiPhi);
        updatedKhoanMucChiPhi.ten(UPDATED_TEN).moTa(UPDATED_MO_TA);
        KhoanMucChiPhiDTO khoanMucChiPhiDTO = khoanMucChiPhiMapper.toDto(updatedKhoanMucChiPhi);

        restKhoanMucChiPhiMockMvc
            .perform(
                put(ENTITY_API_URL_ID, khoanMucChiPhiDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(khoanMucChiPhiDTO))
            )
            .andExpect(status().isOk());

        // Validate the KhoanMucChiPhi in the database
        List<KhoanMucChiPhi> khoanMucChiPhiList = khoanMucChiPhiRepository.findAll();
        assertThat(khoanMucChiPhiList).hasSize(databaseSizeBeforeUpdate);
        KhoanMucChiPhi testKhoanMucChiPhi = khoanMucChiPhiList.get(khoanMucChiPhiList.size() - 1);
        assertThat(testKhoanMucChiPhi.getTen()).isEqualTo(UPDATED_TEN);
        assertThat(testKhoanMucChiPhi.getMoTa()).isEqualTo(UPDATED_MO_TA);
    }

    @Test
    @Transactional
    void putNonExistingKhoanMucChiPhi() throws Exception {
        int databaseSizeBeforeUpdate = khoanMucChiPhiRepository.findAll().size();
        khoanMucChiPhi.setId(count.incrementAndGet());

        // Create the KhoanMucChiPhi
        KhoanMucChiPhiDTO khoanMucChiPhiDTO = khoanMucChiPhiMapper.toDto(khoanMucChiPhi);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restKhoanMucChiPhiMockMvc
            .perform(
                put(ENTITY_API_URL_ID, khoanMucChiPhiDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(khoanMucChiPhiDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the KhoanMucChiPhi in the database
        List<KhoanMucChiPhi> khoanMucChiPhiList = khoanMucChiPhiRepository.findAll();
        assertThat(khoanMucChiPhiList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchKhoanMucChiPhi() throws Exception {
        int databaseSizeBeforeUpdate = khoanMucChiPhiRepository.findAll().size();
        khoanMucChiPhi.setId(count.incrementAndGet());

        // Create the KhoanMucChiPhi
        KhoanMucChiPhiDTO khoanMucChiPhiDTO = khoanMucChiPhiMapper.toDto(khoanMucChiPhi);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restKhoanMucChiPhiMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(khoanMucChiPhiDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the KhoanMucChiPhi in the database
        List<KhoanMucChiPhi> khoanMucChiPhiList = khoanMucChiPhiRepository.findAll();
        assertThat(khoanMucChiPhiList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamKhoanMucChiPhi() throws Exception {
        int databaseSizeBeforeUpdate = khoanMucChiPhiRepository.findAll().size();
        khoanMucChiPhi.setId(count.incrementAndGet());

        // Create the KhoanMucChiPhi
        KhoanMucChiPhiDTO khoanMucChiPhiDTO = khoanMucChiPhiMapper.toDto(khoanMucChiPhi);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restKhoanMucChiPhiMockMvc
            .perform(
                put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(khoanMucChiPhiDTO))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the KhoanMucChiPhi in the database
        List<KhoanMucChiPhi> khoanMucChiPhiList = khoanMucChiPhiRepository.findAll();
        assertThat(khoanMucChiPhiList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateKhoanMucChiPhiWithPatch() throws Exception {
        // Initialize the database
        khoanMucChiPhiRepository.saveAndFlush(khoanMucChiPhi);

        int databaseSizeBeforeUpdate = khoanMucChiPhiRepository.findAll().size();

        // Update the khoanMucChiPhi using partial update
        KhoanMucChiPhi partialUpdatedKhoanMucChiPhi = new KhoanMucChiPhi();
        partialUpdatedKhoanMucChiPhi.setId(khoanMucChiPhi.getId());

        restKhoanMucChiPhiMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedKhoanMucChiPhi.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedKhoanMucChiPhi))
            )
            .andExpect(status().isOk());

        // Validate the KhoanMucChiPhi in the database
        List<KhoanMucChiPhi> khoanMucChiPhiList = khoanMucChiPhiRepository.findAll();
        assertThat(khoanMucChiPhiList).hasSize(databaseSizeBeforeUpdate);
        KhoanMucChiPhi testKhoanMucChiPhi = khoanMucChiPhiList.get(khoanMucChiPhiList.size() - 1);
        assertThat(testKhoanMucChiPhi.getTen()).isEqualTo(DEFAULT_TEN);
        assertThat(testKhoanMucChiPhi.getMoTa()).isEqualTo(DEFAULT_MO_TA);
    }

    @Test
    @Transactional
    void fullUpdateKhoanMucChiPhiWithPatch() throws Exception {
        // Initialize the database
        khoanMucChiPhiRepository.saveAndFlush(khoanMucChiPhi);

        int databaseSizeBeforeUpdate = khoanMucChiPhiRepository.findAll().size();

        // Update the khoanMucChiPhi using partial update
        KhoanMucChiPhi partialUpdatedKhoanMucChiPhi = new KhoanMucChiPhi();
        partialUpdatedKhoanMucChiPhi.setId(khoanMucChiPhi.getId());

        partialUpdatedKhoanMucChiPhi.ten(UPDATED_TEN).moTa(UPDATED_MO_TA);

        restKhoanMucChiPhiMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedKhoanMucChiPhi.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedKhoanMucChiPhi))
            )
            .andExpect(status().isOk());

        // Validate the KhoanMucChiPhi in the database
        List<KhoanMucChiPhi> khoanMucChiPhiList = khoanMucChiPhiRepository.findAll();
        assertThat(khoanMucChiPhiList).hasSize(databaseSizeBeforeUpdate);
        KhoanMucChiPhi testKhoanMucChiPhi = khoanMucChiPhiList.get(khoanMucChiPhiList.size() - 1);
        assertThat(testKhoanMucChiPhi.getTen()).isEqualTo(UPDATED_TEN);
        assertThat(testKhoanMucChiPhi.getMoTa()).isEqualTo(UPDATED_MO_TA);
    }

    @Test
    @Transactional
    void patchNonExistingKhoanMucChiPhi() throws Exception {
        int databaseSizeBeforeUpdate = khoanMucChiPhiRepository.findAll().size();
        khoanMucChiPhi.setId(count.incrementAndGet());

        // Create the KhoanMucChiPhi
        KhoanMucChiPhiDTO khoanMucChiPhiDTO = khoanMucChiPhiMapper.toDto(khoanMucChiPhi);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restKhoanMucChiPhiMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, khoanMucChiPhiDTO.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(khoanMucChiPhiDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the KhoanMucChiPhi in the database
        List<KhoanMucChiPhi> khoanMucChiPhiList = khoanMucChiPhiRepository.findAll();
        assertThat(khoanMucChiPhiList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchKhoanMucChiPhi() throws Exception {
        int databaseSizeBeforeUpdate = khoanMucChiPhiRepository.findAll().size();
        khoanMucChiPhi.setId(count.incrementAndGet());

        // Create the KhoanMucChiPhi
        KhoanMucChiPhiDTO khoanMucChiPhiDTO = khoanMucChiPhiMapper.toDto(khoanMucChiPhi);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restKhoanMucChiPhiMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(khoanMucChiPhiDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the KhoanMucChiPhi in the database
        List<KhoanMucChiPhi> khoanMucChiPhiList = khoanMucChiPhiRepository.findAll();
        assertThat(khoanMucChiPhiList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamKhoanMucChiPhi() throws Exception {
        int databaseSizeBeforeUpdate = khoanMucChiPhiRepository.findAll().size();
        khoanMucChiPhi.setId(count.incrementAndGet());

        // Create the KhoanMucChiPhi
        KhoanMucChiPhiDTO khoanMucChiPhiDTO = khoanMucChiPhiMapper.toDto(khoanMucChiPhi);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restKhoanMucChiPhiMockMvc
            .perform(
                patch(ENTITY_API_URL)
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(khoanMucChiPhiDTO))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the KhoanMucChiPhi in the database
        List<KhoanMucChiPhi> khoanMucChiPhiList = khoanMucChiPhiRepository.findAll();
        assertThat(khoanMucChiPhiList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteKhoanMucChiPhi() throws Exception {
        // Initialize the database
        khoanMucChiPhiRepository.saveAndFlush(khoanMucChiPhi);

        int databaseSizeBeforeDelete = khoanMucChiPhiRepository.findAll().size();

        // Delete the khoanMucChiPhi
        restKhoanMucChiPhiMockMvc
            .perform(delete(ENTITY_API_URL_ID, khoanMucChiPhi.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<KhoanMucChiPhi> khoanMucChiPhiList = khoanMucChiPhiRepository.findAll();
        assertThat(khoanMucChiPhiList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
