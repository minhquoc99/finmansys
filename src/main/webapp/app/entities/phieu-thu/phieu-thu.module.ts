import { NgModule } from '@angular/core';

import { SharedModule } from 'app/shared/shared.module';
import { PhieuThuComponent } from './list/phieu-thu.component';
import { PhieuThuDetailComponent } from './detail/phieu-thu-detail.component';
import { PhieuThuUpdateComponent } from './update/phieu-thu-update.component';
import { PhieuThuDeleteDialogComponent } from './delete/phieu-thu-delete-dialog.component';
import { PhieuThuRoutingModule } from './route/phieu-thu-routing.module';

@NgModule({
  imports: [SharedModule, PhieuThuRoutingModule],
  declarations: [PhieuThuComponent, PhieuThuDetailComponent, PhieuThuUpdateComponent, PhieuThuDeleteDialogComponent],
  entryComponents: [PhieuThuDeleteDialogComponent],
})
export class PhieuThuModule {}
