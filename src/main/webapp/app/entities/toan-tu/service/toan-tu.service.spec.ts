import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { IToanTu, ToanTu } from '../toan-tu.model';

import { ToanTuService } from './toan-tu.service';

describe('Service Tests', () => {
  describe('ToanTu Service', () => {
    let service: ToanTuService;
    let httpMock: HttpTestingController;
    let elemDefault: IToanTu;
    let expectedResult: IToanTu | IToanTu[] | boolean | null;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule],
      });
      expectedResult = null;
      service = TestBed.inject(ToanTuService);
      httpMock = TestBed.inject(HttpTestingController);

      elemDefault = {
        id: 0,
        dau: 'AAAAAAA',
      };
    });

    describe('Service methods', () => {
      it('should find an element', () => {
        const returnedFromService = Object.assign({}, elemDefault);

        service.find(123).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(elemDefault);
      });

      it('should create a ToanTu', () => {
        const returnedFromService = Object.assign(
          {
            id: 0,
          },
          elemDefault
        );

        const expected = Object.assign({}, returnedFromService);

        service.create(new ToanTu()).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'POST' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should update a ToanTu', () => {
        const returnedFromService = Object.assign(
          {
            id: 1,
            dau: 'BBBBBB',
          },
          elemDefault
        );

        const expected = Object.assign({}, returnedFromService);

        service.update(expected).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'PUT' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should partial update a ToanTu', () => {
        const patchObject = Object.assign({}, new ToanTu());

        const returnedFromService = Object.assign(patchObject, elemDefault);

        const expected = Object.assign({}, returnedFromService);

        service.partialUpdate(patchObject).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'PATCH' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should return a list of ToanTu', () => {
        const returnedFromService = Object.assign(
          {
            id: 1,
            dau: 'BBBBBB',
          },
          elemDefault
        );

        const expected = Object.assign({}, returnedFromService);

        service.query().subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush([returnedFromService]);
        httpMock.verify();
        expect(expectedResult).toContainEqual(expected);
      });

      it('should delete a ToanTu', () => {
        service.delete(123).subscribe(resp => (expectedResult = resp.ok));

        const req = httpMock.expectOne({ method: 'DELETE' });
        req.flush({ status: 200 });
        expect(expectedResult);
      });

      describe('addToanTuToCollectionIfMissing', () => {
        it('should add a ToanTu to an empty array', () => {
          const toanTu: IToanTu = { id: 123 };
          expectedResult = service.addToanTuToCollectionIfMissing([], toanTu);
          expect(expectedResult).toHaveLength(1);
          expect(expectedResult).toContain(toanTu);
        });

        it('should not add a ToanTu to an array that contains it', () => {
          const toanTu: IToanTu = { id: 123 };
          const toanTuCollection: IToanTu[] = [
            {
              ...toanTu,
            },
            { id: 456 },
          ];
          expectedResult = service.addToanTuToCollectionIfMissing(toanTuCollection, toanTu);
          expect(expectedResult).toHaveLength(2);
        });

        it("should add a ToanTu to an array that doesn't contain it", () => {
          const toanTu: IToanTu = { id: 123 };
          const toanTuCollection: IToanTu[] = [{ id: 456 }];
          expectedResult = service.addToanTuToCollectionIfMissing(toanTuCollection, toanTu);
          expect(expectedResult).toHaveLength(2);
          expect(expectedResult).toContain(toanTu);
        });

        it('should add only unique ToanTu to an array', () => {
          const toanTuArray: IToanTu[] = [{ id: 123 }, { id: 456 }, { id: 83956 }];
          const toanTuCollection: IToanTu[] = [{ id: 123 }];
          expectedResult = service.addToanTuToCollectionIfMissing(toanTuCollection, ...toanTuArray);
          expect(expectedResult).toHaveLength(3);
        });

        it('should accept varargs', () => {
          const toanTu: IToanTu = { id: 123 };
          const toanTu2: IToanTu = { id: 456 };
          expectedResult = service.addToanTuToCollectionIfMissing([], toanTu, toanTu2);
          expect(expectedResult).toHaveLength(2);
          expect(expectedResult).toContain(toanTu);
          expect(expectedResult).toContain(toanTu2);
        });

        it('should accept null and undefined values', () => {
          const toanTu: IToanTu = { id: 123 };
          expectedResult = service.addToanTuToCollectionIfMissing([], null, toanTu, undefined);
          expect(expectedResult).toHaveLength(1);
          expect(expectedResult).toContain(toanTu);
        });
      });
    });

    afterEach(() => {
      httpMock.verify();
    });
  });
});
