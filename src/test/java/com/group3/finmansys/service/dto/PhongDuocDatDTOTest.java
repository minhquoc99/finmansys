package com.group3.finmansys.service.dto;

import static org.assertj.core.api.Assertions.assertThat;

import com.group3.finmansys.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class PhongDuocDatDTOTest {

    @Test
    void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(PhongDuocDatDTO.class);
        PhongDuocDatDTO phongDuocDatDTO1 = new PhongDuocDatDTO();
        phongDuocDatDTO1.setId(1L);
        PhongDuocDatDTO phongDuocDatDTO2 = new PhongDuocDatDTO();
        assertThat(phongDuocDatDTO1).isNotEqualTo(phongDuocDatDTO2);
        phongDuocDatDTO2.setId(phongDuocDatDTO1.getId());
        assertThat(phongDuocDatDTO1).isEqualTo(phongDuocDatDTO2);
        phongDuocDatDTO2.setId(2L);
        assertThat(phongDuocDatDTO1).isNotEqualTo(phongDuocDatDTO2);
        phongDuocDatDTO1.setId(null);
        assertThat(phongDuocDatDTO1).isNotEqualTo(phongDuocDatDTO2);
    }
}
