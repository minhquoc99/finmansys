package com.group3.finmansys.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.time.Instant;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.*;
import javax.validation.constraints.*;

/**
 * A KhoanPhaiThu.
 */
@Entity
@Table(name = "khoan_phai_thu")
public class KhoanPhaiThu implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @DecimalMin(value = "1")
    @Column(name = "so_tien", nullable = false)
    private Double soTien;

    @NotNull
    @Column(name = "ngay_tao", nullable = false)
    private Instant ngayTao;

    @NotNull
    @Column(name = "ngay_phai_thu", nullable = false)
    private Instant ngayPhaiThu;

    @Column(name = "ghi_chu")
    private String ghiChu;

    @ManyToOne(optional = false)
    @NotNull
    private KhachHang khachHang;

    @ManyToOne(optional = false)
    @NotNull
    private MaKeToan maKeToan;

    @OneToMany(mappedBy = "khoanPhaiThu")
    @JsonIgnoreProperties(value = { "phongDuocDat", "khoanPhaiThu", "quyTien" }, allowSetters = true)
    private Set<PhieuThu> phieuThus = new HashSet<>();

    @ManyToOne
    @JsonIgnoreProperties(value = { "khachHang", "phongDats", "dichVuSuDungs" }, allowSetters = true)
    private PhongDuocDat phongDuocDat;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public KhoanPhaiThu id(Long id) {
        this.id = id;
        return this;
    }

    public Double getSoTien() {
        return this.soTien;
    }

    public KhoanPhaiThu soTien(Double soTien) {
        this.soTien = soTien;
        return this;
    }

    public void setSoTien(Double soTien) {
        this.soTien = soTien;
    }

    public Instant getNgayTao() {
        return this.ngayTao;
    }

    public KhoanPhaiThu ngayTao(Instant ngayTao) {
        this.ngayTao = ngayTao;
        return this;
    }

    public void setNgayTao(Instant ngayTao) {
        this.ngayTao = ngayTao;
    }

    public Instant getNgayPhaiThu() {
        return this.ngayPhaiThu;
    }

    public KhoanPhaiThu ngayPhaiThu(Instant ngayPhaiThu) {
        this.ngayPhaiThu = ngayPhaiThu;
        return this;
    }

    public void setNgayPhaiThu(Instant ngayPhaiThu) {
        this.ngayPhaiThu = ngayPhaiThu;
    }

    public String getGhiChu() {
        return this.ghiChu;
    }

    public KhoanPhaiThu ghiChu(String ghiChu) {
        this.ghiChu = ghiChu;
        return this;
    }

    public void setGhiChu(String ghiChu) {
        this.ghiChu = ghiChu;
    }

    public KhachHang getKhachHang() {
        return this.khachHang;
    }

    public KhoanPhaiThu khachHang(KhachHang khachHang) {
        this.setKhachHang(khachHang);
        return this;
    }

    public void setKhachHang(KhachHang khachHang) {
        this.khachHang = khachHang;
    }

    public MaKeToan getMaKeToan() {
        return this.maKeToan;
    }

    public KhoanPhaiThu maKeToan(MaKeToan maKeToan) {
        this.setMaKeToan(maKeToan);
        return this;
    }

    public void setMaKeToan(MaKeToan maKeToan) {
        this.maKeToan = maKeToan;
    }

    public Set<PhieuThu> getPhieuThus() {
        return this.phieuThus;
    }

    public KhoanPhaiThu phieuThus(Set<PhieuThu> phieuThus) {
        this.setPhieuThus(phieuThus);
        return this;
    }

    public KhoanPhaiThu addPhieuThu(PhieuThu phieuThu) {
        this.phieuThus.add(phieuThu);
        phieuThu.setKhoanPhaiThu(this);
        return this;
    }

    public KhoanPhaiThu removePhieuThu(PhieuThu phieuThu) {
        this.phieuThus.remove(phieuThu);
        phieuThu.setKhoanPhaiThu(null);
        return this;
    }

    public void setPhieuThus(Set<PhieuThu> phieuThus) {
        if (this.phieuThus != null) {
            this.phieuThus.forEach(i -> i.setKhoanPhaiThu(null));
        }
        if (phieuThus != null) {
            phieuThus.forEach(i -> i.setKhoanPhaiThu(this));
        }
        this.phieuThus = phieuThus;
    }

    public PhongDuocDat getPhongDuocDat() {
        return this.phongDuocDat;
    }

    public KhoanPhaiThu phongDuocDat(PhongDuocDat phongDuocDat) {
        this.setPhongDuocDat(phongDuocDat);
        return this;
    }

    public void setPhongDuocDat(PhongDuocDat phongDuocDat) {
        this.phongDuocDat = phongDuocDat;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof KhoanPhaiThu)) {
            return false;
        }
        return id != null && id.equals(((KhoanPhaiThu) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "KhoanPhaiThu{" +
            "id=" + getId() +
            ", soTien=" + getSoTien() +
            ", ngayTao='" + getNgayTao() + "'" +
            ", ngayPhaiThu='" + getNgayPhaiThu() + "'" +
            ", ghiChu='" + getGhiChu() + "'" +
            "}";
    }
}
