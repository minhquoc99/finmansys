import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { isPresent } from 'app/core/util/operators';
import { ApplicationConfigService } from 'app/core/config/application-config.service';
import { createRequestOption } from 'app/core/request/request-util';
import { IPhong, getPhongIdentifier } from '../phong.model';

export type EntityResponseType = HttpResponse<IPhong>;
export type EntityArrayResponseType = HttpResponse<IPhong[]>;

@Injectable({ providedIn: 'root' })
export class PhongService {
  public resourceUrl = this.applicationConfigService.getEndpointFor('api/phongs');

  constructor(protected http: HttpClient, private applicationConfigService: ApplicationConfigService) {}

  create(phong: IPhong): Observable<EntityResponseType> {
    return this.http.post<IPhong>(this.resourceUrl, phong, { observe: 'response' });
  }

  update(phong: IPhong): Observable<EntityResponseType> {
    return this.http.put<IPhong>(`${this.resourceUrl}/${getPhongIdentifier(phong) as number}`, phong, { observe: 'response' });
  }

  partialUpdate(phong: IPhong): Observable<EntityResponseType> {
    return this.http.patch<IPhong>(`${this.resourceUrl}/${getPhongIdentifier(phong) as number}`, phong, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IPhong>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IPhong[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  addPhongToCollectionIfMissing(phongCollection: IPhong[], ...phongsToCheck: (IPhong | null | undefined)[]): IPhong[] {
    const phongs: IPhong[] = phongsToCheck.filter(isPresent);
    if (phongs.length > 0) {
      const phongCollectionIdentifiers = phongCollection.map(phongItem => getPhongIdentifier(phongItem)!);
      const phongsToAdd = phongs.filter(phongItem => {
        const phongIdentifier = getPhongIdentifier(phongItem);
        if (phongIdentifier == null || phongCollectionIdentifiers.includes(phongIdentifier)) {
          return false;
        }
        phongCollectionIdentifiers.push(phongIdentifier);
        return true;
      });
      return [...phongsToAdd, ...phongCollection];
    }
    return phongCollection;
  }
}
