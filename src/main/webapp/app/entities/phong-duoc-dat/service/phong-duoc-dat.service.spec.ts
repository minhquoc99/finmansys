import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import * as dayjs from 'dayjs';

import { DATE_TIME_FORMAT } from 'app/config/input.constants';
import { IPhongDuocDat, PhongDuocDat } from '../phong-duoc-dat.model';

import { PhongDuocDatService } from './phong-duoc-dat.service';

describe('Service Tests', () => {
  describe('PhongDuocDat Service', () => {
    let service: PhongDuocDatService;
    let httpMock: HttpTestingController;
    let elemDefault: IPhongDuocDat;
    let expectedResult: IPhongDuocDat | IPhongDuocDat[] | boolean | null;
    let currentDate: dayjs.Dayjs;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule],
      });
      expectedResult = null;
      service = TestBed.inject(PhongDuocDatService);
      httpMock = TestBed.inject(HttpTestingController);
      currentDate = dayjs();

      elemDefault = {
        id: 0,
        ngayDat: currentDate,
        khuyenMai: 0,
        checkIn: currentDate,
        checkOut: currentDate,
        gia: 0,
      };
    });

    describe('Service methods', () => {
      it('should find an element', () => {
        const returnedFromService = Object.assign(
          {
            ngayDat: currentDate.format(DATE_TIME_FORMAT),
            checkIn: currentDate.format(DATE_TIME_FORMAT),
            checkOut: currentDate.format(DATE_TIME_FORMAT),
          },
          elemDefault
        );

        service.find(123).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(elemDefault);
      });

      it('should create a PhongDuocDat', () => {
        const returnedFromService = Object.assign(
          {
            id: 0,
            ngayDat: currentDate.format(DATE_TIME_FORMAT),
            checkIn: currentDate.format(DATE_TIME_FORMAT),
            checkOut: currentDate.format(DATE_TIME_FORMAT),
          },
          elemDefault
        );

        const expected = Object.assign(
          {
            ngayDat: currentDate,
            checkIn: currentDate,
            checkOut: currentDate,
          },
          returnedFromService
        );

        service.create(new PhongDuocDat()).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'POST' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should update a PhongDuocDat', () => {
        const returnedFromService = Object.assign(
          {
            id: 1,
            ngayDat: currentDate.format(DATE_TIME_FORMAT),
            khuyenMai: 1,
            checkIn: currentDate.format(DATE_TIME_FORMAT),
            checkOut: currentDate.format(DATE_TIME_FORMAT),
            gia: 1,
          },
          elemDefault
        );

        const expected = Object.assign(
          {
            ngayDat: currentDate,
            checkIn: currentDate,
            checkOut: currentDate,
          },
          returnedFromService
        );

        service.update(expected).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'PUT' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should partial update a PhongDuocDat', () => {
        const patchObject = Object.assign(
          {
            khuyenMai: 1,
            checkOut: currentDate.format(DATE_TIME_FORMAT),
          },
          new PhongDuocDat()
        );

        const returnedFromService = Object.assign(patchObject, elemDefault);

        const expected = Object.assign(
          {
            ngayDat: currentDate,
            checkIn: currentDate,
            checkOut: currentDate,
          },
          returnedFromService
        );

        service.partialUpdate(patchObject).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'PATCH' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should return a list of PhongDuocDat', () => {
        const returnedFromService = Object.assign(
          {
            id: 1,
            ngayDat: currentDate.format(DATE_TIME_FORMAT),
            khuyenMai: 1,
            checkIn: currentDate.format(DATE_TIME_FORMAT),
            checkOut: currentDate.format(DATE_TIME_FORMAT),
            gia: 1,
          },
          elemDefault
        );

        const expected = Object.assign(
          {
            ngayDat: currentDate,
            checkIn: currentDate,
            checkOut: currentDate,
          },
          returnedFromService
        );

        service.query().subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush([returnedFromService]);
        httpMock.verify();
        expect(expectedResult).toContainEqual(expected);
      });

      it('should delete a PhongDuocDat', () => {
        service.delete(123).subscribe(resp => (expectedResult = resp.ok));

        const req = httpMock.expectOne({ method: 'DELETE' });
        req.flush({ status: 200 });
        expect(expectedResult);
      });

      describe('addPhongDuocDatToCollectionIfMissing', () => {
        it('should add a PhongDuocDat to an empty array', () => {
          const phongDuocDat: IPhongDuocDat = { id: 123 };
          expectedResult = service.addPhongDuocDatToCollectionIfMissing([], phongDuocDat);
          expect(expectedResult).toHaveLength(1);
          expect(expectedResult).toContain(phongDuocDat);
        });

        it('should not add a PhongDuocDat to an array that contains it', () => {
          const phongDuocDat: IPhongDuocDat = { id: 123 };
          const phongDuocDatCollection: IPhongDuocDat[] = [
            {
              ...phongDuocDat,
            },
            { id: 456 },
          ];
          expectedResult = service.addPhongDuocDatToCollectionIfMissing(phongDuocDatCollection, phongDuocDat);
          expect(expectedResult).toHaveLength(2);
        });

        it("should add a PhongDuocDat to an array that doesn't contain it", () => {
          const phongDuocDat: IPhongDuocDat = { id: 123 };
          const phongDuocDatCollection: IPhongDuocDat[] = [{ id: 456 }];
          expectedResult = service.addPhongDuocDatToCollectionIfMissing(phongDuocDatCollection, phongDuocDat);
          expect(expectedResult).toHaveLength(2);
          expect(expectedResult).toContain(phongDuocDat);
        });

        it('should add only unique PhongDuocDat to an array', () => {
          const phongDuocDatArray: IPhongDuocDat[] = [{ id: 123 }, { id: 456 }, { id: 91438 }];
          const phongDuocDatCollection: IPhongDuocDat[] = [{ id: 123 }];
          expectedResult = service.addPhongDuocDatToCollectionIfMissing(phongDuocDatCollection, ...phongDuocDatArray);
          expect(expectedResult).toHaveLength(3);
        });

        it('should accept varargs', () => {
          const phongDuocDat: IPhongDuocDat = { id: 123 };
          const phongDuocDat2: IPhongDuocDat = { id: 456 };
          expectedResult = service.addPhongDuocDatToCollectionIfMissing([], phongDuocDat, phongDuocDat2);
          expect(expectedResult).toHaveLength(2);
          expect(expectedResult).toContain(phongDuocDat);
          expect(expectedResult).toContain(phongDuocDat2);
        });

        it('should accept null and undefined values', () => {
          const phongDuocDat: IPhongDuocDat = { id: 123 };
          expectedResult = service.addPhongDuocDatToCollectionIfMissing([], null, phongDuocDat, undefined);
          expect(expectedResult).toHaveLength(1);
          expect(expectedResult).toContain(phongDuocDat);
        });
      });
    });

    afterEach(() => {
      httpMock.verify();
    });
  });
});
