import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize, map } from 'rxjs/operators';

import { INganSachThang, NganSachThang } from '../ngan-sach-thang.model';
import { NganSachThangService } from '../service/ngan-sach-thang.service';
import { INganSachNam } from 'app/entities/ngan-sach-nam/ngan-sach-nam.model';
import { NganSachNamService } from 'app/entities/ngan-sach-nam/service/ngan-sach-nam.service';
import { IKhoanMucChiPhi } from 'app/entities/khoan-muc-chi-phi/khoan-muc-chi-phi.model';
import { KhoanMucChiPhiService } from 'app/entities/khoan-muc-chi-phi/service/khoan-muc-chi-phi.service';

@Component({
  selector: 'jhi-ngan-sach-thang-update',
  templateUrl: './ngan-sach-thang-update.component.html',
})
export class NganSachThangUpdateComponent implements OnInit {
  isSaving = false;

  nganSachNamsSharedCollection: INganSachNam[] = [];
  khoanMucChiPhisSharedCollection: IKhoanMucChiPhi[] = [];

  editForm = this.fb.group({
    id: [],
    thang: [null, [Validators.required, Validators.min(1), Validators.max(12)]],
    soTien: [null, [Validators.required, Validators.min(1)]],
    nganSachNam: [null, Validators.required],
    khoanMucChiPhi: [null, Validators.required],
  });

  constructor(
    protected nganSachThangService: NganSachThangService,
    protected nganSachNamService: NganSachNamService,
    protected khoanMucChiPhiService: KhoanMucChiPhiService,
    protected activatedRoute: ActivatedRoute,
    protected fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ nganSachThang }) => {
      this.updateForm(nganSachThang);

      this.loadRelationshipsOptions();
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const nganSachThang = this.createFromForm();
    if (nganSachThang.id !== undefined) {
      this.subscribeToSaveResponse(this.nganSachThangService.update(nganSachThang));
    } else {
      this.subscribeToSaveResponse(this.nganSachThangService.create(nganSachThang));
    }
  }

  trackNganSachNamById(index: number, item: INganSachNam): number {
    return item.id!;
  }

  trackKhoanMucChiPhiById(index: number, item: IKhoanMucChiPhi): number {
    return item.id!;
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<INganSachThang>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(nganSachThang: INganSachThang): void {
    this.editForm.patchValue({
      id: nganSachThang.id,
      thang: nganSachThang.thang,
      soTien: nganSachThang.soTien,
      nganSachNam: nganSachThang.nganSachNam,
      khoanMucChiPhi: nganSachThang.khoanMucChiPhi,
    });

    this.nganSachNamsSharedCollection = this.nganSachNamService.addNganSachNamToCollectionIfMissing(
      this.nganSachNamsSharedCollection,
      nganSachThang.nganSachNam
    );
    this.khoanMucChiPhisSharedCollection = this.khoanMucChiPhiService.addKhoanMucChiPhiToCollectionIfMissing(
      this.khoanMucChiPhisSharedCollection,
      nganSachThang.khoanMucChiPhi
    );
  }

  protected loadRelationshipsOptions(): void {
    this.nganSachNamService
      .query()
      .pipe(map((res: HttpResponse<INganSachNam[]>) => res.body ?? []))
      .pipe(
        map((nganSachNams: INganSachNam[]) =>
          this.nganSachNamService.addNganSachNamToCollectionIfMissing(nganSachNams, this.editForm.get('nganSachNam')!.value)
        )
      )
      .subscribe((nganSachNams: INganSachNam[]) => (this.nganSachNamsSharedCollection = nganSachNams));

    this.khoanMucChiPhiService
      .query()
      .pipe(map((res: HttpResponse<IKhoanMucChiPhi[]>) => res.body ?? []))
      .pipe(
        map((khoanMucChiPhis: IKhoanMucChiPhi[]) =>
          this.khoanMucChiPhiService.addKhoanMucChiPhiToCollectionIfMissing(khoanMucChiPhis, this.editForm.get('khoanMucChiPhi')!.value)
        )
      )
      .subscribe((khoanMucChiPhis: IKhoanMucChiPhi[]) => (this.khoanMucChiPhisSharedCollection = khoanMucChiPhis));
  }

  protected createFromForm(): INganSachThang {
    return {
      ...new NganSachThang(),
      id: this.editForm.get(['id'])!.value,
      thang: this.editForm.get(['thang'])!.value,
      soTien: this.editForm.get(['soTien'])!.value,
      nganSachNam: this.editForm.get(['nganSachNam'])!.value,
      khoanMucChiPhi: this.editForm.get(['khoanMucChiPhi'])!.value,
    };
  }
}
