package com.group3.finmansys.service.mapper;

import com.group3.finmansys.domain.*;
import com.group3.finmansys.service.dto.DichVuSuDungDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link DichVuSuDung} and its DTO {@link DichVuSuDungDTO}.
 */
@Mapper(componentModel = "spring", uses = { DichVuMapper.class, PhongDuocDatMapper.class })
public interface DichVuSuDungMapper extends EntityMapper<DichVuSuDungDTO, DichVuSuDung> {
    @Mapping(target = "dichVu", source = "dichVu", qualifiedByName = "ten")
    @Mapping(target = "phongDuocDat", source = "phongDuocDat", qualifiedByName = "id")
    DichVuSuDungDTO toDto(DichVuSuDung s);
}
