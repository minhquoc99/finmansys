import * as dayjs from 'dayjs';
import { IKhoanPhaiThu } from 'app/entities/khoan-phai-thu/khoan-phai-thu.model';
import { IQuyTien } from 'app/entities/quy-tien/quy-tien.model';
import { IKhachHang } from 'app/entities/khach-hang/khach-hang.model';

export interface IPhieuThu {
  id?: number;
  ngayChungTu?: dayjs.Dayjs;
  soTien?: number;
  khoanPhaiThu?: IKhoanPhaiThu;
  quyTien?: IQuyTien;
  khachHang?: IKhachHang;
}

export class PhieuThu implements IPhieuThu {
  constructor(
    public id?: number,
    public ngayChungTu?: dayjs.Dayjs,
    public soTien?: number,
    public khoanPhaiThu?: IKhoanPhaiThu,
    public quyTien?: IQuyTien,
    public khachHang?: IKhachHang
  ) {}
}

export function getPhieuThuIdentifier(phieuThu: IPhieuThu): number | undefined {
  return phieuThu.id;
}
