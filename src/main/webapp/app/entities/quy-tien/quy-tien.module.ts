import { NgModule } from '@angular/core';

import { SharedModule } from 'app/shared/shared.module';
import { QuyTienComponent } from './list/quy-tien.component';
import { QuyTienDetailComponent } from './detail/quy-tien-detail.component';
import { QuyTienUpdateComponent } from './update/quy-tien-update.component';
import { QuyTienDeleteDialogComponent } from './delete/quy-tien-delete-dialog.component';
import { QuyTienRoutingModule } from './route/quy-tien-routing.module';

@NgModule({
  imports: [SharedModule, QuyTienRoutingModule],
  declarations: [QuyTienComponent, QuyTienDetailComponent, QuyTienUpdateComponent, QuyTienDeleteDialogComponent],
  entryComponents: [QuyTienDeleteDialogComponent],
})
export class QuyTienModule {}
