package com.group3.finmansys.service;

import com.group3.finmansys.domain.*; // for static metamodels
import com.group3.finmansys.domain.ChiTietGiamTSCD;
import com.group3.finmansys.repository.ChiTietGiamTSCDRepository;
import com.group3.finmansys.service.criteria.ChiTietGiamTSCDCriteria;
import com.group3.finmansys.service.dto.ChiTietGiamTSCDDTO;
import com.group3.finmansys.service.mapper.ChiTietGiamTSCDMapper;
import java.util.List;
import javax.persistence.criteria.JoinType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tech.jhipster.service.QueryService;

/**
 * Service for executing complex queries for {@link ChiTietGiamTSCD} entities in the database.
 * The main input is a {@link ChiTietGiamTSCDCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link ChiTietGiamTSCDDTO} or a {@link Page} of {@link ChiTietGiamTSCDDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class ChiTietGiamTSCDQueryService extends QueryService<ChiTietGiamTSCD> {

    private final Logger log = LoggerFactory.getLogger(ChiTietGiamTSCDQueryService.class);

    private final ChiTietGiamTSCDRepository chiTietGiamTSCDRepository;

    private final ChiTietGiamTSCDMapper chiTietGiamTSCDMapper;

    public ChiTietGiamTSCDQueryService(ChiTietGiamTSCDRepository chiTietGiamTSCDRepository, ChiTietGiamTSCDMapper chiTietGiamTSCDMapper) {
        this.chiTietGiamTSCDRepository = chiTietGiamTSCDRepository;
        this.chiTietGiamTSCDMapper = chiTietGiamTSCDMapper;
    }

    /**
     * Return a {@link List} of {@link ChiTietGiamTSCDDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<ChiTietGiamTSCDDTO> findByCriteria(ChiTietGiamTSCDCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<ChiTietGiamTSCD> specification = createSpecification(criteria);
        return chiTietGiamTSCDMapper.toDto(chiTietGiamTSCDRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link ChiTietGiamTSCDDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<ChiTietGiamTSCDDTO> findByCriteria(ChiTietGiamTSCDCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<ChiTietGiamTSCD> specification = createSpecification(criteria);
        return chiTietGiamTSCDRepository.findAll(specification, page).map(chiTietGiamTSCDMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(ChiTietGiamTSCDCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<ChiTietGiamTSCD> specification = createSpecification(criteria);
        return chiTietGiamTSCDRepository.count(specification);
    }

    /**
     * Function to convert {@link ChiTietGiamTSCDCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<ChiTietGiamTSCD> createSpecification(ChiTietGiamTSCDCriteria criteria) {
        Specification<ChiTietGiamTSCD> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getId(), ChiTietGiamTSCD_.id));
            }
            if (criteria.getNgayMua() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getNgayMua(), ChiTietGiamTSCD_.ngayMua));
            }
            if (criteria.getSoLuong() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getSoLuong(), ChiTietGiamTSCD_.soLuong));
            }
            if (criteria.getKhauHao() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getKhauHao(), ChiTietGiamTSCD_.khauHao));
            }
            if (criteria.getSoTienThanhLy() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getSoTienThanhLy(), ChiTietGiamTSCD_.soTienThanhLy));
            }
            if (criteria.getChiPhiMua() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getChiPhiMua(), ChiTietGiamTSCD_.chiPhiMua));
            }
            if (criteria.getTaiSanCDId() != null) {
                specification =
                    specification.and(
                        buildSpecification(
                            criteria.getTaiSanCDId(),
                            root -> root.join(ChiTietGiamTSCD_.taiSanCD, JoinType.LEFT).get(TaiSanCD_.id)
                        )
                    );
            }
        }
        return specification;
    }
}
