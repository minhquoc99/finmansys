import * as dayjs from 'dayjs';
import { ITaiSanCD } from 'app/entities/tai-san-cd/tai-san-cd.model';

export interface IChiTietTangTSCD {
  id?: number;
  ngayThuMua?: dayjs.Dayjs;
  soLuong?: number;
  chiPhiMua?: number;
  taiSanCD?: ITaiSanCD;
}

export class ChiTietTangTSCD implements IChiTietTangTSCD {
  constructor(
    public id?: number,
    public ngayThuMua?: dayjs.Dayjs,
    public soLuong?: number,
    public chiPhiMua?: number,
    public taiSanCD?: ITaiSanCD
  ) {}
}

export function getChiTietTangTSCDIdentifier(chiTietTangTSCD: IChiTietTangTSCD): number | undefined {
  return chiTietTangTSCD.id;
}
